package com.natraj.sangsoft.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_execute.MaintenanceExecuteRow;

import java.util.ArrayList;
import java.util.List;

public class ActivityListAdapter extends RecyclerView.Adapter<ActivityListAdapter.ViewHolder> {

    private List<String> categories = new ArrayList<String>();
    private List<MaintenanceExecuteRow> executeRows;
    private Context context;

    public ActivityListAdapter(Context context, List<MaintenanceExecuteRow> executeRows) {
        this.context = context;
        this.executeRows = executeRows;
        categories.add("Done");
        categories.add("Pending");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_execute_activity_list, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.txtActyName.setText((executeRows.get(position).getActName()));
        //Log.e("ActivityStatus:-", executeRows.get(position).getActStatus());
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, R.layout.spinner_status, categories);
        holder.statusSpinner.setAdapter(dataAdapter);
        //holder.txtStsValue.setText(executeRows.get(position).getActStatus());
        /*holder.linearImageVideo.setOnClickListener(onClickListener);
        holder.linearImageVideo.setTag(position);*/
    }

    @Override
    public int getItemCount() {
        return executeRows.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView txtActyName;
        Spinner statusSpinner;

        ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            txtActyName = (TextView) itemView.findViewById(R.id.txtActyName);
            statusSpinner = (Spinner) itemView.findViewById(R.id.statusSpinner);
        }
    }
}
