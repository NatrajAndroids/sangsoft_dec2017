package com.natraj.sangsoft.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.model.production_modal.item_instructions_modal.instructions_item_details.InstructionsRow;

import java.util.List;

public class InstructionsItemListAdapter extends RecyclerView.Adapter<InstructionsItemListAdapter.ViewHolder> {

    private List<InstructionsRow> overviewItems;
    private View.OnClickListener onClickListener;
    public int selectedPosition = -1;

    public InstructionsItemListAdapter(List<InstructionsRow> overviewItems, View.OnClickListener onClickListener) {
        this.overviewItems = overviewItems;
        this.onClickListener = onClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image_item_list, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.txtSrNo.setText((overviewItems.get(position).getSrNo()).toString());
        holder.txtFirst.setText(overviewItems.get(position).getCaption());
        holder.txtSecond.setText(overviewItems.get(position).getDescription());
        holder.linearImageVideo.setOnClickListener(onClickListener);
        holder.linearImageVideo.setTag(position);

        /*if (selectedPosition!=-1){
            holder.linearImageVideo.setBackgroundColor(context.getResources().getColor(R.color.yellowLight));
        } else {
            holder.linearImageVideo.setBackgroundColor(context.getResources().getColor(R.color.white));
        }*/
    }

    @Override
    public int getItemCount() {
        return overviewItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        View view;
        LinearLayout linearImageVideo;
        TextView txtSrNo, txtFirst, txtSecond;

        ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            txtSrNo = (TextView) itemView.findViewById(R.id.txtImageSrNo);
            txtFirst = (TextView) itemView.findViewById(R.id.txtFirst);
            txtSecond = (TextView) itemView.findViewById(R.id.txtSecond);
            linearImageVideo = (LinearLayout) itemView.findViewById(R.id.linearImageVideo);
        }
    }
}
