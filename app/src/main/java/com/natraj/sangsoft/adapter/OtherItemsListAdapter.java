package com.natraj.sangsoft.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.model.production_modal.item_instructions_modal.instructions_item_details.InstructionsRow;

import java.util.List;

/**
 * Created by Natraj3 on 7/1/2017.
 */

public class OtherItemsListAdapter extends RecyclerView.Adapter<OtherItemsListAdapter.ViewHolder> {

    private List<InstructionsRow> overviewItems;
    private Context context;
    private View.OnClickListener onClickListener;

    public OtherItemsListAdapter(Context context, List<InstructionsRow> overviewItems, View.OnClickListener onClickListener) {
        this.context = context;
        this.overviewItems = overviewItems;
        this.onClickListener = onClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_other_item_list, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.txtSrNo.setText(Integer.toString(overviewItems.get(position).getSrNo()));
        holder.txtFirst.setText(overviewItems.get(position).getFileName());
    }

    @Override
    public int getItemCount() {
        return overviewItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        View view;
        TextView txtSrNo, txtFirst;

        ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            txtSrNo = (TextView) itemView.findViewById(R.id.txtImageSrNo);
            txtFirst = (TextView) itemView.findViewById(R.id.txtFirst);
        }
    }
}
