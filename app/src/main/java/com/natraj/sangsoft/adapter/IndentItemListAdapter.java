package com.natraj.sangsoft.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_indent_item_list.OverviewIndentItem;

import java.util.List;

public class IndentItemListAdapter extends RecyclerView.Adapter<IndentItemListAdapter.ViewHolder> {

    private List<OverviewIndentItem> overviewItems;
    private Context context;
    private View.OnClickListener onClickListener;

    public IndentItemListAdapter(Context context, List<OverviewIndentItem> overviewItems, View.OnClickListener onClickListener) {
        this.context = context;
        this.overviewItems = overviewItems;
        this.onClickListener = onClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_indent_item_list, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.txtIndentItemId.setText(overviewItems.get(position).getItemMcode());
        holder.txtIndentItemDesc.setText(overviewItems.get(position).getItemName());
        holder.btnIndentItemSelect.setOnClickListener(onClickListener);
        holder.btnIndentItemSelect.setTag(position);
        holder.txtIndentItemId.setOnClickListener(onClickListener);
        holder.txtIndentItemId.setTag(position);
        holder.txtIndentItemDesc.setOnClickListener(onClickListener);
        holder.txtIndentItemDesc.setTag(position);
    }

    @Override
    public int getItemCount() {
        return overviewItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        View view;
        TextView txtIndentItemId, txtIndentItemDesc;
        TextView btnIndentItemSelect;

        ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            txtIndentItemId = (TextView) itemView.findViewById(R.id.txtIndentItemId);
            txtIndentItemDesc = (TextView) itemView.findViewById(R.id.txtIndentItemDesc);
            btnIndentItemSelect = (TextView) itemView.findViewById(R.id.btnIndentItemSelect);
        }
    }
}
