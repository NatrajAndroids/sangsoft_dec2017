package com.natraj.sangsoft.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_overview.OverviewMachine;

import java.util.List;

public class OverviewPossibleMachinesListAdapter extends RecyclerView.Adapter<OverviewPossibleMachinesListAdapter.ViewHolder> {

    private List<OverviewMachine> overviewItems;
    private Context context;
    private View.OnClickListener onClickListener;

    public OverviewPossibleMachinesListAdapter(Context context, List<OverviewMachine> overviewItems, View.OnClickListener onClickListener) {
        this.context = context;
        this.overviewItems = overviewItems;
        this.onClickListener = onClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_overview_machines_list, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        //holder.txtTypeValue.setText(overviewItems.get(position).getItemType());
        holder.txtMachineName.setText(overviewItems.get(position).getMchName());
    }

    @Override
    public int getItemCount() {
        return overviewItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View view;
        private TextView txtMachineName;

        public ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            txtMachineName = (TextView) itemView.findViewById(R.id.txtMachineName);
        }
    }
}
