package com.natraj.sangsoft.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.model.production_modal.indent_record_prod.RecordProdOutputItem;

import java.util.List;

public class RecordItemsAdapter extends RecyclerView.Adapter<RecordItemsAdapter.ViewHolder> {

    public List<RecordProdOutputItem> receiveRmInutItems;
    private Context context;

    public RecordItemsAdapter(Context context, List<RecordProdOutputItem> receiveRmInutItems) {
        this.context = context;
        this.receiveRmInutItems = receiveRmInutItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_record_items, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtItemcode.setText(receiveRmInutItems.get(position).getItemcode());
        holder.txtDescription.setText(receiveRmInutItems.get(position).getDescription());
        holder.txtTotalExpected.setText(receiveRmInutItems.get(position).getExpectedQty());
        holder.edtProduced.setText(receiveRmInutItems.get(position).getReceivedQty());
        holder.txtUOM.setText(receiveRmInutItems.get(position).getExpectedUOM());
        holder.view.setTag(holder);
    }

    @Override
    public int getItemCount() {
        return receiveRmInutItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView txtItemcode, txtDescription, txtTotalExpected, txtUOM;
        EditText edtProduced;

        ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            txtItemcode = (TextView) view.findViewById(R.id.txtItemcode);
            txtDescription = (TextView) view.findViewById(R.id.txtDescription);
            txtTotalExpected = (TextView) view.findViewById(R.id.txtTotalExpected);
            edtProduced = (EditText) view.findViewById(R.id.edtProduced);
            txtUOM = (TextView) view.findViewById(R.id.txtUOM);
        }
    }
}