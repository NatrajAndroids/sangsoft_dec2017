package com.natraj.sangsoft.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_execute.MaintenanceExecuteRow;
import com.natraj.sangsoft.model.record_qa_modal.question_set.LookupSet;
import com.natraj.sangsoft.model.record_qa_modal.question_set.QuestionSet;
import com.natraj.sangsoft.model.spinner_modal.InfoSet;
import com.natraj.sangsoft.model.spinner_modal.InputSet;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.ViewHolder> {

    private List<LookupSet> lookupSets;
    private List<QuestionSet> questionSets;
    private Context context;
    
    public QuestionAdapter(Context context, List<QuestionSet> questionSets, List<LookupSet> lookupSets) {
        this.context = context;
        this.questionSets = questionSets;
        this.lookupSets = lookupSets;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_question_set, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.txtQuesText.setText((questionSets.get(position).getQuestionText()));
        holder.txtQuesDesc.setText((questionSets.get(position).getQuestionDescription()));
        String strType = questionSets.get(position).getTypeOfAns();
        String strLookupName = questionSets.get(position).getLookupName();
        if (strType.equals("String") || strType.equals("Number")) {
            holder.edtQues.setVisibility(View.VISIBLE);
            holder.lookupSpinner.setVisibility(View.GONE);
            if (strType.equals("String")) {
                holder.edtQues.setInputType(InputType.TYPE_CLASS_TEXT);
            } else {
                holder.edtQues.setInputType(InputType.TYPE_CLASS_NUMBER);
            }
        } else {
            holder.edtQues.setVisibility(View.GONE);
            holder.lookupSpinner.setVisibility(View.VISIBLE);

            holder.inputList.clear();
            holder.infoList.clear();

            for (int i = 0; i < lookupSets.size(); i++) {
                LookupSet lookupSet = lookupSets.get(i);

                if (lookupSet.getLookupNm().equals("bom_input")) {
                    InputSet inputSet = new InputSet();
                    inputSet.setDataKey(lookupSet.getDataKey());
                    inputSet.setDisplayName(lookupSet.getDisplayName());
                    inputSet.setLookupNm(lookupSet.getLookupNm());
                    inputSet.setSequence(lookupSet.getSequence());
                    holder.inputList.add(inputSet);

                } else if (lookupSet.getLookupNm().equals("info_type")) {
                    InfoSet infoSet = new InfoSet();
                    infoSet.setDataKey(lookupSet.getDataKey());
                    infoSet.setDisplayName(lookupSet.getDisplayName());
                    infoSet.setLookupNm(lookupSet.getLookupNm());
                    infoSet.setSequence(lookupSet.getSequence());
                    holder.infoList.add(infoSet);
                }
            }

            if (strLookupName.equals("info_type")) {
                holder.lookupSpinner.setAdapter(holder.infoAdapter);
                holder.infoAdapter.notifyDataSetChanged();
            } else {
                holder.lookupSpinner.setAdapter(holder.inputAdapter);
                holder.inputAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public int getItemCount() {
        return questionSets.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView txtQuesText, txtQuesDesc;
        Spinner lookupSpinner;
        EditText edtQues;
        List<InputSet> inputList = new ArrayList<>();
        List<InfoSet> infoList = new ArrayList<>();
        ArrayAdapter infoAdapter;
        ArrayAdapter inputAdapter;

        ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            txtQuesText = (TextView) itemView.findViewById(R.id.txtQuesText);
            txtQuesDesc = (TextView) itemView.findViewById(R.id.txtQuesDesc);
            edtQues = (EditText) itemView.findViewById(R.id.edtQues);
            lookupSpinner = (Spinner) itemView.findViewById(R.id.lookupSpinner);

            infoAdapter = new ArrayAdapter<InfoSet>(context, R.layout.spinner_status, infoList);
            inputAdapter = new ArrayAdapter<InputSet>(context, R.layout.spinner_status, inputList);
        }
    }
}
