package com.natraj.sangsoft.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_overview.OverviewItem;

import java.util.List;

/**
 * Created by Natraj3 on 7/1/2017.
 */

public class OverviewMaterialListAdapter extends RecyclerView.Adapter<OverviewMaterialListAdapter.ViewHolder> {

    private List<OverviewItem> overviewItems;
    private Context context;
    private View.OnClickListener onClickListener;

    public OverviewMaterialListAdapter(Context context, List<OverviewItem> overviewItems, View.OnClickListener onClickListener) {
        this.context = context;
        this.overviewItems = overviewItems;
        this.onClickListener = onClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_overview_list, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.txtTypeValue.setText(overviewItems.get(position).getItemType());
        holder.txtItemcodeValue.setText(overviewItems.get(position).getItemMcode());
        holder.txtDescriptionValue.setText(overviewItems.get(position).getItemName());
        holder.txtExpectedValue.setText(overviewItems.get(position).getInputQty());
        holder.txtUomValue.setText(overviewItems.get(position).getInputUom());
        holder.txtItemcodeValue.setOnClickListener(onClickListener);
        holder.txtItemcodeValue.setTag(position);
        holder.txtDescriptionValue.setOnClickListener(onClickListener);
        holder.txtDescriptionValue.setTag(position);
    }

    @Override
    public int getItemCount() {
        return overviewItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        View view;
        private TextView txtTypeValue, txtItemcodeValue, txtDescriptionValue, txtExpectedValue, txtUomValue;

        ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            txtTypeValue = (TextView) itemView.findViewById(R.id.txtTypeValue);
            txtItemcodeValue = (TextView) itemView.findViewById(R.id.txtItemcodeValue);
            txtDescriptionValue = (TextView) itemView.findViewById(R.id.txtDescriptionValue);
            txtExpectedValue = (TextView) itemView.findViewById(R.id.txtExpectedValue);
            txtUomValue = (TextView) itemView.findViewById(R.id.txtUomValue);
        }
    }
}
