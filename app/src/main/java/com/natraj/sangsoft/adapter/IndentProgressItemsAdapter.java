package com.natraj.sangsoft.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.model.production_modal.indent_progress_modal.IndentActivity;

import java.util.List;

public class IndentProgressItemsAdapter extends RecyclerView.Adapter<IndentProgressItemsAdapter.ViewHolder> {

    private List<IndentActivity> indentItemses;
    private Context context;
    private View.OnClickListener onClickListener;

    public IndentProgressItemsAdapter(Context context, List<IndentActivity> indentItemses, View.OnClickListener onClickListener) {
        this.context = context;
        this.indentItemses = indentItemses;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_indent_progress_items_layout, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.txtDate.setText(indentItemses.get(position).getActDate());
        holder.txtTime.setText(indentItemses.get(position).getActTime());
        holder.txtType.setText(indentItemses.get(position).getActType());
        holder.txtActivity.setText(indentItemses.get(position).getActivity());
        holder.txtProduced.setText(indentItemses.get(position).getActProduced());
        holder.txtRejected.setText(indentItemses.get(position).getActRejected());
        holder.txtUser.setText(indentItemses.get(position).getActUser());
        holder.txtRemarks.setText(indentItemses.get(position).getActRremarks());
    }

    @Override
    public int getItemCount() {
        return indentItemses.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        View view;
        private TextView txtDate, txtTime, txtType, txtActivity, txtProduced, txtRejected, txtUser, txtRemarks;

        ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtTime = (TextView) itemView.findViewById(R.id.txtTime);
            txtType = (TextView) itemView.findViewById(R.id.txtType);
            txtActivity = (TextView) itemView.findViewById(R.id.txtActivity);
            txtProduced = (TextView) itemView.findViewById(R.id.txtProduced);
            txtRejected = (TextView) itemView.findViewById(R.id.txtRejected);
            txtUser = (TextView) itemView.findViewById(R.id.txtUser);
            txtRemarks = (TextView) itemView.findViewById(R.id.txtRemarks);
        }
    }
}