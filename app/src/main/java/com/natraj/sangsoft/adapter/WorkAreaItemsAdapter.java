package com.natraj.sangsoft.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.model.receive_rm_data_modal.RMInutItem;

import java.util.List;

public class WorkAreaItemsAdapter extends RecyclerView.Adapter<WorkAreaItemsAdapter.ViewHolder> {

    public List<RMInutItem> receiveRmInutItems;
    private Context context;

    public WorkAreaItemsAdapter(Context context, List<RMInutItem> receiveRmInutItems) {
        this.context = context;
        this.receiveRmInutItems = receiveRmInutItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_work_area_items_layout, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtItemcode.setText(receiveRmInutItems.get(position).getItemcode());
        holder.txtDescription.setText(receiveRmInutItems.get(position).getDescription());
        holder.txtTotalExpected.setText(receiveRmInutItems.get(position).getExpectedQty());
        holder.edtReceived.setText(receiveRmInutItems.get(position).getReceivedQty());
        holder.edtReturned.setText(receiveRmInutItems.get(position).getToReturnQty());
        holder.txtUOM.setText(receiveRmInutItems.get(position).getExpectedUOM());
        holder.view.setTag(holder);
    }

    @Override
    public int getItemCount() {
        return receiveRmInutItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView txtItemcode, txtDescription, txtTotalExpected, txtUOM;
        EditText edtReturned,edtReceived;

        ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            txtItemcode = (TextView) view.findViewById(R.id.txtItemcode);
            txtDescription = (TextView) view.findViewById(R.id.txtDescription);
            txtTotalExpected = (TextView) view.findViewById(R.id.txtTotalExpected);
            edtReceived = (EditText) view.findViewById(R.id.edtReceived);
            edtReturned = (EditText) view.findViewById(R.id.edtReturned);
            txtUOM = (TextView) view.findViewById(R.id.txtUOM);
        }
    }
}