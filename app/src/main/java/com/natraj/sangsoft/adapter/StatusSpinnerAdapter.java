package com.natraj.sangsoft.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_execute.MaintenanceExecuteEventStatusRow;

import java.util.List;

public class StatusSpinnerAdapter extends ArrayAdapter<MaintenanceExecuteEventStatusRow> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<MaintenanceExecuteEventStatusRow> items;
    private final int mResource;

    public StatusSpinnerAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<MaintenanceExecuteEventStatusRow> objects) {
        super(context, 0, objects);
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);

        TextView txtStatus = (TextView) view.findViewById(R.id.txtStatus);
        MaintenanceExecuteEventStatusRow urlModal = items.get(position);
        txtStatus.setText(urlModal.getStatusName());
        return view;
    }
}