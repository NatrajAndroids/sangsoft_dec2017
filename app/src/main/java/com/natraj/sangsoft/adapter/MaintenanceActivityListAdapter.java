package com.natraj.sangsoft.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_activity_list_modal.MaintListRow;

import java.util.List;

public class MaintenanceActivityListAdapter extends RecyclerView.Adapter<MaintenanceActivityListAdapter.ViewHolder> {

    private List<MaintListRow> overviewItems;
    private Context context;
    private View.OnClickListener onClickListener;

    public MaintenanceActivityListAdapter(Context context, List<MaintListRow> overviewItems, View.OnClickListener onClickListener) {
        this.context = context;
        this.overviewItems = overviewItems;
        this.onClickListener = onClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_maintenance_activity_list, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.txtDateRange.setText(overviewItems.get(position).getDateRange());
        holder.txtFromTime.setText(overviewItems.get(position).getStartTime());
        holder.txtTillTime.setText(overviewItems.get(position).getEndTime());
        holder.txtScheduleName.setText(overviewItems.get(position).getSchName());
        holder.txtMandetory.setText(overviewItems.get(position).getMandatory());
        holder.txtBy.setText(overviewItems.get(position).getBY());
        holder.txtStatus.setText(overviewItems.get(position).getSchStatus());
        holder.linearActivityList.setOnClickListener(onClickListener);
        holder.linearActivityList.setTag(position);
    }

    @Override
    public int getItemCount() {
        return overviewItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        View view;
        LinearLayout linearActivityList;
        TextView txtDateRange, txtFromTime, txtTillTime, txtScheduleName, txtMandetory, txtBy, txtStatus;

        ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            txtDateRange = (TextView) itemView.findViewById(R.id.txtDateRange);
            txtFromTime = (TextView) itemView.findViewById(R.id.txtFromTime);
            txtTillTime = (TextView) itemView.findViewById(R.id.txtTillTime);
            txtScheduleName = (TextView) itemView.findViewById(R.id.txtScheduleName);
            txtMandetory = (TextView) itemView.findViewById(R.id.txtMandetory);
            txtBy = (TextView) itemView.findViewById(R.id.txtBy);
            txtStatus = (TextView) itemView.findViewById(R.id.txtStatus);
            linearActivityList = (LinearLayout) itemView.findViewById(R.id.linearActivityList);
        }
    }
}
