package com.natraj.sangsoft.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.adapter.UrlSpinnerAdapter;
import com.natraj.sangsoft.constant.Constant;
import com.natraj.sangsoft.database.DatabaseHandler;
import com.natraj.sangsoft.model.UrlModal;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_header_info.CommonDataModal;
import com.natraj.sangsoft.retrofit_provider.RetrofitApiClient;
import com.natraj.sangsoft.retrofit_provider.RetrofitService;
import com.natraj.sangsoft.retrofit_provider.WebResponse;
import com.natraj.sangsoft.utils.AppPreference;
import com.natraj.sangsoft.utils.BaseActivity;
import com.quenDel.multiUtils.neworks.NetworkHandler;
import com.quenDel.multiUtils.utils.AppAlerts;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class UrlActivity extends AppCompatActivity implements View.OnClickListener {

    private List<UrlModal> list = new ArrayList<>();
    public static String selectUrl = "";
    private String lang, mchId, userId, indentId;
    private String edtMachineId = "";
    private Toolbar toolbar;
    private Spinner spinnerUrl;
    private DatabaseHandler db;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_url);
        mContext = this;
        init();
    }

    private void init() {
        lang = AppPreference.getStringPreference(mContext, Constant.LANGUAGE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = new DatabaseHandler(mContext);

        spinnerUrl = (Spinner) findViewById(R.id.spinnerUrl);
        ((Button) findViewById(R.id.btnSave)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnSaveAsNew)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnUrlCancel)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnChangeUrl)).setOnClickListener(this);
        getIds();
        checkExistUrl();
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Check Url exist or not
    private void checkExistUrl() {
        if (db.getContactsCount()) {
            list = db.getAllUrlList();
            setSpinnerData(list);
            selectedUrl();
        } else {
            AppAlerts.show(mContext, "Url not found");
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Check selected url
    private void selectedUrl() {
        if ((AppPreference.getConstIntegerPreference(mContext, Constant.SELECTED_URL_POSITION)) == 0) {
        } else {
            int pos = (AppPreference.getConstIntegerPreference(mContext, Constant.SELECTED_URL_POSITION));
            spinnerUrl.setSelection(pos);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Get language, mch id, user id, indent id
    private void getIds() {
        lang = AppPreference.getStringPreference(mContext, Constant.LANGUAGE);
        mchId = AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID);
        userId = AppPreference.getStringPreference(mContext, Constant.USERNAME);
        indentId = AppPreference.getStringPreference(mContext, Constant.LANGUAGE);
        if (!AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR).isEmpty()) {
            ((TextView) toolbar.findViewById(R.id.txtMachine))
                    .setText(AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR));
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //Insert new URL in spinner
    private void insertData() {
        edtMachineId = ((EditText) findViewById(R.id.edtMachineId)).getText().toString().trim();
        String urlAddress = ((EditText) findViewById(R.id.edtUrlAddress)).getText().toString().trim();
        if (edtMachineId.isEmpty()) {
            AppAlerts.show(mContext, "Please enter Url name first");
        } else if (urlAddress.isEmpty()) {
            AppAlerts.show(mContext, "Please enter Url Address");
        } else {
            AppPreference.setConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR, edtMachineId);
            db.addUrl(new UrlModal(edtMachineId, urlAddress));

            list = db.getAllUrlList();
            Log.d("Name: ", "" + list);
            setSpinnerData(list);
            AppAlerts.show(mContext, "Server Url save successful...");
            AppPreference.setConstStringPreference(mContext, Constant.URL_SPINNER, urlAddress);
            AppPreference.setConstStringPreference(mContext, Constant.MACHINE_ID, edtMachineId);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //Update selected URL in spinner
    private void updateSelectedUrl() {
        int urlPosition = (AppPreference.getConstIntegerPreference(mContext, Constant.SELECTED_URL_POSITION)) + 1;
        edtMachineId = ((EditText) findViewById(R.id.edtMachineId)).getText().toString().trim();
        String urlAddress = ((EditText) findViewById(R.id.edtUrlAddress)).getText().toString().trim();
        if (edtMachineId.isEmpty()) {
            AppAlerts.show(mContext, "Please enter Url name first");
        } else if (urlAddress.isEmpty()) {
            AppAlerts.show(mContext, "Please enter Url Address");
        } else {
            AppPreference.setConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR, edtMachineId);

            int val = db.updateUrl(new UrlModal(urlPosition, edtMachineId, urlAddress));
            Log.e("Update url", " " + val);
            list = db.getAllUrlList();
            Log.d("Name: ", "" + list);
            setSpinnerData(list);
            AppAlerts.show(mContext, "Url update successful...");
            //AppPreference.setConstIntegerPreference(mContext, Constant.SELECTED_URL_POSITION, urlPosition-1);
            AppPreference.setConstStringPreference(mContext, Constant.URL_SPINNER, urlAddress);
            AppPreference.setConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR, edtMachineId);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Set URL in spinner
    private void setSpinnerData(List<UrlModal> spinnerData) {
        UrlSpinnerAdapter adapter = new UrlSpinnerAdapter(mContext, R.layout.spinner_url_layout, spinnerData);
        spinnerUrl.setAdapter(adapter);

        spinnerUrl.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectUrl = list.get(i).get_url();
                edtMachineId = list.get(i).get_name();
                //AppAlerts.show(mContext, name + " :- " + selectUrl);
                ((EditText) findViewById(R.id.edtMachineId)).setText(edtMachineId);
                ((EditText) findViewById(R.id.edtUrlAddress)).setText(selectUrl);
                AppPreference.setConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR, edtMachineId);
                AppPreference.setConstIntegerPreference(mContext, Constant.SELECTED_URL_POSITION, i);

                if (!AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR).isEmpty()) {
                    ((TextView) toolbar.findViewById(R.id.txtMachine))
                            .setText(AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                updateSelectedUrl();
                break;
            case R.id.btnSaveAsNew:
                insertData();
                break;
            case R.id.btnUrlCancel:
                finish();
                break;
            case R.id.btnChangeUrl:
                changeUrl();
                break;
        }
    }

    private void changeUrl() {
        if (selectUrl.isEmpty() && edtMachineId.isEmpty()) {
            AppAlerts.show(mContext, "Please select Url and Machine Id first...");
        } else {
            Intent intent = new Intent();
            intent.putExtra("url", selectUrl);
            intent.putExtra(Constant.MACHINE_ID_TOOLBAR, edtMachineId);
            AppPreference.setConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR, edtMachineId);
            AppPreference.setConstStringPreference(mContext, Constant.SELECTED_URL, selectUrl);
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
