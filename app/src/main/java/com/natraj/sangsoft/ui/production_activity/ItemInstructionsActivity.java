package com.natraj.sangsoft.ui.production_activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.adapter.IndentItemListAdapter;
import com.natraj.sangsoft.adapter.InstructionsItemListAdapter;
import com.natraj.sangsoft.adapter.ProcessSpinnerAdapter;
import com.natraj.sangsoft.constant.Constant;
import com.natraj.sangsoft.model.production_modal.alert_dialog_label_modal.AlertDialogLabelModal;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_header_info.CommonDataModal;
import com.natraj.sangsoft.model.production_modal.item_instructions_modal.instructions_item_details.InstructionsItemDetailsModal;
import com.natraj.sangsoft.model.production_modal.item_instructions_modal.instructions_item_details.InstructionsRow;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_indent_item_list.OverviewIndentItem;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_indent_item_list.OverviewIndentItemListModal;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_bar.OverviewItemBarModal;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_info.OverviewItemInfoModal;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_info.OverviewItemInfoProcess;
import com.natraj.sangsoft.retrofit_provider.RetrofitService;
import com.natraj.sangsoft.retrofit_provider.WebResponse;
import com.natraj.sangsoft.ui.LoginActivity;
import com.natraj.sangsoft.ui.maintenance_activity.MaintListActivity;
import com.natraj.sangsoft.utils.AppPreference;
import com.natraj.sangsoft.utils.BaseActivity;
import com.quenDel.multiUtils.neworks.NetworkHandler;
import com.quenDel.multiUtils.utils.AppAlerts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class ItemInstructionsActivity extends BaseActivity implements View.OnClickListener {

    private List<InstructionsRow> overviewItemsList = new ArrayList<>();
    private String isSelected = "work";
    private Dialog dialog_indent_item;
    private EditText edtItemId;
    private String itemId = "";
    private String lang;
    private String mchId;
    private String userId;
    String process_id = "DEAFULT";
    private Toolbar toolbar;
    private OverviewIndentItemListModal overviewIndentItemListModal;
    private InstructionsItemDetailsModal detailsModal;
    private RecyclerView recyclerView, recyclerViewIndentItem;
    private Spinner spinnerProcess;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_instructions);
        mContext = this;
        init();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        spinnerProcess = (Spinner) findViewById(R.id.spinnerProcess);
        recyclerView = (RecyclerView) findViewById(R.id.instructionsRecyclerView);
        ((TextView) findViewById(R.id.txtWork)).setOnClickListener(this);
        ((TextView) findViewById(R.id.txtSafety)).setOnClickListener(this);
        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setVisibility(View.VISIBLE);
        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setOnClickListener(this);
        ((LinearLayout) toolbar.findViewById(R.id.linear_main)).setOnClickListener(this);

        ((ImageView) findViewById(R.id.btnGo)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearOverview)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearImgVdo)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearOthers)).setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.relativeIndentItemList)).setOnClickListener(this);
        itemId = AppPreference.getStringPreference(mContext, Constant.ITEM_ID);
        getIds();
        setToolbarBarLabel();
        setItemBarLabel(itemId);
        setItemInfoAndProcess(process_id, itemId);
        String tab_name = "work_instructions";
        setWorkSafetyData(tab_name, process_id);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Get language, mch id, user id, indent id
    private void getIds() {
        lang = AppPreference.getStringPreference(mContext, Constant.LANGUAGE);
        mchId = AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID);
        userId = AppPreference.getStringPreference(mContext, Constant.USERNAME);
        itemId = AppPreference.getStringPreference(mContext, Constant.ITEM_ID);
        if (!AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR).isEmpty()) {
            ((TextView) toolbar.findViewById(R.id.txtMachine))
                    .setText(AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR));
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Toolbar bar label
    private void setToolbarBarLabel() {
        String headerInfo = "headerInfo";
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getCommonDataLbl(new Dialog(mContext), retrofitApiClient.commonDataItems(
                    headerInfo, userId, lang), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<CommonDataModal> response = (Response<CommonDataModal>) result;
                    CommonDataModal dataModal = response.body();
                    if (dataModal.getIsError().equals(0)) {
                        ((TextView) toolbar.findViewById(R.id.prod_tooltip)).setText(dataModal.getStrings().getProdTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_maintenance)).setText(dataModal.getStrings().getMaintTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_qa)).setText(dataModal.getStrings().getQATooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_help)).setText(dataModal.getStrings().getHelpTooltip());
                        ((TextView) toolbar.findViewById(R.id.txtEmpIdToolbar)).setText(dataModal.getJsonData().getUserName());
                    } else {
                        AppAlerts.show(mContext, dataModal.getErrorMsg());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set ItemBarLabel and item name and id
    private void setItemBarLabel(String itemId) {
        String operation = "itemBar";
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getItemOverviewActionBarData(new Dialog(mContext), retrofitApiClient.itemOverviewActionBarData(
                    operation, mchId, lang, itemId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<OverviewItemBarModal> response = (Response<OverviewItemBarModal>) result;
                    OverviewItemBarModal itemBarModal = response.body();
                    if (itemBarModal.getIsError().equals(0)) {
                        ((TextView) findViewById(R.id.txt_lbl_overview)).setText(itemBarModal.getStrings().getBtnOverview());
                        ((TextView) findViewById(R.id.txt_lbl_instructions)).setText(itemBarModal.getStrings().getBtnInstruction());
                        ((TextView) findViewById(R.id.txt_lbl_img_vdo)).setText(itemBarModal.getStrings().getBtnImgVideo());
                        ((TextView) findViewById(R.id.txt_lbl_others)).setText(itemBarModal.getStrings().getBtnOthers());
                        ((TextView) findViewById(R.id.txt_indent_item_name)).setText(itemBarModal.getJsonData().getItemMcode() + " - " + itemBarModal.getJsonData().getItemName());
                    } else {
                        AppAlerts.show(mContext, itemBarModal.getErrorMsg());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set OverviewItem info and Process spinner
    private void setItemInfoAndProcess(String processId, String itemId) {
        String operation = "itemInfo";
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getItemOverviewItemInfo(new Dialog(mContext), retrofitApiClient.itemOverviewItemInfo(
                    operation, mchId, lang, itemId, processId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<OverviewItemInfoModal> response = (Response<OverviewItemInfoModal>) result;
                    OverviewItemInfoModal itemInfoModal = response.body();
                    if (itemInfoModal.getIsError().equals(0)) {
                        ((TextView) findViewById(R.id.txtErpCode)).setText(itemInfoModal.getStrings().getLblERPcode());
                        ((TextView) findViewById(R.id.txtErpCodeValue)).setText(itemInfoModal.getJsonData().getItemRef01());
                        ((TextView) findViewById(R.id.txtLblDescription)).setText(itemInfoModal.getStrings().getLblItemDesc());
                        ((TextView) findViewById(R.id.txtLblDescriptionValue)).setText(itemInfoModal.getJsonData().getItemName());
                        ((TextView) findViewById(R.id.txtLblProcess)).setText(itemInfoModal.getStrings().getLblProcess());

                        if (itemInfoModal.getJsonData().getProcesses().size() == 0) {
                            overviewItemsList.clear();
                            setInstructionsList();
                        }
                        setSpinnerData(itemInfoModal.getJsonData().getProcesses());
                        if (AppPreference.getIntegerPositionPreference(mContext, Constant.SELECTED_PROCESS_POSITION) > 0) {
                            spinnerProcess.setSelection(AppPreference.getIntegerPositionPreference(mContext, Constant.SELECTED_PROCESS_POSITION));
                        }
                    } else {
                        ((TextView) findViewById(R.id.txtErpCode)).setText(itemInfoModal.getStrings().getLblERPcode());
                        ((TextView) findViewById(R.id.txtErpCodeValue)).setText(itemInfoModal.getJsonData().getItemRef01());
                        ((TextView) findViewById(R.id.txtLblDescription)).setText(itemInfoModal.getStrings().getLblItemDesc());
                        ((TextView) findViewById(R.id.txtLblDescriptionValue)).setText(itemInfoModal.getJsonData().getItemName());
                        ((TextView) findViewById(R.id.txtLblProcess)).setText(itemInfoModal.getStrings().getLblProcess());
                        AppAlerts.show(mContext, itemInfoModal.getErrorMsg());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    private void setSpinnerData(final List<OverviewItemInfoProcess> spinnerData) {
        String[] items = new String[spinnerData.size()];
        for (int i = 0; i < spinnerData.size(); i++) {
            items[i] = spinnerData.get(i).getProcessName();
        }

        ProcessSpinnerAdapter adapter = new ProcessSpinnerAdapter(mContext, R.layout.spinner_custom_layout, spinnerData);
        spinnerProcess.setAdapter(adapter);
        spinnerProcess.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                process_id = spinnerData.get(i).getProcessId();
                ((TextView) findViewById(R.id.txtDescription)).setText("");
                if (isSelected.equals("work")) {
                    setTextviewBackground(R.id.txtSafety, R.id.txtWork);
                    setWorkSafetyData("work_instructions", process_id);
                    ((TextView) findViewById(R.id.txtDescription)).setText("");
                } else {
                    setTextviewBackground(R.id.txtWork, R.id.txtSafety);
                    setWorkSafetyData("safety_instructions", process_id);
                    ((TextView) findViewById(R.id.txtDescription)).setText("");
                }
                //setItemInfoAndProcess(process_id, itemId);
                //setTextviewBackground(R.id.txtSafety, R.id.txtWork);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Work and Safety data
    private void setWorkSafetyData(String strType, String strProcess_id) {
        String op = "itemDetails";
        if (networkHandler.isNetworkAvailable()) {

            RequestBody _mch_id = RequestBody.create(MediaType.parse("text/plain"), mchId);
            RequestBody _op = RequestBody.create(MediaType.parse("text/plain"), op);
            RequestBody _user_id = RequestBody.create(MediaType.parse("text/plain"), userId);
            RequestBody _lang = RequestBody.create(MediaType.parse("text/plain"), lang);

            Map<String, Object> jsonObject = new HashMap<>();
            jsonObject.put("process_id", strProcess_id);
            jsonObject.put("tab_nm", strType);

            Log.e("Json data", ": " + jsonObject);

            RetrofitService.getItemInstructionsFinalData(new Dialog(mContext), retrofitApiClient.itemInstructionsFinalData(
                    _mch_id, _op, _user_id, _lang, jsonObject), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<InstructionsItemDetailsModal> response = (Response<InstructionsItemDetailsModal>) result;
                    detailsModal = response.body();

                    ((TextView) findViewById(R.id.txt_area_title)).setText(detailsModal.getStrings().getAreaTitle());
                    ((TextView) findViewById(R.id.txtWork)).setText(detailsModal.getStrings().getTabWork());
                    ((TextView) findViewById(R.id.txtSafety)).setText(detailsModal.getStrings().getTabSafety());
                    overviewItemsList.clear();
                    overviewItemsList.addAll(detailsModal.getJsonData().getRows());
                    setInstructionsList();
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    private void setInstructionsList() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        InstructionsItemListAdapter machineListAdapter = new InstructionsItemListAdapter(overviewItemsList, this);
        recyclerView.setAdapter(machineListAdapter);
        machineListAdapter.notifyDataSetChanged();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Log out
    private void logout() {
        AppAlerts.showAlertWithAction(mContext, "Logout", "Are you sure want to Logout?",
                "YES", "NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        clearAllPreferences();
                    }
                }, true,true);
    }

    private void clearAllPreferences() {
        AppPreference.clearAllPreferences(mContext);
        Intent intent = new Intent(mContext, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Set Order item list in dialog
    private void itemListDialog() {
        dialog_indent_item = new Dialog(this);
        dialog_indent_item.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_indent_item.setContentView(R.layout.dialog_indent_item_list);
        dialog_indent_item.setCanceledOnTouchOutside(false);
        dialog_indent_item.setCancelable(true);
        if (dialog_indent_item.getWindow() != null)
            dialog_indent_item.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        recyclerViewIndentItem = (RecyclerView) dialog_indent_item.findViewById(R.id.recyclerViewIndentItem);
        edtItemId = (EditText) dialog_indent_item.findViewById(R.id.edtItemId);
        ((TextView) dialog_indent_item.findViewById(R.id.btnSendId)).setOnClickListener(this);
        dialog_indent_item.show();
        Window window = dialog_indent_item.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        setDialogueLabel();
    }

    private void setDialogueLabel() {
        String op = "itemSearch";
        if (networkHandler.isNetworkAvailable()) {
            RequestBody _mch_id = RequestBody.create(MediaType.parse("text/plain"), mchId);
            RequestBody _op = RequestBody.create(MediaType.parse("text/plain"), op);
            RequestBody _user_id = RequestBody.create(MediaType.parse("text/plain"), userId);
            RequestBody _lang = RequestBody.create(MediaType.parse("text/plain"), lang);
            Map<String, Object> jsonObject = new HashMap<>();
            jsonObject.put("item_mcode", "Default");
            RetrofitService.getAlertDialogLabel(new Dialog(mContext), retrofitApiClient.alertDialogLabel(
                    _mch_id, _op, _user_id, _lang, jsonObject), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<AlertDialogLabelModal> response = (Response<AlertDialogLabelModal>) result;
                    AlertDialogLabelModal labelModal = response.body();
                    ((EditText) dialog_indent_item.findViewById(R.id.edtItemId)).setHint(labelModal.getStrings().getTitleSearchItem());
                    ((TextView) dialog_indent_item.findViewById(R.id.btnSendId)).setHint(labelModal.getStrings().getBtnGo());
                    ((TextView) dialog_indent_item.findViewById(R.id.btnSendId)).setTextColor(getResources().getColor(R.color.white));
                    ((TextView) dialog_indent_item.findViewById(R.id.txtMcode)).setText(labelModal.getStrings().getLblMCode());
                    ((TextView) dialog_indent_item.findViewById(R.id.txtLblA)).setText(labelModal.getStrings().getCol01());
                    ((TextView) dialog_indent_item.findViewById(R.id.txtLblB)).setText(labelModal.getStrings().getCol02());
                    ((TextView) dialog_indent_item.findViewById(R.id.txtLblC)).setText(labelModal.getStrings().getCol03());
                }

                @Override
                public void onResponseFailed(String error) {
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    private void setIndentItemList() {
        String item_id = edtItemId.getText().toString();
        String op = "itemSearch";

        if (networkHandler.isNetworkAvailable()) {

            RequestBody _mch_id = RequestBody.create(MediaType.parse("text/plain"), mchId);
            RequestBody _op = RequestBody.create(MediaType.parse("text/plain"), op);
            RequestBody _user_id = RequestBody.create(MediaType.parse("text/plain"), userId);
            RequestBody _lang = RequestBody.create(MediaType.parse("text/plain"), lang);

            Map<String, Object> jsonObject = new HashMap<>();
            jsonObject.put("item_mcode", item_id);

            Log.e("Json data", ": " + jsonObject);

            RetrofitService.getIndentItemListDialog(new Dialog(mContext), retrofitApiClient.indentItemListDialog(
                    _mch_id, _op, _user_id, _lang, jsonObject), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<OverviewIndentItemListModal> response = (Response<OverviewIndentItemListModal>) result;
                    overviewIndentItemListModal = response.body();
                    setIndentItemList(overviewIndentItemListModal.getJsonData().getItems());
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    private void setIndentItemList(List<OverviewIndentItem> overviewItemsList) {
        recyclerViewIndentItem.setHasFixedSize(true);
        recyclerViewIndentItem.setLayoutManager(new LinearLayoutManager(mContext));
        IndentItemListAdapter indentListAdapter = new IndentItemListAdapter(mContext, overviewItemsList, this);
        recyclerViewIndentItem.setAdapter(indentListAdapter);
        indentListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtWork:
                if (process_id.equals("DEFAULT")) {
                    setTextviewBackground(R.id.txtSafety, R.id.txtWork);
                    ((TextView) findViewById(R.id.txtDescription)).setText("");
                    setWorkSafetyData("work_instructions", "DEFAULT");
                } else {
                    isSelected = "work";
                    setTextviewBackground(R.id.txtSafety, R.id.txtWork);
                    ((TextView) findViewById(R.id.txtDescription)).setText("");
                    setWorkSafetyData("work_instructions", process_id);
                }
                break;
            case R.id.txtSafety:
                if (process_id.equals("DEFAULT")) {
                    setTextviewBackground(R.id.txtWork, R.id.txtSafety);
                    ((TextView) findViewById(R.id.txtDescription)).setText("");
                    setWorkSafetyData("safety_instructions", "DEFAULT");
                } else {
                    isSelected = "safety";
                    setTextviewBackground(R.id.txtWork, R.id.txtSafety);
                    setWorkSafetyData("safety_instructions", process_id);
                    ((TextView) findViewById(R.id.txtDescription)).setText("");
                }
                break;
            case R.id.linearLogout:
                logout();
                break;
            case R.id.relativeIndentItemList:
            case R.id.btnGo:
                AppPreference.clearAllPosition(mContext);
                itemListDialog();
                break;
            case R.id.btnSendId:
                setIndentItemList();
                break;
            case R.id.linearOverview:
                startActivity(new Intent(mContext, ItemOverviewActivity.class));
                finish();
                break;
            case R.id.linearImgVdo:
                startActivity(new Intent(mContext, ItemImageActivity.class));
                finish();
                break;
            case R.id.linearOthers:
                startActivity(new Intent(mContext, ItemOthersActivity.class));
                finish();
                break;
            case R.id.txtIndentItemId:
            case R.id.txtIndentItemDesc:
            case R.id.btnIndentItemSelect:
                int i = Integer.parseInt(view.getTag().toString());
                ((TextView) findViewById(R.id.txt_indent_item_name)).setText(
                        overviewIndentItemListModal.getJsonData().getItems().get(i).getItemMcode() + " - " +
                                overviewIndentItemListModal.getJsonData().getItems().get(i).getItemName());
                setItemBarLabel(overviewIndentItemListModal.getJsonData().getItems().get(i).getItemId());
                setItemInfoAndProcess(process_id, overviewIndentItemListModal.getJsonData().getItems().get(i).getItemId());
                AppPreference.setStringPreference(mContext, Constant.ITEM_ID, overviewIndentItemListModal.getJsonData().getItems().get(i).getItemId());
                overviewItemsList.clear();
                dialog_indent_item.dismiss();
                break;
            case R.id.linear_main:
                goMainActivity();
                break;
            case R.id.linearImageVideo:
                int pos = Integer.parseInt(view.getTag().toString());
                ((TextView) findViewById(R.id.txtDescription))
                        .setText(detailsModal.getJsonData().getRows().get(pos).getDescription());
                break;
        }
    }

    private void setTextviewBackground(int idWork, int idSafety) {
        ((TextView) findViewById(idWork)).setTextColor(getResources().getColor(R.color.md_teal_400));
        ((TextView) findViewById(idWork)).setBackgroundResource(R.color.transparent);

        ((TextView) findViewById(idSafety)).setTextColor(getResources().getColor(R.color.white));
        ((TextView) findViewById(idSafety)).setBackgroundResource(R.color.md_teal_400);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    //finish all background activities
    private void goMainActivity() {
        Intent intent = new Intent(mContext, MaintListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }
}
