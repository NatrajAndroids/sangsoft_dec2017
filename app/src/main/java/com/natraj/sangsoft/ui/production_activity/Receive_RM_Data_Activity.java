package com.natraj.sangsoft.ui.production_activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.adapter.WorkAreaItemsAdapter;
import com.natraj.sangsoft.constant.Constant;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_info_bar_modal.IndentInfoBarModal;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_actionbar.IndentOverviewActionBar;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_actionbar.Order;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_header_info.CommonDataModal;
import com.natraj.sangsoft.model.receive_rm_data_modal.RMUnit;
import com.natraj.sangsoft.model.receive_rm_data_modal.RMIndentStatus;
import com.natraj.sangsoft.model.receive_rm_data_modal.RmDataModal;
import com.natraj.sangsoft.model.receive_rm_data_modal.RMInutItem;
import com.natraj.sangsoft.retrofit_provider.RetrofitService;
import com.natraj.sangsoft.retrofit_provider.WebResponse;
import com.natraj.sangsoft.ui.LoginActivity;
import com.natraj.sangsoft.utils.AppPreference;
import com.natraj.sangsoft.utils.BaseActivity;
import com.quenDel.multiUtils.utils.AppAlerts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class Receive_RM_Data_Activity extends BaseActivity implements View.OnClickListener {

    private String strRmStatus = "";
    private String strindentStatus = "";
    private IndentOverviewActionBar overviewActionBar;
    private Toolbar toolbar;
    private int currentIndent = 0;
    String order_id = "";
    private List<RMInutItem> receiveRmInutItems = new ArrayList<>();
    private List<RMUnit> rmUnit = new ArrayList<>();
    private RecyclerView recyclerWorkAreaItems;
    private Spinner spinnerCmbStatus;
    private Context mContext;
    private String lang, mchId, userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive__rm__data_);
        mContext = this;
        initToolbar();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setVisibility(View.VISIBLE);
        if (getSupportActionBar() == null) {
            return;
        }
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((Button) findViewById(R.id.btnCancel)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearIndentOverview)).setOnClickListener(this);
        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearForItem)).setOnClickListener(this);
        recyclerWorkAreaItems = (RecyclerView) findViewById(R.id.recyclerWorkAreaItems);
        recyclerWorkAreaItems.setHasFixedSize(true);
        recyclerWorkAreaItems.setLayoutManager(new LinearLayoutManager(mContext));

        ((ImageView) findViewById(R.id.imgLeft)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.imgRight)).setOnClickListener(this);
        spinnerCmbStatus = (Spinner) findViewById(R.id.spinnerCmbStatus);
        ((Button) findViewById(R.id.btnSave)).setOnClickListener(this);
        getIds();
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Get language, mch id, user id, indent id
    private void getIds() {
        lang = AppPreference.getStringPreference(mContext, Constant.LANGUAGE);
        mchId = AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID);
        userId = AppPreference.getStringPreference(mContext, Constant.USERNAME);
        order_id = AppPreference.getStringPreference(mContext, Constant.INDENT_ID);
        if (!AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR).isEmpty()) {
            ((TextView) toolbar.findViewById(R.id.txtMachine))
                    .setText(AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR));
        }
        setToolbarBarLabel();
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Toolbar bar label
    private void setToolbarBarLabel() {
        String headerInfo = "headerInfo";
        String userId = AppPreference.getStringPreference(mContext, Constant.USERNAME);
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getCommonDataLbl(new Dialog(mContext), retrofitApiClient.commonDataItems(
                    headerInfo, userId, lang), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<CommonDataModal> response = (Response<CommonDataModal>) result;
                    CommonDataModal dataModal = response.body();
                    if (dataModal.getIsError().equals(0)) {
                        ((TextView) toolbar.findViewById(R.id.prod_tooltip)).setText(dataModal.getStrings().getProdTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_maintenance)).setText(dataModal.getStrings().getMaintTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_qa)).setText(dataModal.getStrings().getQATooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_help)).setText(dataModal.getStrings().getHelpTooltip());
                        ((TextView) toolbar.findViewById(R.id.txtEmpIdToolbar)).setText(dataModal.getJsonData().getUserName());
                        setIndentActionBarData();
                    } else {
                        AppAlerts.show(mContext, dataModal.getErrorMsg());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Indent Action Bar Data
    private void setIndentActionBarData() {
        String op = "orderActionBar";
        RetrofitService.getIndentActionBarResponce(new Dialog(mContext), retrofitApiClient.indentActionbar(mchId, op, userId, lang),
                new WebResponse() {
                    @Override
                    public void onResponseSuccess(Response<?> result) {
                        Response<IndentOverviewActionBar> response = (Response<IndentOverviewActionBar>) result;
                        overviewActionBar = response.body();
                        if (overviewActionBar.getIsError() == 0) {
                            ((TextView) findViewById(R.id.lbl_overview)).setText(overviewActionBar.getStrings().getBtnOverview());
                            ((TextView) findViewById(R.id.lbl_action)).setText(overviewActionBar.getStrings().getLblRecord());
                            ((TextView) findViewById(R.id.lbl_pause)).setText(overviewActionBar.getStrings().getBtnPause());
                            ((TextView) findViewById(R.id.lbl_close)).setText(overviewActionBar.getStrings().getTooltipClose());
                            ((TextView) findViewById(R.id.lbl_progress)).setText(overviewActionBar.getStrings().getBtnProgress());
                            setIndentValue();
                        }
                    }

                    @Override
                    public void onResponseFailed(String error) {
                        AppAlerts.show(mContext, error);
                    }
                });
    }

    private void setIndentValue() {
        if (overviewActionBar.getJsonData() == null)
            return;
        if (overviewActionBar.getJsonData().getOrderCount() == null)
            return;
        Order overviewActionBarIndent = overviewActionBar.getJsonData().getOrders().get(currentIndent);
        if (overviewActionBarIndent.getOrderID() != null)
            order_id = overviewActionBarIndent.getOrderID();
        ((TextView) findViewById(R.id.indent_name)).setText(overviewActionBarIndent.getIndent() + " " + "(" + order_id + ")");
        ((TextView) findViewById(R.id.indent_status)).setText(overviewActionBarIndent.getOrderStatus());
        strindentStatus = overviewActionBarIndent.getOrderStatus();
        ((TextView) findViewById(R.id.indent_status)).setText(overviewActionBarIndent.getOrderStatus());
        AppPreference.setStringPreference(mContext, Constant.INDENT_ID, order_id);
        setInfoBarInfo(order_id);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Indent Info Bar Label
    private void setInfoBarInfo(String strOrderID) {
        String op = "orderInfoBar";
        RetrofitService.getIndentInfoBarResponce(new Dialog(mContext), retrofitApiClient.indentInfobar(mchId, op, userId, lang, strOrderID), new WebResponse() {
            @Override
            public void onResponseSuccess(Response<?> result) {
                Response<IndentInfoBarModal> response = (Response<IndentInfoBarModal>) result;
                IndentInfoBarModal infoBarModal = response.body();
                if (infoBarModal.getIsError().equals("0")) {
                    ((TextView) findViewById(R.id.indent_no)).setText(infoBarModal.getStrings().getLblIndentNo());
                    ((TextView) findViewById(R.id.indent_timing)).setText(infoBarModal.getStrings().getLblTiming());
                    ((TextView) findViewById(R.id.indent_foritem)).setText(infoBarModal.getStrings().getLblForItem());
                    ((TextView) findViewById(R.id.indent_qtyib)).setText(infoBarModal.getStrings().getLblQty());

                    ((TextView) findViewById(R.id.txtIndentId)).setText(infoBarModal.getJsonData().getIndentRef());
                    ((TextView) findViewById(R.id.txtIndentTiming)).setText(infoBarModal.getJsonData().getTiming());
                    ((TextView) findViewById(R.id.txtIndentForItem)).setText(infoBarModal.getJsonData().getItemName());
                    ((TextView) findViewById(R.id.txtIndentQuantityValue)).setText
                            (infoBarModal.getJsonData().getProducedQty() + " of " + infoBarModal.getJsonData().getOrderQty());
                    setReceiveRmLabel();
                }
            }

            @Override
            public void onResponseFailed(String error) {
                AppAlerts.show(mContext, error);
            }
        });
    }

    private void setReceiveRmLabel() {
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getReceiveRmWorkAreaData(new Dialog(mContext), retrofitApiClient.setReceiveRmWorkArea(
                    "receiveRM", mchId, userId, lang, order_id), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<RmDataModal> response = (Response<RmDataModal>) result;
                    RmDataModal receiveRmDataModal = response.body();
                    receiveRmInutItems.clear();
                    rmUnit.clear();
                    rmUnit.addAll(receiveRmDataModal.getUnits());
                    receiveRmInutItems.addAll(receiveRmDataModal.getJsonData().getInutItems());

                    ((TextView) findViewById(R.id.txtlblItemCode)).setText(receiveRmDataModal.getStrings().getLblColItemCode());
                    ((TextView) findViewById(R.id.txtlblDescription)).setText(receiveRmDataModal.getStrings().getLblColDescription());
                    ((TextView) findViewById(R.id.txtlblExpected)).setText(receiveRmDataModal.getStrings().getLblColTotExp());
                    ((TextView) findViewById(R.id.txtlblReceived)).setText(receiveRmDataModal.getStrings().getLblColRecived());
                    ((TextView) findViewById(R.id.txtLblReturned)).setText(receiveRmDataModal.getStrings().getLblColTobeReturned());
                    ((TextView) findViewById(R.id.txtlblUomB)).setText(receiveRmDataModal.getStrings().getLblColUom());

                    ((TextView) findViewById(R.id.area_title)).setText(receiveRmDataModal.getStrings().getAreaTitle());
                    ((Button) findViewById(R.id.btnSave)).setText(receiveRmDataModal.getStrings().getBtnSave());
                    ((Button) findViewById(R.id.btnCancel)).setText(receiveRmDataModal.getStrings().getBtnCancel());
                    ((TextView) findViewById(R.id.txtLabelIndentedQty)).setText(receiveRmDataModal.getStrings().getLblIndentQty());
                    ((EditText) findViewById(R.id.txtindentQty)).setText(receiveRmDataModal.getJsonData().getIndentQty());
                    ((TextView) findViewById(R.id.txtInputsNeeded)).setText(receiveRmDataModal.getStrings().getLbltableCaption());
                    ((TextView) findViewById(R.id.txtComment)).setText(receiveRmDataModal.getStrings().getLblComment());
                    ((TextView) findViewById(R.id.txtStatus)).setText(receiveRmDataModal.getStrings().getLblStatus());
                    ((EditText) findViewById(R.id.edtComments)).setText(receiveRmDataModal.getJsonData().getComment());

                    setItemList();
                    setSpinnerUOM(receiveRmDataModal.getUnits(), receiveRmDataModal.getJsonData().getIndentUOM());
                    setSpinnerData(receiveRmDataModal.getIndentRMStatus(), receiveRmDataModal.getJsonData().getStatusVal());
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    private void setItemList() {
        WorkAreaItemsAdapter adapter = new WorkAreaItemsAdapter(mContext, receiveRmInutItems);
        recyclerWorkAreaItems.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void setSpinnerUOM(final List<RMUnit> spinnerData, String strIndentUOM) {
        Spinner spinnerUom = (Spinner) findViewById(R.id.spinnerUom);
        spinnerUom.setEnabled(false);
        String[] items = new String[spinnerData.size()];
        for (int i = 0; i < spinnerData.size(); i++) {
            items[i] = spinnerData.get(i).getDisplayName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.spinner_process, items);
        spinnerUom.setAdapter(adapter);
        for (int i = 0; i < spinnerData.size(); i++) {
            if (strIndentUOM.equals(spinnerData.get(i).getDataKey())) {
                spinnerUom.setSelection(i);
            }
        }
        spinnerUom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String strUOM = spinnerData.get(i).getDataKey();
                Log.e("UOM_Value", strUOM);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setSpinnerData(final List<RMIndentStatus> spinnerData, String strStatus) {
        String[] items = new String[spinnerData.size()];
        for (int i = 0; i < spinnerData.size(); i++) {
            items[i] = spinnerData.get(i).getDisplayName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.spinner_process, items);
        spinnerCmbStatus.setAdapter(adapter);
        for (int i = 0; i < spinnerData.size(); i++) {
            if (strStatus.equals(spinnerData.get(i).getDataKey())) {
                spinnerCmbStatus.setSelection(i);
            }
        }
        spinnerCmbStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strRmStatus = spinnerData.get(i).getDataKey();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                tableJson();
                break;
            case R.id.btnCancel:
            case R.id.linearIndentOverview:
                finish();
                break;
            case R.id.linearLogout:
                logout();
                break;
            case R.id.linearForItem:
                startActivity(new Intent(mContext, ItemOverviewActivity.class));
                break;
            case R.id.imgLeft:
                if (currentIndent > 0) {
                    currentIndent--;
                    setIndentValue();
                } else {
                    AppAlerts.show(mContext, "This is First Order ");
                }
                break;
            case R.id.imgRight:
                if (overviewActionBar.getJsonData().getOrderCount() == null)
                    return;
                if (currentIndent < (overviewActionBar.getJsonData().getOrderCount()) - 1) {
                    currentIndent++;
                    setIndentValue();
                } else {
                    AppAlerts.show(mContext, "This is Last Order ");
                }
                break;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Log out
    private void logout() {
        AppAlerts.showAlertWithAction(mContext, "Logout", "Are you sure want to Logout?",
                "YES", "NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        clearAllPreferences();
                    }
                }, true, true);
    }

    private void clearAllPreferences() {
        AppPreference.clearAllPreferences(mContext);
        Intent intent = new Intent(mContext, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    private void tableJson() {
        JSONObject mainRowObjects = new JSONObject();
        JSONObject mainFilter = new JSONObject();
        try {
            mainRowObjects.put("order_id", order_id);
            mainRowObjects.put("rm_received_status", strRmStatus);
            mainRowObjects.put("rm_received_comments", ((EditText) findViewById(R.id.edtComments)).getText().toString());
            String strSts = "getDataKey('indent_status','material_received')";
            mainRowObjects.put("cur_status", strSts);
            mainRowObjects.put("updation_date", "NOW()");
            mainRowObjects.put("updated_by", userId);

            mainFilter.put("order_id", order_id);
            mainRowObjects.put("filter", mainFilter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONArray mainRow = new JSONArray();
        mainRow.put(mainRowObjects);
        JSONObject mainTable = new JSONObject();

        /*Table detail*/
        JSONObject dtlRowObjects = new JSONObject();
        for (int i = 0; i < receiveRmInutItems.size(); i++) {
            View view = recyclerWorkAreaItems.getChildAt(i);
            EditText edtReceived = (EditText) view.findViewById(R.id.edtReceived);
            EditText edtReturned = (EditText) view.findViewById(R.id.edtReturned);
            int intReceived = Integer.parseInt(receiveRmInutItems.get(i).getReceivedQty());
            int received = Integer.parseInt(edtReceived.getText().toString());
            if (received >= intReceived || intReceived == 0) {
                receiveRmInutItems.get(i).setReceivedQty(edtReceived.getText().toString());
                createMainTbl(i, dtlRowObjects, mainTable, mainRow, edtReceived, edtReturned);
            } else {
                AppAlerts.show(mContext, "Please enter greater than updated value");
            }
        }
    }

    private void createMainTbl(int i, JSONObject jRobj, JSONObject jMTbl, JSONArray jMRow, EditText edtRec, EditText edtRet) {
        JSONObject dtlFilter = new JSONObject();
        try {
            jRobj.put("received_qty", edtRec.getText().toString());
            jRobj.put("tobe_returned_qty", edtRet.getText().toString());
            jRobj.put("updation_date", "NOW()");
            jRobj.put("updated_by", userId);

            dtlFilter.put("order_bom_ID", receiveRmInutItems.get(i).getOrderBomID());
            jRobj.put("filter", dtlFilter);
            createJsonData(jRobj, jMTbl, jMRow);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void createJsonData(JSONObject objDtlRow, JSONObject objMainTbl, JSONArray objMainRow) {
        JSONArray dtlRow = new JSONArray();
        dtlRow.put(objDtlRow);

        JSONObject dtlTable = new JSONObject();
        JSONObject mainJsonObject = new JSONObject();
        try {
            objMainTbl.put("rows", objMainRow);
            dtlTable.put("rows", dtlRow);
            mainJsonObject.put("machine_id", mchId);
            mainJsonObject.put("op", "saveRM");
            mainJsonObject.put("user_id", userId);
            mainJsonObject.put("lang", lang);
            mainJsonObject.put("TblMain", objMainTbl);
            mainJsonObject.put("TblDetl", dtlTable);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        saveRmData(mainJsonObject);
    }

    private void saveRmData(JSONObject jsonObject) {
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                ((jsonObject)).toString());
        RetrofitService.getSaveRMData(new Dialog(mContext), retrofitApiClient.saveRMData(body), new WebResponse() {
            @Override
            public void onResponseSuccess(Response<?> result) {
                Response<ResponseBody> response = (Response<ResponseBody>) result;
                try {
                    String s = response.body().string();
                    JSONObject jsonObject = new JSONObject(s);
                    Log.e("Save_response:-", jsonObject + "");
                    if (jsonObject.getString("is_error").equals("0")) {
                        AppAlerts.show(mContext, "Received Raw Material Saved Successfully.");
                        setReceiveRmLabel();
                    } else {
                        AppAlerts.show(mContext, jsonObject.getString("error_msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseFailed(String error) {
                AppAlerts.show(mContext, error);
            }
        });
    }

}