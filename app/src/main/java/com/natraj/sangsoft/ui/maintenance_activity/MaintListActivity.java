package com.natraj.sangsoft.ui.maintenance_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.adapter.MaintenanceActivityListAdapter;
import com.natraj.sangsoft.constant.Constant;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_action_bar.MActionBarModal;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_activity_list_modal.MaintListModal;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_activity_list_modal.MaintListRow;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_header_info.CommonDataModal;
import com.natraj.sangsoft.retrofit_provider.RetrofitService;
import com.natraj.sangsoft.retrofit_provider.WebResponse;
import com.natraj.sangsoft.ui.LoginActivity;
import com.natraj.sangsoft.ui.production_activity.IndentOverViewActivity;
import com.natraj.sangsoft.utils.AppPreference;
import com.natraj.sangsoft.utils.BaseActivity;
import com.quenDel.multiUtils.neworks.NetworkHandler;
import com.quenDel.multiUtils.utils.AppAlerts;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class MaintListActivity extends BaseActivity implements View.OnClickListener {


    public static MActionBarModal actionBarModal;
    private CheckBox check_area_Executed, check_area_canceled;
    private Toolbar toolbar;
    private String mId;
    private String lang;
    private String userId;
    private RecyclerView recycler_activity_list;
    private List<MaintListRow> maintListRows = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintenance_list);
        getToolbarView();
    }

    private void getToolbarView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() == null) return;
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((ImageView) toolbar.findViewById(R.id.imgDrill)).setBackground(getResources().getDrawable(R.drawable.image_back));
        ((ImageView) toolbar.findViewById(R.id.imgCart)).setBackground(getResources().getDrawable(R.drawable.image_transparent_back));
        if (!AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR).isEmpty()) {
            ((TextView) toolbar.findViewById(R.id.txtMachine))
                    .setText(AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR));
        }
        mId = AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID);
        lang = AppPreference.getStringPreference(mContext, Constant.LANGUAGE);
        userId = AppPreference.getStringPreference(mContext, Constant.USERNAME);

        initialisation();
    }

    private void initialisation() {
        recycler_activity_list = (RecyclerView) findViewById(R.id.recycler_activity_list);
        check_area_Executed = (CheckBox) findViewById(R.id.check_area_Executed);
        check_area_canceled = (CheckBox) findViewById(R.id.check_area_canceled);
        check_area_Executed.setOnClickListener(this);
        check_area_canceled.setOnClickListener(this);

        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setVisibility(View.VISIBLE);
        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setOnClickListener(this);
        ((LinearLayout) toolbar.findViewById(R.id.linear_prod)).setOnClickListener(this);

        ((TextView) findViewById(R.id.txt_view)).setOnClickListener(this);
        ((TextView) findViewById(R.id.txt_execute)).setOnClickListener(this);
        ((TextView) findViewById(R.id.txt_break_down)).setOnClickListener(this);

        setHeaderInfoLabel();
        setMaintenanceActionBarData();
        maintListData("planned");
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Header Info label
    private void setHeaderInfoLabel() {
        String headerInfo = "headerInfo";
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getCommonDataLbl(new Dialog(mContext), retrofitApiClient.commonDataItems(
                    headerInfo, userId, lang), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<CommonDataModal> response = (Response<CommonDataModal>) result;
                    CommonDataModal dataModal = response.body();
                    if (dataModal.getIsError().equals(0)) {
                        ((TextView) toolbar.findViewById(R.id.prod_tooltip)).setText(dataModal.getStrings().getProdTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_maintenance)).setText(dataModal.getStrings().getMaintTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_qa)).setText(dataModal.getStrings().getQATooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_help)).setText(dataModal.getStrings().getHelpTooltip());
                        ((TextView) toolbar.findViewById(R.id.txtEmpIdToolbar)).setText(dataModal.getJsonData().getUserName());

                    } else {
                        AppAlerts.show(mContext, dataModal.getErrorMsg());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Maintenance Action Bar Data
    private void setMaintenanceActionBarData() {
        String op = "maintActionBar";
        RetrofitService.maintenanceActionBarData(new Dialog(mContext), retrofitApiClient.maintenanceActionbar(mId, op, userId, lang),
                new WebResponse() {
                    @Override
                    public void onResponseSuccess(Response<?> result) {
                        Response<MActionBarModal> response = (Response<MActionBarModal>) result;
                        actionBarModal = response.body();
                        if (actionBarModal.getIsError() == 0) {
                            ((TextView) findViewById(R.id.txt_activity_list)).setText(actionBarModal.getStrings().getBtnActList());
                            ((TextView) findViewById(R.id.txt_view)).setText(actionBarModal.getStrings().getBtnView());
                            ((TextView) findViewById(R.id.txt_execute)).setText(actionBarModal.getStrings().getBtnExecute());
                            ((TextView) findViewById(R.id.txt_break_down)).setText(actionBarModal.getStrings().getBtnBreakDown());
                        }
                    }

                    @Override
                    public void onResponseFailed(String error) {
                        AppAlerts.show(mContext, error);
                    }
                });
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    // Maintenance Activity List
    private void maintListData(String status) {
        String op = "maintList";
        RetrofitService.maintActivityListData(new Dialog(mContext), retrofitApiClient.maintActivityList(mId, op, userId, lang, status),
                new WebResponse() {
                    @Override
                    public void onResponseSuccess(Response<?> result) {
                        Response<MaintListModal> response = (Response<MaintListModal>) result;
                        MaintListModal maintListModal = response.body();
                        maintListRows.clear();
                        recycler_activity_list.setVisibility(View.VISIBLE);
                        if (maintListModal.getJsonData() != null) {
                            maintListRows.addAll(maintListModal.getJsonData().getRows());
                            //   setAlarm();
                            if (actionBarModal.getIsError() == 0) {
                                ((TextView) findViewById(R.id.txt_area_title)).setText(maintListModal.getStrings().getAreaTitle());
                                check_area_Executed.setText(maintListModal.getStrings().getChkExecuted());
                                check_area_canceled.setText(maintListModal.getStrings().getChkCanceled());
                                ((TextView) findViewById(R.id.txtLblDateRange)).setText(maintListModal.getStrings().getColDate());
                                ((TextView) findViewById(R.id.txtLblFromTime)).setText(maintListModal.getStrings().getColFromTime());
                                ((TextView) findViewById(R.id.txtLblTillTime)).setText(maintListModal.getStrings().getColTillTime());
                                ((TextView) findViewById(R.id.txtLblScheduleName)).setText(maintListModal.getStrings().getColSchName());
                                ((TextView) findViewById(R.id.txtLblMandetory)).setText(maintListModal.getStrings().getColMandetory());
                                ((TextView) findViewById(R.id.txtLblBy)).setText(maintListModal.getStrings().getColBy());
                                ((TextView) findViewById(R.id.txtLblStatus)).setText(maintListModal.getStrings().getColStatus());
                            }
                            setActivityList();
                        } else {
                            AppAlerts.show(mContext, "Your activity list is empty");
                            recycler_activity_list.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onResponseFailed(String error) {
                        AppAlerts.show(mContext, error);
                        recycler_activity_list.setVisibility(View.GONE);
                    }
                });
    }

    private void setActivityList() {
        recycler_activity_list.setHasFixedSize(true);
        recycler_activity_list.setLayoutManager(new LinearLayoutManager(mContext));
        MaintenanceActivityListAdapter activityListAdapter = new MaintenanceActivityListAdapter(mContext, maintListRows, this);
        recycler_activity_list.setAdapter(activityListAdapter);
        activityListAdapter.notifyDataSetChanged();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    // OnClick method
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.linearLogout:
                logout();
                break;
            case R.id.linear_prod:
                goMainActivity();
                break;
            case R.id.txt_execute:
                //startActivity(new Intent(mContext, MaintExecuteActivity.class));
                break;
            case R.id.check_area_Executed:
                if (check_area_Executed.isChecked() && check_area_canceled.isChecked()) {
                    maintListData("Executed,Canceled");
                } else if (check_area_Executed.isChecked() && !check_area_canceled.isChecked()) {
                    maintListData("Executed");
                } else if (!check_area_Executed.isChecked() && check_area_canceled.isChecked()) {
                    maintListData("Canceled");
                } else {
                    maintListData("planned");
                }
                break;
            case R.id.check_area_canceled:
                if (check_area_Executed.isChecked() && check_area_canceled.isChecked()) {
                    maintListData("Executed,Canceled");
                } else if (check_area_Executed.isChecked() && !check_area_canceled.isChecked()) {
                    maintListData("Executed");
                } else if (!check_area_Executed.isChecked() && check_area_canceled.isChecked()) {
                    maintListData("Canceled");
                } else {
                    maintListData("planned");
                }
                break;
            case R.id.linearActivityList:
                int position = Integer.parseInt(view.getTag().toString());
                MaintListRow listRow = maintListRows.get(position);

                Intent intent = new Intent(mContext, MaintExecuteActivity.class);
                intent.putExtra("sch_id", listRow.getSchID());
                intent.putExtra("sch_name", listRow.getSchName());
                startActivityForResult(intent, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            getToolbarView();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Log out
    private void logout() {
        AppAlerts.showAlertWithAction(mContext, "Logout", "Are you sure want to Logout?",
                "YES", "NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        clearAllPreferences();
                    }
                }, true, true);
    }

    private void clearAllPreferences() {
        AppPreference.clearAllPreferences(mContext);
        Intent intent = new Intent(mContext, LoginActivity.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    private void goMainActivity() {
        Intent intent = new Intent(mContext, IndentOverViewActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }
}