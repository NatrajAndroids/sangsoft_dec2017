package com.natraj.sangsoft.ui.production_activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.natraj.sangsoft.R;
import com.natraj.sangsoft.constant.Constant;
import com.natraj.sangsoft.model.production_modal.data_indent_overview.DataIndentOverview;
import com.natraj.sangsoft.model.production_modal.data_indent_overview.JsonData;
import com.natraj.sangsoft.model.production_modal.data_indent_overview.Strings;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_info_bar_modal.IndentInfoBarModal;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_actionbar.IndentOverviewActionBar;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_actionbar.Order;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_header_info.CommonDataModal;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.order_info_bar.OrderInfoBarModal;
import com.natraj.sangsoft.retrofit_provider.RetrofitService;
import com.natraj.sangsoft.retrofit_provider.WebResponse;
import com.natraj.sangsoft.ui.LoginActivity;
import com.natraj.sangsoft.ui.RecordQaActivity;
import com.natraj.sangsoft.ui.RequestQaActivity;
import com.natraj.sangsoft.ui.maintenance_activity.MaintListActivity;
import com.natraj.sangsoft.utils.AppPreference;
import com.natraj.sangsoft.utils.BaseActivity;
import com.quenDel.multiUtils.utils.AppAlerts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import im.dacer.androidcharts.PieHelper;
import im.dacer.androidcharts.PieView;
import retrofit2.Response;

public class IndentOverViewActivity extends BaseActivity implements View.OnClickListener {

    private String time = "00:00:00";
    private String stringTime = "00:00:00";
    private Handler someHandler;
    private DataIndentOverview dataIndentOverWA;
    private IndentOverviewActionBar overviewActionBar;
    private IndentInfoBarModal infoBarModal;
    private String order_id = "";
    private Toolbar toolbar;
    private String mId;
    private String lang;
    private String userId;
    private Dialog main_dialog;
    private int currentIndent = 0;
    private Context mContext;
    private boolean indentPlay = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_indent_overview);
        mContext = this;
        Constant.BASE_URL = AppPreference.getStringPreference(mContext, "base_url");

        initialisation();
    }

    private void initialisation() {

        mId = AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID);
        lang = AppPreference.getStringPreference(mContext, Constant.LANGUAGE);
        userId = AppPreference.getStringPreference(mContext, Constant.USERNAME);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setVisibility(View.VISIBLE);

        if (!AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR).isEmpty()) {
            ((TextView) toolbar.findViewById(R.id.txtMachine))
                    .setText(AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR));
        }
        timeHandler();
        clickOnItems();
        setHeaderInfoLabel();
    }

    private void timeHandler() {
        someHandler = new Handler(getMainLooper());
        someHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                currentTime(stringTime, time);
                someHandler.postDelayed(this, 1000);
            }
        }, 10);
    }

    private void clickOnItems() {
        ((TextView) findViewById(R.id.txtReceiveRm)).setOnClickListener(this);
        ((TextView) findViewById(R.id.txtQA)).setOnClickListener(this);
        ((TextView) findViewById(R.id.txtPR)).setOnClickListener(this);
        ((TextView) findViewById(R.id.txtRecordP)).setOnClickListener(this);
        ((TextView) findViewById(R.id.txtMark)).setOnClickListener(this);
        ((TextView) findViewById(R.id.txtClose)).setOnClickListener(this);

        ((LinearLayout) findViewById(R.id.linearOverview)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearRecord)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearPauseResume)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearClose)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearProgress)).setOnClickListener(this);
        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setOnClickListener(this);
        ((LinearLayout) toolbar.findViewById(R.id.linear_main)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.imgLeft)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.imgRight)).setOnClickListener(this);
        ((TextView) findViewById(R.id.txtBtnItemOverview)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearForItem)).setOnClickListener(this);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Header Info label
    private void setHeaderInfoLabel() {
        String headerInfo = "headerInfo";
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getCommonDataLbl(new Dialog(mContext), retrofitApiClient.commonDataItems(
                    headerInfo, userId, lang), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<CommonDataModal> response = (Response<CommonDataModal>) result;
                    CommonDataModal dataModal = response.body();
                    if (dataModal.getIsError().equals(0)) {
                        ((TextView) toolbar.findViewById(R.id.prod_tooltip)).setText(dataModal.getStrings().getProdTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_maintenance)).setText(dataModal.getStrings().getMaintTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_qa)).setText(dataModal.getStrings().getQATooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_help)).setText(dataModal.getStrings().getHelpTooltip());
                        ((TextView) toolbar.findViewById(R.id.txtEmpIdToolbar)).setText(dataModal.getJsonData().getUserName());
                        setIndentActionBarData();

                    } else {
                        AppAlerts.show(mContext, dataModal.getErrorMsg());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Indent Action Bar Data
    private void setIndentActionBarData() {
        String op = "orderActionBar";
        RetrofitService.getIndentActionBarResponce(new Dialog(mContext), retrofitApiClient.indentActionbar(mId, op, userId, lang),
                new WebResponse() {
                    @Override
                    public void onResponseSuccess(Response<?> result) {
                        Response<IndentOverviewActionBar> response = (Response<IndentOverviewActionBar>) result;
                        overviewActionBar = response.body();
                        if (overviewActionBar.getIsError() == 0) {
                            ((TextView) findViewById(R.id.lbl_overview)).setText(overviewActionBar.getStrings().getBtnOverview());
                            ((TextView) findViewById(R.id.lbl_action)).setText(overviewActionBar.getStrings().getLblRecord());
                            ((TextView) findViewById(R.id.lbl_pause)).setText(overviewActionBar.getStrings().getBtnPause());
                            ((TextView) findViewById(R.id.lbl_close)).setText(overviewActionBar.getStrings().getTooltipClose());
                            ((TextView) findViewById(R.id.lbl_progress)).setText(overviewActionBar.getStrings().getBtnProgress());
                            setIndentValue();
                        }
                    }

                    @Override
                    public void onResponseFailed(String error) {
                        AppAlerts.show(mContext, error);
                    }
                });
    }

    private void setIndentValue() {
        if (overviewActionBar.getJsonData() == null)
            return;
        if (overviewActionBar.getJsonData().getOrderCount() == null)
            return;
        Order overviewActionBarIndent = overviewActionBar.getJsonData().getOrders().get(currentIndent);
        if (overviewActionBarIndent.getOrderID() != null)
            order_id = overviewActionBarIndent.getOrderID();
        ((TextView) findViewById(R.id.indent_name)).setText(overviewActionBarIndent.getIndent() + " " + "(" + order_id + ")");
        ((TextView) findViewById(R.id.indent_status)).setText(overviewActionBarIndent.getOrderStatus());
        Log.e("Order_ID", order_id);
        Log.e("Machine_ID", mId);
        AppPreference.setStringPreference(mContext, Constant.INDENT_ID, order_id);
        setInfoBarInfo(order_id);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Indent Info Bar Label
    private void setInfoBarInfo(String strOrderID) {
        String op = "orderInfoBar";
        RetrofitService.getOrderInfoBarResponce(new Dialog(mContext), retrofitApiClient.orderInfobar(mId, op, userId, lang, strOrderID), new WebResponse() {
            @Override
            public void onResponseSuccess(Response<?> result) {
                Response<IndentInfoBarModal> response = (Response<IndentInfoBarModal>) result;
                infoBarModal = response.body();
                if (infoBarModal.getIsError().equals("0")) {
                    ((TextView) findViewById(R.id.indent_no)).setText(infoBarModal.getStrings().getLblIndentNo());
                    ((TextView) findViewById(R.id.indent_timing)).setText(infoBarModal.getStrings().getLblTiming());
                    ((TextView) findViewById(R.id.indent_foritem)).setText(infoBarModal.getStrings().getLblForItem());
                    ((TextView) findViewById(R.id.indent_qtyib)).setText(infoBarModal.getStrings().getLblQty());

                    ((TextView) findViewById(R.id.txtIndentId)).setText(infoBarModal.getJsonData().getIndentRef());
                    ((TextView) findViewById(R.id.txtIndentTiming)).setText(infoBarModal.getJsonData().getTiming());
                    ((TextView) findViewById(R.id.txtIndentForItem)).setText(infoBarModal.getJsonData().getItemName());
                    ((TextView) findViewById(R.id.txtIndentQuantityValue)).setText
                            (infoBarModal.getJsonData().getProducedQty() + " of " + infoBarModal.getJsonData().getOrderQty());
                    setWorkAreaInfo(order_id);
                }
            }

            @Override
            public void onResponseFailed(String error) {
                AppAlerts.show(mContext, error);
            }
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Set All work Info
    private void setWorkAreaInfo(String orderId) {
        String op = "indentOverview";
        RetrofitService.getIndentWAResponce(new Dialog(mContext), retrofitApiClient.indentOverviewWA(mId, op, userId, lang, orderId), new WebResponse() {
            @Override
            public void onResponseSuccess(Response<?> result) {
                Response<DataIndentOverview> response = (Response<DataIndentOverview>) result;
                dataIndentOverWA = response.body();
                Strings obLabel = dataIndentOverWA.getStrings();
                JsonData obData = dataIndentOverWA.getJsonData();

                ((TextView) findViewById(R.id.txtReceiveRm)).setText(obLabel.getMnuReceiveRM());
                ((TextView) findViewById(R.id.txtQA)).setText(obLabel.getMnuRequestQA());
                ((TextView) findViewById(R.id.txtPR)).setText(obLabel.getMnuPause());
                ((TextView) findViewById(R.id.txtRecordP)).setText(obLabel.getMnuRecordProd());
                ((TextView) findViewById(R.id.txtMark)).setText(obLabel.getMnuComplete());
                ((TextView) findViewById(R.id.txtClose)).setText(obLabel.getMnuClose());

                ((TextView) findViewById(R.id.area_title)).setText(obLabel.getAreaTitle());
                ((TextView) findViewById(R.id.indent_qty)).setText(obLabel.getLblIndentQty());
                ((TextView) findViewById(R.id.indent_produced)).setText(obLabel.getLblProduced());
                ((TextView) findViewById(R.id.indent_rejected)).setText(obLabel.getLblRejected());
                ((TextView) findViewById(R.id.indent_balance)).setText(obLabel.getLblBalance());
                ((TextView) findViewById(R.id.pro_time)).setText(obLabel.getLblProdTime());
                ((TextView) findViewById(R.id.pro_rate)).setText(obLabel.getLblprodRate());

                if (response.isSuccessful()) {
                    if (dataIndentOverWA.getIsError().equals("0")) {
                        float orderQty = 0;
                        float rejQty = 0;
                        float prodQty = 0;

                        ((TextView) findViewById(R.id.txtPro)).setText(obLabel.getLblProduced());
                        ((TextView) findViewById(R.id.txtRej)).setText(obLabel.getLblRejected());
                        ((TextView) findViewById(R.id.txtBal)).setText(obLabel.getLblBalance());

                        orderQty = Float.parseFloat((obData.getOrderQty()));
                        prodQty = Float.parseFloat(obData.getProducedQty());
                        rejQty = Float.parseFloat(obData.getRejectedQty());
                        if (obData.getProdTime() != null) {
                            time = ((obData.getProdTime()));
                        } else {
                            time = "00:00:00";
                        }

                        if (obData.getProdTimeString() != null) {
                            stringTime = obData.getProdTimeString();
                        } else {
                            stringTime = "00:00:00";
                        }

                        float balance = orderQty - (prodQty + rejQty);

                        if (prodQty > 0) {
                            float proRate = prodQty / timeInMinute(time);
                            String strRate = String.format("%.2f", proRate);
                            ((TextView) findViewById(R.id.pro_rate_val)).setText(strRate + " " + obData.getProdRate());
                        } else {
                            ((TextView) findViewById(R.id.pro_rate_val)).setText(0 + " " + obData.getProdRate());
                        }

                        //((TextView) findViewById(R.id.area_status)).setText(obData.getActionStatus());
                        ((TextView) findViewById(R.id.indent_qty_val)).setText(obData.getOrderQty());
                        ((TextView) findViewById(R.id.indent_produced_val)).setText(obData.getProducedQty());
                        ((TextView) findViewById(R.id.indent_rejected_val)).setText(obData.getRejectedQty());
                        ((TextView) findViewById(R.id.indent_balance_val)).setText("" + balance);
                        /*
                         * pro_rate_val = indent_qty_val/pro_time_val
                         * */
                        setChart(orderQty, prodQty, rejQty, balance);
                        //timeDifference(obData.getProdTimeString());
                        //currentTime(stringTime, time);

                    } else {
                        AppAlerts.showAlertMessage(mContext, "Error", dataIndentOverWA.getErrorMsg());
                    }
                }
            }

            @Override
            public void onResponseFailed(String error) {
                AppAlerts.show(mContext, error);
            }
        });
    }

    // Set Chart view data of Activity
    private void setChart(float orderQty, float produced, float rejected, float balance) {
        produced = ((produced * 100.0f) / orderQty);
        rejected = (rejected * 100.0f) / orderQty;
        balance = (balance * 100.0f) / orderQty;

        PieView pieView = (PieView) findViewById(R.id.pieView);

        ArrayList<PieHelper> pieHelperArrayList = new ArrayList<PieHelper>();
        if (produced == 0) {
            pieHelperArrayList.add(new PieHelper(rejected, getResources().getColor(R.color.color_rejected)));/*#F5CA1F*/
            pieHelperArrayList.add(new PieHelper(balance, getResources().getColor(R.color.color_balance)));/*#004281*/
        } else if (rejected == 0) {
            pieHelperArrayList.add(new PieHelper(produced, getResources().getColor(R.color.color_produced)));/*#F53F0D*/
            pieHelperArrayList.add(new PieHelper(balance, getResources().getColor(R.color.color_balance)));/*#004281*/
        } else if (balance == 0) {
            pieHelperArrayList.add(new PieHelper(rejected, getResources().getColor(R.color.color_rejected)));/*#F5CA1F*/
            pieHelperArrayList.add(new PieHelper(produced, getResources().getColor(R.color.color_produced)));/*#F53F0D*/
        } else if (produced == 0 && rejected == 0) {
            pieHelperArrayList.add(new PieHelper(balance, getResources().getColor(R.color.color_balance)));/*#004281*/
        } else {
            pieHelperArrayList.add(new PieHelper(rejected, getResources().getColor(R.color.color_rejected)));/*#F5CA1F*/
            pieHelperArrayList.add(new PieHelper(produced, getResources().getColor(R.color.color_produced)));/*#F53F0D*/
            pieHelperArrayList.add(new PieHelper(balance, getResources().getColor(R.color.color_balance)));/*#004281*/
        }

        pieView.setDate(pieHelperArrayList);
        pieView.setOnPieClickListener(new PieView.OnPieClickListener() {
            @Override
            public void onPieClick(int index) {
               /* if (index != PieView.NO_SELECTED_INDEX) {
                    AppAlerts.show(mContext, index + " selected");
                } else {
                    AppAlerts.show(mContext, "No selected pie");
                }*/
            }
        });
        pieView.selectedPie(2);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    // OnClick method
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.linearOverview:
                //AppAlerts.show(mContext, "in process");
                break;
            case R.id.linearRecord:
                showCustomDialog();
                break;
            case R.id.linearPauseResume:
                AppAlerts.show(mContext, "in process");
                if (indentPlay) {
                    ((ImageView) findViewById(R.id.pause_resume)).setImageResource(R.drawable.ic_play);
                    indentPlay = false;
                } else {
                    ((ImageView) findViewById(R.id.pause_resume)).setImageResource(R.drawable.ic_pause);
                    indentPlay = true;
                }
                break;
            case R.id.linearClose:
                AppAlerts.show(mContext, "in process");
                break;
            case R.id.linearProgress:
                AppAlerts.show(mContext, "in process");
                break;
            case R.id.linearLogout:
                logout();
                break;
            case R.id.linear_main:
                startActivity(new Intent(mContext, MaintListActivity.class));
                break;
            case R.id.linearForItem:
            case R.id.txtBtnItemOverview:
                startActivity(new Intent(mContext, ItemOverviewActivity.class));
                break;
            case R.id.imgLeft:
                if (currentIndent > 0) {
                    currentIndent--;
                    setIndentValue();
                } else {
                    AppAlerts.show(mContext, "This is First Order ");
                }
                break;
            case R.id.imgRight:
                if (overviewActionBar.getJsonData().getOrderCount() == null)
                    return;
                if (currentIndent < (overviewActionBar.getJsonData().getOrderCount()) - 1) {
                    currentIndent++;
                    setIndentValue();
                } else {
                    AppAlerts.show(mContext, "This is Last Order ");
                }
                break;
            case R.id.txtReceiveRm:
                startActivity(new Intent(mContext, Receive_RM_Data_Activity.class));
                break;
            case R.id.dialogBtnIndentProgress:
                startActivity(new Intent(mContext, IndentProgressActivity.class));
                main_dialog.dismiss();
                break;
            case R.id.txtQA:
                startActivity(new Intent(mContext, RequestQaActivity.class));
                break;
            case R.id.dialogBtnItemOverview:
                startActivity(new Intent(mContext, ItemOverviewActivity.class));
                main_dialog.dismiss();
                break;
            case R.id.dialogBtnRecordQA:
                startActivity(new Intent(mContext, RecordQaActivity.class));
                main_dialog.dismiss();
                break;
            case R.id.txtPR:
                startActivity(new Intent(mContext, PauseResumeActivity.class));
                break;
            case R.id.txtRecordP:
                startActivity(new Intent(mContext, IndentRecordActivity.class));
                break;
            case R.id.dialogBtnMark:
                AppAlerts.show(mContext, ".... in process");
                main_dialog.dismiss();
                break;
            case R.id.dialogBtnClose:
                main_dialog.dismiss();
                break;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    // Custom dialog for Action bar Items List
    private void showCustomDialog() {
        main_dialog = new Dialog(this);
        main_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        main_dialog.setContentView(R.layout.layout_dialog);

        main_dialog.setCanceledOnTouchOutside(false);
        main_dialog.setCancelable(true);
        if (main_dialog.getWindow() != null)
            main_dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ((Button) main_dialog.findViewById(R.id.dialogBtnClose)).setText(overviewActionBar.getStrings().getBtnClose());
        ((Button) main_dialog.findViewById(R.id.dialogBtnIndentProgress)).setOnClickListener(this);
        ((Button) main_dialog.findViewById(R.id.dialogBtnItemOverview)).setOnClickListener(this);
        ((Button) main_dialog.findViewById(R.id.dialogBtnMark)).setOnClickListener(this);
        ((Button) main_dialog.findViewById(R.id.dialogBtnRecordQA)).setOnClickListener(this);
        ((Button) main_dialog.findViewById(R.id.dialogBtnClose)).setOnClickListener(this);
        Window window = main_dialog.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        main_dialog.show();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Log out
    private void logout() {
        AppAlerts.showAlertWithAction(mContext, "Logout", "Are you sure want to Logout?",
                "YES", "NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        clearAllPreferences();
                    }
                }, true, true);
    }

    private void clearAllPreferences() {
        AppPreference.clearAllPreferences(mContext);
        Intent intent = new Intent(mContext, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    private void currentTime(final String stringTime, final String strProTime) {
        ((TextView) findViewById(R.id.txtTimeDiff)).setText("Time: " + stringTime);
        String strcurrentTime = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH).format(new Date());
        float crntTime = timeInMinute(strcurrentTime);
        float strTime = timeInMinute(stringTime);
        float proTime = timeInMinute(strProTime);

        float minutesDiff = crntTime - strTime;
        float proDiff = crntTime - proTime;
        int strDiff = (int) proDiff;
        int strMnt = (int) (minutesDiff);

        ((TextView) findViewById(R.id.cr_time)).setText(strMnt + " min");

        ((TextView) findViewById(R.id.pro_time_val)).setText(strDiff + " min");
    }

    private float timeInMinute(String time) {
        String[] units = time.split(":");
        float hour = Integer.parseInt(units[0]);
        float minute = Integer.parseInt(units[1]);
        float second = Integer.parseInt(units[2]);

        float totalSecond = 60 * 60 * hour + 60 * minute + second;
        return totalSecond / 60;
    }
}