package com.natraj.sangsoft.ui.maintenance_activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.adapter.ActivityListAdapter;
import com.natraj.sangsoft.adapter.StatusSpinnerAdapter;
import com.natraj.sangsoft.constant.Constant;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_action_bar.MActionBarModal;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_activity_list_modal.MaintListRow;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_execute.MaintenanceExecuteEventStatusRow;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_execute.MaintenanceExecuteModal;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_execute.MaintenanceExecuteRow;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_header_info.CommonDataModal;
import com.natraj.sangsoft.retrofit_provider.RetrofitApiClient;
import com.natraj.sangsoft.retrofit_provider.RetrofitService;
import com.natraj.sangsoft.retrofit_provider.WebResponse;
import com.natraj.sangsoft.ui.LoginActivity;
import com.natraj.sangsoft.ui.production_activity.IndentOverViewActivity;

import com.natraj.sangsoft.utils.AppPreference;
import com.natraj.sangsoft.utils.BaseActivity;
import com.quenDel.multiUtils.neworks.NetworkHandler;
import com.quenDel.multiUtils.utils.AppAlerts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class MaintExecuteActivity extends BaseActivity implements View.OnClickListener {

    List<MaintenanceExecuteRow> list = new ArrayList<>();
    private String strStatus = "Done";
    private String schStatus = "";
    private String strDate = "";
    private String endDate = "";
    private String strTime = "";
    private String endTime = "";
    private String schID = "";
    private String schName = "";
    private String mId = "";

    public static MActionBarModal actionBarModal;
    public MaintenanceExecuteModal executeModal;
    private NetworkHandler networkHandler;
    private Toolbar toolbar;
    private String lang = "";
    private String userId = "";
    private Context mContext;
    private RetrofitApiClient retrofitApiClient;
    private Spinner spinnerStatusA;
    private RecyclerView recycleExecuteActivityList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maint_execute);
        mContext = this;
        Constant.BASE_URL = AppPreference.getStringPreference(mContext, "base_url");

        getToolbarView();
    }

    private void getToolbarView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() == null) return;
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((ImageView) toolbar.findViewById(R.id.imgDrill)).setBackground(getResources().getDrawable(R.drawable.image_back));
        ((ImageView) toolbar.findViewById(R.id.imgCart)).setBackground(getResources().getDrawable(R.drawable.image_transparent_back));

        mId = AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID);
        lang = AppPreference.getStringPreference(mContext, Constant.LANGUAGE);
        userId = AppPreference.getStringPreference(mContext, Constant.USERNAME);

        initialisation();
    }

    private void initialisation() {
        networkHandler = new NetworkHandler(MaintExecuteActivity.this);
        try {
            retrofitApiClient = RetrofitService.getRetrofit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        recycleExecuteActivityList = (RecyclerView) findViewById(R.id.recycleExecuteActivityList);
        recycleExecuteActivityList.setHasFixedSize(true);
        recycleExecuteActivityList.setLayoutManager(new LinearLayoutManager(mContext));

        spinnerStatusA = (Spinner) findViewById(R.id.spinnerStatusA);
        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setVisibility(View.VISIBLE);
        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setOnClickListener(this);
        ((LinearLayout) toolbar.findViewById(R.id.linear_prod)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnStartActivity)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnStopActivity)).setOnClickListener(this);
        ((Button) findViewById(R.id.btn_save)).setOnClickListener(this);
        ((Button) findViewById(R.id.btn_cancel)).setOnClickListener(this);
        ((TextView) findViewById(R.id.txt_activity_list)).setOnClickListener(this);
        if (!AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR).isEmpty()) {
            ((TextView) toolbar.findViewById(R.id.txtMachine))
                    .setText(AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR));
        }
        getListRow();
        setHeaderInfoLabel();
        setMaintenanceActionBarData();
        setMaintenanceExecute();
    }

    private void getListRow() {
        if (getIntent().getExtras() != null) {
            schID = getIntent().getStringExtra("sch_id");
            schName = getIntent().getStringExtra("sch_name");
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Header Info label
    private void setHeaderInfoLabel() {
        String headerInfo = "headerInfo";
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getCommonDataLbl(new Dialog(mContext), retrofitApiClient.commonDataItems(
                    headerInfo, userId, lang), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<CommonDataModal> response = (Response<CommonDataModal>) result;
                    CommonDataModal dataModal = response.body();
                    if (dataModal.getIsError().equals(0)) {
                        ((TextView) toolbar.findViewById(R.id.prod_tooltip)).setText(dataModal.getStrings().getProdTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_maintenance)).setText(dataModal.getStrings().getMaintTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_qa)).setText(dataModal.getStrings().getQATooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_help)).setText(dataModal.getStrings().getHelpTooltip());
                        ((TextView) toolbar.findViewById(R.id.txtEmpIdToolbar)).setText(dataModal.getJsonData().getUserName());

                    } else {
                        AppAlerts.show(mContext, dataModal.getErrorMsg());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Maintenance Action Bar Data
    private void setMaintenanceActionBarData() {
        String op = "maintActionBar";
        RetrofitService.maintenanceActionBarData(new Dialog(mContext), retrofitApiClient.maintenanceActionbar(mId, op, userId, lang),
                new WebResponse() {
                    @Override
                    public void onResponseSuccess(Response<?> result) {
                        Response<MActionBarModal> response = (Response<MActionBarModal>) result;
                        actionBarModal = response.body();
                        if (actionBarModal.getIsError() == 0) {
                            ((TextView) findViewById(R.id.txt_activity_list)).setText(actionBarModal.getStrings().getBtnActList());
                            ((TextView) findViewById(R.id.txt_activity_list)).setBackgroundColor(getResources().getColor(R.color.TRANSPARENT));
                            ((TextView) findViewById(R.id.txt_activity_list)).setTextColor(getResources().getColor(R.color.colorBlack));
                            ((TextView) findViewById(R.id.txt_view)).setText(actionBarModal.getStrings().getBtnView());
                            ((TextView) findViewById(R.id.txt_execute)).setText(actionBarModal.getStrings().getBtnExecute());
                            ((TextView) findViewById(R.id.txt_execute)).setTextColor(getResources().getColor(R.color.white));
                            ((TextView) findViewById(R.id.txt_execute)).setBackgroundColor(getResources().getColor(R.color.md_teal_400));
                            ((TextView) findViewById(R.id.txt_break_down)).setText(actionBarModal.getStrings().getBtnBreakDown());

                            ((TextView) findViewById(R.id.txt_execute_area_title)).setText(actionBarModal.getStrings().getBtnExecute());
                            ((TextView) findViewById(R.id.txt_activity_name)).setText(schName);
                        }
                    }

                    @Override
                    public void onResponseFailed(String error) {
                        AppAlerts.show(mContext, error);
                    }
                });
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Maintenance Execute
    private void setMaintenanceExecute() {
        String op = "maintExecute";
        Log.e("Details:-", op + "-" + userId + "-" + mId + "-" + lang + "-" + schID);
        RetrofitService.maintenanceExecuteData(new Dialog(mContext), retrofitApiClient.maintenanceExecute
                (op, userId, mId, lang, schID), new WebResponse() {
            @Override
            public void onResponseSuccess(Response<?> result) {
                Response<MaintenanceExecuteModal> response = (Response<MaintenanceExecuteModal>) result;
                executeModal = response.body();

                ((Button) findViewById(R.id.btn_cancel)).setText(executeModal.getStrings().getBtnCancel());
                //((Button) findViewById(R.id.btnStopActivity)).setText(executeModal.getStrings().get());
                ((TextView) findViewById(R.id.txtStartDateTimeLbl)).setText(executeModal.getStrings().getLblStartDate());
                ((TextView) findViewById(R.id.txtEndDateTimeLbl)).setText(executeModal.getStrings().getLblStopDate());
                ((TextView) findViewById(R.id.txtStatusLbl)).setText(executeModal.getStrings().getColStatus());
                ((TextView) findViewById(R.id.txtCommentLbl)).setText(executeModal.getStrings().getLblComments());
                ((TextView) findViewById(R.id.txtActivityLbl)).setText(executeModal.getStrings().getColActivity());
                ((TextView) findViewById(R.id.txtStatusLblB)).setText(executeModal.getStrings().getColStatus());
                setSpinnerData(executeModal.getEventStatusRows());

                if (executeModal.getIsError() == 0) {
                    list.clear();
                    list.addAll(executeModal.getJsonData().getRows());
                    ((Button) findViewById(R.id.btnStartActivity)).setEnabled(true);
                    ActivityListAdapter adapter = new ActivityListAdapter(mContext, executeModal.getJsonData().getRows());
                    recycleExecuteActivityList.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    AppAlerts.show(mContext, executeModal.getErrorMsg());
                    ((Button) findViewById(R.id.btnStartActivity)).setEnabled(false);
                }
            }

            @Override
            public void onResponseFailed(String error) {
                AppAlerts.show(mContext, error);
                ((Button) findViewById(R.id.btnStartActivity)).setEnabled(false);
            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    // set Status spinner data
    private void setSpinnerData(final List<MaintenanceExecuteEventStatusRow> spinnerData) {
        StatusSpinnerAdapter spinnerAdapter = new StatusSpinnerAdapter(mContext, R.layout.spinner_status_layout, spinnerData);
        spinnerStatusA.setAdapter(spinnerAdapter);
        spinnerStatusA.setSelection(1);
        spinnerStatusA.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                schStatus = spinnerData.get(i).getStatusValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    // OnClick method
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linearLogout:
                logout();
                break;
            case R.id.linear_prod:
                goMainActivity();
                break;
            case R.id.txt_activity_list:
                setResult(RESULT_OK);
                finish();
                break;
            case R.id.btn_cancel:
                setResult(RESULT_OK);
                finish();
                break;
            case R.id.btnStartActivity:
                onStartStopBtnClick(R.id.txtStartDateValue, R.id.txtStartTimeValue);
                ((Button) findViewById(R.id.btnStartActivity)).setVisibility(View.GONE);
                ((Button) findViewById(R.id.btnStopActivity)).setVisibility(View.VISIBLE);
                break;
            case R.id.btnStopActivity:
                onStartStopBtnClick(R.id.txtEndDateValue, R.id.txtEndTimeValue);
                ((Button) findViewById(R.id.btnStopActivity)).setVisibility(View.GONE);
                ((Button) findViewById(R.id.btn_save)).setVisibility(View.VISIBLE);
                for (int i = 0; i < list.size(); i++) {
                    getStatusSpinnerData(i);
                }
                break;
            case R.id.btn_save:
                strDate = ((TextView) findViewById(R.id.txtStartDateValue)).getText().toString();
                strTime = ((TextView) findViewById(R.id.txtStartTimeValue)).getText().toString();
                endTime = ((TextView) findViewById(R.id.txtEndTimeValue)).getText().toString();
                endDate = ((TextView) findViewById(R.id.txtEndDateValue)).getText().toString();
                Log.e("End Date n Time", endTime + " " + endDate);
                tableJson();
                break;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    // on Start/Stop Activity
    private void onStartStopBtnClick(int idDate, int idTime) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat mdTime = new SimpleDateFormat("HH:mm:ss",Locale.ENGLISH);
        strDate = mdformat.format(calendar.getTime());
        strTime = mdTime.format(calendar.getTime());
        ((TextView) findViewById(idDate)).setText(strDate);
        ((TextView) findViewById(idTime)).setText(strTime);
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Log out
    private void logout() {
        AppAlerts.showAlertWithAction(mContext, "Logout", "Are you sure want to Logout?",
                "YES", "NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        clearAllPreferences();
                    }
                }, true, true);
    }

    private void clearAllPreferences() {
        AppPreference.clearAllPreferences(mContext);
        Intent intent = new Intent(mContext, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    private void goMainActivity() {
        Intent intent = new Intent(mContext, IndentOverViewActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Create Json data
    private void tableJson() {
        JSONObject mainRowObjects = new JSONObject();
        JSONObject mainFilter = new JSONObject();
        try {
            mainRowObjects.put("start_date", strDate);
            mainRowObjects.put("stop_date", endDate);
            mainRowObjects.put("start_time", strTime);
            mainRowObjects.put("end_time", endTime);
            mainRowObjects.put("sch_status", schStatus);
            mainRowObjects.put("updation_date", endDate);
            mainRowObjects.put("updated_by", userId);

            mainFilter.put("sch_ID", schID);
            mainFilter.put("mch_ID", mId);
            mainRowObjects.put("filter", mainFilter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONArray mainRow = new JSONArray();
        mainRow.put(mainRowObjects);
        JSONObject mainTable = new JSONObject();

        /*Table detail*/
        JSONObject dtlRowObjects = new JSONObject();
        for (int i = 0; i < list.size(); i++) {
            getStatusSpinnerData(i);
            JSONObject dtlFilter = new JSONObject();
            try {
                dtlRowObjects.put("activity_status", strStatus);
                dtlRowObjects.put("updation_date", endDate);
                dtlRowObjects.put("updated_by", userId);

                dtlFilter.put("schedule_activity_id", list.get(i).getSchActID());
                dtlFilter.put("schedule_id", list.get(i).getSchID());
                dtlFilter.put("machine_id", mId);
                dtlRowObjects.put("filter", dtlFilter);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        JSONArray dtlRow = new JSONArray();
        dtlRow.put(dtlRowObjects);

        JSONObject dtlTable = new JSONObject();

        JSONObject mainJsonObject = new JSONObject();
        try {
            mainTable.put("rows", mainRow);
            dtlTable.put("rows", dtlRow);
            mainJsonObject.put("machine_id", mId);
            mainJsonObject.put("op", "UpdateMaintAct");
            mainJsonObject.put("user_id", userId);
            mainJsonObject.put("lang", lang);
            mainJsonObject.put("TblMain", mainTable);
            mainJsonObject.put("TblDetl", dtlTable);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),
                ((mainJsonObject)).toString());
        Log.e("Json data:-", mainJsonObject.toString());
        RetrofitService.getSaveDataResponse(new Dialog(mContext), retrofitApiClient.saveJsonData(body), new WebResponse() {
            @Override
            public void onResponseSuccess(Response<?> result) {
                Response<ResponseBody> response = (Response<ResponseBody>) result;
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    if (jsonObject.getString("is_error").equals("0")) {
                        //AppAlerts.show(mContext, "Data save");
                        itemListDialog();
                    } else {
                        AppAlerts.show(mContext, jsonObject.getString("error_msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseFailed(String error) {
                AppAlerts.show(mContext, error);
            }
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Set Order item list in dialog
    private void itemListDialog() {
        Dialog dialog_indent_item = new Dialog(this);
        dialog_indent_item.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_indent_item.setContentView(R.layout.dialog_data_save_success);
        dialog_indent_item.setCanceledOnTouchOutside(false);
        dialog_indent_item.setCancelable(true);
        if (dialog_indent_item.getWindow() != null)
            dialog_indent_item.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ((TextView) dialog_indent_item.findViewById(R.id.txtOK)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK);
                finish();
            }
        });
        dialog_indent_item.show();
        Window window = dialog_indent_item.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
    }

    private void getStatusSpinnerData(int i) {
        View view = recycleExecuteActivityList.getChildAt(i);
        Spinner statusSpinner = (Spinner) view.findViewById(R.id.statusSpinner);
        statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strStatus = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}