package com.natraj.sangsoft.ui.production_activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.adapter.IndentProgressItemsAdapter;
import com.natraj.sangsoft.constant.Constant;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_info_bar_modal.IndentInfoBarModal;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_actionbar.IndentOverviewActionBar;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_actionbar.Order;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_header_info.CommonDataModal;
import com.natraj.sangsoft.model.production_modal.indent_progress_modal.IndentActivity;
import com.natraj.sangsoft.model.production_modal.indent_progress_modal.IndentProgressItemsModal;
import com.natraj.sangsoft.retrofit_provider.RetrofitApiClient;
import com.natraj.sangsoft.retrofit_provider.RetrofitService;
import com.natraj.sangsoft.retrofit_provider.WebResponse;
import com.natraj.sangsoft.ui.LoginActivity;
import com.natraj.sangsoft.utils.AppPreference;
import com.natraj.sangsoft.utils.BaseActivity;
import com.quenDel.multiUtils.neworks.NetworkHandler;
import com.quenDel.multiUtils.utils.AppAlerts;

import java.util.List;

import retrofit2.Response;

public class IndentProgressActivity extends BaseActivity implements View.OnClickListener {

    private int currentIndent = 0;
    private IndentOverviewActionBar overviewActionBar;
    private String lang, mchId, userId;
    private String order_id = "";
    private Toolbar toolbar;
    private RecyclerView recyclerIndentProgress;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indent_progress);
        mContext = this;
        initToolbar();
    }

    private void initToolbar() {
        getIds();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setVisibility(View.VISIBLE);
        if (getSupportActionBar() == null) {
            return;
        }
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (!AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR).isEmpty()) {
            ((TextView) toolbar.findViewById(R.id.txtMachine))
                    .setText(AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR));
        }

        ((ImageView) findViewById(R.id.imgLeft)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.imgRight)).setOnClickListener(this);
        recyclerIndentProgress = (RecyclerView) findViewById(R.id.recyclerIndentProgress);
        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.imgRefresh)).setOnClickListener(this);
        setToolbarBarLabel();
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Get language, mch id, user id, indent id
    private void getIds() {
        lang = AppPreference.getStringPreference(mContext, Constant.LANGUAGE);
        mchId = AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID);
        userId = AppPreference.getStringPreference(mContext, Constant.USERNAME);
        order_id = AppPreference.getStringPreference(mContext, Constant.INDENT_ID);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Toolbar bar label
    private void setToolbarBarLabel() {
        String headerInfo = "headerInfo";
        String userId = AppPreference.getStringPreference(mContext, Constant.USERNAME);
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getCommonDataLbl(new Dialog(mContext), retrofitApiClient.commonDataItems(
                    headerInfo, userId, lang), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<CommonDataModal> response = (Response<CommonDataModal>) result;
                    CommonDataModal dataModal = response.body();
                    if (dataModal.getIsError().equals(0)) {
                        ((TextView) toolbar.findViewById(R.id.prod_tooltip)).setText(dataModal.getStrings().getProdTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_maintenance)).setText(dataModal.getStrings().getMaintTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_qa)).setText(dataModal.getStrings().getQATooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_help)).setText(dataModal.getStrings().getHelpTooltip());
                        ((TextView) toolbar.findViewById(R.id.txtEmpIdToolbar)).setText(dataModal.getJsonData().getUserName());
                        setIndentActionBarData();
                    } else {
                        AppAlerts.show(mContext, dataModal.getErrorMsg());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Indent Action Bar Data
    private void setIndentActionBarData() {
        String op = "orderActionBar";
        RetrofitService.getIndentActionBarResponce(new Dialog(mContext), retrofitApiClient.indentActionbar(mchId, op, userId, lang),
                new WebResponse() {
                    @Override
                    public void onResponseSuccess(Response<?> result) {
                        Response<IndentOverviewActionBar> response = (Response<IndentOverviewActionBar>) result;
                        overviewActionBar = response.body();
                        if (overviewActionBar.getIsError() == 0) {
                            ((TextView) findViewById(R.id.lbl_overview)).setText(overviewActionBar.getStrings().getBtnOverview());
                            ((TextView) findViewById(R.id.lbl_action)).setText(overviewActionBar.getStrings().getLblRecord());
                            ((TextView) findViewById(R.id.lbl_pause)).setText(overviewActionBar.getStrings().getBtnPause());
                            ((TextView) findViewById(R.id.lbl_close)).setText(overviewActionBar.getStrings().getTooltipClose());
                            ((TextView) findViewById(R.id.lbl_progress)).setText(overviewActionBar.getStrings().getBtnProgress());
                            setIndentValue();
                        }
                    }

                    @Override
                    public void onResponseFailed(String error) {
                        AppAlerts.show(mContext, error);
                    }
                });
    }

    private void setIndentValue() {
        if (overviewActionBar.getJsonData() == null)
            return;
        if (overviewActionBar.getJsonData().getOrderCount() == null)
            return;
        Order overviewActionBarIndent = overviewActionBar.getJsonData().getOrders().get(currentIndent);
        if (overviewActionBarIndent.getOrderID() != null)
            order_id = overviewActionBarIndent.getOrderID();
        ((TextView) findViewById(R.id.indent_name)).setText(String.format("%s (%s)", overviewActionBarIndent.getIndent(), order_id));
        ((TextView) findViewById(R.id.indent_status)).setText(overviewActionBarIndent.getOrderStatus());
        order_id = overviewActionBarIndent.getOrderID();
        AppPreference.setStringPreference(mContext, Constant.INDENT_ID, order_id);
        setInfoBarInfo(order_id);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Indent Info Bar Label
    private void setInfoBarInfo(String strOrderId) {
        String op = "orderInfoBar";
        RetrofitService.getIndentInfoBarResponce(new Dialog(mContext), retrofitApiClient.indentInfobar(mchId, op, userId, lang, strOrderId), new WebResponse() {
            @Override
            public void onResponseSuccess(Response<?> result) {
                Response<IndentInfoBarModal> response = (Response<IndentInfoBarModal>) result;
                IndentInfoBarModal infoBarModal = response.body();
                if (infoBarModal.getIsError().equals("0")) {
                    ((TextView) findViewById(R.id.indent_no)).setText(infoBarModal.getStrings().getLblIndentNo());
                    ((TextView) findViewById(R.id.indent_timing)).setText(infoBarModal.getStrings().getLblTiming());
                    ((TextView) findViewById(R.id.indent_foritem)).setText(infoBarModal.getStrings().getLblForItem());
                    ((TextView) findViewById(R.id.indent_qtyib)).setText(infoBarModal.getStrings().getLblQty());

                    ((TextView) findViewById(R.id.txtIndentId)).setText(infoBarModal.getJsonData().getIndentRef());
                    ((TextView) findViewById(R.id.txtIndentTiming)).setText(infoBarModal.getJsonData().getTiming());
                    ((TextView) findViewById(R.id.txtIndentForItem)).setText(infoBarModal.getJsonData().getItemName());
                    ((TextView) findViewById(R.id.txtIndentQuantityValue)).setText
                            (infoBarModal.getJsonData().getProducedQty() + " of " + infoBarModal.getJsonData().getOrderQty());
                    setIndentDataList();
                }
            }

            @Override
            public void onResponseFailed(String error) {
                AppAlerts.show(mContext, error);
            }
        });
    }

    private void setIndentDataList() {
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getIndentDataList(new Dialog(mContext), retrofitApiClient.indentProgressDataList
                            ("IndentActivity"),new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<IndentProgressItemsModal> response = (Response<IndentProgressItemsModal>) result;
                    IndentProgressItemsModal indentProgressItemsModal = response.body();
                    ((TextView) findViewById(R.id.area_title)).setText(indentProgressItemsModal.getStrings().getAreaTitle());
                    setItemList(indentProgressItemsModal.getIndentActivity());
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    private void setItemList(List<IndentActivity> indentItemses) {
        recyclerIndentProgress.setHasFixedSize(true);
        recyclerIndentProgress.setLayoutManager(new LinearLayoutManager(mContext));
        IndentProgressItemsAdapter adapter = new IndentProgressItemsAdapter(mContext, indentItemses, this);
        recyclerIndentProgress.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgRefresh:
                setIndentDataList();
                break;
            case R.id.linearLogout:
                logout();
                break;
            case R.id.imgLeft:
                if (currentIndent > 0) {
                    currentIndent--;
                    setIndentValue();
                } else {
                    AppAlerts.show(mContext, "This is First Order ");
                }
                break;
            case R.id.imgRight:
                if (overviewActionBar.getJsonData().getOrderCount() == null)
                    return;
                if (currentIndent < (overviewActionBar.getJsonData().getOrderCount()) - 1) {
                    currentIndent++;
                    setIndentValue();
                } else {
                    AppAlerts.show(mContext, "This is Last Order ");
                }
                break;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Log out
    private void logout() {
        AppAlerts.showAlertWithAction(mContext, "Logout", "Are you sure want to Logout?",
                "YES", "NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        clearAllPreferences();
                    }
                }, true, true);
    }

    private void clearAllPreferences() {
        AppPreference.clearAllPreferences(mContext);
        Intent intent = new Intent(mContext, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }
}
