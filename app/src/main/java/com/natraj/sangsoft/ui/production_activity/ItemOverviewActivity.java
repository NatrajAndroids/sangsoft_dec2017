package com.natraj.sangsoft.ui.production_activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.adapter.IndentItemListAdapter;
import com.natraj.sangsoft.adapter.OverviewMaterialListAdapter;
import com.natraj.sangsoft.adapter.OverviewPossibleMachinesListAdapter;
import com.natraj.sangsoft.adapter.ProcessSpinnerAdapter;
import com.natraj.sangsoft.constant.Constant;
import com.natraj.sangsoft.model.production_modal.alert_dialog_label_modal.AlertDialogLabelModal;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_header_info.CommonDataModal;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_indent_item_list.OverviewIndentItem;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_indent_item_list.OverviewIndentItemListModal;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_bar.OverviewItemBarModal;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_info.OverviewItemInfoModal;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_info.OverviewItemInfoProcess;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_overview.OverviewItem;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_overview.OverviewItemModal;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_overview.OverviewMachine;
import com.natraj.sangsoft.retrofit_provider.RetrofitService;
import com.natraj.sangsoft.retrofit_provider.WebResponse;
import com.natraj.sangsoft.ui.LoginActivity;
import com.natraj.sangsoft.ui.maintenance_activity.MaintListActivity;
import com.natraj.sangsoft.utils.AppPreference;
import com.natraj.sangsoft.utils.BaseActivity;
import com.quenDel.multiUtils.neworks.NetworkHandler;
import com.quenDel.multiUtils.utils.AppAlerts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class ItemOverviewActivity extends BaseActivity implements View.OnClickListener {

    private List<OverviewMachine> overviewMachineList = new ArrayList<>();
    private List<OverviewItem> overviewItemsList = new ArrayList<>();
    String process_id = "DEFAULT";
    String item_id = "567";
    private String lang;
    private String mchId;
    private String userId;
    private OverviewIndentItemListModal detailsModal;
    private OverviewItemModal itemModal;
    private Toolbar toolbar;
    private EditText edtItemId;
    private RecyclerView recyclerView, machinesRecyclerView, recyclerViewIndentItem;
    private Spinner spinnerProcess;
    private Context mContext;
    private Dialog dialog_indent_item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_overview);
        mContext = this;
        init();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setVisibility(View.VISIBLE);
        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setOnClickListener(this);
        spinnerProcess = (Spinner) findViewById(R.id.spinnerProcess);
        recyclerView = (RecyclerView) findViewById(R.id.overviewRecyclerView);
        machinesRecyclerView = (RecyclerView) findViewById(R.id.machinesRecyclerView);
        ((ImageView) findViewById(R.id.btnGo)).setOnClickListener(this);
        ((LinearLayout) toolbar.findViewById(R.id.linear_main)).setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.relativeIndentItemList)).setOnClickListener(this);

        ((LinearLayout) findViewById(R.id.linearOverview)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearInstruction)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearImgVdo)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearOthers)).setOnClickListener(this);

        if (!AppPreference.getStringPreference(mContext, Constant.ITEM_ID).isEmpty()) {
            item_id = AppPreference.getStringPreference(mContext, Constant.ITEM_ID);
        }
        AppPreference.setStringPreference(mContext, Constant.ITEM_ID, item_id);
        getIds();
        setToolbarBarLabel();
        setItemBarLabel(item_id);
        setItemInfoAndProcess(process_id, item_id);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Get language, mch id, user id, indent id
    private void getIds() {
        lang = AppPreference.getStringPreference(mContext, Constant.LANGUAGE);
        mchId = AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID);
        userId = AppPreference.getStringPreference(mContext, Constant.USERNAME);
        if (!AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR).isEmpty()) {
            ((TextView) toolbar.findViewById(R.id.txtMachine))
                    .setText(AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR));
        }
        if (AppPreference.getStringPreference(mContext, Constant.ITEM_ID) != null) {
            item_id = AppPreference.getStringPreference(mContext, Constant.ITEM_ID);
        } else {
            item_id = "567";
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Toolbar bar label
    private void setToolbarBarLabel() {
        String headerInfo = "headerInfo";
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getCommonDataLbl(new Dialog(mContext), retrofitApiClient.commonDataItems(
                    headerInfo, userId, lang), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<CommonDataModal> response = (Response<CommonDataModal>) result;
                    CommonDataModal dataModal = response.body();
                    if (dataModal.getIsError().equals(0)) {
                        ((TextView) toolbar.findViewById(R.id.prod_tooltip)).setText(dataModal.getStrings().getProdTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_maintenance)).setText(dataModal.getStrings().getMaintTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_qa)).setText(dataModal.getStrings().getQATooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_help)).setText(dataModal.getStrings().getHelpTooltip());
                        ((TextView) toolbar.findViewById(R.id.txtEmpIdToolbar)).setText(dataModal.getJsonData().getUserName());
                    } else {
                        AppAlerts.show(mContext, "error 1");
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set ItemBarLabel and item name and id
    private void setItemBarLabel(String itemId) {
        String operation = "itemBar";
        Log.e("Item_Bar_Data", mchId + " ," + lang + " ," + itemId);
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getItemOverviewActionBarData(new Dialog(mContext), retrofitApiClient.itemOverviewActionBarData(
                    operation, mchId, lang, itemId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<OverviewItemBarModal> response = (Response<OverviewItemBarModal>) result;
                    OverviewItemBarModal itemBarModal = response.body();
                    if (itemBarModal.getIsError().equals(0)) {
                        ((TextView) findViewById(R.id.txt_lbl_overview)).setText(itemBarModal.getStrings().getBtnOverview());
                        ((TextView) findViewById(R.id.txt_lbl_instructions)).setText(itemBarModal.getStrings().getBtnInstruction());
                        ((TextView) findViewById(R.id.txt_lbl_img_vdo)).setText(itemBarModal.getStrings().getBtnImgVideo());
                        ((TextView) findViewById(R.id.txt_lbl_others)).setText(itemBarModal.getStrings().getBtnOthers());
                        ((TextView) findViewById(R.id.txt_indent_item_name)).setText(itemBarModal.getJsonData().getItemMcode() + " - " + itemBarModal.getJsonData().getItemName());
                    } else {
                        AppAlerts.show(mContext, itemBarModal.getErrorMsg());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set OverviewItem info and Process spinner
    private void setItemInfoAndProcess(String processId, String itemId) {
        String operation = "itemInfo";
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getItemOverviewItemInfo(new Dialog(mContext), retrofitApiClient.itemOverviewItemInfo(
                    operation, mchId, lang, itemId, processId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<OverviewItemInfoModal> response = (Response<OverviewItemInfoModal>) result;
                    OverviewItemInfoModal itemInfoModal = response.body();
                    if (itemInfoModal.getIsError().equals(0)) {
                        ((TextView) findViewById(R.id.txtErpCode)).setText(itemInfoModal.getStrings().getLblERPcode());
                        ((TextView) findViewById(R.id.txtErpCodeValue)).setText(itemInfoModal.getJsonData().getItemRef01());
                        ((TextView) findViewById(R.id.txtLblProcess)).setText(itemInfoModal.getStrings().getLblProcess());

                        if (itemInfoModal.getJsonData().getProcesses().size() == 0) {
                            overviewItemsList.clear();
                            overviewMachineList.clear();
                            setMaterialList();
                            setMachineList();
                        }

                        setSpinnerData(itemInfoModal.getJsonData().getProcesses());
                        if (AppPreference.getIntegerPositionPreference(mContext, Constant.SELECTED_PROCESS_POSITION) > 0) {
                            spinnerProcess.setSelection(AppPreference.getIntegerPositionPreference(mContext, Constant.SELECTED_PROCESS_POSITION));
                        }
                    } else {
                        ((TextView) findViewById(R.id.txtErpCode)).setText(itemInfoModal.getStrings().getLblERPcode());
                        ((TextView) findViewById(R.id.txtErpCodeValue)).setText(itemInfoModal.getJsonData().getItemRef01());
                        ((TextView) findViewById(R.id.txtLblProcess)).setText(itemInfoModal.getStrings().getLblProcess());
                        AppAlerts.show(mContext, itemInfoModal.getErrorMsg());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    private void setSpinnerData(final List<OverviewItemInfoProcess> spinnerData) {
        String[] items = new String[spinnerData.size()];
        for (int i = 0; i < spinnerData.size(); i++) {
            items[i] = spinnerData.get(i).getProcessName();
        }
        ProcessSpinnerAdapter adapter = new ProcessSpinnerAdapter(mContext, R.layout.spinner_custom_layout, spinnerData);
        spinnerProcess.setAdapter(adapter);
        spinnerProcess.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                process_id = spinnerData.get(i).getProcessId();
                setAllRemainData(process_id, item_id);
                AppPreference.setIntegerPositionPreference(mContext, Constant.SELECTED_PROCESS_POSITION, i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set All remaining data
    private void setAllRemainData(String processId, String itemId) {
        String operation = "itemOverview";
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getItemOverviewAllData(new Dialog(mContext), retrofitApiClient.itemOverviewAllData(
                    operation, mchId, lang, itemId, processId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<OverviewItemModal> response = (Response<OverviewItemModal>) result;
                    itemModal = response.body();
                    if (itemModal.getIsError().equals(0)) {
                        ((TextView) findViewById(R.id.txt_area_title)).setText(itemModal.getStrings().getAreaTitle());
                        ((TextView) findViewById(R.id.txtLblQuantity)).setText(itemModal.getStrings().getLblQuantity());
                        ((TextView) findViewById(R.id.txtMaterialDetails)).setText(itemModal.getStrings().getLblMatDetails());
                        ((TextView) findViewById(R.id.txtPossibleMch)).setText(itemModal.getStrings().getLblMachines());
                        ((TextView) findViewById(R.id.txtLblType)).setText(itemModal.getStrings().getCol01());
                        ((TextView) findViewById(R.id.txtLblItmCode)).setText(itemModal.getStrings().getCol02());
                        ((TextView) findViewById(R.id.txtLblDescription2)).setText(itemModal.getStrings().getCol03());
                        ((TextView) findViewById(R.id.txtLblExpected)).setText(itemModal.getStrings().getCol04());
                        ((TextView) findViewById(R.id.txtLblUom)).setText(itemModal.getStrings().getCol05());
                        overviewItemsList.clear();
                        overviewItemsList.addAll(itemModal.getJsonData().getItems());

                        overviewMachineList.clear();
                        overviewMachineList.addAll(itemModal.getJsonData().getMachines());
                        setMaterialList();
                        setMachineList();
                    } else {
                        AppAlerts.show(mContext, itemModal.getErrorMsg());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    private void setMaterialList() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        OverviewMaterialListAdapter materialListAdapter = new OverviewMaterialListAdapter(mContext, overviewItemsList, this);
        recyclerView.setAdapter(materialListAdapter);
        materialListAdapter.notifyDataSetChanged();
    }

    private void setMachineList() {
        machinesRecyclerView.setHasFixedSize(true);
        machinesRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        OverviewPossibleMachinesListAdapter machineListAdapter = new OverviewPossibleMachinesListAdapter(mContext, overviewMachineList, this);
        machinesRecyclerView.setAdapter(machineListAdapter);
        machineListAdapter.notifyDataSetChanged();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Set Order item list in dialog
    private void itemListDialog() {
        dialog_indent_item = new Dialog(this);
        dialog_indent_item.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_indent_item.setContentView(R.layout.dialog_indent_item_list);
        dialog_indent_item.setCanceledOnTouchOutside(false);
        dialog_indent_item.setCancelable(true);
        if (dialog_indent_item.getWindow() != null)
            dialog_indent_item.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        recyclerViewIndentItem = (RecyclerView) dialog_indent_item.findViewById(R.id.recyclerViewIndentItem);
        edtItemId = (EditText) dialog_indent_item.findViewById(R.id.edtItemId);
        ((TextView) dialog_indent_item.findViewById(R.id.btnSendId)).setOnClickListener(this);
        dialog_indent_item.show();
        Window window = dialog_indent_item.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        setDialogueLabel();
    }

    private void setDialogueLabel() {
        String op = "itemSearch";
        if (networkHandler.isNetworkAvailable()) {
            RequestBody _mch_id = RequestBody.create(MediaType.parse("text/plain"), mchId);
            RequestBody _op = RequestBody.create(MediaType.parse("text/plain"), op);
            RequestBody _user_id = RequestBody.create(MediaType.parse("text/plain"), userId);
            RequestBody _lang = RequestBody.create(MediaType.parse("text/plain"), lang);
            Map<String, Object> jsonObject = new HashMap<>();
            jsonObject.put("item_mcode", "Default");
            RetrofitService.getAlertDialogLabel(new Dialog(mContext), retrofitApiClient.alertDialogLabel(
                    _mch_id, _op, _user_id, _lang, jsonObject), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<AlertDialogLabelModal> response = (Response<AlertDialogLabelModal>) result;
                    AlertDialogLabelModal labelModal = response.body();
                    ((TextView) dialog_indent_item.findViewById(R.id.btnSendId)).setHint(labelModal.getStrings().getBtnGo());
                    ((TextView) dialog_indent_item.findViewById(R.id.btnSendId)).setTextColor(getResources().getColor(R.color.white));
                    ((EditText) dialog_indent_item.findViewById(R.id.edtItemId)).setHint(labelModal.getStrings().getTitleSearchItem());
                    ((TextView) dialog_indent_item.findViewById(R.id.txtMcode)).setText(labelModal.getStrings().getLblMCode());
                    ((TextView) dialog_indent_item.findViewById(R.id.txtLblA)).setText(labelModal.getStrings().getCol01());
                    ((TextView) dialog_indent_item.findViewById(R.id.txtLblB)).setText(labelModal.getStrings().getCol02());
                    ((TextView) dialog_indent_item.findViewById(R.id.txtLblC)).setText(labelModal.getStrings().getCol03());
                }

                @Override
                public void onResponseFailed(String error) {
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    private void setIndentItemList() {
        String item_id = edtItemId.getText().toString();
        String op = "itemSearch";

        if (networkHandler.isNetworkAvailable()) {

            RequestBody _mch_id = RequestBody.create(MediaType.parse("text/plain"), mchId);
            RequestBody _op = RequestBody.create(MediaType.parse("text/plain"), op);
            RequestBody _user_id = RequestBody.create(MediaType.parse("text/plain"), userId);
            RequestBody _lang = RequestBody.create(MediaType.parse("text/plain"), lang);

            Map<String, Object> jsonObject = new HashMap<>();
            jsonObject.put("item_mcode", item_id);

            Log.e("Json data", ": " + jsonObject);

            RetrofitService.getIndentItemListDialog(new Dialog(mContext), retrofitApiClient.indentItemListDialog(
                    _mch_id, _op, _user_id, _lang, jsonObject), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<OverviewIndentItemListModal> response = (Response<OverviewIndentItemListModal>) result;
                    detailsModal = response.body();
                    setIndentItemList(detailsModal.getJsonData().getItems());
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    private void setIndentItemList(List<OverviewIndentItem> overviewItemsList) {
        recyclerViewIndentItem.setHasFixedSize(true);
        recyclerViewIndentItem.setLayoutManager(new LinearLayoutManager(mContext));
        IndentItemListAdapter indentListAdapter = new IndentItemListAdapter(mContext, overviewItemsList, this);
        recyclerViewIndentItem.setAdapter(indentListAdapter);
        indentListAdapter.notifyDataSetChanged();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Log out
    private void logout() {
        AppAlerts.showAlertWithAction(mContext, "Logout", "Are you sure want to Logout?",
                "YES", "NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        clearAllPreferences();
                    }
                }, true,true);
    }

    private void clearAllPreferences() {
        AppPreference.clearAllPreferences(mContext);
        Intent intent = new Intent(mContext, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    private void goMainActivity() {
        Intent intent = new Intent(mContext, MaintListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnGo:
            case R.id.relativeIndentItemList:
                AppPreference.clearAllPosition(mContext);
                itemListDialog();
                break;
            case R.id.btnSendId:
                setIndentItemList();
                break;
            case R.id.txtIndentItemId:
            case R.id.txtIndentItemDesc:
            case R.id.btnIndentItemSelect:
                onAlertClickItem(view);
                break;
            case R.id.txtItemcodeValue:
            case R.id.txtDescriptionValue:
                onClickItem(view);
                break;
            case R.id.linearLogout:
                logout();
                break;
            case R.id.linearOverview:
                ((ImageView) findViewById(R.id.ind_overview)).setImageResource(R.drawable.magnify_color);
                ((ImageView) findViewById(R.id.imgInstructions)).setImageResource(R.drawable.ic_instructions);
                ((ImageView) findViewById(R.id.imgImgVdo)).setImageResource(R.drawable.ic_gallery);
                ((ImageView) findViewById(R.id.imgOthers)).setImageResource(R.drawable.ic_others);
                String itemId = AppPreference.getStringPreference(mContext, Constant.ITEM_ID);
                setItemBarLabel(itemId);
                setAllRemainData(process_id, itemId);
                setItemInfoAndProcess(process_id, itemId);
                //goMainActivity();
                break;
            case R.id.linearInstruction:
                startActivity(new Intent(mContext, ItemInstructionsActivity.class));
                break;
            case R.id.linearImgVdo:
                startActivity(new Intent(mContext, ItemImageActivity.class));
                break;
            case R.id.linearOthers:
                startActivity(new Intent(mContext, ItemOthersActivity.class));
                break;
            case R.id.linear_main:
                goMainActivity();
                break;
        }
    }

    private void onAlertClickItem(View view) {
        int i = Integer.parseInt(view.getTag().toString());
        ((TextView) findViewById(R.id.txt_indent_item_name)).setText(
                detailsModal.getJsonData().getItems().get(i).getItemMcode() + " - " +
                        detailsModal.getJsonData().getItems().get(i).getItemName());
        setItemBarLabel(detailsModal.getJsonData().getItems().get(i).getItemId());
        setAllRemainData(process_id, detailsModal.getJsonData().getItems().get(i).getItemId());
        setItemInfoAndProcess(process_id, detailsModal.getJsonData().getItems().get(i).getItemId());
        AppPreference.setStringPreference(mContext, Constant.ITEM_ID, detailsModal.getJsonData().getItems().get(i).getItemId());
        dialog_indent_item.dismiss();
    }

    private void onClickItem(View view) {
        ((ImageView) toolbar.findViewById(R.id.imgReading)).setImageResource(R.drawable.image_back);
        int i = Integer.parseInt(view.getTag().toString());
        ((TextView) findViewById(R.id.txt_indent_item_name))
                .setText(itemModal.getJsonData().getItems().get(i).getItemMcode()
                        + " - " + itemModal.getJsonData().getItems().get(i).getItemName());
        setItemBarLabel(itemModal.getJsonData().getItems().get(i).getItemId());
        setAllRemainData(process_id, itemModal.getJsonData().getItems().get(i).getItemId());
        setItemInfoAndProcess(process_id, itemModal.getJsonData().getItems().get(i).getItemId());
        AppPreference.setStringPreference(mContext, Constant.ITEM_ID, itemModal.getJsonData().getItems().get(i).getItemId());
    }
}