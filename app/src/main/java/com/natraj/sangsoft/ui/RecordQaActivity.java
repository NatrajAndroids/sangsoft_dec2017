package com.natraj.sangsoft.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.adapter.QuestionAdapter;
import com.natraj.sangsoft.constant.Constant;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_info_bar_modal.IndentInfoBarModal;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_actionbar.IndentOverviewActionBar;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_actionbar.Order;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_header_info.CommonDataModal;
import com.natraj.sangsoft.model.record_qa_modal.load_form.RecordQaModal;
import com.natraj.sangsoft.model.record_qa_modal.load_form.RequestQaList;
import com.natraj.sangsoft.model.record_qa_modal.load_form.RequestQaResult;
import com.natraj.sangsoft.model.record_qa_modal.question_set.LookupSet;
import com.natraj.sangsoft.model.record_qa_modal.question_set.QuestionModal;
import com.natraj.sangsoft.model.record_qa_modal.question_set.QuestionSet;
import com.natraj.sangsoft.retrofit_provider.RetrofitService;
import com.natraj.sangsoft.retrofit_provider.WebResponse;
import com.natraj.sangsoft.ui.maintenance_activity.MaintListActivity;
import com.natraj.sangsoft.ui.production_activity.ItemOverviewActivity;
import com.natraj.sangsoft.utils.AppPreference;
import com.natraj.sangsoft.utils.BaseActivity;
import com.quenDel.multiUtils.neworks.NetworkHandler;
import com.quenDel.multiUtils.utils.AppAlerts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class RecordQaActivity extends BaseActivity implements View.OnClickListener {

    private List<LookupSet> lookupSetList = new ArrayList<>();
    private List<QuestionSet> questionSetList = new ArrayList<>();
    private int currentIndent = 0;
    public static IndentOverviewActionBar overviewActionBar;
    private String lang, mchId, userId;
    private String order_id = "";
    private String strRequestFor = "";
    private String strResult = "";
    private Toolbar toolbar;
    private Spinner spinnerRequestFor, spinnerResultStatus;
    private RecyclerView recyclerQuestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_qa);

        initToolbar();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setVisibility(View.VISIBLE);
        ((ImageView) toolbar.findViewById(R.id.imgDrill)).setOnClickListener(this);
        if (getSupportActionBar() == null) {
            return;
        }
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        networkHandler = new NetworkHandler(RecordQaActivity.this);
        spinnerResultStatus = (Spinner) findViewById(R.id.spinnerResultStatus);
        spinnerRequestFor = (Spinner) findViewById(R.id.spinnerRequestFor);
        //spinnerRequestFor.setEnabled(false);
        //spinnerResultStatus.setEnabled(false);

        recyclerQuestion = (RecyclerView) findViewById(R.id.recyclerQuestion);
        recyclerQuestion.setHasFixedSize(true);
        recyclerQuestion.setLayoutManager(new LinearLayoutManager(mContext));

        ((Button) findViewById(R.id.btnSave)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearForItem)).setOnClickListener(this);
        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.linearIndentOverview)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.imgLeft)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.imgRight)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnCancel)).setOnClickListener(this);
        getIds();
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Get language, mch id, user id, indent id
    private void getIds() {
        lang = AppPreference.getStringPreference(mContext, Constant.LANGUAGE);
        mchId = AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID);
        userId = AppPreference.getStringPreference(mContext, Constant.USERNAME);
        order_id = AppPreference.getStringPreference(mContext, Constant.INDENT_ID);
        if (!AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR).isEmpty()) {
            ((TextView) toolbar.findViewById(R.id.txtMachine))
                    .setText(AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR));
        }
        setToolbarBarLabel();
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Toolbar bar label
    private void setToolbarBarLabel() {
        String headerInfo = "headerInfo";
        String userId = AppPreference.getStringPreference(mContext, Constant.USERNAME);
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getCommonDataLbl(new Dialog(mContext), retrofitApiClient.commonDataItems(
                    headerInfo, userId, lang), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<CommonDataModal> response = (Response<CommonDataModal>) result;
                    CommonDataModal dataModal = response.body();
                    if (dataModal.getIsError().equals(0)) {
                        ((TextView) toolbar.findViewById(R.id.prod_tooltip)).setText(dataModal.getStrings().getProdTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_maintenance)).setText(dataModal.getStrings().getMaintTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_qa)).setText(dataModal.getStrings().getQATooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_help)).setText(dataModal.getStrings().getHelpTooltip());
                        ((TextView) toolbar.findViewById(R.id.txtEmpIdToolbar)).setText(dataModal.getJsonData().getUserName());
                        setIndentActionBarData();
                    } else {
                        AppAlerts.show(mContext, dataModal.getErrorMsg());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Indent Action Bar Data
    private void setIndentActionBarData() {
        String op = "orderActionBar";
        RetrofitService.getIndentActionBarResponce(new Dialog(mContext), retrofitApiClient.indentActionbar(mchId, op, userId, lang),
                new WebResponse() {
                    @Override
                    public void onResponseSuccess(Response<?> result) {
                        Response<IndentOverviewActionBar> response = (Response<IndentOverviewActionBar>) result;
                        overviewActionBar = response.body();
                        if (overviewActionBar.getIsError() == 0) {
                            ((TextView) findViewById(R.id.lbl_overview)).setText(overviewActionBar.getStrings().getBtnOverview());
                            ((TextView) findViewById(R.id.lbl_action)).setText(overviewActionBar.getStrings().getLblRecord());
                            ((TextView) findViewById(R.id.lbl_pause)).setText(overviewActionBar.getStrings().getBtnPause());
                            ((TextView) findViewById(R.id.lbl_close)).setText(overviewActionBar.getStrings().getTooltipClose());
                            ((TextView) findViewById(R.id.lbl_progress)).setText(overviewActionBar.getStrings().getBtnProgress());
                            setIndentValue();
                        }
                    }

                    @Override
                    public void onResponseFailed(String error) {
                        AppAlerts.show(mContext, error);
                    }
                });
    }

    private void setIndentValue() {
        if (overviewActionBar.getJsonData() == null)
            return;
        if (overviewActionBar.getJsonData().getOrderCount() == null)
            return;
        Order overviewActionBarIndent = overviewActionBar.getJsonData().getOrders().get(currentIndent);
        if (overviewActionBarIndent.getOrderID() != null)
            order_id = overviewActionBarIndent.getOrderID();
        ((TextView) findViewById(R.id.indent_name)).setText(overviewActionBarIndent.getIndent() + " " + "(" + order_id + ")");
        ((TextView) findViewById(R.id.indent_status)).setText(overviewActionBarIndent.getOrderStatus());
        AppPreference.setStringPreference(mContext, Constant.INDENT_ID, order_id);
        //setWorkAreaInfo(overviewActionBarIndent.getOrderID());
        setInfoBarInfo(order_id);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Indent Info Bar Label
    private void setInfoBarInfo(String strOrderID) {
        String op = "orderInfoBar";
        RetrofitService.getIndentInfoBarResponce(new Dialog(mContext), retrofitApiClient.indentInfobar(mchId, op, userId, lang, strOrderID), new WebResponse() {
            @Override
            public void onResponseSuccess(Response<?> result) {
                Response<IndentInfoBarModal> response = (Response<IndentInfoBarModal>) result;
                IndentInfoBarModal infoBarModal = response.body();
                if (infoBarModal.getIsError().equals("0")) {
                    ((TextView) findViewById(R.id.indent_no)).setText(infoBarModal.getStrings().getLblIndentNo());
                    ((TextView) findViewById(R.id.indent_timing)).setText(infoBarModal.getStrings().getLblTiming());
                    ((TextView) findViewById(R.id.indent_foritem)).setText(infoBarModal.getStrings().getLblForItem());
                    ((TextView) findViewById(R.id.indent_qtyib)).setText(infoBarModal.getStrings().getLblQty());

                    ((TextView) findViewById(R.id.txtIndentId)).setText(infoBarModal.getJsonData().getIndentRef());
                    ((TextView) findViewById(R.id.txtIndentTiming)).setText(infoBarModal.getJsonData().getTiming());
                    ((TextView) findViewById(R.id.txtIndentForItem)).setText(infoBarModal.getJsonData().getItemName());
                    ((TextView) findViewById(R.id.txtIndentQuantityValue)).setText
                            (String.format("%s of %s", infoBarModal.getJsonData().getProducedQty(), infoBarModal.getJsonData().getOrderQty()));
                    setRequestQaData();
                }
            }

            @Override
            public void onResponseFailed(String error) {
                AppAlerts.show(mContext, error);
            }
        });
    }

    /*
     * Load form
     * */
    private void setRequestQaData() {
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getRecordQAData(new Dialog(mContext), retrofitApiClient.recordQaData(
                    mchId, userId, "loadForm", order_id, "", lang), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<RecordQaModal> response = (Response<RecordQaModal>) result;
                    RecordQaModal recordQaModal = response.body();

                    /*((Button) findViewById(R.id.btnSave)).setText(recordQaModal.getStrings().getBtnSave());
                    ((Button) findViewById(R.id.btnCancel)).setText(recordQaModal.getStrings().getBtnCancel());
                    ((TextView) findViewById(R.id.area_title)).setText(recordQaModal.getStrings().getAreaTitle());
                    ((TextView) findViewById(R.id.txtRequest)).setText(recordQaModal.getStrings().getLblRequestFor());
                    ((TextView) findViewById(R.id.txtComment)).setText(recordQaModal.getStrings().getLblComment());
                    ((TextView) findViewById(R.id.txtStatus)).setText(recordQaModal.getStrings().getLblStatus());
                    ((TextView) findViewById(R.id.txtStatus)).setText(recordQaModal.getStrings().getLblStatus());*/

                    if (recordQaModal.getJsonData() != null)
                        setSpinnerRequestFor(recordQaModal.getJsonData().getRequestList());
                    setSpinnerResult(recordQaModal.getQaResult());
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        }
    }

    private void setSpinnerRequestFor(final List<RequestQaList> spinnerData) {
        String[] items = new String[spinnerData.size()];
        for (int i = 0; i < spinnerData.size(); i++) {
            items[i] = spinnerData.get(i).getDisplayName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.spinner_language, items);
        spinnerRequestFor.setAdapter(adapter);
        spinnerRequestFor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strRequestFor = spinnerData.get(position).getQaID();
                if (strRequestFor != null) {
                    setRecordQaData(strRequestFor);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setSpinnerResult(final List<RequestQaResult> spinnerData) {
        String[] items = new String[spinnerData.size()];
        for (int i = 0; i < spinnerData.size(); i++) {
            items[i] = spinnerData.get(i).getDisplayName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.spinner_language, items);
        spinnerResultStatus.setAdapter(adapter);
        spinnerResultStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strResult = spinnerData.get(position).getDataKey();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /*
     * Get Question Set
     * */
    private void setRecordQaData(String strQaId) {
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getRequestQuesData(new Dialog(mContext), retrofitApiClient.recordQuesData(
                    mchId, userId, "getQuestionSet", "", strQaId, lang), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<QuestionModal> response = (Response<QuestionModal>) result;
                    QuestionModal questionModal = response.body();
                    lookupSetList.clear();
                    questionSetList.clear();
                    questionSetList.addAll(questionModal.getQuestionSet());
                    lookupSetList.addAll(questionModal.getLookupSet());
                    setItemList();
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        }
    }

    private void setItemList() {
        QuestionAdapter adapter = new QuestionAdapter(mContext, questionSetList, lookupSetList);
        recyclerQuestion.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linearLogout:
                logout();
                break;
            case R.id.btnSave:
                tableJson();
                break;
            case R.id.btnCancel:
                finish();
                break;
            case R.id.linearForItem:
                startActivity(new Intent(mContext, ItemOverviewActivity.class));
                break;
            case R.id.linearIndentOverview:
                finish();
                break;
            case R.id.imgDrill:
                goMaintListActivity();
                break;
            case R.id.imgLeft:
                if (currentIndent > 0) {
                    currentIndent--;
                    setIndentValue();
                } else {
                    AppAlerts.show(mContext, "This is First Order ");
                }
                break;
            case R.id.imgRight:
                if (overviewActionBar.getJsonData().getOrderCount() == null)
                    return;
                if (currentIndent < (overviewActionBar.getJsonData().getOrderCount()) - 1) {
                    currentIndent++;
                    setIndentValue();
                } else {
                    AppAlerts.show(mContext, "This is Last Order ");
                }
                break;
        }
    }

    /*------------------------------------------------------------------------------------------------------------------------*/

    private void tableJson() {
        JSONObject mainObject = new JSONObject();

        try {
            mainObject.put("machine_id", mchId);
            mainObject.put("op", "recordQA");
            mainObject.put("user_id", userId);
            mainObject.put("lang", lang);
            mainObject.put("order_id", order_id);
            mainObject.put("TblMain", mainTblRow());
            mainObject.put("TblDetl", detailTblRow());

            Log.e("Table_Main", mainObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        saveApi(mainObject);
    }

    public JSONObject mainTblRow() throws JSONException {
        JSONObject obj = new JSONObject();
        JSONObject mainFilter = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < questionSetList.size(); i++) {
            try {
                String strSts = "getDataKey('qa_status','completed')";
                obj.put("qa_comment", ((EditText) findViewById(R.id.edtComments)).getText().toString());
                obj.put("reject_qty", ((EditText) findViewById(R.id.edtRejectedQty)).getText().toString());
                obj.put("qa_result", strResult);
                obj.put("req_attend_time", "NOW()");
                obj.put("req_attend_by", userId);
                obj.put("req_status", strSts);
                obj.put("updation_date", "NOW()");
                obj.put("updated_by", userId);
                mainFilter.put("qa_ID", questionSetList.get(i).getQaID());
                obj.put("filter", mainFilter);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        jsonArray.put(obj);
        JSONObject finalobject = new JSONObject();
        finalobject.put("rows", jsonArray);
        return finalobject;
    }

    public JSONObject detailTblRow() throws JSONException {
        JSONObject obj = null;
        JSONObject mainFilter = null;
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < questionSetList.size(); i++) {
            obj = new JSONObject();
            mainFilter = new JSONObject();
            try {
                String qaRowId = questionSetList.get(i).getQaRowID();
                String strType = questionSetList.get(i).getTypeOfAns();
                View view = recyclerQuestion.getChildAt(i);
                if (strType.equals("String") || strType.equals("Number")) {
                    EditText edtAnsValue = view.findViewById(R.id.edtQues);
                    String strAns = edtAnsValue.getText().toString();
                    obj.put("ans_value", strAns);
                } else {
                    obj.put("ans_value", getAnsSpinnerData(i));
                }
                obj.put("updated_by", userId);
                obj.put("updation_date", "NOW()");
                mainFilter.put("qa_row_ID", qaRowId);
                obj.put("filter", mainFilter);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(obj);
        }
        JSONObject finalobject = new JSONObject();
        finalobject.put("rows", jsonArray);
        return finalobject;
    }

    private void saveApi(final JSONObject jsonObject) {
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"),
                ((jsonObject)).toString());
        RetrofitService.getSaveRecordQAData(new Dialog(mContext), retrofitApiClient.saveRecordQAData
                (body), new WebResponse() {
            @Override
            public void onResponseSuccess(Response<?> result) {
                Response<ResponseBody> response = (Response<ResponseBody>) result;
                try {
                    JSONObject jsonObject1 = new JSONObject(response.body().string());
                    if (jsonObject.getString("is_error").equals("0")) {
                        AppAlerts.show(mContext, "RequestQA saved successfully");
                    } else {
                        AppAlerts.show(mContext, jsonObject.getString("error_msg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onResponseFailed(String error) {
                AppAlerts.show(mContext, error);
            }
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Log out
    private void logout() {
        AppAlerts.showAlertWithAction(mContext, "Logout", "Are you sure want to Logout?",
                "YES", "NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        clearAllPreferences();
                    }
                }, true, true);
    }

    private void clearAllPreferences() {
        AppPreference.clearAllPreferences(mContext);
        Intent intent = new Intent(mContext, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    private void goMaintListActivity() {
        Intent intent = new Intent(mContext, MaintListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    private String getAnsSpinnerData(int i) {
        View view = recyclerQuestion.getChildAt(i);
        final String[] str = new String[1];
        Spinner lookupSpinner = (Spinner) view.findViewById(R.id.lookupSpinner);
        lookupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                str[0] = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        return str[0];
    }
}