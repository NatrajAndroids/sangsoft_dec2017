package com.natraj.sangsoft.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.constant.Constant;
import com.natraj.sangsoft.model.login_data.LoginUserModal;
import com.natraj.sangsoft.model.login_page_label.LoginFormPageData;
import com.natraj.sangsoft.retrofit_provider.RetrofitApiClient;
import com.natraj.sangsoft.retrofit_provider.RetrofitService;
import com.natraj.sangsoft.retrofit_provider.WebResponse;
import com.natraj.sangsoft.ui.production_activity.IndentOverViewActivity;
import com.natraj.sangsoft.ui.production_activity.Receive_RM_Data_Activity;
import com.natraj.sangsoft.utils.AppPreference;
import com.quenDel.multiUtils.neworks.NetworkHandler;
import com.quenDel.multiUtils.utils.AppAlerts;

import retrofit2.Response;

import static com.quenDel.multiUtils.imagepicker.DevicePermission.REQUEST_CODE;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener,
        AdapterView.OnItemSelectedListener {

    private String mch_id = "";
    private static final int TEXT_ID = 0;
    public static LoginFormPageData pageDataModel;
    public static LoginUserModal userLoginDataModel;
    String BASE_URL = "";
    private Toolbar toolbar;
    private Context mContext;
    private EditText userID, pass;
    private String language = "";
    private TextView tvUserID, tvPass, tvHelpText;
    private Button login;
    private RetrofitApiClient retrofitApiClient;
    private boolean isUrlValid = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext = this;
        checkLoginData();
    }

    private void checkLoginData() {
        if (AppPreference.getStringPreference(mContext, Constant.USERNAME).isEmpty()) {
            initToolbar();
            init("");
        } else {
            startActivity(new Intent(mContext, IndentOverViewActivity.class));
            finish();
        }
    }

    private void initToolbar() {

        NetworkHandler networkHandler = new NetworkHandler(LoginActivity.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((View) findViewById(R.id.viewVisibility)).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.prod_tooltip)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.prod_maintenance)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.prod_qa)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.prod_help)).setVisibility(View.GONE);

        Spinner langSpinner = (Spinner) findViewById(R.id.lang_spinner);
        langSpinner.setVisibility(View.VISIBLE);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.language, R.layout.spinner_language);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        langSpinner.setOnItemSelectedListener(this);
        langSpinner.setAdapter(adapter);

        language = AppPreference.getConstStringPreference(mContext, Constant.SELECTED_LANGUAGE);
        language = language.equals("") ? "En" : language;
        langSpinner.setSelection((AppPreference.getConstIntegerPreference(mContext, Constant.SELECTED_LANGUAGE_POSITION)));

        if (AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID).isEmpty()) {
            ((TextView) toolbar.findViewById(R.id.txtMachine)).setText("");
        } else {
            ((TextView) toolbar.findViewById(R.id.txtMachine)).setText
                    (AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR));
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Toolbar bar label
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data.getExtras() == null)
                    return;
                String newString = data.getExtras().getString("url");
                String getMachineId = data.getExtras().getString(Constant.MACHINE_ID_TOOLBAR);
                ((TextView) toolbar.findViewById(R.id.txtMachine))
                        .setText(AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR));
                AppAlerts.show(mContext, "Url change successful");
                //Toast.makeText(mContext, getMachineId, Toast.LENGTH_SHORT).show();
                init(newString);
            } else {
                if ((AppPreference.getConstIntegerPreference(mContext, Constant.SELECTED_URL_POSITION)) == 0) {
                    AppAlerts.show(mContext, "Url not selected");
                }
                //Log.e("Result code: ", "error");
            }
        } else {
            if ((AppPreference.getConstIntegerPreference(mContext, Constant.SELECTED_URL_POSITION)) == 0) {
                AppAlerts.show(mContext, "Url not selected");
            }
        }
    }

    private void init(String newString) {
        BASE_URL = "";
        if (AppPreference.getConstStringPreference(mContext, Constant.SELECTED_URL).isEmpty()) {
            if (newString.isEmpty()) {
                BASE_URL = "http://www.sangsoft.in/demo/lt_api/api/";
                //  Constant.BASE_URL  = "http://www.sangsoft.in/demo/lt_api/api/";
            } else {
                BASE_URL = newString;
                //    Constant.BASE_URL = UrlActivity.selectUrl;
            }
        } else {
            BASE_URL = AppPreference.getConstStringPreference(mContext, Constant.SELECTED_URL);
        }

        try {
            Constant.BASE_URL = BASE_URL;
            retrofitApiClient = RetrofitService.getRetrofit();
            isUrlValid = true;
        } catch (Exception e) {
            isUrlValid = false;
            Toast.makeText(mContext, "Invalid url -- " + "Base url : - " + BASE_URL, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        userID = (EditText) findViewById(R.id.edt_userid);
        pass = (EditText) findViewById(R.id.edt_pass);

        // userID.setShowSoftInputOnFocus(false);
        userID.setKeyListener(null);
        pass.setKeyListener(null);

        login = (Button) findViewById(R.id.login);
        tvUserID = (TextView) findViewById(R.id.tv_userId);
        tvPass = (TextView) findViewById(R.id.tv_password);
        tvHelpText = (TextView) findViewById(R.id.tv_helptext);

        login.setOnClickListener(this);
        ((ImageView) findViewById(R.id.imgSettings)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnCheck)).setOnClickListener(this);
        ((TextView) findViewById(R.id.num_one)).setOnClickListener(this);
        ((TextView) findViewById(R.id.num_two)).setOnClickListener(this);
        ((TextView) findViewById(R.id.num_three)).setOnClickListener(this);
        ((TextView) findViewById(R.id.num_four)).setOnClickListener(this);
        ((TextView) findViewById(R.id.num_five)).setOnClickListener(this);
        ((TextView) findViewById(R.id.num_six)).setOnClickListener(this);
        ((TextView) findViewById(R.id.num_seven)).setOnClickListener(this);
        ((TextView) findViewById(R.id.num_eight)).setOnClickListener(this);
        ((TextView) findViewById(R.id.num_nine)).setOnClickListener(this);
        ((TextView) findViewById(R.id.num_zero)).setOnClickListener(this);
        ((TextView) findViewById(R.id.num_clear)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.num_undo)).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCheck:
                startActivity(new Intent(mContext, Receive_RM_Data_Activity.class));
                break;
            case R.id.num_one:
                if (userID.hasFocus())
                    userID.append("1");
                else if (pass.hasFocus())
                    pass.append("1");
                break;
            case R.id.num_two:
                if (userID.hasFocus())
                    userID.append("2");
                else if (pass.hasFocus())
                    pass.append("2");
                break;
            case R.id.num_three:
                if (userID.hasFocus())
                    userID.append("3");
                else if (pass.hasFocus())
                    pass.append("3");
                break;
            case R.id.num_four:
                if (userID.hasFocus())
                    userID.append("4");
                else if (pass.hasFocus())
                    pass.append("4");
                break;
            case R.id.num_five:
                if (userID.hasFocus())
                    userID.append("5");
                else if (pass.hasFocus())
                    pass.append("5");
                break;
            case R.id.num_six:
                if (userID.hasFocus())
                    userID.append("6");
                else if (pass.hasFocus())
                    pass.append("6");
                break;
            case R.id.num_seven:
                if (userID.hasFocus())
                    userID.append("7");
                else if (pass.hasFocus())
                    pass.append("7");
                break;
            case R.id.num_eight:
                if (userID.hasFocus())
                    userID.append("8");
                else if (pass.hasFocus())
                    pass.append("8");
                break;
            case R.id.num_nine:
                if (userID.hasFocus())
                    userID.append("9");
                else if (pass.hasFocus())
                    pass.append("9");
                break;
            case R.id.num_zero:
                if (userID.hasFocus())
                    userID.append("0");
                else if (pass.hasFocus())
                    pass.append("0");
                break;
            case R.id.num_clear:
                if (userID.hasFocus())
                    userID.setText("");
                else if (pass.hasFocus())
                    pass.setText("");
                break;
            case R.id.num_undo:
                Editable editable = null;
                if (userID.hasFocus())
                    editable = userID.getText();
                else if (pass.hasFocus())
                    editable = pass.getText();
                int charCount = editable.length();
                if (charCount > 0) {
                    editable.delete(charCount - 1, charCount);
                }
                break;
            case R.id.login:
                login();
                break;
            case R.id.imgSettings:
                alertForVarify();
                break;
        }
    }

    private void alertForVarify() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Verify");
        builder.setMessage("Please enter Id to continue...");

        final EditText input = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(50, 50, 50, 50);
        input.setLayoutParams(lp);
        input.setId(TEXT_ID);
        input.setPadding(20, 20, 20, 20);
        builder.setView(input);

        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();
                /*this password used :- SANGS0ft*/
                if (value.equals("")) {
                    startActivityForResult(new Intent(mContext, UrlActivity.class), REQUEST_CODE);
                } else {
                    AppAlerts.show(mContext, "Wrong id");
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void login() {
        if (!isUrlValid) {
            AppAlerts.showAlertMessage(mContext, "Error", "You have selected invalid url. Please select valid url.");
            return;
        }

        final String user = userID.getText().toString().trim();
        final String psd = pass.getText().toString().trim();
        String lan = "";
        if (language.equals("En")) {
            lan = "en";
        } else if (language.equals("Ma")) {
            lan = "ma";
        }
        if (userID.length() == 0) {
            AppAlerts.show(mContext, "Please enter a valid user ID");
        } else if (pass.length() == 0) {
            AppAlerts.show(mContext, "Please enter a valid password");
        } else {
            // AppAlerts.show(mContext,"UserId = "+user+" & Password = "+psd);
            String op = "login";
            if (AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR).isEmpty()) {
                mch_id = "";
            } else {
                mch_id = AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR);
            }
            final String lang = lan;

            RetrofitService.getLoginPageUserResponse(new Dialog(mContext), retrofitApiClient.userLogin(mch_id, op, lang, user, psd),
                    new WebResponse() {
                        @Override
                        public void onResponseSuccess(Response<?> result) {
                            Response<LoginUserModal> response = (Response<LoginUserModal>) result;
                            userLoginDataModel = response.body();
                            if (response.isSuccessful()) {
                                if (userLoginDataModel.getIsError() == 0) {
                                    if ("1".equals(userLoginDataModel.getJsonData().getSuccess())) {
                                        AppAlerts.show(mContext, userLoginDataModel.getJsonData().getLoginMsg());

                                        Bundle bundle = new Bundle();
                                        bundle.putString("mch_id", mch_id);
                                        bundle.putString("lang", lang);
                                        bundle.putString("user_id", user);

                                        AppPreference.setStringPreference(mContext, "user_name", userLoginDataModel.getJsonData().getUserName());
                                        AppPreference.setStringPreference(mContext, Constant.USERNAME, user);
                                        AppPreference.setStringPreference(mContext, Constant.PASSWORD, psd);
                                        AppPreference.setConstStringPreference(mContext, Constant.MACHINE_ID, userLoginDataModel.getJsonData().getMachineId());
                                        AppPreference.setStringPreference(mContext, Constant.LANGUAGE, lang);
                                        AppPreference.setStringPreference(mContext, "base_url", Constant.BASE_URL);
                                        Intent intent = new Intent(mContext, IndentOverViewActivity.class);
                                        intent.putExtras(bundle);
                                        startActivity(intent);
                                        finish();
                                        //  startActivity(new Intent(mContext, MainActivity.class));
                                    } else {
                                        AppAlerts.show(mContext, userLoginDataModel.getJsonData().getLoginMsg());
                                    }
                                } else {
                                    AppAlerts.show(mContext, userLoginDataModel.getErrorMsg());
                                }
                            }
                        }

                        @Override
                        public void onResponseFailed(String error) {
                            AppAlerts.show(mContext, error);
                        }
                    });
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (i == 0) {
            language = "";
            return;
        }
        language = adapterView.getItemAtPosition(i).toString();
        AppPreference.setConstIntegerPreference(mContext, Constant.SELECTED_LANGUAGE_POSITION, i);

        String op = "loadform";
        String mch_id = AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID);
        String lang = "";
        if (language.equals("En")) {
            lang = "en";
        } else if (language.equals("Ma")) {
            lang = "ma";
        }
        changeLanguage(mch_id, op, lang);
    }

    private void changeLanguage(String mch_id, String op, String lang) {

        RetrofitService.getLoginPageLabelResponse(new Dialog(mContext), retrofitApiClient.logiPageLabel(mch_id, op, lang), new WebResponse() {
            @Override
            public void onResponseSuccess(Response<?> result) {

                Response<LoginFormPageData> response = (Response<LoginFormPageData>) result;
                pageDataModel = response.body();
                if (response.isSuccessful()) {
                    if (pageDataModel.getIsError() == 0) {
                        AppPreference.setConstStringPreference(mContext, Constant.SELECTED_LANGUAGE, language);
                        if (pageDataModel.getStrings().getFailedMsg() != null) {
                            String fMsg = pageDataModel.getStrings().getFailedMsg();
                            if (pageDataModel.getStrings().getGreetingMsg() != null) {
                                String grtMsg = pageDataModel.getStrings().getGreetingMsg();
                                tvHelpText.setText(fMsg + " / " + grtMsg);
                            }
                        }
                        if (pageDataModel.getStrings().getEmpID() != null)
                            tvUserID.setText(pageDataModel.getStrings().getEmpID());
                        if (pageDataModel.getStrings().getPIN() != null)
                            tvPass.setText(pageDataModel.getStrings().getPIN());
                        if (pageDataModel.getStrings().getLoginBtn() != null)
                            login.setText(pageDataModel.getStrings().getLoginBtn());
                    } else {
                    }
                }
            }

            @Override
            public void onResponseFailed(String error) {
                AppAlerts.show(mContext, error);
            }
        });
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
