package com.natraj.sangsoft.ui.production_activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.constant.Constant;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_info_bar_modal.IndentInfoBarModal;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_actionbar.IndentOverviewActionBar;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_actionbar.Order;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_header_info.CommonDataModal;
import com.natraj.sangsoft.model.receive_rm_data_modal.RMInutItem;
import com.natraj.sangsoft.model.receive_rm_data_modal.RmDataModal;
import com.natraj.sangsoft.retrofit_provider.RetrofitService;
import com.natraj.sangsoft.retrofit_provider.WebResponse;
import com.natraj.sangsoft.ui.LoginActivity;
import com.natraj.sangsoft.utils.AppPreference;
import com.natraj.sangsoft.utils.BaseActivity;
import com.quenDel.multiUtils.neworks.NetworkHandler;
import com.quenDel.multiUtils.utils.AppAlerts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Response;

public class PauseResumeActivity extends BaseActivity implements View.OnClickListener {

    private boolean b = true;
    private int currentIndent = 0;
    private IndentOverviewActionBar overviewActionBar;
    private Toolbar toolbar;
    private RmDataModal receiveRmDataModal;
    private String indent_id = "1";
    private String lang, mchId, userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pause_resume);
        initToolbar();
        setToolbarBarLabel();
        setIndentActionBarData();
        setInfoBarInfo();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setVisibility(View.VISIBLE);
        if (getSupportActionBar() == null) {
            return;
        }
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((LinearLayout) findViewById(R.id.linearIndentOverview)).setOnClickListener(this);
        ((LinearLayout) toolbar.findViewById(R.id.linearLogout)).setOnClickListener(this);
        networkHandler = new NetworkHandler(PauseResumeActivity.this);

        ((Button) findViewById(R.id.btnPauseResume)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.imgLeft)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.imgRight)).setOnClickListener(this);
        getIds();
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Get language, mch id, user id, indent id
    private void getIds() {
        lang = AppPreference.getStringPreference(mContext, Constant.LANGUAGE);
        mchId = AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID);
        userId = AppPreference.getStringPreference(mContext, Constant.USERNAME);
        indent_id = AppPreference.getStringPreference(mContext, Constant.LANGUAGE);
        if (!AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR).isEmpty()) {
            ((TextView) toolbar.findViewById(R.id.txtMachine))
                    .setText(AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID_TOOLBAR));
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Toolbar bar label
    private void setToolbarBarLabel() {
        String headerInfo = "headerInfo";
        String userId = AppPreference.getStringPreference(mContext, Constant.USERNAME);
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getCommonDataLbl(new Dialog(mContext), retrofitApiClient.commonDataItems(
                    headerInfo, userId, lang), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<CommonDataModal> response = (Response<CommonDataModal>) result;
                    CommonDataModal dataModal = response.body();
                    if (dataModal.getIsError().equals(0)) {
                        ((TextView) toolbar.findViewById(R.id.prod_tooltip)).setText(dataModal.getStrings().getProdTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_maintenance)).setText(dataModal.getStrings().getMaintTooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_qa)).setText(dataModal.getStrings().getQATooltip());
                        ((TextView) toolbar.findViewById(R.id.prod_help)).setText(dataModal.getStrings().getHelpTooltip());
                    } else {
                        AppAlerts.show(mContext, dataModal.getErrorMsg());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Indent Info Bar Label
    private void setInfoBarInfo() {
        String op = "orderInfoBar";
        RetrofitService.getIndentInfoBarResponce(new Dialog(mContext), retrofitApiClient.indentInfobar(mchId, op, userId, lang, indent_id), new WebResponse() {
            @Override
            public void onResponseSuccess(Response<?> result) {
                Response<IndentInfoBarModal> response = (Response<IndentInfoBarModal>) result;
                IndentInfoBarModal infoBarModal = response.body();
                if (infoBarModal.getIsError().equals("0")) {
                    ((TextView) findViewById(R.id.indent_no)).setText(infoBarModal.getStrings().getLblIndentNo());
                    ((TextView) findViewById(R.id.indent_timing)).setText(infoBarModal.getStrings().getLblTiming());
                    ((TextView) findViewById(R.id.indent_foritem)).setText(infoBarModal.getStrings().getLblForItem());
                    ((TextView) findViewById(R.id.indent_qtyib)).setText(infoBarModal.getStrings().getLblQty());

                    ((TextView) findViewById(R.id.txtIndentId)).setText(infoBarModal.getJsonData().getIndentRef());
                    ((TextView) findViewById(R.id.txtIndentTiming)).setText(infoBarModal.getJsonData().getTiming());
                    ((TextView) findViewById(R.id.txtIndentForItem)).setText(infoBarModal.getJsonData().getItemName());
                    ((TextView) findViewById(R.id.txtIndentQuantityValue)).setText
                            (infoBarModal.getJsonData().getProducedQty() + " of " + infoBarModal.getJsonData().getOrderQty());
                    setReceiveRmLabel();
                }
            }

            @Override
            public void onResponseFailed(String error) {
                AppAlerts.show(mContext, error);
            }
        });
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //Set Indent Action Bar Data
    private void setIndentActionBarData() {
        String op = "orderActionBar";
        RetrofitService.getIndentActionBarResponce(new Dialog(mContext), retrofitApiClient.indentActionbar(mchId, op, userId, lang),
                new WebResponse() {
                    @Override
                    public void onResponseSuccess(Response<?> result) {
                        Response<IndentOverviewActionBar> response = (Response<IndentOverviewActionBar>) result;
                        overviewActionBar = response.body();
                        if (overviewActionBar.getIsError() == 0) {
                            ((TextView) findViewById(R.id.lbl_overview)).setText(overviewActionBar.getStrings().getBtnOverview());
                            ((TextView) findViewById(R.id.lbl_action)).setText(overviewActionBar.getStrings().getLblRecord());
                            ((TextView) findViewById(R.id.lbl_pause)).setText(overviewActionBar.getStrings().getBtnPause());
                            ((TextView) findViewById(R.id.lbl_close)).setText(overviewActionBar.getStrings().getTooltipClose());
                            ((TextView) findViewById(R.id.lbl_progress)).setText(overviewActionBar.getStrings().getBtnProgress());
                            setIndentValue();
                        }
                    }

                    @Override
                    public void onResponseFailed(String error) {
                        AppAlerts.show(mContext, error);
                    }
                });
    }

    /*
     *  Set indent value and status
     * */
    private void setIndentValue() {
        if (overviewActionBar.getJsonData() == null)
            return;
        if (overviewActionBar.getJsonData().getOrderCount() == null)
            return;
        Order overviewActionBarIndent = overviewActionBar.getJsonData().getOrders().get(currentIndent);
        if (overviewActionBarIndent.getOrderID() != null)
            ((TextView) findViewById(R.id.indent_name)).setText(overviewActionBarIndent.getOrderID());
        ((TextView) findViewById(R.id.indent_status)).setText(overviewActionBarIndent.getOrderStatus());
        indent_id = overviewActionBarIndent.getOrderID();
        AppPreference.setStringPreference(mContext, Constant.INDENT_ID, indent_id);
    }

    private void setReceiveRmLabel() {
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.getReceiveRmWorkAreaData(new Dialog(mContext), retrofitApiClient.setReceiveRmWorkArea(
                    "receiveRM", mchId, userId, lang, indent_id), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<RmDataModal> response = (Response<RmDataModal>) result;
                    receiveRmDataModal = response.body();

                    ((TextView) findViewById(R.id.area_title)).setText(receiveRmDataModal.getStrings().getAreaTitle());
                    //((Button) findViewById(R.id.btnPauseResume)).setText(receiveRmDataModal.getStrings().getBtnSave());
                    ((Button) findViewById(R.id.btnCancel)).setText(receiveRmDataModal.getStrings().getBtnCancel());
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPauseResume:
                if (b) {
                    ((Button) findViewById(R.id.btnPauseResume)).setText(R.string.str_resume);
                    ((TextView) findViewById(R.id.area_title)).setText(R.string.str_resume_prod);
                    b = false;
                } else {
                    ((Button) findViewById(R.id.btnPauseResume)).setText(R.string.str_pause);
                    ((TextView) findViewById(R.id.area_title)).setText(R.string.str_pause_prod);
                    b = true;
                }
                break;
            case R.id.linearLogout:
                logout();
                break;

            case R.id.imgLeft:
                if (currentIndent > 0) {
                    currentIndent--;
                    setIndentValue();
                } else {
                    AppAlerts.show(mContext, "This is First Order ");
                }
                break;
            case R.id.imgRight:
                if (overviewActionBar.getJsonData().getOrderCount() == null)
                    return;
                if (currentIndent < (overviewActionBar.getJsonData().getOrderCount()) - 1) {
                    currentIndent++;
                    setIndentValue();
                } else {
                    AppAlerts.show(mContext, "This is Last Order ");
                }
                break;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Log out
    private void logout() {
        AppAlerts.showAlertWithAction(mContext, "Logout", "Are you sure want to Logout?",
                "YES", "NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        clearAllPreferences();
                    }
                }, true, true);
    }

    private void clearAllPreferences() {
        AppPreference.clearAllPreferences(mContext);
        Intent intent = new Intent(mContext, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    public void makJsonObject(List<RMInutItem> list) {

        JSONObject obj = null;
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < list.size(); i++) {
            obj = new JSONObject();
            try {
                obj.put("Itemcode", list.get(i).getItemcode());
                obj.put("ReceivedUOM", list.get(i).getReceivedQty());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(obj);
        }

        JSONObject finalobject = new JSONObject();
        try {
            finalobject.put("inutItems", jsonArray);
            finalobject.put("comments", ((EditText) findViewById(R.id.edtComments)).getText().toString());
            finalobject.put("statusVal", "00000123_received");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Json items data", " : " + finalobject);
    }
}