
package com.natraj.sangsoft.model.request_qa_modal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CmbQAstatus implements Parcelable
{

    @SerializedName("QAstatusID")
    @Expose
    private String qAstatusID;
    @SerializedName("QAStatus")
    @Expose
    private String qAStatus;
    public final static Parcelable.Creator<CmbQAstatus> CREATOR = new Creator<CmbQAstatus>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CmbQAstatus createFromParcel(Parcel in) {
            return new CmbQAstatus(in);
        }

        public CmbQAstatus[] newArray(int size) {
            return (new CmbQAstatus[size]);
        }

    }
            ;

    protected CmbQAstatus(Parcel in) {
        this.qAstatusID = ((String) in.readValue((String.class.getClassLoader())));
        this.qAStatus = ((String) in.readValue((String.class.getClassLoader())));
    }

    public CmbQAstatus() {
    }

    public String getQAstatusID() {
        return qAstatusID;
    }

    public void setQAstatusID(String qAstatusID) {
        this.qAstatusID = qAstatusID;
    }

    public String getQAStatus() {
        return qAStatus;
    }

    public void setQAStatus(String qAStatus) {
        this.qAStatus = qAStatus;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(qAstatusID);
        dest.writeValue(qAStatus);
    }

    public int describeContents() {
        return 0;
    }

}
