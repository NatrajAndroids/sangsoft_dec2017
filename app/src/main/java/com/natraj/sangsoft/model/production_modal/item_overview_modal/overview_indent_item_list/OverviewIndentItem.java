
package com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_indent_item_list;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OverviewIndentItem implements Parcelable
{

    @SerializedName("item_id")
    @Expose
    private String itemId="";
    @SerializedName("item_mcode")
    @Expose
    private String itemMcode="";
    @SerializedName("item_name")
    @Expose
    private String itemName="";
    public final static Creator<OverviewIndentItem> CREATOR = new Creator<OverviewIndentItem>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OverviewIndentItem createFromParcel(Parcel in) {
            return new OverviewIndentItem(in);
        }

        public OverviewIndentItem[] newArray(int size) {
            return (new OverviewIndentItem[size]);
        }

    }
            ;

    protected OverviewIndentItem(Parcel in) {
        this.itemId = ((String) in.readValue((String.class.getClassLoader())));
        this.itemMcode = ((String) in.readValue((String.class.getClassLoader())));
        this.itemName = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public OverviewIndentItem() {
    }

    /**
     *
     * @param itemName
     * @param itemMcode
     * @param itemId
     */
    public OverviewIndentItem(String itemId, String itemMcode, String itemName) {
        super();
        this.itemId = itemId;
        this.itemMcode = itemMcode;
        this.itemName = itemName;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemMcode() {
        return itemMcode;
    }

    public void setItemMcode(String itemMcode) {
        this.itemMcode = itemMcode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(itemId);
        dest.writeValue(itemMcode);
        dest.writeValue(itemName);
    }

    public int describeContents() {
        return  0;
    }

}
