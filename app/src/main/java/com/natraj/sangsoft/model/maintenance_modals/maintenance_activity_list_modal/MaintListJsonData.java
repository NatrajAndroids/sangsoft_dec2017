
package com.natraj.sangsoft.model.maintenance_modals.maintenance_activity_list_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MaintListJsonData implements Parcelable
{

    @SerializedName("rows")
    @Expose
    private List<MaintListRow> rows = new ArrayList<MaintListRow>();
    public final static Creator<MaintListJsonData> CREATOR = new Creator<MaintListJsonData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MaintListJsonData createFromParcel(Parcel in) {
            return new MaintListJsonData(in);
        }

        public MaintListJsonData[] newArray(int size) {
            return (new MaintListJsonData[size]);
        }

    }
            ;

    protected MaintListJsonData(Parcel in) {
        in.readList(this.rows, (MaintListRow.class.getClassLoader()));
    }

    public MaintListJsonData() {
    }

    public List<MaintListRow> getRows() {
        return rows;
    }

    public void setRows(List<MaintListRow> rows) {
        this.rows = rows;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(rows);
    }

    public int describeContents() {
        return 0;
    }

}