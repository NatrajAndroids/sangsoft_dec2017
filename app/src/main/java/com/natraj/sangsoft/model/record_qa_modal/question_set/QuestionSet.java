package com.natraj.sangsoft.model.record_qa_modal.question_set;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuestionSet implements Parcelable
{

    @SerializedName("qa_row_ID")
    @Expose
    private String qaRowID;
    @SerializedName("qa_ID")
    @Expose
    private String qaID;
    @SerializedName("question_text")
    @Expose
    private String questionText;
    @SerializedName("question_text_ml")
    @Expose
    private String questionTextMl;
    @SerializedName("question_description")
    @Expose
    private String questionDescription;
    @SerializedName("ans_type")
    @Expose
    private String ansType;
    @SerializedName("type_of_ans")
    @Expose
    private String typeOfAns;
    @SerializedName("lookup_name")
    @Expose
    private String lookupName;
    @SerializedName("qa_row_seq")
    @Expose
    private String qaRowSeq;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("ans_value")
    @Expose
    private String ansValue;
    @SerializedName("ans_comment")
    @Expose
    private String ansComment;
    public final static Parcelable.Creator<QuestionSet> CREATOR = new Creator<QuestionSet>() {


        @SuppressWarnings({
                "unchecked"
        })
        public QuestionSet createFromParcel(Parcel in) {
            return new QuestionSet(in);
        }

        public QuestionSet[] newArray(int size) {
            return (new QuestionSet[size]);
        }

    }
            ;

    protected QuestionSet(Parcel in) {
        this.qaRowID = ((String) in.readValue((String.class.getClassLoader())));
        this.qaID = ((String) in.readValue((String.class.getClassLoader())));
        this.questionText = ((String) in.readValue((String.class.getClassLoader())));
        this.questionTextMl = ((String) in.readValue((String.class.getClassLoader())));
        this.questionDescription = ((String) in.readValue((String.class.getClassLoader())));
        this.ansType = ((String) in.readValue((String.class.getClassLoader())));
        this.typeOfAns = ((String) in.readValue((String.class.getClassLoader())));
        this.lookupName = ((String) in.readValue((String.class.getClassLoader())));
        this.qaRowSeq = ((String) in.readValue((String.class.getClassLoader())));
        this.isActive = ((String) in.readValue((String.class.getClassLoader())));
        this.ansValue = ((String) in.readValue((String.class.getClassLoader())));
        this.ansComment = ((String) in.readValue((String.class.getClassLoader())));
    }

    public QuestionSet() {
    }

    public String getQaRowID() {
        return qaRowID;
    }

    public void setQaRowID(String qaRowID) {
        this.qaRowID = qaRowID;
    }

    public String getQaID() {
        return qaID;
    }

    public void setQaID(String qaID) {
        this.qaID = qaID;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getQuestionTextMl() {
        return questionTextMl;
    }

    public void setQuestionTextMl(String questionTextMl) {
        this.questionTextMl = questionTextMl;
    }

    public String getQuestionDescription() {
        return questionDescription;
    }

    public void setQuestionDescription(String questionDescription) {
        this.questionDescription = questionDescription;
    }

    public String getAnsType() {
        return ansType;
    }

    public void setAnsType(String ansType) {
        this.ansType = ansType;
    }

    public String getTypeOfAns() {
        return typeOfAns;
    }

    public void setTypeOfAns(String typeOfAns) {
        this.typeOfAns = typeOfAns;
    }

    public String getLookupName() {
        return lookupName;
    }

    public void setLookupName(String lookupName) {
        this.lookupName = lookupName;
    }

    public String getQaRowSeq() {
        return qaRowSeq;
    }

    public void setQaRowSeq(String qaRowSeq) {
        this.qaRowSeq = qaRowSeq;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getAnsValue() {
        return ansValue;
    }

    public void setAnsValue(String ansValue) {
        this.ansValue = ansValue;
    }

    public String getAnsComment() {
        return ansComment;
    }

    public void setAnsComment(String ansComment) {
        this.ansComment = ansComment;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(qaRowID);
        dest.writeValue(qaID);
        dest.writeValue(questionText);
        dest.writeValue(questionTextMl);
        dest.writeValue(questionDescription);
        dest.writeValue(ansType);
        dest.writeValue(typeOfAns);
        dest.writeValue(lookupName);
        dest.writeValue(qaRowSeq);
        dest.writeValue(isActive);
        dest.writeValue(ansValue);
        dest.writeValue(ansComment);
    }

    public int describeContents() {
        return 0;
    }

}
