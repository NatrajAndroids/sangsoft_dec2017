
package com.natraj.sangsoft.model.production_modal.data_indent_overview_actionbar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class JsonData implements Parcelable
{

    @SerializedName("indentCount")
    @Expose
    private Integer indentCount=0;
    @SerializedName("selected_indent_id")
    @Expose
    private String selectedIndentId="";
    @SerializedName("indents")
    @Expose
    private List<Indent> indents = new ArrayList<Indent>();
    public final static Creator<JsonData> CREATOR = new Creator<JsonData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public JsonData createFromParcel(Parcel in) {
            return new JsonData(in);
        }

        public JsonData[] newArray(int size) {
            return (new JsonData[size]);
        }

    }
            ;

    protected JsonData(Parcel in) {
        this.indentCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.selectedIndentId = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.indents, (com.natraj.sangsoft.model.production_modal.data_indent_overview_actionbar.Indent.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public JsonData() {
    }

    /**
     *
     * @param indentCount
     * @param indents
     * @param selectedIndentId
     */
    public JsonData(Integer indentCount, String selectedIndentId, List<Indent> indents) {
        super();
        this.indentCount = indentCount;
        this.selectedIndentId = selectedIndentId;
        this.indents = indents;
    }

    public Integer getIndentCount() {
        return indentCount;
    }

    public void setIndentCount(Integer indentCount) {
        this.indentCount = indentCount;
    }

    public String getSelectedIndentId() {
        return selectedIndentId;
    }

    public void setSelectedIndentId(String selectedIndentId) {
        this.selectedIndentId = selectedIndentId;
    }

    public List<Indent> getIndents() {
        return indents;
    }

    public void setIndents(List<Indent> indents) {
        this.indents = indents;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(indentCount);
        dest.writeValue(selectedIndentId);
        dest.writeList(indents);
    }

    public int describeContents() {
        return 0;
    }

}