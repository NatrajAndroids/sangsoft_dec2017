
package com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_info;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OverviewItemInfoModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError=0;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg="";
    @SerializedName("strings")
    @Expose
    private OverviewItemInfoStrings strings;
    @SerializedName("jsonData")
    @Expose
    private OverviewItemInfoJsonData jsonData;
    public final static Creator<OverviewItemInfoModal> CREATOR = new Creator<OverviewItemInfoModal>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OverviewItemInfoModal createFromParcel(Parcel in) {
            return new OverviewItemInfoModal(in);
        }

        public OverviewItemInfoModal[] newArray(int size) {
            return (new OverviewItemInfoModal[size]);
        }

    }
    ;

    protected OverviewItemInfoModal(Parcel in) {
        this.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((OverviewItemInfoStrings) in.readValue((OverviewItemInfoStrings.class.getClassLoader())));
        this.jsonData = ((OverviewItemInfoJsonData) in.readValue((OverviewItemInfoJsonData.class.getClassLoader())));
    }

    public OverviewItemInfoModal() {
    }

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public OverviewItemInfoStrings getStrings() {
        return strings;
    }

    public void setStrings(OverviewItemInfoStrings strings) {
        this.strings = strings;
    }

    public OverviewItemInfoJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(OverviewItemInfoJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return  0;
    }

}
