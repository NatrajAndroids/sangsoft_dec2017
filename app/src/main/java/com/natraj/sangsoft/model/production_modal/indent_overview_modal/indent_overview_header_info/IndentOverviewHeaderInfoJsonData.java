package com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_header_info;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IndentOverviewHeaderInfoJsonData implements Parcelable
{

    @SerializedName("UserId")
    @Expose
    private String userId="";
    @SerializedName("UserName")
    @Expose
    private String userName="";
    @SerializedName("prodinfo")
    @Expose
    private String prodinfo="";
    @SerializedName("maintinfo")
    @Expose
    private String maintinfo="";
    @SerializedName("QAinfo")
    @Expose
    private String qAinfo="";
    @SerializedName("machineName")
    @Expose
    private String machineName="";
    public final static Creator<IndentOverviewHeaderInfoJsonData> CREATOR = new Creator<IndentOverviewHeaderInfoJsonData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public IndentOverviewHeaderInfoJsonData createFromParcel(Parcel in) {
            return new IndentOverviewHeaderInfoJsonData(in);
        }

        public IndentOverviewHeaderInfoJsonData[] newArray(int size) {
            return (new IndentOverviewHeaderInfoJsonData[size]);
        }

    }
            ;

    protected IndentOverviewHeaderInfoJsonData(Parcel in) {
        this.userId = ((String) in.readValue((String.class.getClassLoader())));
        this.userName = ((String) in.readValue((String.class.getClassLoader())));
        this.prodinfo = ((String) in.readValue((String.class.getClassLoader())));
        this.maintinfo = ((String) in.readValue((String.class.getClassLoader())));
        this.qAinfo = ((String) in.readValue((String.class.getClassLoader())));
        this.machineName = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public IndentOverviewHeaderInfoJsonData() {
    }

    /**
     *
     * @param machineName
     * @param prodinfo
     * @param qAinfo
     * @param maintinfo
     * @param userId
     * @param userName
     */
    public IndentOverviewHeaderInfoJsonData(String userId, String userName, String prodinfo, String maintinfo, String qAinfo, String machineName) {
        super();
        this.userId = userId;
        this.userName = userName;
        this.prodinfo = prodinfo;
        this.maintinfo = maintinfo;
        this.qAinfo = qAinfo;
        this.machineName = machineName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProdinfo() {
        return prodinfo;
    }

    public void setProdinfo(String prodinfo) {
        this.prodinfo = prodinfo;
    }

    public String getMaintinfo() {
        return maintinfo;
    }

    public void setMaintinfo(String maintinfo) {
        this.maintinfo = maintinfo;
    }

    public String getQAinfo() {
        return qAinfo;
    }

    public void setQAinfo(String qAinfo) {
        this.qAinfo = qAinfo;
    }

    public String getMachineName() {
        return machineName;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(userId);
        dest.writeValue(userName);
        dest.writeValue(prodinfo);
        dest.writeValue(maintinfo);
        dest.writeValue(qAinfo);
        dest.writeValue(machineName);
    }

    public int describeContents() {
        return 0;
    }

}