
package com.natraj.sangsoft.model.execute_modal;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExecuteJsonMainModal implements Parcelable
{

    @SerializedName("TblDetl")
    @Expose
    private TblDetl tblDetl;
    @SerializedName("TblMain")
    @Expose
    private TblMain tblMain;
    public final static Creator<ExecuteJsonMainModal> CREATOR = new Creator<ExecuteJsonMainModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ExecuteJsonMainModal createFromParcel(Parcel in) {
            return new ExecuteJsonMainModal(in);
        }

        public ExecuteJsonMainModal[] newArray(int size) {
            return (new ExecuteJsonMainModal[size]);
        }

    }
            ;

    protected ExecuteJsonMainModal(Parcel in) {
        this.tblDetl = ((TblDetl) in.readValue((TblDetl.class.getClassLoader())));
        this.tblMain = ((TblMain) in.readValue((TblMain.class.getClassLoader())));
    }

    public ExecuteJsonMainModal() {
    }

    public TblDetl getTblDetl() {
        return tblDetl;
    }

    public void setTblDetl(TblDetl tblDetl) {
        this.tblDetl = tblDetl;
    }

    public TblMain getTblMain() {
        return tblMain;
    }

    public void setTblMain(TblMain tblMain) {
        this.tblMain = tblMain;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(tblDetl);
        dest.writeValue(tblMain);
    }

    public int describeContents() {
        return 0;
    }

}