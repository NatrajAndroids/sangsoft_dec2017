
package com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_indent_item_list;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OverviewIndentItemListModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError=0;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg="";
    @SerializedName("strings")
    @Expose
    private OverviewIndentStrings strings;
    @SerializedName("jsonData")
    @Expose
    private OverviewIndentJsonData jsonData;
    public final static Creator<OverviewIndentItemListModal> CREATOR = new Creator<OverviewIndentItemListModal>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OverviewIndentItemListModal createFromParcel(Parcel in) {
            return new OverviewIndentItemListModal(in);
        }

        public OverviewIndentItemListModal[] newArray(int size) {
            return (new OverviewIndentItemListModal[size]);
        }

    }
    ;

    protected OverviewIndentItemListModal(Parcel in) {
        this.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((OverviewIndentStrings) in.readValue((OverviewIndentStrings.class.getClassLoader())));
        this.jsonData = ((OverviewIndentJsonData) in.readValue((OverviewIndentJsonData.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public OverviewIndentItemListModal() {
    }

    /**
     * 
     * @param strings
     * @param errorMsg
     * @param isError
     * @param jsonData
     */
    public OverviewIndentItemListModal(Integer isError, String errorMsg, OverviewIndentStrings strings, OverviewIndentJsonData jsonData) {
        super();
        this.isError = isError;
        this.errorMsg = errorMsg;
        this.strings = strings;
        this.jsonData = jsonData;
    }

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public OverviewIndentStrings getStrings() {
        return strings;
    }

    public void setStrings(OverviewIndentStrings strings) {
        this.strings = strings;
    }

    public OverviewIndentJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(OverviewIndentJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return  0;
    }

}
