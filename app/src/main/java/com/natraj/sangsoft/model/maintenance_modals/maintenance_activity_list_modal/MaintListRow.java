package com.natraj.sangsoft.model.maintenance_modals.maintenance_activity_list_modal;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaintListRow implements Parcelable
{

    @SerializedName("srNo")
    @Expose
    private Integer srNo=0;
    @SerializedName("schID")
    @Expose
    private String schID="";
    @SerializedName("schGrpID")
    @Expose
    private String schGrpID="";
    @SerializedName("startDate")
    @Expose
    private String startDate="";
    @SerializedName("stopDate")
    @Expose
    private String stopDate="";
    @SerializedName("DateRange")
    @Expose
    private String dateRange="";
    @SerializedName("schName")
    @Expose
    private String schName="";
    @SerializedName("startTime")
    @Expose
    private String startTime="";
    @SerializedName("endTime")
    @Expose
    private String endTime="";
    @SerializedName("mandatory")
    @Expose
    private String mandatory="";
    @SerializedName("mandatoryKey")
    @Expose
    private String mandatoryKey="";
    @SerializedName("BY")
    @Expose
    private String bY="";
    @SerializedName("schStatus")
    @Expose
    private String schStatus="";
    @SerializedName("schStatusKey")
    @Expose
    private String schStatusKey="";
    public final static Creator<MaintListRow> CREATOR = new Creator<MaintListRow>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MaintListRow createFromParcel(Parcel in) {
            return new MaintListRow(in);
        }

        public MaintListRow[] newArray(int size) {
            return (new MaintListRow[size]);
        }

    }
            ;

    protected MaintListRow(Parcel in) {
        this.srNo = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.schID = ((String) in.readValue((String.class.getClassLoader())));
        this.schGrpID = ((String) in.readValue((String.class.getClassLoader())));
        this.startDate = ((String) in.readValue((String.class.getClassLoader())));
        this.stopDate = ((String) in.readValue((String.class.getClassLoader())));
        this.dateRange = ((String) in.readValue((String.class.getClassLoader())));
        this.schName = ((String) in.readValue((String.class.getClassLoader())));
        this.startTime = ((String) in.readValue((String.class.getClassLoader())));
        this.endTime = ((String) in.readValue((String.class.getClassLoader())));
        this.mandatory = ((String) in.readValue((String.class.getClassLoader())));
        this.mandatoryKey = ((String) in.readValue((String.class.getClassLoader())));
        this.bY = ((String) in.readValue((String.class.getClassLoader())));
        this.schStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.schStatusKey = ((String) in.readValue((String.class.getClassLoader())));
    }

    public MaintListRow() {
    }

    public Integer getSrNo() {
        return srNo;
    }

    public void setSrNo(Integer srNo) {
        this.srNo = srNo;
    }

    public String getSchID() {
        return schID;
    }

    public void setSchID(String schID) {
        this.schID = schID;
    }

    public String getSchGrpID() {
        return schGrpID;
    }

    public void setSchGrpID(String schGrpID) {
        this.schGrpID = schGrpID;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStopDate() {
        return stopDate;
    }

    public void setStopDate(String stopDate) {
        this.stopDate = stopDate;
    }

    public String getDateRange() {
        return dateRange;
    }

    public void setDateRange(String dateRange) {
        this.dateRange = dateRange;
    }

    public String getSchName() {
        return schName;
    }

    public void setSchName(String schName) {
        this.schName = schName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public String getMandatoryKey() {
        return mandatoryKey;
    }

    public void setMandatoryKey(String mandatoryKey) {
        this.mandatoryKey = mandatoryKey;
    }

    public String getBY() {
        return bY;
    }

    public void setBY(String bY) {
        this.bY = bY;
    }

    public String getSchStatus() {
        return schStatus;
    }

    public void setSchStatus(String schStatus) {
        this.schStatus = schStatus;
    }

    public String getSchStatusKey() {
        return schStatusKey;
    }

    public void setSchStatusKey(String schStatusKey) {
        this.schStatusKey = schStatusKey;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(srNo);
        dest.writeValue(schID);
        dest.writeValue(schGrpID);
        dest.writeValue(startDate);
        dest.writeValue(stopDate);
        dest.writeValue(dateRange);
        dest.writeValue(schName);
        dest.writeValue(startTime);
        dest.writeValue(endTime);
        dest.writeValue(mandatory);
        dest.writeValue(mandatoryKey);
        dest.writeValue(bY);
        dest.writeValue(schStatus);
        dest.writeValue(schStatusKey);
    }

    public int describeContents() {
        return 0;
    }

}