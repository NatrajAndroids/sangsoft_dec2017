
package com.natraj.sangsoft.model.receive_rm_data_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RMInutItem implements Parcelable
{

    @SerializedName("item_id")
    @Expose
    private String itemId;
    @SerializedName("order_bom_ID")
    @Expose
    private String orderBomID;
    @SerializedName("itemcode")
    @Expose
    private String itemcode;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("ExpectedQty")
    @Expose
    private String expectedQty;
    @SerializedName("ExpectedUOM")
    @Expose
    private String expectedUOM;
    @SerializedName("ReceivedQty")
    @Expose
    private String receivedQty;
    @SerializedName("toReturnQty")
    @Expose
    private String toReturnQty;
    public final static Parcelable.Creator<RMInutItem> CREATOR = new Creator<RMInutItem>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RMInutItem createFromParcel(Parcel in) {
            return new RMInutItem(in);
        }

        public RMInutItem[] newArray(int size) {
            return (new RMInutItem[size]);
        }

    }
            ;

    protected RMInutItem(Parcel in) {
        this.itemId = ((String) in.readValue((String.class.getClassLoader())));
        this.orderBomID = ((String) in.readValue((String.class.getClassLoader())));
        this.itemcode = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.expectedQty = ((String) in.readValue((String.class.getClassLoader())));
        this.expectedUOM = ((String) in.readValue((String.class.getClassLoader())));
        this.receivedQty = ((String) in.readValue((String.class.getClassLoader())));
        this.toReturnQty = ((String) in.readValue((String.class.getClassLoader())));
    }

    public RMInutItem() {
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getOrderBomID() {
        return orderBomID;
    }

    public void setOrderBomID(String orderBomID) {
        this.orderBomID = orderBomID;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpectedQty() {
        return expectedQty;
    }

    public void setExpectedQty(String expectedQty) {
        this.expectedQty = expectedQty;
    }

    public String getExpectedUOM() {
        return expectedUOM;
    }

    public void setExpectedUOM(String expectedUOM) {
        this.expectedUOM = expectedUOM;
    }

    public String getReceivedQty() {
        return receivedQty;
    }

    public void setReceivedQty(String receivedQty) {
        this.receivedQty = receivedQty;
    }

    public String getToReturnQty() {
        return toReturnQty;
    }

    public void setToReturnQty(String toReturnQty) {
        this.toReturnQty = toReturnQty;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(itemId);
        dest.writeValue(orderBomID);
        dest.writeValue(itemcode);
        dest.writeValue(description);
        dest.writeValue(expectedQty);
        dest.writeValue(expectedUOM);
        dest.writeValue(receivedQty);
        dest.writeValue(toReturnQty);
    }

    public int describeContents() {
        return 0;
    }

}
