
package com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_info;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OverviewItemInfoProcess implements Parcelable
{

    @SerializedName("process_id")
    @Expose
    private String processId="";
    @SerializedName("process_name")
    @Expose
    private String processName="";
    public final static Creator<OverviewItemInfoProcess> CREATOR = new Creator<OverviewItemInfoProcess>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OverviewItemInfoProcess createFromParcel(Parcel in) {
            return new OverviewItemInfoProcess(in);
        }

        public OverviewItemInfoProcess[] newArray(int size) {
            return (new OverviewItemInfoProcess[size]);
        }

    }
    ;

    protected OverviewItemInfoProcess(Parcel in) {
        this.processId = ((String) in.readValue((String.class.getClassLoader())));
        this.processName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public OverviewItemInfoProcess() {
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(processId);
        dest.writeValue(processName);
    }

    public int describeContents() {
        return  0;
    }

}
