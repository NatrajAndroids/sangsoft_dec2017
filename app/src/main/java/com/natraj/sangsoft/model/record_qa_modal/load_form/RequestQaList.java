package com.natraj.sangsoft.model.record_qa_modal.load_form;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestQaList implements Parcelable
{

    @SerializedName("qa_ID")
    @Expose
    private String qaID;
    @SerializedName("data_key")
    @Expose
    private String dataKey;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    public final static Parcelable.Creator<RequestQaList> CREATOR = new Creator<RequestQaList>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RequestQaList createFromParcel(Parcel in) {
            return new RequestQaList(in);
        }

        public RequestQaList[] newArray(int size) {
            return (new RequestQaList[size]);
        }

    }
            ;

    protected RequestQaList(Parcel in) {
        this.qaID = ((String) in.readValue((String.class.getClassLoader())));
        this.dataKey = ((String) in.readValue((String.class.getClassLoader())));
        this.displayName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public RequestQaList() {
    }

    public String getQaID() {
        return qaID;
    }

    public void setQaID(String qaID) {
        this.qaID = qaID;
    }

    public String getDataKey() {
        return dataKey;
    }

    public void setDataKey(String dataKey) {
        this.dataKey = dataKey;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(qaID);
        dest.writeValue(dataKey);
        dest.writeValue(displayName);
    }

    public int describeContents() {
        return 0;
    }

}
