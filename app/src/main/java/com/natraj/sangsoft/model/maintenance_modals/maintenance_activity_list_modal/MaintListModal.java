
package com.natraj.sangsoft.model.maintenance_modals.maintenance_activity_list_modal;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaintListModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError=0;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg="";
    @SerializedName("strings")
    @Expose
    private MaintListStrings strings;
    @SerializedName("jsonData")
    @Expose
    private MaintListJsonData jsonData;
    public final static Creator<MaintListModal> CREATOR = new Creator<MaintListModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MaintListModal createFromParcel(Parcel in) {
            return new MaintListModal(in);
        }

        public MaintListModal[] newArray(int size) {
            return (new MaintListModal[size]);
        }

    }
            ;

    protected MaintListModal(Parcel in) {
        this.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((MaintListStrings) in.readValue((MaintListStrings.class.getClassLoader())));
        this.jsonData = ((MaintListJsonData) in.readValue((MaintListJsonData.class.getClassLoader())));
    }

    public MaintListModal() {
    }

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public MaintListStrings getStrings() {
        return strings;
    }

    public void setStrings(MaintListStrings strings) {
        this.strings = strings;
    }

    public MaintListJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(MaintListJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return 0;
    }

}