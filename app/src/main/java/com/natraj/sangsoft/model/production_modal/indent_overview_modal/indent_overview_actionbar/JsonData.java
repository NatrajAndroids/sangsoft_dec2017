
package com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_actionbar;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonData implements Parcelable
{

    @SerializedName("orderCount")
    @Expose
    private Integer orderCount;
    @SerializedName("orders")
    @Expose
    private List<Order> orders = new ArrayList<Order>();
    public final static Parcelable.Creator<JsonData> CREATOR = new Creator<JsonData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public JsonData createFromParcel(Parcel in) {
            return new JsonData(in);
        }

        public JsonData[] newArray(int size) {
            return (new JsonData[size]);
        }

    }
            ;

    protected JsonData(Parcel in) {
        this.orderCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.orders, (com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_actionbar.Order.class.getClassLoader()));
    }

    public JsonData() {
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(orderCount);
        dest.writeList(orders);
    }

    public int describeContents() {
        return 0;
    }

}
