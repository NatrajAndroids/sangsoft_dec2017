package com.natraj.sangsoft.model.postpone_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostponeStrings implements Parcelable
{

    @SerializedName("areaTitle")
    @Expose
    private String areaTitle;
    @SerializedName("btnRecordAct")
    @Expose
    private String btnRecordAct;
    @SerializedName("lblOr")
    @Expose
    private String lblOr;
    @SerializedName("lblPostpone")
    @Expose
    private String lblPostpone;
    @SerializedName("lblDue")
    @Expose
    private String lblDue;
    public final static Parcelable.Creator<PostponeStrings> CREATOR = new Creator<PostponeStrings>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PostponeStrings createFromParcel(Parcel in) {
            return new PostponeStrings(in);
        }

        public PostponeStrings[] newArray(int size) {
            return (new PostponeStrings[size]);
        }

    }
            ;

    protected PostponeStrings(Parcel in) {
        this.areaTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.btnRecordAct = ((String) in.readValue((String.class.getClassLoader())));
        this.lblOr = ((String) in.readValue((String.class.getClassLoader())));
        this.lblPostpone = ((String) in.readValue((String.class.getClassLoader())));
        this.lblDue = ((String) in.readValue((String.class.getClassLoader())));
    }

    public PostponeStrings() {
    }

    public String getAreaTitle() {
        return areaTitle;
    }

    public void setAreaTitle(String areaTitle) {
        this.areaTitle = areaTitle;
    }

    public String getBtnRecordAct() {
        return btnRecordAct;
    }

    public void setBtnRecordAct(String btnRecordAct) {
        this.btnRecordAct = btnRecordAct;
    }

    public String getLblOr() {
        return lblOr;
    }

    public void setLblOr(String lblOr) {
        this.lblOr = lblOr;
    }

    public String getLblPostpone() {
        return lblPostpone;
    }

    public void setLblPostpone(String lblPostpone) {
        this.lblPostpone = lblPostpone;
    }

    public String getLblDue() {
        return lblDue;
    }

    public void setLblDue(String lblDue) {
        this.lblDue = lblDue;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(areaTitle);
        dest.writeValue(btnRecordAct);
        dest.writeValue(lblOr);
        dest.writeValue(lblPostpone);
        dest.writeValue(lblDue);
    }

    public int describeContents() {
        return 0;
    }

}
