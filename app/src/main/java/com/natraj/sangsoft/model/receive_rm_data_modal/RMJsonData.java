
package com.natraj.sangsoft.model.receive_rm_data_modal;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RMJsonData implements Parcelable
{

    @SerializedName("indentUOM")
    @Expose
    private String indentUOM;
    @SerializedName("indentQty")
    @Expose
    private String indentQty;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("statusVal")
    @Expose
    private String statusVal;
    @SerializedName("inutItems")
    @Expose
    private List<RMInutItem> inutItems = new ArrayList<RMInutItem>();
    public final static Parcelable.Creator<RMJsonData> CREATOR = new Creator<RMJsonData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RMJsonData createFromParcel(Parcel in) {
            return new RMJsonData(in);
        }

        public RMJsonData[] newArray(int size) {
            return (new RMJsonData[size]);
        }

    }
            ;

    protected RMJsonData(Parcel in) {
        this.indentUOM = ((String) in.readValue((String.class.getClassLoader())));
        this.indentQty = ((String) in.readValue((String.class.getClassLoader())));
        this.comment = ((String) in.readValue((String.class.getClassLoader())));
        this.statusVal = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.inutItems, (RMInutItem.class.getClassLoader()));
    }

    public RMJsonData() {
    }

    public String getIndentUOM() {
        return indentUOM;
    }

    public void setIndentUOM(String indentUOM) {
        this.indentUOM = indentUOM;
    }

    public String getIndentQty() {
        return indentQty;
    }

    public void setIndentQty(String indentQty) {
        this.indentQty = indentQty;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatusVal() {
        return statusVal;
    }

    public void setStatusVal(String statusVal) {
        this.statusVal = statusVal;
    }

    public List<RMInutItem> getInutItems() {
        return inutItems;
    }

    public void setInutItems(List<RMInutItem> inutItems) {
        this.inutItems = inutItems;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(indentUOM);
        dest.writeValue(indentQty);
        dest.writeValue(comment);
        dest.writeValue(statusVal);
        dest.writeList(inutItems);
    }

    public int describeContents() {
        return 0;
    }

}
