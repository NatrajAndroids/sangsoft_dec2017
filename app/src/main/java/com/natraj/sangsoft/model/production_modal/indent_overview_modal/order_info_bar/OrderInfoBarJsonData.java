package com.natraj.sangsoft.model.production_modal.indent_overview_modal.order_info_bar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderInfoBarJsonData implements Parcelable
{

    @SerializedName("indent_id")
    @Expose
    private String indentId;
    @SerializedName("indent_ref")
    @Expose
    private String indentRef;
    @SerializedName("item_name")
    @Expose
    private String itemName;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("order_qty")
    @Expose
    private String orderQty;
    @SerializedName("produced_qty")
    @Expose
    private String producedQty;
    @SerializedName("rejected_qty")
    @Expose
    private String rejectedQty;
    @SerializedName("timing")
    @Expose
    private String timing;
    public final static Parcelable.Creator<OrderInfoBarJsonData> CREATOR = new Creator<OrderInfoBarJsonData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OrderInfoBarJsonData createFromParcel(Parcel in) {
            return new OrderInfoBarJsonData(in);
        }

        public OrderInfoBarJsonData[] newArray(int size) {
            return (new OrderInfoBarJsonData[size]);
        }

    }
            ;

    protected OrderInfoBarJsonData(Parcel in) {
        this.indentId = ((String) in.readValue((String.class.getClassLoader())));
        this.indentRef = ((String) in.readValue((String.class.getClassLoader())));
        this.itemName = ((String) in.readValue((String.class.getClassLoader())));
        this.orderId = ((String) in.readValue((String.class.getClassLoader())));
        this.orderQty = ((String) in.readValue((String.class.getClassLoader())));
        this.producedQty = ((String) in.readValue((String.class.getClassLoader())));
        this.rejectedQty = ((String) in.readValue((String.class.getClassLoader())));
        this.timing = ((String) in.readValue((String.class.getClassLoader())));
    }

    public OrderInfoBarJsonData() {
    }

    public String getIndentId() {
        return indentId;
    }

    public void setIndentId(String indentId) {
        this.indentId = indentId;
    }

    public String getIndentRef() {
        return indentRef;
    }

    public void setIndentRef(String indentRef) {
        this.indentRef = indentRef;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(String orderQty) {
        this.orderQty = orderQty;
    }

    public String getProducedQty() {
        return producedQty;
    }

    public void setProducedQty(String producedQty) {
        this.producedQty = producedQty;
    }

    public String getRejectedQty() {
        return rejectedQty;
    }

    public void setRejectedQty(String rejectedQty) {
        this.rejectedQty = rejectedQty;
    }

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(indentId);
        dest.writeValue(indentRef);
        dest.writeValue(itemName);
        dest.writeValue(orderId);
        dest.writeValue(orderQty);
        dest.writeValue(producedQty);
        dest.writeValue(rejectedQty);
        dest.writeValue(timing);
    }

    public int describeContents() {
        return 0;
    }

}
