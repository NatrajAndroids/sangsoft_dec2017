package com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_header_info;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommonDataModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError=0;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg="";
    @SerializedName("strings")
    @Expose
    private CommonDataStrings strings;
    @SerializedName("jsonData")
    @Expose
    private IndentOverviewHeaderInfoJsonData jsonData;
    public final static Creator<CommonDataModal> CREATOR = new Creator<CommonDataModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CommonDataModal createFromParcel(Parcel in) {
            return new CommonDataModal(in);
        }

        public CommonDataModal[] newArray(int size) {
            return (new CommonDataModal[size]);
        }

    }
            ;

    protected CommonDataModal(Parcel in) {
        this.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((CommonDataStrings) in.readValue((CommonDataStrings.class.getClassLoader())));
        this.jsonData = ((IndentOverviewHeaderInfoJsonData) in.readValue((IndentOverviewHeaderInfoJsonData.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public CommonDataModal() {
    }

    /**
     *
     * @param strings
     * @param errorMsg
     * @param isError
     * @param jsonData
     */
    public CommonDataModal(Integer isError, String errorMsg, CommonDataStrings strings, IndentOverviewHeaderInfoJsonData jsonData) {
        super();
        this.isError = isError;
        this.errorMsg = errorMsg;
        this.strings = strings;
        this.jsonData = jsonData;
    }

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public CommonDataStrings getStrings() {
        return strings;
    }

    public void setStrings(CommonDataStrings strings) {
        this.strings = strings;
    }

    public IndentOverviewHeaderInfoJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(IndentOverviewHeaderInfoJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return 0;
    }

}