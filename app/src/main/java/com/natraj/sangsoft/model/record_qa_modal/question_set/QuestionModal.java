package com.natraj.sangsoft.model.record_qa_modal.question_set;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuestionModal implements Parcelable
{

    @SerializedName("current_field")
    @Expose
    private String currentField;
    @SerializedName("field_count")
    @Expose
    private String fieldCount;
    @SerializedName("lengths")
    @Expose
    private String lengths;
    @SerializedName("num_rows")
    @Expose
    private String numRows;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("QuestionSet")
    @Expose
    private List<QuestionSet> questionSet = new ArrayList<QuestionSet>();
    @SerializedName("LookupSet")
    @Expose
    private List<LookupSet> lookupSet = new ArrayList<LookupSet>();
    public final static Parcelable.Creator<QuestionModal> CREATOR = new Creator<QuestionModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public QuestionModal createFromParcel(Parcel in) {
            return new QuestionModal(in);
        }

        public QuestionModal[] newArray(int size) {
            return (new QuestionModal[size]);
        }

    }
            ;

    protected QuestionModal(Parcel in) {
        this.currentField = ((String) in.readValue((String.class.getClassLoader())));
        this.fieldCount = ((String) in.readValue((String.class.getClassLoader())));
        this.lengths = ((String) in.readValue((String.class.getClassLoader())));
        this.numRows = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.questionSet, (com.natraj.sangsoft.model.record_qa_modal.question_set.QuestionSet.class.getClassLoader()));
        in.readList(this.lookupSet, (com.natraj.sangsoft.model.record_qa_modal.question_set.LookupSet.class.getClassLoader()));
    }

    public QuestionModal() {
    }

    public String getCurrentField() {
        return currentField;
    }

    public void setCurrentField(String currentField) {
        this.currentField = currentField;
    }

    public String getFieldCount() {
        return fieldCount;
    }

    public void setFieldCount(String fieldCount) {
        this.fieldCount = fieldCount;
    }

    public String getLengths() {
        return lengths;
    }

    public void setLengths(String lengths) {
        this.lengths = lengths;
    }

    public String getNumRows() {
        return numRows;
    }

    public void setNumRows(String numRows) {
        this.numRows = numRows;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<QuestionSet> getQuestionSet() {
        return questionSet;
    }

    public void setQuestionSet(List<QuestionSet> questionSet) {
        this.questionSet = questionSet;
    }

    public List<LookupSet> getLookupSet() {
        return lookupSet;
    }

    public void setLookupSet(List<LookupSet> lookupSet) {
        this.lookupSet = lookupSet;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(currentField);
        dest.writeValue(fieldCount);
        dest.writeValue(lengths);
        dest.writeValue(numRows);
        dest.writeValue(type);
        dest.writeList(questionSet);
        dest.writeList(lookupSet);
    }

    public int describeContents() {
        return 0;
    }

}
