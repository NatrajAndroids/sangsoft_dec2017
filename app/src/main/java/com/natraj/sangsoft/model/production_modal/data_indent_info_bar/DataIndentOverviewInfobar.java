
package com.natraj.sangsoft.model.production_modal.data_indent_info_bar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataIndentOverviewInfobar implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError=0;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg="";
    @SerializedName("strings")
    @Expose
    private IndentinfoLabel strings;
    @SerializedName("jsonData")
    @Expose
    private IndentInfoData jsonData;
    @SerializedName("sample")
    @Expose
    private String sample="";
    public final static Creator<DataIndentOverviewInfobar> CREATOR = new Creator<DataIndentOverviewInfobar>() {


        @SuppressWarnings({
            "unchecked"
        })
        public DataIndentOverviewInfobar createFromParcel(Parcel in) {
            DataIndentOverviewInfobar instance = new DataIndentOverviewInfobar();
            instance.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
            instance.strings = ((IndentinfoLabel) in.readValue((IndentinfoLabel.class.getClassLoader())));
            instance.jsonData = ((IndentInfoData) in.readValue((IndentInfoData.class.getClassLoader())));
            instance.sample = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public DataIndentOverviewInfobar[] newArray(int size) {
            return (new DataIndentOverviewInfobar[size]);
        }

    }
    ;

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public IndentinfoLabel getStrings() {
        return strings;
    }

    public void setStrings(IndentinfoLabel strings) {
        this.strings = strings;
    }

    public IndentInfoData getJsonData() {
        return jsonData;
    }

    public void setJsonData(IndentInfoData jsonData) {
        this.jsonData = jsonData;
    }

    public String getSample() {
        return sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeValue(jsonData);
        dest.writeValue(sample);
    }

    public int describeContents() {
        return  0;
    }

}
