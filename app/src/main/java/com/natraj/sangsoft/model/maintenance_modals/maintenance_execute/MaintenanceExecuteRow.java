
package com.natraj.sangsoft.model.maintenance_modals.maintenance_execute;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaintenanceExecuteRow implements Parcelable
{

    @SerializedName("srNo")
    @Expose
    private Integer srNo=0;
    @SerializedName("SchActID")
    @Expose
    private String schActID="";
    @SerializedName("schID")
    @Expose
    private String schID="";
    @SerializedName("ActName")
    @Expose
    private String actName="";
    @SerializedName("ActStatus")
    @Expose
    private String actStatus="";
    public final static Creator<MaintenanceExecuteRow> CREATOR = new Creator<MaintenanceExecuteRow>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MaintenanceExecuteRow createFromParcel(Parcel in) {
            return new MaintenanceExecuteRow(in);
        }

        public MaintenanceExecuteRow[] newArray(int size) {
            return (new MaintenanceExecuteRow[size]);
        }

    }
            ;

    protected MaintenanceExecuteRow(Parcel in) {
        this.srNo = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.schActID = ((String) in.readValue((String.class.getClassLoader())));
        this.schID = ((String) in.readValue((String.class.getClassLoader())));
        this.actName = ((String) in.readValue((String.class.getClassLoader())));
        this.actStatus = ((String) in.readValue((String.class.getClassLoader())));
    }

    public MaintenanceExecuteRow() {
    }

    public Integer getSrNo() {
        return srNo;
    }

    public void setSrNo(Integer srNo) {
        this.srNo = srNo;
    }

    public String getSchActID() {
        return schActID;
    }

    public void setSchActID(String schActID) {
        this.schActID = schActID;
    }

    public String getSchID() {
        return schID;
    }

    public void setSchID(String schID) {
        this.schID = schID;
    }

    public String getActName() {
        return actName;
    }

    public void setActName(String actName) {
        this.actName = actName;
    }

    public String getActStatus() {
        return actStatus;
    }

    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(srNo);
        dest.writeValue(schActID);
        dest.writeValue(schID);
        dest.writeValue(actName);
        dest.writeValue(actStatus);
    }

    public int describeContents() {
        return 0;
    }

}