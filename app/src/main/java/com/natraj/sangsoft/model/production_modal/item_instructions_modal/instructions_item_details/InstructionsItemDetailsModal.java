
package com.natraj.sangsoft.model.production_modal.item_instructions_modal.instructions_item_details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InstructionsItemDetailsModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError=0;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg="";
    @SerializedName("strings")
    @Expose
    private InstructionsStrings strings;
    @SerializedName("jsonData")
    @Expose
    private InstructionsJsonData jsonData;
    public final static Creator<InstructionsItemDetailsModal> CREATOR = new Creator<InstructionsItemDetailsModal>() {


        @SuppressWarnings({
            "unchecked"
        })
        public InstructionsItemDetailsModal createFromParcel(Parcel in) {
            return new InstructionsItemDetailsModal(in);
        }

        public InstructionsItemDetailsModal[] newArray(int size) {
            return (new InstructionsItemDetailsModal[size]);
        }

    }
    ;

    protected InstructionsItemDetailsModal(Parcel in) {
        this.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((InstructionsStrings) in.readValue((InstructionsStrings.class.getClassLoader())));
        this.jsonData = ((InstructionsJsonData) in.readValue((InstructionsJsonData.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public InstructionsItemDetailsModal() {
    }

    /**
     * 
     * @param strings
     * @param errorMsg
     * @param isError
     * @param jsonData
     */
    public InstructionsItemDetailsModal(Integer isError, String errorMsg, InstructionsStrings strings, InstructionsJsonData jsonData) {
        super();
        this.isError = isError;
        this.errorMsg = errorMsg;
        this.strings = strings;
        this.jsonData = jsonData;
    }

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public InstructionsStrings getStrings() {
        return strings;
    }

    public void setStrings(InstructionsStrings strings) {
        this.strings = strings;
    }

    public InstructionsJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(InstructionsJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return  0;
    }

}
