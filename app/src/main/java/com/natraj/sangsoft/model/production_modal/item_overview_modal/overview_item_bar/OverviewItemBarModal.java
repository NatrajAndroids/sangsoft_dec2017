
package com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_bar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OverviewItemBarModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError=0;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg="";
    @SerializedName("strings")
    @Expose
    private OverviewItemBarStrings strings;
    @SerializedName("jsonData")
    @Expose
    private OverviewItemBarJsonData jsonData;
    public final static Creator<OverviewItemBarModal> CREATOR = new Creator<OverviewItemBarModal>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OverviewItemBarModal createFromParcel(Parcel in) {
            return new OverviewItemBarModal(in);
        }

        public OverviewItemBarModal[] newArray(int size) {
            return (new OverviewItemBarModal[size]);
        }

    }
    ;

    protected OverviewItemBarModal(Parcel in) {
        this.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((OverviewItemBarStrings) in.readValue((OverviewItemBarStrings.class.getClassLoader())));
        this.jsonData = ((OverviewItemBarJsonData) in.readValue((OverviewItemBarJsonData.class.getClassLoader())));
    }

    public OverviewItemBarModal() {
    }

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public OverviewItemBarStrings getStrings() {
        return strings;
    }

    public void setStrings(OverviewItemBarStrings strings) {
        this.strings = strings;
    }

    public OverviewItemBarJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(OverviewItemBarJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return  0;
    }

}
