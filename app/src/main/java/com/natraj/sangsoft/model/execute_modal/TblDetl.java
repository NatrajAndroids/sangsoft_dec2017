
package com.natraj.sangsoft.model.execute_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TblDetl implements Parcelable
{

    @SerializedName("rows")
    @Expose
    private List<TblRow> rows = new ArrayList<TblRow>();
    public final static Creator<TblDetl> CREATOR = new Creator<TblDetl>() {


        @SuppressWarnings({
                "unchecked"
        })
        public TblDetl createFromParcel(Parcel in) {
            return new TblDetl(in);
        }

        public TblDetl[] newArray(int size) {
            return (new TblDetl[size]);
        }

    }
            ;

    protected TblDetl(Parcel in) {
        in.readList(this.rows, (TblRow.class.getClassLoader()));
    }

    public TblDetl() {
    }

    public List<TblRow> getRows() {
        return rows;
    }

    public void setRows(List<TblRow> rows) {
        this.rows = rows;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(rows);
    }

    public int describeContents() {
        return 0;
    }

}