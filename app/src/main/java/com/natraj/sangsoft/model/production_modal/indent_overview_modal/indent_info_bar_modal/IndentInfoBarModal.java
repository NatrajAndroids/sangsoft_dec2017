
package com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_info_bar_modal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IndentInfoBarModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private String isError;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;
    @SerializedName("strings")
    @Expose
    private Strings strings;
    @SerializedName("jsonData")
    @Expose
    private JsonData jsonData;
    public final static Parcelable.Creator<IndentInfoBarModal> CREATOR = new Creator<IndentInfoBarModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public IndentInfoBarModal createFromParcel(Parcel in) {
            return new IndentInfoBarModal(in);
        }

        public IndentInfoBarModal[] newArray(int size) {
            return (new IndentInfoBarModal[size]);
        }

    }
            ;

    protected IndentInfoBarModal(Parcel in) {
        this.isError = ((String) in.readValue((String.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((Strings) in.readValue((Strings.class.getClassLoader())));
        this.jsonData = ((JsonData) in.readValue((JsonData.class.getClassLoader())));
    }

    public IndentInfoBarModal() {
    }

    public String getIsError() {
        return isError;
    }

    public void setIsError(String isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Strings getStrings() {
        return strings;
    }

    public void setStrings(Strings strings) {
        this.strings = strings;
    }

    public JsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(JsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return 0;
    }

}
