package com.natraj.sangsoft.model.maintenance_modals.maintenance_action_bar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MActionBarStrings implements Parcelable
{

    @SerializedName("btnActList")
    @Expose
    private String btnActList="";
    @SerializedName("btnBreakDown")
    @Expose
    private String btnBreakDown="";
    @SerializedName("btnExecute")
    @Expose
    private String btnExecute="";
    @SerializedName("btnView")
    @Expose
    private String btnView="";
    public final static Creator<MActionBarStrings> CREATOR = new Creator<MActionBarStrings>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MActionBarStrings createFromParcel(Parcel in) {
            return new MActionBarStrings(in);
        }

        public MActionBarStrings[] newArray(int size) {
            return (new MActionBarStrings[size]);
        }

    }
            ;

    protected MActionBarStrings(Parcel in) {
        this.btnActList = ((String) in.readValue((String.class.getClassLoader())));
        this.btnBreakDown = ((String) in.readValue((String.class.getClassLoader())));
        this.btnExecute = ((String) in.readValue((String.class.getClassLoader())));
        this.btnView = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public MActionBarStrings() {
    }

    /**
     *
     * @param btnExecute
     * @param btnBreakDown
     * @param btnActList
     * @param btnView
     */
    public MActionBarStrings(String btnActList, String btnBreakDown, String btnExecute, String btnView) {
        super();
        this.btnActList = btnActList;
        this.btnBreakDown = btnBreakDown;
        this.btnExecute = btnExecute;
        this.btnView = btnView;
    }

    public String getBtnActList() {
        return btnActList;
    }

    public void setBtnActList(String btnActList) {
        this.btnActList = btnActList;
    }

    public MActionBarStrings withBtnActList(String btnActList) {
        this.btnActList = btnActList;
        return this;
    }

    public String getBtnBreakDown() {
        return btnBreakDown;
    }

    public void setBtnBreakDown(String btnBreakDown) {
        this.btnBreakDown = btnBreakDown;
    }

    public MActionBarStrings withBtnBreakDown(String btnBreakDown) {
        this.btnBreakDown = btnBreakDown;
        return this;
    }

    public String getBtnExecute() {
        return btnExecute;
    }

    public void setBtnExecute(String btnExecute) {
        this.btnExecute = btnExecute;
    }

    public MActionBarStrings withBtnExecute(String btnExecute) {
        this.btnExecute = btnExecute;
        return this;
    }

    public String getBtnView() {
        return btnView;
    }

    public void setBtnView(String btnView) {
        this.btnView = btnView;
    }

    public MActionBarStrings withBtnView(String btnView) {
        this.btnView = btnView;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(btnActList);
        dest.writeValue(btnBreakDown);
        dest.writeValue(btnExecute);
        dest.writeValue(btnView);
    }

    public int describeContents() {
        return 0;
    }

}