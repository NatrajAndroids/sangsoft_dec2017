package com.natraj.sangsoft.model.blocker_modal.load;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoadBlockerJsonData implements Parcelable
{

    @SerializedName("sch_id")
    @Expose
    private String schId;
    @SerializedName("from_Time")
    @Expose
    private String fromTime;
    @SerializedName("till_Time")
    @Expose
    private String tillTime;
    public final static Parcelable.Creator<LoadBlockerJsonData> CREATOR = new Creator<LoadBlockerJsonData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LoadBlockerJsonData createFromParcel(Parcel in) {
            return new LoadBlockerJsonData(in);
        }

        public LoadBlockerJsonData[] newArray(int size) {
            return (new LoadBlockerJsonData[size]);
        }

    }
            ;

    protected LoadBlockerJsonData(Parcel in) {
        this.schId = ((String) in.readValue((String.class.getClassLoader())));
        this.fromTime = ((String) in.readValue((String.class.getClassLoader())));
        this.tillTime = ((String) in.readValue((String.class.getClassLoader())));
    }

    public LoadBlockerJsonData() {
    }

    public String getSchId() {
        return schId;
    }

    public void setSchId(String schId) {
        this.schId = schId;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getTillTime() {
        return tillTime;
    }

    public void setTillTime(String tillTime) {
        this.tillTime = tillTime;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(schId);
        dest.writeValue(fromTime);
        dest.writeValue(tillTime);
    }

    public int describeContents() {
        return 0;
    }

}
