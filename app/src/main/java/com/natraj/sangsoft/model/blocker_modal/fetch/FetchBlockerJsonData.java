package com.natraj.sangsoft.model.blocker_modal.fetch;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FetchBlockerJsonData implements Parcelable
{

    @SerializedName("sch_count")
    @Expose
    private String schCount;
    @SerializedName("machine_id")
    @Expose
    private String machineId;
    @SerializedName("sch_id")
    @Expose
    private String schId;
    @SerializedName("post_time")
    @Expose
    private String postTime;
    public final static Parcelable.Creator<FetchBlockerJsonData> CREATOR = new Creator<FetchBlockerJsonData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public FetchBlockerJsonData createFromParcel(Parcel in) {
            return new FetchBlockerJsonData(in);
        }

        public FetchBlockerJsonData[] newArray(int size) {
            return (new FetchBlockerJsonData[size]);
        }

    }
            ;

    protected FetchBlockerJsonData(Parcel in) {
        this.schCount = ((String) in.readValue((String.class.getClassLoader())));
        this.machineId = ((String) in.readValue((String.class.getClassLoader())));
        this.schId = ((String) in.readValue((String.class.getClassLoader())));
        this.postTime = ((String) in.readValue((String.class.getClassLoader())));
    }

    public FetchBlockerJsonData() {
    }

    public String getSchCount() {
        return schCount;
    }

    public void setSchCount(String schCount) {
        this.schCount = schCount;
    }

    public String getMachineId() {
        return machineId;
    }

    public void setMachineId(String machineId) {
        this.machineId = machineId;
    }

    public String getSchId() {
        return schId;
    }

    public void setSchId(String schId) {
        this.schId = schId;
    }

    public String getPostTime() {
        return postTime;
    }

    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(schCount);
        dest.writeValue(machineId);
        dest.writeValue(schId);
        dest.writeValue(postTime);
    }

    public int describeContents() {
        return 0;
    }

}