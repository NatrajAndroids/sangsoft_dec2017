
package com.natraj.sangsoft.model.production_modal.data_indent_info_bar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IndentInfoData implements Parcelable
{

    @SerializedName("indentCount")
    @Expose
    private String indentCount="";
    @SerializedName("indents")
    @Expose
    private Object indents;
    @SerializedName("values")
    @Expose
    private String values="";
    public final static Creator<IndentInfoData> CREATOR = new Creator<IndentInfoData>() {


        @SuppressWarnings({
            "unchecked"
        })
        public IndentInfoData createFromParcel(Parcel in) {
            IndentInfoData instance = new IndentInfoData();
            instance.indentCount = ((String) in.readValue((String.class.getClassLoader())));
            instance.indents = ((Object) in.readValue((Object.class.getClassLoader())));
            instance.values = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public IndentInfoData[] newArray(int size) {
            return (new IndentInfoData[size]);
        }

    }
    ;

    public String getIndentCount() {
        return indentCount;
    }

    public void setIndentCount(String indentCount) {
        this.indentCount = indentCount;
    }

    public Object getIndents() {
        return indents;
    }

    public void setIndents(Object indents) {
        this.indents = indents;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(indentCount);
        dest.writeValue(indents);
        dest.writeValue(values);
    }

    public int describeContents() {
        return  0;
    }

}
