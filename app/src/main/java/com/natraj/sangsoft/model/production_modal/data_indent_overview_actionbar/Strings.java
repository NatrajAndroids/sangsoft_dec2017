
package com.natraj.sangsoft.model.production_modal.data_indent_overview_actionbar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Strings implements Parcelable
{

    @SerializedName("tooltipIndentOverview")
    @Expose
    private String tooltipIndentOverview="";
    @SerializedName("btnAction")
    @Expose
    private String btnAction="";
    @SerializedName("btnPauseResume")
    @Expose
    private String btnPauseResume="";
    @SerializedName("tooltipProgress")
    @Expose
    private String tooltipProgress="";
    @SerializedName("btnClose")
    @Expose
    private String btnClose="";
    @SerializedName("tooltipClose")
    @Expose
    private String tooltipClose="";
    @SerializedName("btnReceiveRawMaterial")
    @Expose
    private String btnReceiveRawMaterial="";
    @SerializedName("btnRequestQA")
    @Expose
    private String btnRequestQA="";
    @SerializedName("btnRecordProduction")
    @Expose
    private String btnRecordProduction="";
    @SerializedName("btnMarkAsComplete")
    @Expose
    private String btnMarkAsComplete="";
    @SerializedName("btnIndentProgress")
    @Expose
    private String btnIndentProgress="";
    @SerializedName("btnItemOverview")
    @Expose
    private String btnItemOverview="";
    @SerializedName("btnItemInstructions")
    @Expose
    private String btnItemInstructions="";
    @SerializedName("btnItemImagesVideos")
    @Expose
    private String btnItemImagesVideos="";
    @SerializedName("btnItemOthers")
    @Expose
    private String btnItemOthers="";
    @SerializedName("lblRecords")
    @Expose
    private String lblRecords="";
    @SerializedName("tooltipRecords")
    @Expose
    private String tooltipRecords="";
    public final static Creator<Strings> CREATOR = new Creator<Strings>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Strings createFromParcel(Parcel in) {
            return new Strings(in);
        }

        public Strings[] newArray(int size) {
            return (new Strings[size]);
        }

    }
            ;

    protected Strings(Parcel in) {
        this.tooltipIndentOverview = ((String) in.readValue((String.class.getClassLoader())));
        this.btnAction = ((String) in.readValue((String.class.getClassLoader())));
        this.btnPauseResume = ((String) in.readValue((String.class.getClassLoader())));
        this.tooltipProgress = ((String) in.readValue((String.class.getClassLoader())));
        this.btnClose = ((String) in.readValue((String.class.getClassLoader())));
        this.tooltipClose = ((String) in.readValue((String.class.getClassLoader())));
        this.btnReceiveRawMaterial = ((String) in.readValue((String.class.getClassLoader())));
        this.btnRequestQA = ((String) in.readValue((String.class.getClassLoader())));
        this.btnRecordProduction = ((String) in.readValue((String.class.getClassLoader())));
        this.btnMarkAsComplete = ((String) in.readValue((String.class.getClassLoader())));
        this.btnIndentProgress = ((String) in.readValue((String.class.getClassLoader())));
        this.btnItemOverview = ((String) in.readValue((String.class.getClassLoader())));
        this.btnItemInstructions = ((String) in.readValue((String.class.getClassLoader())));
        this.btnItemImagesVideos = ((String) in.readValue((String.class.getClassLoader())));
        this.btnItemOthers = ((String) in.readValue((String.class.getClassLoader())));
        this.lblRecords = ((String) in.readValue((String.class.getClassLoader())));
        this.tooltipRecords = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Strings() {
    }

    /**
     *
     * @param btnReceiveRawMaterial
     * @param tooltipClose
     * @param btnRequestQA
     * @param btnMarkAsComplete
     * @param btnItemImagesVideos
     * @param lblRecords
     * @param btnClose
     * @param tooltipProgress
     * @param btnIndentProgress
     * @param btnAction
     * @param btnItemInstructions
     * @param btnPauseResume
     * @param tooltipRecords
     * @param tooltipIndentOverview
     * @param btnItemOthers
     * @param btnRecordProduction
     * @param btnItemOverview
     */
    public Strings(String tooltipIndentOverview, String btnAction, String btnPauseResume, String tooltipProgress, String btnClose, String tooltipClose, String btnReceiveRawMaterial, String btnRequestQA, String btnRecordProduction, String btnMarkAsComplete, String btnIndentProgress, String btnItemOverview, String btnItemInstructions, String btnItemImagesVideos, String btnItemOthers, String lblRecords, String tooltipRecords) {
        super();
        this.tooltipIndentOverview = tooltipIndentOverview;
        this.btnAction = btnAction;
        this.btnPauseResume = btnPauseResume;
        this.tooltipProgress = tooltipProgress;
        this.btnClose = btnClose;
        this.tooltipClose = tooltipClose;
        this.btnReceiveRawMaterial = btnReceiveRawMaterial;
        this.btnRequestQA = btnRequestQA;
        this.btnRecordProduction = btnRecordProduction;
        this.btnMarkAsComplete = btnMarkAsComplete;
        this.btnIndentProgress = btnIndentProgress;
        this.btnItemOverview = btnItemOverview;
        this.btnItemInstructions = btnItemInstructions;
        this.btnItemImagesVideos = btnItemImagesVideos;
        this.btnItemOthers = btnItemOthers;
        this.lblRecords = lblRecords;
        this.tooltipRecords = tooltipRecords;
    }

    public String getTooltipIndentOverview() {
        return tooltipIndentOverview;
    }

    public void setTooltipIndentOverview(String tooltipIndentOverview) {
        this.tooltipIndentOverview = tooltipIndentOverview;
    }

    public String getBtnAction() {
        return btnAction;
    }

    public void setBtnAction(String btnAction) {
        this.btnAction = btnAction;
    }

    public String getBtnPauseResume() {
        return btnPauseResume;
    }

    public void setBtnPauseResume(String btnPauseResume) {
        this.btnPauseResume = btnPauseResume;
    }

    public String getTooltipProgress() {
        return tooltipProgress;
    }

    public void setTooltipProgress(String tooltipProgress) {
        this.tooltipProgress = tooltipProgress;
    }

    public String getBtnClose() {
        return btnClose;
    }

    public void setBtnClose(String btnClose) {
        this.btnClose = btnClose;
    }

    public String getTooltipClose() {
        return tooltipClose;
    }

    public void setTooltipClose(String tooltipClose) {
        this.tooltipClose = tooltipClose;
    }

    public String getBtnReceiveRawMaterial() {
        return btnReceiveRawMaterial;
    }

    public void setBtnReceiveRawMaterial(String btnReceiveRawMaterial) {
        this.btnReceiveRawMaterial = btnReceiveRawMaterial;
    }

    public String getBtnRequestQA() {
        return btnRequestQA;
    }

    public void setBtnRequestQA(String btnRequestQA) {
        this.btnRequestQA = btnRequestQA;
    }

    public String getBtnRecordProduction() {
        return btnRecordProduction;
    }

    public void setBtnRecordProduction(String btnRecordProduction) {
        this.btnRecordProduction = btnRecordProduction;
    }

    public String getBtnMarkAsComplete() {
        return btnMarkAsComplete;
    }

    public void setBtnMarkAsComplete(String btnMarkAsComplete) {
        this.btnMarkAsComplete = btnMarkAsComplete;
    }

    public String getBtnIndentProgress() {
        return btnIndentProgress;
    }

    public void setBtnIndentProgress(String btnIndentProgress) {
        this.btnIndentProgress = btnIndentProgress;
    }

    public String getBtnItemOverview() {
        return btnItemOverview;
    }

    public void setBtnItemOverview(String btnItemOverview) {
        this.btnItemOverview = btnItemOverview;
    }

    public String getBtnItemInstructions() {
        return btnItemInstructions;
    }

    public void setBtnItemInstructions(String btnItemInstructions) {
        this.btnItemInstructions = btnItemInstructions;
    }

    public String getBtnItemImagesVideos() {
        return btnItemImagesVideos;
    }

    public void setBtnItemImagesVideos(String btnItemImagesVideos) {
        this.btnItemImagesVideos = btnItemImagesVideos;
    }

    public String getBtnItemOthers() {
        return btnItemOthers;
    }

    public void setBtnItemOthers(String btnItemOthers) {
        this.btnItemOthers = btnItemOthers;
    }

    public String getLblRecords() {
        return lblRecords;
    }

    public void setLblRecords(String lblRecords) {
        this.lblRecords = lblRecords;
    }

    public String getTooltipRecords() {
        return tooltipRecords;
    }

    public void setTooltipRecords(String tooltipRecords) {
        this.tooltipRecords = tooltipRecords;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(tooltipIndentOverview);
        dest.writeValue(btnAction);
        dest.writeValue(btnPauseResume);
        dest.writeValue(tooltipProgress);
        dest.writeValue(btnClose);
        dest.writeValue(tooltipClose);
        dest.writeValue(btnReceiveRawMaterial);
        dest.writeValue(btnRequestQA);
        dest.writeValue(btnRecordProduction);
        dest.writeValue(btnMarkAsComplete);
        dest.writeValue(btnIndentProgress);
        dest.writeValue(btnItemOverview);
        dest.writeValue(btnItemInstructions);
        dest.writeValue(btnItemImagesVideos);
        dest.writeValue(btnItemOthers);
        dest.writeValue(lblRecords);
        dest.writeValue(tooltipRecords);
    }

    public int describeContents() {
        return 0;
    }

}