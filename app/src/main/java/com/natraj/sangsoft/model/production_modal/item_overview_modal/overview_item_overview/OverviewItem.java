
package com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_overview;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OverviewItem implements Parcelable
{

    @SerializedName("item_type")
    @Expose
    private String itemType="";
    @SerializedName("item_mcode")
    @Expose
    private String itemMcode="";
    @SerializedName("item_name")
    @Expose
    private String itemName="";
    @SerializedName("input_qty")
    @Expose
    private String inputQty="";
    @SerializedName("preQty")
    @Expose
    private String preQty="";
    @SerializedName("input_uom")
    @Expose
    private String inputUom="";
    @SerializedName("item_id")
    @Expose
    private String itemId="";
    public final static Creator<OverviewItem> CREATOR = new Creator<OverviewItem>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OverviewItem createFromParcel(Parcel in) {
            return new OverviewItem(in);
        }

        public OverviewItem[] newArray(int size) {
            return (new OverviewItem[size]);
        }

    }
            ;

    protected OverviewItem(Parcel in) {
        this.itemType = ((String) in.readValue((String.class.getClassLoader())));
        this.itemMcode = ((String) in.readValue((String.class.getClassLoader())));
        this.itemName = ((String) in.readValue((String.class.getClassLoader())));
        this.inputQty = ((String) in.readValue((String.class.getClassLoader())));
        this.preQty = ((String) in.readValue((String.class.getClassLoader())));
        this.inputUom = ((String) in.readValue((String.class.getClassLoader())));
        this.itemId = ((String) in.readValue((String.class.getClassLoader())));
    }

    public OverviewItem() {
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getItemMcode() {
        return itemMcode;
    }

    public void setItemMcode(String itemMcode) {
        this.itemMcode = itemMcode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getInputQty() {
        return inputQty;
    }

    public void setInputQty(String inputQty) {
        this.inputQty = inputQty;
    }

    public String getPreQty() {
        return preQty;
    }

    public void setPreQty(String preQty) {
        this.preQty = preQty;
    }

    public String getInputUom() {
        return inputUom;
    }

    public void setInputUom(String inputUom) {
        this.inputUom = inputUom;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(itemType);
        dest.writeValue(itemMcode);
        dest.writeValue(itemName);
        dest.writeValue(inputQty);
        dest.writeValue(preQty);
        dest.writeValue(inputUom);
        dest.writeValue(itemId);
    }

    public int describeContents() {
        return 0;
    }

}