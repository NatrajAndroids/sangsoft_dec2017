
package com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_indent_item_list;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OverviewIndentJsonData implements Parcelable
{

    @SerializedName("items")
    @Expose
    private List<OverviewIndentItem> items = new ArrayList<OverviewIndentItem>();
    public final static Creator<OverviewIndentJsonData> CREATOR = new Creator<OverviewIndentJsonData>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OverviewIndentJsonData createFromParcel(Parcel in) {
            return new OverviewIndentJsonData(in);
        }

        public OverviewIndentJsonData[] newArray(int size) {
            return (new OverviewIndentJsonData[size]);
        }

    }
    ;

    protected OverviewIndentJsonData(Parcel in) {
        in.readList(this.items, (OverviewIndentItem.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public OverviewIndentJsonData() {
    }

    /**
     * 
     * @param items
     */
    public OverviewIndentJsonData(List<OverviewIndentItem> items) {
        super();
        this.items = items;
    }

    public List<OverviewIndentItem> getItems() {
        return items;
    }

    public void setItems(List<OverviewIndentItem> items) {
        this.items = items;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(items);
    }

    public int describeContents() {
        return  0;
    }

}
