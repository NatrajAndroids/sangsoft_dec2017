
package com.natraj.sangsoft.model.production_modal.item_instructions_modal.instructions_item_details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InstructionsRow implements Parcelable
{

    @SerializedName("info_id")
    @Expose
    private String infoId="";
    @SerializedName("caption")
    @Expose
    private String caption="";
    @SerializedName("description")
    @Expose
    private String description="";
    @SerializedName("info_type")
    @Expose
    private String infoType="";
    @SerializedName("file_ID")
    @Expose
    private String fileID="";
    @SerializedName("file_name")
    @Expose
    private String fileName="";
    @SerializedName("file_location")
    @Expose
    private String fileLocation="";
    @SerializedName("srNo")
    @Expose
    private Integer srNo=0;
    public final static Creator<InstructionsRow> CREATOR = new Creator<InstructionsRow>() {


        @SuppressWarnings({
                "unchecked"
        })
        public InstructionsRow createFromParcel(Parcel in) {
            return new InstructionsRow(in);
        }

        public InstructionsRow[] newArray(int size) {
            return (new InstructionsRow[size]);
        }

    }
            ;

    protected InstructionsRow(Parcel in) {
        this.infoId = ((String) in.readValue((String.class.getClassLoader())));
        this.caption = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.infoType = ((String) in.readValue((String.class.getClassLoader())));
        this.fileID = ((String) in.readValue((String.class.getClassLoader())));
        this.fileName = ((String) in.readValue((String.class.getClassLoader())));
        this.fileLocation = ((String) in.readValue((String.class.getClassLoader())));
        this.srNo = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public InstructionsRow() {
    }

    /**
     *
     * @param fileLocation
     * @param fileID
     * @param infoType
     * @param description
     * @param fileName
     * @param caption
     * @param infoId
     * @param srNo
     */
    public InstructionsRow(String infoId, String caption, String description, String infoType, String fileID, String fileName, String fileLocation, Integer srNo) {
        super();
        this.infoId = infoId;
        this.caption = caption;
        this.description = description;
        this.infoType = infoType;
        this.fileID = fileID;
        this.fileName = fileName;
        this.fileLocation = fileLocation;
        this.srNo = srNo;
    }

    public String getInfoId() {
        return infoId;
    }

    public void setInfoId(String infoId) {
        this.infoId = infoId;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInfoType() {
        return infoType;
    }

    public void setInfoType(String infoType) {
        this.infoType = infoType;
    }

    public String getFileID() {
        return fileID;
    }

    public void setFileID(String fileID) {
        this.fileID = fileID;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public Integer getSrNo() {
        return srNo;
    }

    public void setSrNo(Integer srNo) {
        this.srNo = srNo;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(infoId);
        dest.writeValue(caption);
        dest.writeValue(description);
        dest.writeValue(infoType);
        dest.writeValue(fileID);
        dest.writeValue(fileName);
        dest.writeValue(fileLocation);
        dest.writeValue(srNo);
    }

    public int describeContents() {
        return 0;
    }

}
