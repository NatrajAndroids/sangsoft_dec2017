
package com.natraj.sangsoft.model.production_modal.data_indent_overview;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Strings implements Parcelable
{

    @SerializedName("areaTitle")
    @Expose
    private String areaTitle;
    @SerializedName("mnuPause")
    @Expose
    private String mnuPause;
    @SerializedName("lblIndentQty")
    @Expose
    private String lblIndentQty;
    @SerializedName("lblProduced")
    @Expose
    private String lblProduced;
    @SerializedName("lblRejected")
    @Expose
    private String lblRejected;
    @SerializedName("lblBalance")
    @Expose
    private String lblBalance;
    @SerializedName("lblProdTime")
    @Expose
    private String lblProdTime;
    @SerializedName("lblprodRate")
    @Expose
    private String lblprodRate;
    @SerializedName("mnuReceiveRM")
    @Expose
    private String mnuReceiveRM;
    @SerializedName("mnuRequestQA")
    @Expose
    private String mnuRequestQA;
    @SerializedName("mnuRecordProd")
    @Expose
    private String mnuRecordProd;
    @SerializedName("mnuComplete")
    @Expose
    private String mnuComplete;
    @SerializedName("mnuClose")
    @Expose
    private String mnuClose;
    public final static Parcelable.Creator<Strings> CREATOR = new Creator<Strings>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Strings createFromParcel(Parcel in) {
            return new Strings(in);
        }

        public Strings[] newArray(int size) {
            return (new Strings[size]);
        }

    }
            ;

    protected Strings(Parcel in) {
        this.areaTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.mnuPause = ((String) in.readValue((String.class.getClassLoader())));
        this.lblIndentQty = ((String) in.readValue((String.class.getClassLoader())));
        this.lblProduced = ((String) in.readValue((String.class.getClassLoader())));
        this.lblRejected = ((String) in.readValue((String.class.getClassLoader())));
        this.lblBalance = ((String) in.readValue((String.class.getClassLoader())));
        this.lblProdTime = ((String) in.readValue((String.class.getClassLoader())));
        this.lblprodRate = ((String) in.readValue((String.class.getClassLoader())));
        this.mnuReceiveRM = ((String) in.readValue((String.class.getClassLoader())));
        this.mnuRequestQA = ((String) in.readValue((String.class.getClassLoader())));
        this.mnuRecordProd = ((String) in.readValue((String.class.getClassLoader())));
        this.mnuComplete = ((String) in.readValue((String.class.getClassLoader())));
        this.mnuClose = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Strings() {
    }

    public String getAreaTitle() {
        return areaTitle;
    }

    public void setAreaTitle(String areaTitle) {
        this.areaTitle = areaTitle;
    }

    public String getMnuPause() {
        return mnuPause;
    }

    public void setMnuPause(String mnuPause) {
        this.mnuPause = mnuPause;
    }

    public String getLblIndentQty() {
        return lblIndentQty;
    }

    public void setLblIndentQty(String lblIndentQty) {
        this.lblIndentQty = lblIndentQty;
    }

    public String getLblProduced() {
        return lblProduced;
    }

    public void setLblProduced(String lblProduced) {
        this.lblProduced = lblProduced;
    }

    public String getLblRejected() {
        return lblRejected;
    }

    public void setLblRejected(String lblRejected) {
        this.lblRejected = lblRejected;
    }

    public String getLblBalance() {
        return lblBalance;
    }

    public void setLblBalance(String lblBalance) {
        this.lblBalance = lblBalance;
    }

    public String getLblProdTime() {
        return lblProdTime;
    }

    public void setLblProdTime(String lblProdTime) {
        this.lblProdTime = lblProdTime;
    }

    public String getLblprodRate() {
        return lblprodRate;
    }

    public void setLblprodRate(String lblprodRate) {
        this.lblprodRate = lblprodRate;
    }

    public String getMnuReceiveRM() {
        return mnuReceiveRM;
    }

    public void setMnuReceiveRM(String mnuReceiveRM) {
        this.mnuReceiveRM = mnuReceiveRM;
    }

    public String getMnuRequestQA() {
        return mnuRequestQA;
    }

    public void setMnuRequestQA(String mnuRequestQA) {
        this.mnuRequestQA = mnuRequestQA;
    }

    public String getMnuRecordProd() {
        return mnuRecordProd;
    }

    public void setMnuRecordProd(String mnuRecordProd) {
        this.mnuRecordProd = mnuRecordProd;
    }

    public String getMnuComplete() {
        return mnuComplete;
    }

    public void setMnuComplete(String mnuComplete) {
        this.mnuComplete = mnuComplete;
    }

    public String getMnuClose() {
        return mnuClose;
    }

    public void setMnuClose(String mnuClose) {
        this.mnuClose = mnuClose;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(areaTitle);
        dest.writeValue(mnuPause);
        dest.writeValue(lblIndentQty);
        dest.writeValue(lblProduced);
        dest.writeValue(lblRejected);
        dest.writeValue(lblBalance);
        dest.writeValue(lblProdTime);
        dest.writeValue(lblprodRate);
        dest.writeValue(mnuReceiveRM);
        dest.writeValue(mnuRequestQA);
        dest.writeValue(mnuRecordProd);
        dest.writeValue(mnuComplete);
        dest.writeValue(mnuClose);
    }

    public int describeContents() {
        return 0;
    }

}