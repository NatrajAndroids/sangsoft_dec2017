package com.natraj.sangsoft.model.production_modal.item_image_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemImageModal implements Parcelable {

    public final static Creator<ItemImageModal> CREATOR = new Creator<ItemImageModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ItemImageModal createFromParcel(Parcel in) {
            return new ItemImageModal(in);
        }

        public ItemImageModal[] newArray(int size) {
            return (new ItemImageModal[size]);
        }

    };
    @SerializedName("name")
    @Expose
    private String name="";
    @SerializedName("url")
    @Expose
    private String url="";

    protected ItemImageModal(Parcel in) {
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.url = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ItemImageModal() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
        dest.writeValue(url);
    }

    public int describeContents() {
        return 0;
    }

}
