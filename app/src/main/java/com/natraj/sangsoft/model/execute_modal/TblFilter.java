
package com.natraj.sangsoft.model.execute_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TblFilter implements Parcelable
{

    @SerializedName("machine_id")
    @Expose
    private String machineId="";
    @SerializedName("schedule_activity_id")
    @Expose
    private String scheduleActivityId="";
    @SerializedName("schedule_id")
    @Expose
    private String scheduleId="";
    public final static Creator<TblFilter> CREATOR = new Creator<TblFilter>() {


        @SuppressWarnings({
                "unchecked"
        })
        public TblFilter createFromParcel(Parcel in) {
            return new TblFilter(in);
        }

        public TblFilter[] newArray(int size) {
            return (new TblFilter[size]);
        }

    }
            ;

    protected TblFilter(Parcel in) {
        this.machineId = ((String) in.readValue((String.class.getClassLoader())));
        this.scheduleActivityId = ((String) in.readValue((String.class.getClassLoader())));
        this.scheduleId = ((String) in.readValue((String.class.getClassLoader())));
    }

    public TblFilter() {
    }

    public String getMachineId() {
        return machineId;
    }

    public void setMachineId(String machineId) {
        this.machineId = machineId;
    }

    public String getScheduleActivityId() {
        return scheduleActivityId;
    }

    public void setScheduleActivityId(String scheduleActivityId) {
        this.scheduleActivityId = scheduleActivityId;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(machineId);
        dest.writeValue(scheduleActivityId);
        dest.writeValue(scheduleId);
    }

    public int describeContents() {
        return 0;
    }

}