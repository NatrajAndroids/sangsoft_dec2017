
package com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_bar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OverviewItemBarJsonData implements Parcelable
{

    @SerializedName("item_id")
    @Expose
    private String itemId="";
    @SerializedName("item_mcode")
    @Expose
    private String itemMcode="";
    @SerializedName("item_name")
    @Expose
    private String itemName="";
    @SerializedName("item_type")
    @Expose
    private String itemType="";
    public final static Creator<OverviewItemBarJsonData> CREATOR = new Creator<OverviewItemBarJsonData>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OverviewItemBarJsonData createFromParcel(Parcel in) {
            return new OverviewItemBarJsonData(in);
        }

        public OverviewItemBarJsonData[] newArray(int size) {
            return (new OverviewItemBarJsonData[size]);
        }

    }
    ;

    protected OverviewItemBarJsonData(Parcel in) {
        this.itemId = ((String) in.readValue((String.class.getClassLoader())));
        this.itemMcode = ((String) in.readValue((String.class.getClassLoader())));
        this.itemName = ((String) in.readValue((String.class.getClassLoader())));
        this.itemType = ((String) in.readValue((String.class.getClassLoader())));
    }

    public OverviewItemBarJsonData() {
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemMcode() {
        return itemMcode;
    }

    public void setItemMcode(String itemMcode) {
        this.itemMcode = itemMcode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(itemId);
        dest.writeValue(itemMcode);
        dest.writeValue(itemName);
        dest.writeValue(itemType);
    }

    public int describeContents() {
        return  0;
    }

}
