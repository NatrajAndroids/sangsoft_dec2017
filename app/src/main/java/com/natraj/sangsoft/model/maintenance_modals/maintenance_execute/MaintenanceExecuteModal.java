
package com.natraj.sangsoft.model.maintenance_modals.maintenance_execute;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MaintenanceExecuteModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError=0;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg="";
    @SerializedName("strings")
    @Expose
    private MaintenanceExecuteStrings strings;
    @SerializedName("eventStatus_rows")
    @Expose
    private List<MaintenanceExecuteEventStatusRow> eventStatusRows = new ArrayList<MaintenanceExecuteEventStatusRow>();
    @SerializedName("jsonData")
    @Expose
    private MaintenanceExecuteJsonData jsonData;
    public final static Creator<MaintenanceExecuteModal> CREATOR = new Creator<MaintenanceExecuteModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MaintenanceExecuteModal createFromParcel(Parcel in) {
            return new MaintenanceExecuteModal(in);
        }

        public MaintenanceExecuteModal[] newArray(int size) {
            return (new MaintenanceExecuteModal[size]);
        }

    }
            ;

    protected MaintenanceExecuteModal(Parcel in) {
        this.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((MaintenanceExecuteStrings) in.readValue((MaintenanceExecuteStrings.class.getClassLoader())));
        in.readList(this.eventStatusRows, (MaintenanceExecuteEventStatusRow.class.getClassLoader()));
        this.jsonData = ((MaintenanceExecuteJsonData) in.readValue((MaintenanceExecuteJsonData.class.getClassLoader())));
    }

    public MaintenanceExecuteModal() {
    }

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public MaintenanceExecuteStrings getStrings() {
        return strings;
    }

    public void setStrings(MaintenanceExecuteStrings strings) {
        this.strings = strings;
    }

    public List<MaintenanceExecuteEventStatusRow> getEventStatusRows() {
        return eventStatusRows;
    }

    public void setEventStatusRows(List<MaintenanceExecuteEventStatusRow> eventStatusRows) {
        this.eventStatusRows = eventStatusRows;
    }

    public MaintenanceExecuteJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(MaintenanceExecuteJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeList(eventStatusRows);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return 0;
    }

}