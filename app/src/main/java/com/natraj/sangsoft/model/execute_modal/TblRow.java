
package com.natraj.sangsoft.model.execute_modal;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TblRow implements Parcelable
{

    @SerializedName("activity_status")
    @Expose
    private String activityStatus="";
    @SerializedName("filter")
    @Expose
    private TblFilter filter;
    @SerializedName("updated_by")
    @Expose
    private String updatedBy="";
    @SerializedName("updation_date")
    @Expose
    private String updationDate="";
    public final static Creator<TblRow> CREATOR = new Creator<TblRow>() {


        @SuppressWarnings({
                "unchecked"
        })
        public TblRow createFromParcel(Parcel in) {
            return new TblRow(in);
        }

        public TblRow[] newArray(int size) {
            return (new TblRow[size]);
        }

    }
            ;

    protected TblRow(Parcel in) {
        this.activityStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.filter = ((TblFilter) in.readValue((TblFilter.class.getClassLoader())));
        this.updatedBy = ((String) in.readValue((String.class.getClassLoader())));
        this.updationDate = ((String) in.readValue((String.class.getClassLoader())));
    }

    public TblRow() {
    }

    public String getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }

    public TblFilter getFilter() {
        return filter;
    }

    public void setFilter(TblFilter filter) {
        this.filter = filter;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdationDate() {
        return updationDate;
    }

    public void setUpdationDate(String updationDate) {
        this.updationDate = updationDate;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(activityStatus);
        dest.writeValue(filter);
        dest.writeValue(updatedBy);
        dest.writeValue(updationDate);
    }

    public int describeContents() {
        return 0;
    }

}