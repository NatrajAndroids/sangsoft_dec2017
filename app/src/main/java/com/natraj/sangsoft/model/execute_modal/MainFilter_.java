
package com.natraj.sangsoft.model.execute_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainFilter_ implements Parcelable
{

    @SerializedName("mch_ID")
    @Expose
    private String mchID="";
    @SerializedName("sch_ID")
    @Expose
    private String schID="";
    public final static Creator<MainFilter_> CREATOR = new Creator<MainFilter_>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MainFilter_ createFromParcel(Parcel in) {
            return new MainFilter_(in);
        }

        public MainFilter_[] newArray(int size) {
            return (new MainFilter_[size]);
        }

    }
            ;

    protected MainFilter_(Parcel in) {
        this.mchID = ((String) in.readValue((String.class.getClassLoader())));
        this.schID = ((String) in.readValue((String.class.getClassLoader())));
    }

    public MainFilter_() {
    }

    public String getMchID() {
        return mchID;
    }

    public void setMchID(String mchID) {
        this.mchID = mchID;
    }

    public String getSchID() {
        return schID;
    }

    public void setSchID(String schID) {
        this.schID = schID;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(mchID);
        dest.writeValue(schID);
    }

    public int describeContents() {
        return 0;
    }

}