
package com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_info_bar_modal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Strings implements Parcelable
{

    @SerializedName("lblForItem")
    @Expose
    private String lblForItem;
    @SerializedName("lblIndentNo")
    @Expose
    private String lblIndentNo;
    @SerializedName("lblQty")
    @Expose
    private String lblQty;
    @SerializedName("lblTiming")
    @Expose
    private String lblTiming;
    public final static Parcelable.Creator<Strings> CREATOR = new Creator<Strings>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Strings createFromParcel(Parcel in) {
            return new Strings(in);
        }

        public Strings[] newArray(int size) {
            return (new Strings[size]);
        }

    }
            ;

    protected Strings(Parcel in) {
        this.lblForItem = ((String) in.readValue((String.class.getClassLoader())));
        this.lblIndentNo = ((String) in.readValue((String.class.getClassLoader())));
        this.lblQty = ((String) in.readValue((String.class.getClassLoader())));
        this.lblTiming = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Strings() {
    }

    public String getLblForItem() {
        return lblForItem;
    }

    public void setLblForItem(String lblForItem) {
        this.lblForItem = lblForItem;
    }

    public String getLblIndentNo() {
        return lblIndentNo;
    }

    public void setLblIndentNo(String lblIndentNo) {
        this.lblIndentNo = lblIndentNo;
    }

    public String getLblQty() {
        return lblQty;
    }

    public void setLblQty(String lblQty) {
        this.lblQty = lblQty;
    }

    public String getLblTiming() {
        return lblTiming;
    }

    public void setLblTiming(String lblTiming) {
        this.lblTiming = lblTiming;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(lblForItem);
        dest.writeValue(lblIndentNo);
        dest.writeValue(lblQty);
        dest.writeValue(lblTiming);
    }

    public int describeContents() {
        return 0;
    }

}
