package com.natraj.sangsoft.model.production_modal.indent_record_prod;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecordProdModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private String isError;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;
    @SerializedName("Units")
    @Expose
    private List<RecordProdUnit> units = new ArrayList<RecordProdUnit>();
    @SerializedName("strings")
    @Expose
    private RecordProdStrings strings;
    @SerializedName("jsonData")
    @Expose
    private RecordProdJsonData jsonData;
    public final static Parcelable.Creator<RecordProdModal> CREATOR = new Creator<RecordProdModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RecordProdModal createFromParcel(Parcel in) {
            return new RecordProdModal(in);
        }

        public RecordProdModal[] newArray(int size) {
            return (new RecordProdModal[size]);
        }

    }
            ;

    protected RecordProdModal(Parcel in) {
        this.isError = ((String) in.readValue((String.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.units, (RecordProdUnit.class.getClassLoader()));
        this.strings = ((RecordProdStrings) in.readValue((RecordProdStrings.class.getClassLoader())));
        this.jsonData = ((RecordProdJsonData) in.readValue((RecordProdJsonData.class.getClassLoader())));
    }

    public RecordProdModal() {
    }

    public String getIsError() {
        return isError;
    }

    public void setIsError(String isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<RecordProdUnit> getUnits() {
        return units;
    }

    public void setUnits(List<RecordProdUnit> units) {
        this.units = units;
    }

    public RecordProdStrings getStrings() {
        return strings;
    }

    public void setStrings(RecordProdStrings strings) {
        this.strings = strings;
    }

    public RecordProdJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(RecordProdJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeList(units);
        dest.writeValue(strings);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return 0;
    }

}
