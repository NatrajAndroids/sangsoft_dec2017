
package com.natraj.sangsoft.model.receive_rm_data_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RMStrings implements Parcelable
{

    @SerializedName("lblIndentQty")
    @Expose
    private String lblIndentQty;
    @SerializedName("areaTitle")
    @Expose
    private String areaTitle;
    @SerializedName("btnModify")
    @Expose
    private String btnModify;
    @SerializedName("lblComment")
    @Expose
    private String lblComment;
    @SerializedName("lblStatus")
    @Expose
    private String lblStatus;
    @SerializedName("btnSave")
    @Expose
    private String btnSave;
    @SerializedName("btnCancel")
    @Expose
    private String btnCancel;
    @SerializedName("lblColDescription")
    @Expose
    private String lblColDescription;
    @SerializedName("lblColItemCode")
    @Expose
    private String lblColItemCode;
    @SerializedName("lbltableCaption")
    @Expose
    private String lbltableCaption;
    @SerializedName("lblColTotExp")
    @Expose
    private String lblColTotExp;
    @SerializedName("lblColRecived")
    @Expose
    private String lblColRecived;
    @SerializedName("lblColTobeReturned")
    @Expose
    private String lblColTobeReturned;
    @SerializedName("lblColUom")
    @Expose
    private String lblColUom;
    public final static Parcelable.Creator<RMStrings> CREATOR = new Creator<RMStrings>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RMStrings createFromParcel(Parcel in) {
            return new RMStrings(in);
        }

        public RMStrings[] newArray(int size) {
            return (new RMStrings[size]);
        }

    }
            ;

    protected RMStrings(Parcel in) {
        this.lblIndentQty = ((String) in.readValue((String.class.getClassLoader())));
        this.areaTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.btnModify = ((String) in.readValue((String.class.getClassLoader())));
        this.lblComment = ((String) in.readValue((String.class.getClassLoader())));
        this.lblStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.btnSave = ((String) in.readValue((String.class.getClassLoader())));
        this.btnCancel = ((String) in.readValue((String.class.getClassLoader())));
        this.lblColDescription = ((String) in.readValue((String.class.getClassLoader())));
        this.lblColItemCode = ((String) in.readValue((String.class.getClassLoader())));
        this.lbltableCaption = ((String) in.readValue((String.class.getClassLoader())));
        this.lblColTotExp = ((String) in.readValue((String.class.getClassLoader())));
        this.lblColRecived = ((String) in.readValue((String.class.getClassLoader())));
        this.lblColTobeReturned = ((String) in.readValue((String.class.getClassLoader())));
        this.lblColUom = ((String) in.readValue((String.class.getClassLoader())));
    }

    public RMStrings() {
    }

    public String getLblIndentQty() {
        return lblIndentQty;
    }

    public void setLblIndentQty(String lblIndentQty) {
        this.lblIndentQty = lblIndentQty;
    }

    public String getAreaTitle() {
        return areaTitle;
    }

    public void setAreaTitle(String areaTitle) {
        this.areaTitle = areaTitle;
    }

    public String getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(String btnModify) {
        this.btnModify = btnModify;
    }

    public String getLblComment() {
        return lblComment;
    }

    public void setLblComment(String lblComment) {
        this.lblComment = lblComment;
    }

    public String getLblStatus() {
        return lblStatus;
    }

    public void setLblStatus(String lblStatus) {
        this.lblStatus = lblStatus;
    }

    public String getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(String btnSave) {
        this.btnSave = btnSave;
    }

    public String getBtnCancel() {
        return btnCancel;
    }

    public void setBtnCancel(String btnCancel) {
        this.btnCancel = btnCancel;
    }

    public String getLblColDescription() {
        return lblColDescription;
    }

    public void setLblColDescription(String lblColDescription) {
        this.lblColDescription = lblColDescription;
    }

    public String getLblColItemCode() {
        return lblColItemCode;
    }

    public void setLblColItemCode(String lblColItemCode) {
        this.lblColItemCode = lblColItemCode;
    }

    public String getLbltableCaption() {
        return lbltableCaption;
    }

    public void setLbltableCaption(String lbltableCaption) {
        this.lbltableCaption = lbltableCaption;
    }

    public String getLblColTotExp() {
        return lblColTotExp;
    }

    public void setLblColTotExp(String lblColTotExp) {
        this.lblColTotExp = lblColTotExp;
    }

    public String getLblColRecived() {
        return lblColRecived;
    }

    public void setLblColRecived(String lblColRecived) {
        this.lblColRecived = lblColRecived;
    }

    public String getLblColTobeReturned() {
        return lblColTobeReturned;
    }

    public void setLblColTobeReturned(String lblColTobeReturned) {
        this.lblColTobeReturned = lblColTobeReturned;
    }

    public String getLblColUom() {
        return lblColUom;
    }

    public void setLblColUom(String lblColUom) {
        this.lblColUom = lblColUom;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(lblIndentQty);
        dest.writeValue(areaTitle);
        dest.writeValue(btnModify);
        dest.writeValue(lblComment);
        dest.writeValue(lblStatus);
        dest.writeValue(btnSave);
        dest.writeValue(btnCancel);
        dest.writeValue(lblColDescription);
        dest.writeValue(lblColItemCode);
        dest.writeValue(lbltableCaption);
        dest.writeValue(lblColTotExp);
        dest.writeValue(lblColRecived);
        dest.writeValue(lblColTobeReturned);
        dest.writeValue(lblColUom);
    }

    public int describeContents() {
        return 0;
    }

}
