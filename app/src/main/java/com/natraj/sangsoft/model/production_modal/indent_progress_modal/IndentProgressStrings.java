package com.natraj.sangsoft.model.production_modal.indent_progress_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IndentProgressStrings implements Parcelable
{

    @SerializedName("areaTitle")
    @Expose
    private String areaTitle;
    public final static Parcelable.Creator<IndentProgressStrings> CREATOR = new Creator<IndentProgressStrings>() {


        @SuppressWarnings({
                "unchecked"
        })
        public IndentProgressStrings createFromParcel(Parcel in) {
            return new IndentProgressStrings(in);
        }

        public IndentProgressStrings[] newArray(int size) {
            return (new IndentProgressStrings[size]);
        }

    }
            ;

    protected IndentProgressStrings(Parcel in) {
        this.areaTitle = ((String) in.readValue((String.class.getClassLoader())));
    }

    public IndentProgressStrings() {
    }

    public String getAreaTitle() {
        return areaTitle;
    }

    public void setAreaTitle(String areaTitle) {
        this.areaTitle = areaTitle;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(areaTitle);
    }

    public int describeContents() {
        return 0;
    }

}
