
package com.natraj.sangsoft.model.request_qa_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RequestQAJsonData implements Parcelable
{

    @SerializedName("cmbRequestFor")
    @Expose
    private List<CmbRequestFor> cmbRequestFor = new ArrayList<CmbRequestFor>();
    @SerializedName("cmbQAStatus")
    @Expose
    private List<CmbQAstatus> cmbQAStatus = new ArrayList<CmbQAstatus>();
    public final static Creator<RequestQAJsonData> CREATOR = new Creator<RequestQAJsonData>() {


        @SuppressWarnings({
            "unchecked"
        })
        public RequestQAJsonData createFromParcel(Parcel in) {
            return new RequestQAJsonData(in);
        }

        public RequestQAJsonData[] newArray(int size) {
            return (new RequestQAJsonData[size]);
        }

    }
    ;

    protected RequestQAJsonData(Parcel in) {
        in.readList(this.cmbRequestFor, (CmbRequestFor.class.getClassLoader()));
        in.readList(this.cmbQAStatus, (CmbQAstatus.class.getClassLoader()));
    }

    public RequestQAJsonData() {
    }

    public List<CmbRequestFor> getCmbRequestFor() {
        return cmbRequestFor;
    }

    public void setCmbRequestFor(List<CmbRequestFor> cmbRequestFor) {
        this.cmbRequestFor = cmbRequestFor;
    }

    public List<CmbQAstatus> getCmbQAStatus() {
        return cmbQAStatus;
    }

    public void setCmbQAStatus(List<CmbQAstatus> cmbQAStatus) {
        this.cmbQAStatus = cmbQAStatus;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(cmbRequestFor);
        dest.writeList(cmbQAStatus);
    }

    public int describeContents() {
        return  0;
    }

}
