
package com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_overview;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OverviewItemModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError=0;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg="";
    @SerializedName("strings")
    @Expose
    private OverviewStrings strings;
    @SerializedName("jsonData")
    @Expose
    private OverviewJsonData jsonData;
    public final static Creator<OverviewItemModal> CREATOR = new Creator<OverviewItemModal>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OverviewItemModal createFromParcel(Parcel in) {
            return new OverviewItemModal(in);
        }

        public OverviewItemModal[] newArray(int size) {
            return (new OverviewItemModal[size]);
        }

    }
    ;

    protected OverviewItemModal(Parcel in) {
        this.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((OverviewStrings) in.readValue((OverviewStrings.class.getClassLoader())));
        this.jsonData = ((OverviewJsonData) in.readValue((OverviewJsonData.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public OverviewItemModal() {
    }

    /**
     * 
     * @param strings
     * @param errorMsg
     * @param isError
     * @param jsonData
     */
    public OverviewItemModal(Integer isError, String errorMsg, OverviewStrings strings, OverviewJsonData jsonData) {
        super();
        this.isError = isError;
        this.errorMsg = errorMsg;
        this.strings = strings;
        this.jsonData = jsonData;
    }

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public OverviewStrings getStrings() {
        return strings;
    }

    public void setStrings(OverviewStrings strings) {
        this.strings = strings;
    }

    public OverviewJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(OverviewJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return  0;
    }

}
