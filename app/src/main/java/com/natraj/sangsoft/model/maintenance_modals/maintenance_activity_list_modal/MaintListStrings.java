
package com.natraj.sangsoft.model.maintenance_modals.maintenance_activity_list_modal;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaintListStrings implements Parcelable
{

    @SerializedName("colStatus")
    @Expose
    private String colStatus="";
    @SerializedName("areaTitle")
    @Expose
    private String areaTitle="";
    @SerializedName("chkExecuted")
    @Expose
    private String chkExecuted="";
    @SerializedName("chkCanceled")
    @Expose
    private String chkCanceled="";
    @SerializedName("colDate")
    @Expose
    private String colDate="";
    @SerializedName("colFromTime")
    @Expose
    private String colFromTime="";
    @SerializedName("colTillTime")
    @Expose
    private String colTillTime="";
    @SerializedName("colSchName")
    @Expose
    private String colSchName="";
    @SerializedName("colMandetory")
    @Expose
    private String colMandetory="";
    @SerializedName("colBy")
    @Expose
    private String colBy="";
    public final static Creator<MaintListStrings> CREATOR = new Creator<MaintListStrings>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MaintListStrings createFromParcel(Parcel in) {
            return new MaintListStrings(in);
        }

        public MaintListStrings[] newArray(int size) {
            return (new MaintListStrings[size]);
        }

    }
            ;

    protected MaintListStrings(Parcel in) {
        this.colStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.areaTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.chkExecuted = ((String) in.readValue((String.class.getClassLoader())));
        this.chkCanceled = ((String) in.readValue((String.class.getClassLoader())));
        this.colDate = ((String) in.readValue((String.class.getClassLoader())));
        this.colFromTime = ((String) in.readValue((String.class.getClassLoader())));
        this.colTillTime = ((String) in.readValue((String.class.getClassLoader())));
        this.colSchName = ((String) in.readValue((String.class.getClassLoader())));
        this.colMandetory = ((String) in.readValue((String.class.getClassLoader())));
        this.colBy = ((String) in.readValue((String.class.getClassLoader())));
    }

    public MaintListStrings() {
    }

    public String getColStatus() {
        return colStatus;
    }

    public void setColStatus(String colStatus) {
        this.colStatus = colStatus;
    }

    public String getAreaTitle() {
        return areaTitle;
    }

    public void setAreaTitle(String areaTitle) {
        this.areaTitle = areaTitle;
    }

    public String getChkExecuted() {
        return chkExecuted;
    }

    public void setChkExecuted(String chkExecuted) {
        this.chkExecuted = chkExecuted;
    }

    public String getChkCanceled() {
        return chkCanceled;
    }

    public void setChkCanceled(String chkCanceled) {
        this.chkCanceled = chkCanceled;
    }

    public String getColDate() {
        return colDate;
    }

    public void setColDate(String colDate) {
        this.colDate = colDate;
    }

    public String getColFromTime() {
        return colFromTime;
    }

    public void setColFromTime(String colFromTime) {
        this.colFromTime = colFromTime;
    }

    public String getColTillTime() {
        return colTillTime;
    }

    public void setColTillTime(String colTillTime) {
        this.colTillTime = colTillTime;
    }

    public String getColSchName() {
        return colSchName;
    }

    public void setColSchName(String colSchName) {
        this.colSchName = colSchName;
    }

    public String getColMandetory() {
        return colMandetory;
    }

    public void setColMandetory(String colMandetory) {
        this.colMandetory = colMandetory;
    }

    public String getColBy() {
        return colBy;
    }

    public void setColBy(String colBy) {
        this.colBy = colBy;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(colStatus);
        dest.writeValue(areaTitle);
        dest.writeValue(chkExecuted);
        dest.writeValue(chkCanceled);
        dest.writeValue(colDate);
        dest.writeValue(colFromTime);
        dest.writeValue(colTillTime);
        dest.writeValue(colSchName);
        dest.writeValue(colMandetory);
        dest.writeValue(colBy);
    }

    public int describeContents() {
        return 0;
    }

}