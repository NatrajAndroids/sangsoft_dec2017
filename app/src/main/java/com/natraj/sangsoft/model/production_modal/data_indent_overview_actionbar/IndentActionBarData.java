
package com.natraj.sangsoft.model.production_modal.data_indent_overview_actionbar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IndentActionBarData implements Parcelable
{

    @SerializedName("indentCount")
    @Expose
    private String indentCount="";
    @SerializedName("indents")
    @Expose
    private List<IndentABArray> indents = null;
    @SerializedName("values")
    @Expose
    private String values="";
    public final static Creator<IndentActionBarData> CREATOR = new Creator<IndentActionBarData>() {


        @SuppressWarnings({
            "unchecked"
        })
        public IndentActionBarData createFromParcel(Parcel in) {
            IndentActionBarData instance = new IndentActionBarData();
            instance.indentCount = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.indents, (IndentABArray.class.getClassLoader()));
            instance.values = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public IndentActionBarData[] newArray(int size) {
            return (new IndentActionBarData[size]);
        }

    }
    ;

    public String getIndentCount() {
        return indentCount;
    }

    public void setIndentCount(String indentCount) {
        this.indentCount = indentCount;
    }

    public List<IndentABArray> getIndents() {
        return indents;
    }

    public void setIndents(List<IndentABArray> indents) {
        this.indents = indents;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(indentCount);
        dest.writeList(indents);
        dest.writeValue(values);
    }

    public int describeContents() {
        return  0;
    }

}
