package com.natraj.sangsoft.model;

/**
 * Created by Natraj3 on 10/11/2017.
 */

public class UrlModal {

    private int _id=0;
    private String _name="";
    private String _url="";

    public UrlModal() {

    }

    public UrlModal(String _name, String _url) {
        this._name = _name;
        this._url = _url;
    }

    public UrlModal(int _id, String _name, String _url) {
        this._id = _id;
        this._name = _name;
        this._url = _url;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_url() {
        return _url;
    }

    public void set_url(String _url) {
        this._url = _url;
    }

  /*  @Override
    public String toString() {
        return _url;
    }*/
}
