
package com.natraj.sangsoft.model.login_data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginUserModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError=0;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg="";
    @SerializedName("jsonData")
    @Expose
    private LoginJsonData jsonData;
    public final static Creator<LoginUserModal> CREATOR = new Creator<LoginUserModal>() {


        @SuppressWarnings({
            "unchecked"
        })
        public LoginUserModal createFromParcel(Parcel in) {
            LoginUserModal instance = new LoginUserModal();
            instance.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
            instance.jsonData = ((LoginJsonData) in.readValue((LoginJsonData.class.getClassLoader())));
            return instance;
        }

        public LoginUserModal[] newArray(int size) {
            return (new LoginUserModal[size]);
        }

    }
    ;

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public LoginJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(LoginJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return  0;
    }

}
