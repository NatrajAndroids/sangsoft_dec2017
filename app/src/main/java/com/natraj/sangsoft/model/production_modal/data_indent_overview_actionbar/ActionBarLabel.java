
package com.natraj.sangsoft.model.production_modal.data_indent_overview_actionbar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActionBarLabel implements Parcelable
{

    @SerializedName("btnOverview")
    @Expose
    private String btnOverview="";
    @SerializedName("btnAction")
    @Expose
    private String btnAction="";
    @SerializedName("btnPause")
    @Expose
    private String btnPause="";
    @SerializedName("btnProgress")
    @Expose
    private String btnProgress="";
    @SerializedName("btnClose")
    @Expose
    private String btnClose="";
    public final static Creator<ActionBarLabel> CREATOR = new Creator<ActionBarLabel>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ActionBarLabel createFromParcel(Parcel in) {
            ActionBarLabel instance = new ActionBarLabel();
            instance.btnOverview = ((String) in.readValue((String.class.getClassLoader())));
            instance.btnAction = ((String) in.readValue((String.class.getClassLoader())));
            instance.btnPause = ((String) in.readValue((String.class.getClassLoader())));
            instance.btnProgress = ((String) in.readValue((String.class.getClassLoader())));
            instance.btnClose = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public ActionBarLabel[] newArray(int size) {
            return (new ActionBarLabel[size]);
        }

    }
    ;

    public String getBtnOverview() {
        return btnOverview;
    }

    public void setBtnOverview(String btnOverview) {
        this.btnOverview = btnOverview;
    }

    public String getBtnAction() {
        return btnAction;
    }

    public void setBtnAction(String btnAction) {
        this.btnAction = btnAction;
    }

    public String getBtnPause() {
        return btnPause;
    }

    public void setBtnPause(String btnPause) {
        this.btnPause = btnPause;
    }

    public String getBtnProgress() {
        return btnProgress;
    }

    public void setBtnProgress(String btnProgress) {
        this.btnProgress = btnProgress;
    }

    public String getBtnClose() {
        return btnClose;
    }

    public void setBtnClose(String btnClose) {
        this.btnClose = btnClose;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(btnOverview);
        dest.writeValue(btnAction);
        dest.writeValue(btnPause);
        dest.writeValue(btnProgress);
        dest.writeValue(btnClose);
    }

    public int describeContents() {
        return  0;
    }

}
