
package com.natraj.sangsoft.model.receive_rm_data_modal;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RmDataModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private String isError;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;
    @SerializedName("IndentRMStatus")
    @Expose
    private List<RMIndentStatus> indentRMStatus = new ArrayList<RMIndentStatus>();
    @SerializedName("Units")
    @Expose
    private List<RMUnit> units = new ArrayList<RMUnit>();
    @SerializedName("strings")
    @Expose
    private RMStrings strings;
    @SerializedName("jsonData")
    @Expose
    private RMJsonData jsonData;
    public final static Parcelable.Creator<RmDataModal> CREATOR = new Creator<RmDataModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RmDataModal createFromParcel(Parcel in) {
            return new RmDataModal(in);
        }

        public RmDataModal[] newArray(int size) {
            return (new RmDataModal[size]);
        }

    }
            ;

    protected RmDataModal(Parcel in) {
        this.isError = ((String) in.readValue((String.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.indentRMStatus, (RMIndentStatus.class.getClassLoader()));
        in.readList(this.units, (RMUnit.class.getClassLoader()));
        this.strings = ((RMStrings) in.readValue((RMStrings.class.getClassLoader())));
        this.jsonData = ((RMJsonData) in.readValue((RMJsonData.class.getClassLoader())));
    }

    public RmDataModal() {
    }

    public String getIsError() {
        return isError;
    }

    public void setIsError(String isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<RMIndentStatus> getIndentRMStatus() {
        return indentRMStatus;
    }

    public void setIndentRMStatus(List<RMIndentStatus> indentRMStatus) {
        this.indentRMStatus = indentRMStatus;
    }

    public List<RMUnit> getUnits() {
        return units;
    }

    public void setUnits(List<RMUnit> units) {
        this.units = units;
    }

    public RMStrings getStrings() {
        return strings;
    }

    public void setStrings(RMStrings strings) {
        this.strings = strings;
    }

    public RMJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(RMJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeList(indentRMStatus);
        dest.writeList(units);
        dest.writeValue(strings);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return 0;
    }

}
