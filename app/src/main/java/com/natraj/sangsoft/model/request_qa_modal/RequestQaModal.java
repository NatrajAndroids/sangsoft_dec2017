
package com.natraj.sangsoft.model.request_qa_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestQaModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;
    @SerializedName("strings")
    @Expose
    private RequestQAStrings strings;
    @SerializedName("jsonData")
    @Expose
    private RequestQAJsonData jsonData;
    public final static Parcelable.Creator<RequestQaModal> CREATOR = new Creator<RequestQaModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RequestQaModal createFromParcel(Parcel in) {
            return new RequestQaModal(in);
        }

        public RequestQaModal[] newArray(int size) {
            return (new RequestQaModal[size]);
        }

    }
            ;

    protected RequestQaModal(Parcel in) {
        this.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((RequestQAStrings) in.readValue((RequestQAStrings.class.getClassLoader())));
        this.jsonData = ((RequestQAJsonData) in.readValue((RequestQAJsonData.class.getClassLoader())));
    }

    public RequestQaModal() {
    }

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public RequestQAStrings getStrings() {
        return strings;
    }

    public void setStrings(RequestQAStrings strings) {
        this.strings = strings;
    }

    public RequestQAJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(RequestQAJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return 0;
    }

}
