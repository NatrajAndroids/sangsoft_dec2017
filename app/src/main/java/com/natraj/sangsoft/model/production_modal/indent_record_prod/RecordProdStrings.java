package com.natraj.sangsoft.model.production_modal.indent_record_prod;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecordProdStrings implements Parcelable
{

    @SerializedName("lblIndentQty")
    @Expose
    private String lblIndentQty;
    @SerializedName("lblRequestQa")
    @Expose
    private String lblRequestQa;
    @SerializedName("areaTitle")
    @Expose
    private String areaTitle;
    @SerializedName("lblComment")
    @Expose
    private String lblComment;
    @SerializedName("lblStatus")
    @Expose
    private String lblStatus;
    @SerializedName("btnSave")
    @Expose
    private String btnSave;
    @SerializedName("btnCancel")
    @Expose
    private String btnCancel;
    @SerializedName("lblColDescription")
    @Expose
    private String lblColDescription;
    @SerializedName("lblColItemCode")
    @Expose
    private String lblColItemCode;
    @SerializedName("lbltableCaption")
    @Expose
    private String lbltableCaption;
    @SerializedName("lblQaComment")
    @Expose
    private String lblQaComment;
    @SerializedName("lblColTotExp")
    @Expose
    private String lblColTotExp;
    @SerializedName("lblColRecived")
    @Expose
    private String lblColRecived;
    @SerializedName("lblColUom")
    @Expose
    private String lblColUom;
    public final static Parcelable.Creator<RecordProdStrings> CREATOR = new Creator<RecordProdStrings>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RecordProdStrings createFromParcel(Parcel in) {
            return new RecordProdStrings(in);
        }

        public RecordProdStrings[] newArray(int size) {
            return (new RecordProdStrings[size]);
        }

    }
            ;

    protected RecordProdStrings(Parcel in) {
        this.lblIndentQty = ((String) in.readValue((String.class.getClassLoader())));
        this.lblRequestQa = ((String) in.readValue((String.class.getClassLoader())));
        this.areaTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.lblComment = ((String) in.readValue((String.class.getClassLoader())));
        this.lblStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.btnSave = ((String) in.readValue((String.class.getClassLoader())));
        this.btnCancel = ((String) in.readValue((String.class.getClassLoader())));
        this.lblColDescription = ((String) in.readValue((String.class.getClassLoader())));
        this.lblColItemCode = ((String) in.readValue((String.class.getClassLoader())));
        this.lbltableCaption = ((String) in.readValue((String.class.getClassLoader())));
        this.lblQaComment = ((String) in.readValue((String.class.getClassLoader())));
        this.lblColTotExp = ((String) in.readValue((String.class.getClassLoader())));
        this.lblColRecived = ((String) in.readValue((String.class.getClassLoader())));
        this.lblColUom = ((String) in.readValue((String.class.getClassLoader())));
    }

    public RecordProdStrings() {
    }

    public String getLblIndentQty() {
        return lblIndentQty;
    }

    public void setLblIndentQty(String lblIndentQty) {
        this.lblIndentQty = lblIndentQty;
    }

    public String getLblRequestQa() {
        return lblRequestQa;
    }

    public void setLblRequestQa(String lblRequestQa) {
        this.lblRequestQa = lblRequestQa;
    }

    public String getAreaTitle() {
        return areaTitle;
    }

    public void setAreaTitle(String areaTitle) {
        this.areaTitle = areaTitle;
    }

    public String getLblComment() {
        return lblComment;
    }

    public void setLblComment(String lblComment) {
        this.lblComment = lblComment;
    }

    public String getLblStatus() {
        return lblStatus;
    }

    public void setLblStatus(String lblStatus) {
        this.lblStatus = lblStatus;
    }

    public String getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(String btnSave) {
        this.btnSave = btnSave;
    }

    public String getBtnCancel() {
        return btnCancel;
    }

    public void setBtnCancel(String btnCancel) {
        this.btnCancel = btnCancel;
    }

    public String getLblColDescription() {
        return lblColDescription;
    }

    public void setLblColDescription(String lblColDescription) {
        this.lblColDescription = lblColDescription;
    }

    public String getLblColItemCode() {
        return lblColItemCode;
    }

    public void setLblColItemCode(String lblColItemCode) {
        this.lblColItemCode = lblColItemCode;
    }

    public String getLbltableCaption() {
        return lbltableCaption;
    }

    public void setLbltableCaption(String lbltableCaption) {
        this.lbltableCaption = lbltableCaption;
    }

    public String getLblQaComment() {
        return lblQaComment;
    }

    public void setLblQaComment(String lblQaComment) {
        this.lblQaComment = lblQaComment;
    }

    public String getLblColTotExp() {
        return lblColTotExp;
    }

    public void setLblColTotExp(String lblColTotExp) {
        this.lblColTotExp = lblColTotExp;
    }

    public String getLblColRecived() {
        return lblColRecived;
    }

    public void setLblColRecived(String lblColRecived) {
        this.lblColRecived = lblColRecived;
    }

    public String getLblColUom() {
        return lblColUom;
    }

    public void setLblColUom(String lblColUom) {
        this.lblColUom = lblColUom;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(lblIndentQty);
        dest.writeValue(lblRequestQa);
        dest.writeValue(areaTitle);
        dest.writeValue(lblComment);
        dest.writeValue(lblStatus);
        dest.writeValue(btnSave);
        dest.writeValue(btnCancel);
        dest.writeValue(lblColDescription);
        dest.writeValue(lblColItemCode);
        dest.writeValue(lbltableCaption);
        dest.writeValue(lblQaComment);
        dest.writeValue(lblColTotExp);
        dest.writeValue(lblColRecived);
        dest.writeValue(lblColUom);
    }

    public int describeContents() {
        return 0;
    }

}
