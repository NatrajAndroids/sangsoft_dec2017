
package com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_actionbar;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Strings implements Parcelable
{

    @SerializedName("btnClose")
    @Expose
    private String btnClose;
    @SerializedName("tooltipClose")
    @Expose
    private String tooltipClose;
    @SerializedName("btnOverview")
    @Expose
    private String btnOverview;
    @SerializedName("btnPause")
    @Expose
    private String btnPause;
    @SerializedName("tooltipPauseResume")
    @Expose
    private String tooltipPauseResume;
    @SerializedName("btnProgress")
    @Expose
    private String btnProgress;
    @SerializedName("lblStatus")
    @Expose
    private String lblStatus;
    @SerializedName("lblRecord")
    @Expose
    private String lblRecord;
    @SerializedName("tooltipRecords")
    @Expose
    private String tooltipRecords;
    public final static Parcelable.Creator<Strings> CREATOR = new Creator<Strings>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Strings createFromParcel(Parcel in) {
            return new Strings(in);
        }

        public Strings[] newArray(int size) {
            return (new Strings[size]);
        }

    }
            ;

    protected Strings(Parcel in) {
        this.btnClose = ((String) in.readValue((String.class.getClassLoader())));
        this.tooltipClose = ((String) in.readValue((String.class.getClassLoader())));
        this.btnOverview = ((String) in.readValue((String.class.getClassLoader())));
        this.btnPause = ((String) in.readValue((String.class.getClassLoader())));
        this.tooltipPauseResume = ((String) in.readValue((String.class.getClassLoader())));
        this.btnProgress = ((String) in.readValue((String.class.getClassLoader())));
        this.lblStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.lblRecord = ((String) in.readValue((String.class.getClassLoader())));
        this.tooltipRecords = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Strings() {
    }

    public String getBtnClose() {
        return btnClose;
    }

    public void setBtnClose(String btnClose) {
        this.btnClose = btnClose;
    }

    public String getTooltipClose() {
        return tooltipClose;
    }

    public void setTooltipClose(String tooltipClose) {
        this.tooltipClose = tooltipClose;
    }

    public String getBtnOverview() {
        return btnOverview;
    }

    public void setBtnOverview(String btnOverview) {
        this.btnOverview = btnOverview;
    }

    public String getBtnPause() {
        return btnPause;
    }

    public void setBtnPause(String btnPause) {
        this.btnPause = btnPause;
    }

    public String getTooltipPauseResume() {
        return tooltipPauseResume;
    }

    public void setTooltipPauseResume(String tooltipPauseResume) {
        this.tooltipPauseResume = tooltipPauseResume;
    }

    public String getBtnProgress() {
        return btnProgress;
    }

    public void setBtnProgress(String btnProgress) {
        this.btnProgress = btnProgress;
    }

    public String getLblStatus() {
        return lblStatus;
    }

    public void setLblStatus(String lblStatus) {
        this.lblStatus = lblStatus;
    }

    public String getLblRecord() {
        return lblRecord;
    }

    public void setLblRecord(String lblRecord) {
        this.lblRecord = lblRecord;
    }

    public String getTooltipRecords() {
        return tooltipRecords;
    }

    public void setTooltipRecords(String tooltipRecords) {
        this.tooltipRecords = tooltipRecords;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(btnClose);
        dest.writeValue(tooltipClose);
        dest.writeValue(btnOverview);
        dest.writeValue(btnPause);
        dest.writeValue(tooltipPauseResume);
        dest.writeValue(btnProgress);
        dest.writeValue(lblStatus);
        dest.writeValue(lblRecord);
        dest.writeValue(tooltipRecords);
    }

    public int describeContents() {
        return 0;
    }

}
