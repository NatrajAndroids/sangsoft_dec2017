
package com.natraj.sangsoft.model.production_modal.data_indent_overview_actionbar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Indent implements Parcelable
{

    @SerializedName("indentID")
    @Expose
    private String indentID="";
    @SerializedName("indentStatus")
    @Expose
    private String indentStatus="";
    public final static Creator<Indent> CREATOR = new Creator<Indent>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Indent createFromParcel(Parcel in) {
            Indent instance = new Indent();
            instance.indentID = ((String) in.readValue((String.class.getClassLoader())));
            instance.indentStatus = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Indent[] newArray(int size) {
            return (new Indent[size]);
        }

    }
    ;

    public String getIndentID() {
        return indentID;
    }

    public void setIndentID(String indentID) {
        this.indentID = indentID;
    }

    public String getIndentStatus() {
        return indentStatus;
    }

    public void setIndentStatus(String indentStatus) {
        this.indentStatus = indentStatus;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(indentID);
        dest.writeValue(indentStatus);
    }

    public int describeContents() {
        return  0;
    }

}
