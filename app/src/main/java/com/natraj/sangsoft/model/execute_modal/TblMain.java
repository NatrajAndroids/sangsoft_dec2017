
package com.natraj.sangsoft.model.execute_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TblMain implements Parcelable
{

    @SerializedName("rows")
    @Expose
    private List<MainRow_> rows = new ArrayList<MainRow_>();
    public final static Creator<TblMain> CREATOR = new Creator<TblMain>() {


        @SuppressWarnings({
                "unchecked"
        })
        public TblMain createFromParcel(Parcel in) {
            return new TblMain(in);
        }

        public TblMain[] newArray(int size) {
            return (new TblMain[size]);
        }

    }
            ;

    protected TblMain(Parcel in) {
        in.readList(this.rows, (MainRow_.class.getClassLoader()));
    }

    public TblMain() {
    }

    public List<MainRow_> getRows() {
        return rows;
    }

    public void setRows(List<MainRow_> rows) {
        this.rows = rows;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(rows);
    }

    public int describeContents() {
        return 0;
    }

}