package com.natraj.sangsoft.model.spinner_modal;

public class InfoSet {

    private String lookupNm = "";
    private String sequence = "";
    private String dataKey = "";
    private String displayName = "";

    public InfoSet() {

    }

    public String getLookupNm() {
        return lookupNm;
    }

    public void setLookupNm(String lookupNm) {
        this.lookupNm = lookupNm;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getDataKey() {
        return dataKey;
    }

    public void setDataKey(String dataKey) {
        this.dataKey = dataKey;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public InfoSet(String lookupNm, String sequence, String dataKey, String displayName) {
        this.lookupNm = lookupNm;
        this.sequence = sequence;
        this.dataKey = dataKey;
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }
}
