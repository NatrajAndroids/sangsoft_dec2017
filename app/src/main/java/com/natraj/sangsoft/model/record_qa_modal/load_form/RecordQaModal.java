package com.natraj.sangsoft.model.record_qa_modal.load_form;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecordQaModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private String isError;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;
    @SerializedName("qaResult")
    @Expose
    private List<RequestQaResult> qaResult = new ArrayList<RequestQaResult>();
    @SerializedName("jsonData")
    @Expose
    private RequestQaJsonData jsonData;
    public final static Parcelable.Creator<RecordQaModal> CREATOR = new Creator<RecordQaModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RecordQaModal createFromParcel(Parcel in) {
            return new RecordQaModal(in);
        }

        public RecordQaModal[] newArray(int size) {
            return (new RecordQaModal[size]);
        }

    }
            ;

    protected RecordQaModal(Parcel in) {
        this.isError = ((String) in.readValue((String.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.qaResult, (RequestQaResult.class.getClassLoader()));
        this.jsonData = ((RequestQaJsonData) in.readValue((RequestQaJsonData.class.getClassLoader())));
    }

    public RecordQaModal() {
    }

    public String getIsError() {
        return isError;
    }

    public void setIsError(String isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<RequestQaResult> getQaResult() {
        return qaResult;
    }

    public void setQaResult(List<RequestQaResult> qaResult) {
        this.qaResult = qaResult;
    }

    public RequestQaJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(RequestQaJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeList(qaResult);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return 0;
    }

}
