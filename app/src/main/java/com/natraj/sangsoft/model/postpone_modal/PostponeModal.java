package com.natraj.sangsoft.model.postpone_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostponeModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private String isError;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;
    @SerializedName("strings")
    @Expose
    private PostponeStrings strings;
    @SerializedName("jsonData")
    @Expose
    private PostponeJsonData jsonData;
    public final static Parcelable.Creator<PostponeModal> CREATOR = new Creator<PostponeModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PostponeModal createFromParcel(Parcel in) {
            return new PostponeModal(in);
        }

        public PostponeModal[] newArray(int size) {
            return (new PostponeModal[size]);
        }

    }
            ;

    protected PostponeModal(Parcel in) {
        this.isError = ((String) in.readValue((String.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((PostponeStrings) in.readValue((PostponeStrings.class.getClassLoader())));
        this.jsonData = ((PostponeJsonData) in.readValue((PostponeJsonData.class.getClassLoader())));
    }

    public PostponeModal() {
    }

    public String getIsError() {
        return isError;
    }

    public void setIsError(String isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public PostponeStrings getStrings() {
        return strings;
    }

    public void setStrings(PostponeStrings strings) {
        this.strings = strings;
    }

    public PostponeJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(PostponeJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return 0;
    }

}
