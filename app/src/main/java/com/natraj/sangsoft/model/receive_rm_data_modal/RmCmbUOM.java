
package com.natraj.sangsoft.model.receive_rm_data_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RmCmbUOM implements Parcelable
{

    @SerializedName("uomID")
    @Expose
    private String uomID="";
    @SerializedName("uomName")
    @Expose
    private String uomName="";
    public final static Creator<RmCmbUOM> CREATOR = new Creator<RmCmbUOM>() {


        @SuppressWarnings({
            "unchecked"
        })
        public RmCmbUOM createFromParcel(Parcel in) {
            return new RmCmbUOM(in);
        }

        public RmCmbUOM[] newArray(int size) {
            return (new RmCmbUOM[size]);
        }

    }
    ;

    protected RmCmbUOM(Parcel in) {
        this.uomID = ((String) in.readValue((String.class.getClassLoader())));
        this.uomName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public RmCmbUOM() {
    }

    public String getUomID() {
        return uomID;
    }

    public void setUomID(String uomID) {
        this.uomID = uomID;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(uomID);
        dest.writeValue(uomName);
    }

    public int describeContents() {
        return  0;
    }

}
