
package com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_indent_item_list;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OverviewIndentStrings implements Parcelable
{

    @SerializedName("col02")
    @Expose
    private String col02="";
    @SerializedName("col03")
    @Expose
    private String col03="";
    @SerializedName("titleSearchItem")
    @Expose
    private String titleSearchItem="";
    @SerializedName("lblMCode")
    @Expose
    private String lblMCode="";
    @SerializedName("btnGo")
    @Expose
    private String btnGo="";
    @SerializedName("col01")
    @Expose
    private String col01="";
    public final static Creator<OverviewIndentStrings> CREATOR = new Creator<OverviewIndentStrings>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OverviewIndentStrings createFromParcel(Parcel in) {
            return new OverviewIndentStrings(in);
        }

        public OverviewIndentStrings[] newArray(int size) {
            return (new OverviewIndentStrings[size]);
        }

    }
    ;

    protected OverviewIndentStrings(Parcel in) {
        this.col02 = ((String) in.readValue((String.class.getClassLoader())));
        this.col03 = ((String) in.readValue((String.class.getClassLoader())));
        this.titleSearchItem = ((String) in.readValue((String.class.getClassLoader())));
        this.lblMCode = ((String) in.readValue((String.class.getClassLoader())));
        this.btnGo = ((String) in.readValue((String.class.getClassLoader())));
        this.col01 = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public OverviewIndentStrings() {
    }

    /**
     * 
     * @param btnGo
     * @param titleSearchItem
     * @param lblMCode
     * @param col01
     * @param col03
     * @param col02
     */
    public OverviewIndentStrings(String col02, String col03, String titleSearchItem, String lblMCode, String btnGo, String col01) {
        super();
        this.col02 = col02;
        this.col03 = col03;
        this.titleSearchItem = titleSearchItem;
        this.lblMCode = lblMCode;
        this.btnGo = btnGo;
        this.col01 = col01;
    }

    public String getCol02() {
        return col02;
    }

    public void setCol02(String col02) {
        this.col02 = col02;
    }

    public String getCol03() {
        return col03;
    }

    public void setCol03(String col03) {
        this.col03 = col03;
    }

    public String getTitleSearchItem() {
        return titleSearchItem;
    }

    public void setTitleSearchItem(String titleSearchItem) {
        this.titleSearchItem = titleSearchItem;
    }

    public String getLblMCode() {
        return lblMCode;
    }

    public void setLblMCode(String lblMCode) {
        this.lblMCode = lblMCode;
    }

    public String getBtnGo() {
        return btnGo;
    }

    public void setBtnGo(String btnGo) {
        this.btnGo = btnGo;
    }

    public String getCol01() {
        return col01;
    }

    public void setCol01(String col01) {
        this.col01 = col01;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(col02);
        dest.writeValue(col03);
        dest.writeValue(titleSearchItem);
        dest.writeValue(lblMCode);
        dest.writeValue(btnGo);
        dest.writeValue(col01);
    }

    public int describeContents() {
        return  0;
    }

}
