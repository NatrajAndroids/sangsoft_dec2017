
package com.natraj.sangsoft.model.request_qa_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestQAStrings implements Parcelable
{

    @SerializedName("btnCancel")
    @Expose
    private String btnCancel;
    @SerializedName("lblComment")
    @Expose
    private String lblComment;
    @SerializedName("lblRequestFor")
    @Expose
    private String lblRequestFor;
    @SerializedName("areaTitle")
    @Expose
    private String areaTitle;
    @SerializedName("btnSave")
    @Expose
    private String btnSave;
    @SerializedName("lblStatus")
    @Expose
    private String lblStatus;
    public final static Parcelable.Creator<RequestQAStrings> CREATOR = new Creator<RequestQAStrings>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RequestQAStrings createFromParcel(Parcel in) {
            return new RequestQAStrings(in);
        }

        public RequestQAStrings[] newArray(int size) {
            return (new RequestQAStrings[size]);
        }

    }
            ;

    protected RequestQAStrings(Parcel in) {
        this.btnCancel = ((String) in.readValue((String.class.getClassLoader())));
        this.lblComment = ((String) in.readValue((String.class.getClassLoader())));
        this.lblRequestFor = ((String) in.readValue((String.class.getClassLoader())));
        this.areaTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.btnSave = ((String) in.readValue((String.class.getClassLoader())));
        this.lblStatus = ((String) in.readValue((String.class.getClassLoader())));
    }

    public RequestQAStrings() {
    }

    public String getBtnCancel() {
        return btnCancel;
    }

    public void setBtnCancel(String btnCancel) {
        this.btnCancel = btnCancel;
    }

    public String getLblComment() {
        return lblComment;
    }

    public void setLblComment(String lblComment) {
        this.lblComment = lblComment;
    }

    public String getLblRequestFor() {
        return lblRequestFor;
    }

    public void setLblRequestFor(String lblRequestFor) {
        this.lblRequestFor = lblRequestFor;
    }

    public String getAreaTitle() {
        return areaTitle;
    }

    public void setAreaTitle(String areaTitle) {
        this.areaTitle = areaTitle;
    }

    public String getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(String btnSave) {
        this.btnSave = btnSave;
    }

    public String getLblStatus() {
        return lblStatus;
    }

    public void setLblStatus(String lblStatus) {
        this.lblStatus = lblStatus;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(btnCancel);
        dest.writeValue(lblComment);
        dest.writeValue(lblRequestFor);
        dest.writeValue(areaTitle);
        dest.writeValue(btnSave);
        dest.writeValue(lblStatus);
    }

    public int describeContents() {
        return 0;
    }

}
