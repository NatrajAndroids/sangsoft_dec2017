package com.natraj.sangsoft.model.record_qa_modal.load_form;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestQaJsonData implements Parcelable
{

    @SerializedName("requestList")
    @Expose
    private List<RequestQaList> requestList = new ArrayList<RequestQaList>();
    public final static Parcelable.Creator<RequestQaJsonData> CREATOR = new Creator<RequestQaJsonData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RequestQaJsonData createFromParcel(Parcel in) {
            return new RequestQaJsonData(in);
        }

        public RequestQaJsonData[] newArray(int size) {
            return (new RequestQaJsonData[size]);
        }

    }
            ;

    protected RequestQaJsonData(Parcel in) {
        in.readList(this.requestList, (RequestQaList.class.getClassLoader()));
    }

    public RequestQaJsonData() {
    }

    public List<RequestQaList> getRequestList() {
        return requestList;
    }

    public void setRequestList(List<RequestQaList> requestList) {
        this.requestList = requestList;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(requestList);
    }

    public int describeContents() {
        return 0;
    }

}
