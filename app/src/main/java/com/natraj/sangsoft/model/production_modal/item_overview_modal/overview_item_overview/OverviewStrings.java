
package com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_overview;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OverviewStrings implements Parcelable
{

    @SerializedName("lblQuantity")
    @Expose
    private String lblQuantity="";
    @SerializedName("col03")
    @Expose
    private String col03="";
    @SerializedName("areaTitle")
    @Expose
    private String areaTitle="";
    @SerializedName("lblMachines")
    @Expose
    private String lblMachines="";
    @SerializedName("lblMatDetails")
    @Expose
    private String lblMatDetails="";
    @SerializedName("col01")
    @Expose
    private String col01="";
    @SerializedName("col02")
    @Expose
    private String col02="";
    @SerializedName("col04")
    @Expose
    private String col04="";
    @SerializedName("col05")
    @Expose
    private String col05="";
    public final static Creator<OverviewStrings> CREATOR = new Creator<OverviewStrings>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OverviewStrings createFromParcel(Parcel in) {
            return new OverviewStrings(in);
        }

        public OverviewStrings[] newArray(int size) {
            return (new OverviewStrings[size]);
        }

    }
            ;

    protected OverviewStrings(Parcel in) {
        this.lblQuantity = ((String) in.readValue((String.class.getClassLoader())));
        this.col03 = ((String) in.readValue((String.class.getClassLoader())));
        this.areaTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.lblMachines = ((String) in.readValue((String.class.getClassLoader())));
        this.lblMatDetails = ((String) in.readValue((String.class.getClassLoader())));
        this.col01 = ((String) in.readValue((String.class.getClassLoader())));
        this.col02 = ((String) in.readValue((String.class.getClassLoader())));
        this.col04 = ((String) in.readValue((String.class.getClassLoader())));
        this.col05 = ((String) in.readValue((String.class.getClassLoader())));
    }

    public OverviewStrings() {
    }

    public String getLblQuantity() {
        return lblQuantity;
    }

    public void setLblQuantity(String lblQuantity) {
        this.lblQuantity = lblQuantity;
    }

    public String getCol03() {
        return col03;
    }

    public void setCol03(String col03) {
        this.col03 = col03;
    }

    public String getAreaTitle() {
        return areaTitle;
    }

    public void setAreaTitle(String areaTitle) {
        this.areaTitle = areaTitle;
    }

    public String getLblMachines() {
        return lblMachines;
    }

    public void setLblMachines(String lblMachines) {
        this.lblMachines = lblMachines;
    }

    public String getLblMatDetails() {
        return lblMatDetails;
    }

    public void setLblMatDetails(String lblMatDetails) {
        this.lblMatDetails = lblMatDetails;
    }

    public String getCol01() {
        return col01;
    }

    public void setCol01(String col01) {
        this.col01 = col01;
    }

    public String getCol02() {
        return col02;
    }

    public void setCol02(String col02) {
        this.col02 = col02;
    }

    public String getCol04() {
        return col04;
    }

    public void setCol04(String col04) {
        this.col04 = col04;
    }

    public String getCol05() {
        return col05;
    }

    public void setCol05(String col05) {
        this.col05 = col05;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(lblQuantity);
        dest.writeValue(col03);
        dest.writeValue(areaTitle);
        dest.writeValue(lblMachines);
        dest.writeValue(lblMatDetails);
        dest.writeValue(col01);
        dest.writeValue(col02);
        dest.writeValue(col04);
        dest.writeValue(col05);
    }

    public int describeContents() {
        return 0;
    }

}