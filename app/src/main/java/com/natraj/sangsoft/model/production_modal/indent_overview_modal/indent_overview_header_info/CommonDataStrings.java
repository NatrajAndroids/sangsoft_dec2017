
package com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_header_info;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommonDataStrings implements Parcelable
{

    @SerializedName("helpTooltip")
    @Expose
    private String helpTooltip="";
    @SerializedName("maintTooltip")
    @Expose
    private String maintTooltip="";
    @SerializedName("prodTooltip")
    @Expose
    private String prodTooltip="";
    @SerializedName("prodBtn")
    @Expose
    private String prodBtn="";
    @SerializedName("QATooltip")
    @Expose
    private String qATooltip="";
    public final static Creator<CommonDataStrings> CREATOR = new Creator<CommonDataStrings>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CommonDataStrings createFromParcel(Parcel in) {
            return new CommonDataStrings(in);
        }

        public CommonDataStrings[] newArray(int size) {
            return (new CommonDataStrings[size]);
        }

    }
            ;

    protected CommonDataStrings(Parcel in) {
        this.helpTooltip = ((String) in.readValue((String.class.getClassLoader())));
        this.maintTooltip = ((String) in.readValue((String.class.getClassLoader())));
        this.prodTooltip = ((String) in.readValue((String.class.getClassLoader())));
        this.prodBtn = ((String) in.readValue((String.class.getClassLoader())));
        this.qATooltip = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public CommonDataStrings() {
    }

    /**
     *
     * @param maintTooltip
     * @param qATooltip
     * @param helpTooltip
     * @param prodBtn
     * @param prodTooltip
     */
    public CommonDataStrings(String helpTooltip, String maintTooltip, String prodTooltip, String prodBtn, String qATooltip) {
        super();
        this.helpTooltip = helpTooltip;
        this.maintTooltip = maintTooltip;
        this.prodTooltip = prodTooltip;
        this.prodBtn = prodBtn;
        this.qATooltip = qATooltip;
    }

    public String getHelpTooltip() {
        return helpTooltip;
    }

    public void setHelpTooltip(String helpTooltip) {
        this.helpTooltip = helpTooltip;
    }

    public String getMaintTooltip() {
        return maintTooltip;
    }

    public void setMaintTooltip(String maintTooltip) {
        this.maintTooltip = maintTooltip;
    }

    public String getProdTooltip() {
        return prodTooltip;
    }

    public void setProdTooltip(String prodTooltip) {
        this.prodTooltip = prodTooltip;
    }

    public String getProdBtn() {
        return prodBtn;
    }

    public void setProdBtn(String prodBtn) {
        this.prodBtn = prodBtn;
    }

    public String getQATooltip() {
        return qATooltip;
    }

    public void setQATooltip(String qATooltip) {
        this.qATooltip = qATooltip;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(helpTooltip);
        dest.writeValue(maintTooltip);
        dest.writeValue(prodTooltip);
        dest.writeValue(prodBtn);
        dest.writeValue(qATooltip);
    }

    public int describeContents() {
        return 0;
    }

}