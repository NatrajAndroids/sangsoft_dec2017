
package com.natraj.sangsoft.model.maintenance_modals.maintenance_execute;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaintenanceExecuteStrings implements Parcelable
{

    @SerializedName("lblComments")
    @Expose
    private String lblComments="";
    @SerializedName("colStatus")
    @Expose
    private String colStatus="";
    @SerializedName("btnActivity")
    @Expose
    private String btnActivity="";
    @SerializedName("btnCancel")
    @Expose
    private String btnCancel="";
    @SerializedName("areaTitle")
    @Expose
    private String areaTitle="";
    @SerializedName("chkExecuted")
    @Expose
    private String chkExecuted="";
    @SerializedName("lblStartDate")
    @Expose
    private String lblStartDate="";
    @SerializedName("lblStopDate")
    @Expose
    private String lblStopDate="";
    @SerializedName("colActivity")
    @Expose
    private String colActivity="";
    public final static Creator<MaintenanceExecuteStrings> CREATOR = new Creator<MaintenanceExecuteStrings>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MaintenanceExecuteStrings createFromParcel(Parcel in) {
            return new MaintenanceExecuteStrings(in);
        }

        public MaintenanceExecuteStrings[] newArray(int size) {
            return (new MaintenanceExecuteStrings[size]);
        }

    }
            ;

    protected MaintenanceExecuteStrings(Parcel in) {
        this.lblComments = ((String) in.readValue((String.class.getClassLoader())));
        this.colStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.btnActivity = ((String) in.readValue((String.class.getClassLoader())));
        this.btnCancel = ((String) in.readValue((String.class.getClassLoader())));
        this.areaTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.chkExecuted = ((String) in.readValue((String.class.getClassLoader())));
        this.lblStartDate = ((String) in.readValue((String.class.getClassLoader())));
        this.lblStopDate = ((String) in.readValue((String.class.getClassLoader())));
        this.colActivity = ((String) in.readValue((String.class.getClassLoader())));
    }

    public MaintenanceExecuteStrings() {
    }

    public String getLblComments() {
        return lblComments;
    }

    public void setLblComments(String lblComments) {
        this.lblComments = lblComments;
    }

    public String getColStatus() {
        return colStatus;
    }

    public void setColStatus(String colStatus) {
        this.colStatus = colStatus;
    }

    public String getBtnActivity() {
        return btnActivity;
    }

    public void setBtnActivity(String btnActivity) {
        this.btnActivity = btnActivity;
    }

    public String getBtnCancel() {
        return btnCancel;
    }

    public void setBtnCancel(String btnCancel) {
        this.btnCancel = btnCancel;
    }

    public String getAreaTitle() {
        return areaTitle;
    }

    public void setAreaTitle(String areaTitle) {
        this.areaTitle = areaTitle;
    }

    public String getChkExecuted() {
        return chkExecuted;
    }

    public void setChkExecuted(String chkExecuted) {
        this.chkExecuted = chkExecuted;
    }

    public String getLblStartDate() {
        return lblStartDate;
    }

    public void setLblStartDate(String lblStartDate) {
        this.lblStartDate = lblStartDate;
    }

    public String getLblStopDate() {
        return lblStopDate;
    }

    public void setLblStopDate(String lblStopDate) {
        this.lblStopDate = lblStopDate;
    }

    public String getColActivity() {
        return colActivity;
    }

    public void setColActivity(String colActivity) {
        this.colActivity = colActivity;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(lblComments);
        dest.writeValue(colStatus);
        dest.writeValue(btnActivity);
        dest.writeValue(btnCancel);
        dest.writeValue(areaTitle);
        dest.writeValue(chkExecuted);
        dest.writeValue(lblStartDate);
        dest.writeValue(lblStopDate);
        dest.writeValue(colActivity);
    }

    public int describeContents() {
        return 0;
    }

}