
package com.natraj.sangsoft.model.production_modal.data_indent_info_bar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IndentinfoLabel implements Parcelable
{

    @SerializedName("lblIndentNo")
    @Expose
    private String lblIndentNo="";
    @SerializedName("lblForItem")
    @Expose
    private String lblForItem="";
    @SerializedName("lblTiming")
    @Expose
    private String lblTiming="";
    @SerializedName("lblQty")
    @Expose
    private String lblQty="";
    public final static Creator<IndentinfoLabel> CREATOR = new Creator<IndentinfoLabel>() {


        @SuppressWarnings({
            "unchecked"
        })
        public IndentinfoLabel createFromParcel(Parcel in) {
            IndentinfoLabel instance = new IndentinfoLabel();
            instance.lblIndentNo = ((String) in.readValue((String.class.getClassLoader())));
            instance.lblForItem = ((String) in.readValue((String.class.getClassLoader())));
            instance.lblTiming = ((String) in.readValue((String.class.getClassLoader())));
            instance.lblQty = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public IndentinfoLabel[] newArray(int size) {
            return (new IndentinfoLabel[size]);
        }

    }
    ;

    public String getLblIndentNo() {
        return lblIndentNo;
    }

    public void setLblIndentNo(String lblIndentNo) {
        this.lblIndentNo = lblIndentNo;
    }

    public String getLblForItem() {
        return lblForItem;
    }

    public void setLblForItem(String lblForItem) {
        this.lblForItem = lblForItem;
    }

    public String getLblTiming() {
        return lblTiming;
    }

    public void setLblTiming(String lblTiming) {
        this.lblTiming = lblTiming;
    }

    public String getLblQty() {
        return lblQty;
    }

    public void setLblQty(String lblQty) {
        this.lblQty = lblQty;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(lblIndentNo);
        dest.writeValue(lblForItem);
        dest.writeValue(lblTiming);
        dest.writeValue(lblQty);
    }

    public int describeContents() {
        return  0;
    }

}
