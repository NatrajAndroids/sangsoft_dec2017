package com.natraj.sangsoft.model.login_page_label;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginLabel implements Parcelable {

    public final static Creator<LoginLabel> CREATOR = new Creator<LoginLabel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LoginLabel createFromParcel(Parcel in) {
            return new LoginLabel(in);
        }

        public LoginLabel[] newArray(int size) {
            return (new LoginLabel[size]);
        }

    };
    @SerializedName("failedMsg")
    @Expose
    private String failedMsg="";
    @SerializedName("empID")
    @Expose
    private String empID="";
    @SerializedName("loginBtn")
    @Expose
    private String loginBtn="";
    @SerializedName("PIN")
    @Expose
    private String pIN="";
    @SerializedName("greetingMsg")
    @Expose
    private String greetingMsg="";

    protected LoginLabel(Parcel in) {
        this.failedMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.empID = ((String) in.readValue((String.class.getClassLoader())));
        this.loginBtn = ((String) in.readValue((String.class.getClassLoader())));
        this.pIN = ((String) in.readValue((String.class.getClassLoader())));
        this.greetingMsg = ((String) in.readValue((String.class.getClassLoader())));
    }

    public LoginLabel() {
    }

    public String getFailedMsg() {
        return failedMsg;
    }

    public void setFailedMsg(String failedMsg) {
        this.failedMsg = failedMsg;
    }

    public String getEmpID() {
        return empID;
    }

    public void setEmpID(String empID) {
        this.empID = empID;
    }

    public String getLoginBtn() {
        return loginBtn;
    }

    public void setLoginBtn(String loginBtn) {
        this.loginBtn = loginBtn;
    }

    public String getPIN() {
        return pIN;
    }

    public void setPIN(String pIN) {
        this.pIN = pIN;
    }

    public String getGreetingMsg() {
        return greetingMsg;
    }

    public void setGreetingMsg(String greetingMsg) {
        this.greetingMsg = greetingMsg;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(failedMsg);
        dest.writeValue(empID);
        dest.writeValue(loginBtn);
        dest.writeValue(pIN);
        dest.writeValue(greetingMsg);
    }

    public int describeContents() {
        return 0;
    }

}