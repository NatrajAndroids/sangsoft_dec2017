
package com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_info;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OverviewItemInfoJsonData implements Parcelable
{

    @SerializedName("item_id")
    @Expose
    private String itemId="";
    @SerializedName("item_mcode")
    @Expose
    private String itemMcode="";
    @SerializedName("item_ref01")
    @Expose
    private String itemRef01="";
    @SerializedName("item_name")
    @Expose
    private String itemName="";
    @SerializedName("item_type")
    @Expose
    private String itemType="";
    @SerializedName("selected_process_id")
    @Expose
    private String selectedProcessId="";
    @SerializedName("processes")
    @Expose
    private List<OverviewItemInfoProcess> processes = new ArrayList<OverviewItemInfoProcess>();
    public final static Creator<OverviewItemInfoJsonData> CREATOR = new Creator<OverviewItemInfoJsonData>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OverviewItemInfoJsonData createFromParcel(Parcel in) {
            return new OverviewItemInfoJsonData(in);
        }

        public OverviewItemInfoJsonData[] newArray(int size) {
            return (new OverviewItemInfoJsonData[size]);
        }

    }
    ;

    protected OverviewItemInfoJsonData(Parcel in) {
        this.itemId = ((String) in.readValue((String.class.getClassLoader())));
        this.itemMcode = ((String) in.readValue((String.class.getClassLoader())));
        this.itemRef01 = ((String) in.readValue((String.class.getClassLoader())));
        this.itemName = ((String) in.readValue((String.class.getClassLoader())));
        this.itemType = ((String) in.readValue((String.class.getClassLoader())));
        this.selectedProcessId = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.processes, (OverviewItemInfoProcess.class.getClassLoader()));
    }

    public OverviewItemInfoJsonData() {
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemMcode() {
        return itemMcode;
    }

    public void setItemMcode(String itemMcode) {
        this.itemMcode = itemMcode;
    }

    public String getItemRef01() {
        return itemRef01;
    }

    public void setItemRef01(String itemRef01) {
        this.itemRef01 = itemRef01;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getSelectedProcessId() {
        return selectedProcessId;
    }

    public void setSelectedProcessId(String selectedProcessId) {
        this.selectedProcessId = selectedProcessId;
    }

    public List<OverviewItemInfoProcess> getProcesses() {
        return processes;
    }

    public void setProcesses(List<OverviewItemInfoProcess> processes) {
        this.processes = processes;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(itemId);
        dest.writeValue(itemMcode);
        dest.writeValue(itemRef01);
        dest.writeValue(itemName);
        dest.writeValue(itemType);
        dest.writeValue(selectedProcessId);
        dest.writeList(processes);
    }

    public int describeContents() {
        return  0;
    }

}
