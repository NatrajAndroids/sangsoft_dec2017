
package com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_actionbar;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order implements Parcelable
{

    @SerializedName("orderID")
    @Expose
    private String orderID;
    @SerializedName("indent")
    @Expose
    private String indent;
    @SerializedName("orderStatus")
    @Expose
    private String orderStatus;
    public final static Parcelable.Creator<Order> CREATOR = new Creator<Order>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        public Order[] newArray(int size) {
            return (new Order[size]);
        }

    }
            ;

    protected Order(Parcel in) {
        this.orderID = ((String) in.readValue((String.class.getClassLoader())));
        this.indent = ((String) in.readValue((String.class.getClassLoader())));
        this.orderStatus = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Order() {
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getIndent() {
        return indent;
    }

    public void setIndent(String indent) {
        this.indent = indent;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(orderID);
        dest.writeValue(indent);
        dest.writeValue(orderStatus);
    }

    public int describeContents() {
        return 0;
    }

}
