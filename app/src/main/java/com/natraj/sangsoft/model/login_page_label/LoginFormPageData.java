
package com.natraj.sangsoft.model.login_page_label;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class LoginFormPageData implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError=0;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg="";
    @SerializedName("strings")
    @Expose
    private LoginLabel strings;
    @SerializedName("jsonData")
    @Expose
    private List<Object> jsonData = new ArrayList<Object>();
    public final static Creator<LoginFormPageData> CREATOR = new Creator<LoginFormPageData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LoginFormPageData createFromParcel(Parcel in) {
            return new LoginFormPageData(in);
        }

        public LoginFormPageData[] newArray(int size) {
            return (new LoginFormPageData[size]);
        }

    }
            ;

    protected LoginFormPageData(Parcel in) {
        this.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((LoginLabel) in.readValue((LoginLabel.class.getClassLoader())));
        in.readList(this.jsonData, (Object.class.getClassLoader()));
    }

    public LoginFormPageData() {
    }

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public LoginLabel getStrings() {
        return strings;
    }

    public void setStrings(LoginLabel strings) {
        this.strings = strings;
    }

    public List<Object> getJsonData() {
        return jsonData;
    }

    public void setJsonData(List<Object> jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeList(jsonData);
    }

    public int describeContents() {
        return 0;
    }

}