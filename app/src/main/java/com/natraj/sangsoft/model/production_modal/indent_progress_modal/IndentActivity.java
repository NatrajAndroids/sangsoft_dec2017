
package com.natraj.sangsoft.model.production_modal.indent_progress_modal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IndentActivity implements Parcelable
{

    @SerializedName("actDate")
    @Expose
    private String actDate;
    @SerializedName("actTime")
    @Expose
    private String actTime;
    @SerializedName("actType")
    @Expose
    private String actType;
    @SerializedName("activity")
    @Expose
    private String activity;
    @SerializedName("actProduced")
    @Expose
    private String actProduced;
    @SerializedName("actRejected")
    @Expose
    private String actRejected;
    @SerializedName("actUser")
    @Expose
    private String actUser;
    @SerializedName("actRremarks")
    @Expose
    private String actRremarks;
    public final static Parcelable.Creator<IndentActivity> CREATOR = new Creator<IndentActivity>() {


        @SuppressWarnings({
                "unchecked"
        })
        public IndentActivity createFromParcel(Parcel in) {
            return new IndentActivity(in);
        }

        public IndentActivity[] newArray(int size) {
            return (new IndentActivity[size]);
        }

    }
            ;

    protected IndentActivity(Parcel in) {
        this.actDate = ((String) in.readValue((String.class.getClassLoader())));
        this.actTime = ((String) in.readValue((String.class.getClassLoader())));
        this.actType = ((String) in.readValue((String.class.getClassLoader())));
        this.activity = ((String) in.readValue((String.class.getClassLoader())));
        this.actProduced = ((String) in.readValue((String.class.getClassLoader())));
        this.actRejected = ((String) in.readValue((String.class.getClassLoader())));
        this.actUser = ((String) in.readValue((String.class.getClassLoader())));
        this.actRremarks = ((String) in.readValue((String.class.getClassLoader())));
    }

    public IndentActivity() {
    }

    public String getActDate() {
        return actDate;
    }

    public void setActDate(String actDate) {
        this.actDate = actDate;
    }

    public String getActTime() {
        return actTime;
    }

    public void setActTime(String actTime) {
        this.actTime = actTime;
    }

    public String getActType() {
        return actType;
    }

    public void setActType(String actType) {
        this.actType = actType;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getActProduced() {
        return actProduced;
    }

    public void setActProduced(String actProduced) {
        this.actProduced = actProduced;
    }

    public String getActRejected() {
        return actRejected;
    }

    public void setActRejected(String actRejected) {
        this.actRejected = actRejected;
    }

    public String getActUser() {
        return actUser;
    }

    public void setActUser(String actUser) {
        this.actUser = actUser;
    }

    public String getActRremarks() {
        return actRremarks;
    }

    public void setActRremarks(String actRremarks) {
        this.actRremarks = actRremarks;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(actDate);
        dest.writeValue(actTime);
        dest.writeValue(actType);
        dest.writeValue(activity);
        dest.writeValue(actProduced);
        dest.writeValue(actRejected);
        dest.writeValue(actUser);
        dest.writeValue(actRremarks);
    }

    public int describeContents() {
        return 0;
    }

}
