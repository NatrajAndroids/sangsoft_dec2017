
package com.natraj.sangsoft.model.production_modal.data_indent_overview;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonData implements Parcelable
{

    @SerializedName("indent_id")
    @Expose
    private String indentId;
    @SerializedName("indent_ref")
    @Expose
    private String indentRef;
    @SerializedName("item_name")
    @Expose
    private String itemName;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("order_qty")
    @Expose
    private String orderQty;
    @SerializedName("produced_qty")
    @Expose
    private String producedQty;
    @SerializedName("rejected_qty")
    @Expose
    private String rejectedQty;
    @SerializedName("prodTimeString")
    @Expose
    private String prodTimeString;
    @SerializedName("prodTime")
    @Expose
    private String prodTime;
    @SerializedName("prodRate")
    @Expose
    private String prodRate;
    public final static Parcelable.Creator<JsonData> CREATOR = new Creator<JsonData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public JsonData createFromParcel(Parcel in) {
            return new JsonData(in);
        }

        public JsonData[] newArray(int size) {
            return (new JsonData[size]);
        }

    }
            ;

    protected JsonData(Parcel in) {
        this.indentId = ((String) in.readValue((String.class.getClassLoader())));
        this.indentRef = ((String) in.readValue((String.class.getClassLoader())));
        this.itemName = ((String) in.readValue((String.class.getClassLoader())));
        this.orderId = ((String) in.readValue((String.class.getClassLoader())));
        this.orderQty = ((String) in.readValue((String.class.getClassLoader())));
        this.producedQty = ((String) in.readValue((String.class.getClassLoader())));
        this.rejectedQty = ((String) in.readValue((String.class.getClassLoader())));
        this.prodTimeString = ((String) in.readValue((String.class.getClassLoader())));
        this.prodTime = ((String) in.readValue((String.class.getClassLoader())));
        this.prodRate = ((String) in.readValue((String.class.getClassLoader())));
    }

    public JsonData() {
    }

    public String getIndentId() {
        return indentId;
    }

    public void setIndentId(String indentId) {
        this.indentId = indentId;
    }

    public String getIndentRef() {
        return indentRef;
    }

    public void setIndentRef(String indentRef) {
        this.indentRef = indentRef;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(String orderQty) {
        this.orderQty = orderQty;
    }

    public String getProducedQty() {
        return producedQty;
    }

    public void setProducedQty(String producedQty) {
        this.producedQty = producedQty;
    }

    public String getRejectedQty() {
        return rejectedQty;
    }

    public void setRejectedQty(String rejectedQty) {
        this.rejectedQty = rejectedQty;
    }

    public String getProdTimeString() {
        return prodTimeString;
    }

    public void setProdTimeString(String prodTimeString) {
        this.prodTimeString = prodTimeString;
    }

    public String getProdTime() {
        return prodTime;
    }

    public void setProdTime(String prodTime) {
        this.prodTime = prodTime;
    }

    public String getProdRate() {
        return prodRate;
    }

    public void setProdRate(String prodRate) {
        this.prodRate = prodRate;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(indentId);
        dest.writeValue(indentRef);
        dest.writeValue(itemName);
        dest.writeValue(orderId);
        dest.writeValue(orderQty);
        dest.writeValue(producedQty);
        dest.writeValue(rejectedQty);
        dest.writeValue(prodTimeString);
        dest.writeValue(prodTime);
        dest.writeValue(prodRate);
    }

    public int describeContents() {
        return 0;
    }

}
