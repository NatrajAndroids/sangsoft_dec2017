package com.natraj.sangsoft.model.record_qa_modal.question_set;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LookupSet implements Parcelable
{

    @SerializedName("lookup_nm")
    @Expose
    private String lookupNm;
    @SerializedName("sequence")
    @Expose
    private String sequence;
    @SerializedName("data_key")
    @Expose
    private String dataKey;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    public final static Parcelable.Creator<LookupSet> CREATOR = new Creator<LookupSet>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LookupSet createFromParcel(Parcel in) {
            return new LookupSet(in);
        }

        public LookupSet[] newArray(int size) {
            return (new LookupSet[size]);
        }

    }
            ;

    protected LookupSet(Parcel in) {
        this.lookupNm = ((String) in.readValue((String.class.getClassLoader())));
        this.sequence = ((String) in.readValue((String.class.getClassLoader())));
        this.dataKey = ((String) in.readValue((String.class.getClassLoader())));
        this.displayName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public LookupSet() {
    }

    public String getLookupNm() {
        return lookupNm;
    }

    public void setLookupNm(String lookupNm) {
        this.lookupNm = lookupNm;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getDataKey() {
        return dataKey;
    }

    public void setDataKey(String dataKey) {
        this.dataKey = dataKey;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(lookupNm);
        dest.writeValue(sequence);
        dest.writeValue(dataKey);
        dest.writeValue(displayName);
    }

    public int describeContents() {
        return 0;
    }

}
