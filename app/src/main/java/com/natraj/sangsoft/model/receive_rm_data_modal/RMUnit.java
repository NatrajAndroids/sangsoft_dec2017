package com.natraj.sangsoft.model.receive_rm_data_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RMUnit implements Parcelable
{

    @SerializedName("valueid")
    @Expose
    private String valueid;
    @SerializedName("keyid")
    @Expose
    private String keyid;
    @SerializedName("data_key")
    @Expose
    private String dataKey;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    public final static Parcelable.Creator<RMUnit> CREATOR = new Creator<RMUnit>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RMUnit createFromParcel(Parcel in) {
            return new RMUnit(in);
        }

        public RMUnit[] newArray(int size) {
            return (new RMUnit[size]);
        }

    }
            ;

    protected RMUnit(Parcel in) {
        this.valueid = ((String) in.readValue((String.class.getClassLoader())));
        this.keyid = ((String) in.readValue((String.class.getClassLoader())));
        this.dataKey = ((String) in.readValue((String.class.getClassLoader())));
        this.displayName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public RMUnit() {
    }

    public String getValueid() {
        return valueid;
    }

    public void setValueid(String valueid) {
        this.valueid = valueid;
    }

    public String getKeyid() {
        return keyid;
    }

    public void setKeyid(String keyid) {
        this.keyid = keyid;
    }

    public String getDataKey() {
        return dataKey;
    }

    public void setDataKey(String dataKey) {
        this.dataKey = dataKey;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(valueid);
        dest.writeValue(keyid);
        dest.writeValue(dataKey);
        dest.writeValue(displayName);
    }

    public int describeContents() {
        return 0;
    }

}
