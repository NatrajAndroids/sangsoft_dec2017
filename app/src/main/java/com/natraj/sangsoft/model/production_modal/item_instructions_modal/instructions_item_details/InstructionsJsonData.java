
package com.natraj.sangsoft.model.production_modal.item_instructions_modal.instructions_item_details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class InstructionsJsonData implements Parcelable
{

    @SerializedName("rows")
    @Expose
    private List<InstructionsRow> rows = new ArrayList<InstructionsRow>();
    public final static Creator<InstructionsJsonData> CREATOR = new Creator<InstructionsJsonData>() {


        @SuppressWarnings({
            "unchecked"
        })
        public InstructionsJsonData createFromParcel(Parcel in) {
            return new InstructionsJsonData(in);
        }

        public InstructionsJsonData[] newArray(int size) {
            return (new InstructionsJsonData[size]);
        }

    }
    ;

    protected InstructionsJsonData(Parcel in) {
        in.readList(this.rows, (InstructionsRow.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public InstructionsJsonData() {
    }

    /**
     * 
     * @param rows
     */
    public InstructionsJsonData(List<InstructionsRow> rows) {
        super();
        this.rows = rows;
    }

    public List<InstructionsRow> getRows() {
        return rows;
    }

    public void setRows(List<InstructionsRow> rows) {
        this.rows = rows;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(rows);
    }

    public int describeContents() {
        return  0;
    }

}
