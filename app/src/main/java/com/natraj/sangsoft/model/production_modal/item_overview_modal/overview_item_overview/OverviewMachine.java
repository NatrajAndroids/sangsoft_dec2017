
package com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_overview;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OverviewMachine implements Parcelable
{

    @SerializedName("mch_id")
    @Expose
    private String mchId="";
    @SerializedName("mch_name")
    @Expose
    private String mchName="";
    public final static Creator<OverviewMachine> CREATOR = new Creator<OverviewMachine>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OverviewMachine createFromParcel(Parcel in) {
            return new OverviewMachine(in);
        }

        public OverviewMachine[] newArray(int size) {
            return (new OverviewMachine[size]);
        }

    }
    ;

    protected OverviewMachine(Parcel in) {
        this.mchId = ((String) in.readValue((String.class.getClassLoader())));
        this.mchName = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public OverviewMachine() {
    }

    /**
     * 
     * @param mchId
     * @param mchName
     */
    public OverviewMachine(String mchId, String mchName) {
        super();
        this.mchId = mchId;
        this.mchName = mchName;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getMchName() {
        return mchName;
    }

    public void setMchName(String mchName) {
        this.mchName = mchName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(mchId);
        dest.writeValue(mchName);
    }

    public int describeContents() {
        return  0;
    }

}
