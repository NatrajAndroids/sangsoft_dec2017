package com.natraj.sangsoft.model.production_modal.indent_record_prod;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecordProdOutputItem implements Parcelable
{

    @SerializedName("item_id")
    @Expose
    private String itemId;
    @SerializedName("order_bom_ID")
    @Expose
    private String orderBomID;
    @SerializedName("itemcode")
    @Expose
    private String itemcode;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("ExpectedQty")
    @Expose
    private String expectedQty;
    @SerializedName("ExpectedUOM")
    @Expose
    private String expectedUOM;
    @SerializedName("ReceivedQty")
    @Expose
    private String receivedQty;
    public final static Parcelable.Creator<RecordProdOutputItem> CREATOR = new Creator<RecordProdOutputItem>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RecordProdOutputItem createFromParcel(Parcel in) {
            return new RecordProdOutputItem(in);
        }

        public RecordProdOutputItem[] newArray(int size) {
            return (new RecordProdOutputItem[size]);
        }

    }
            ;

    protected RecordProdOutputItem(Parcel in) {
        this.itemId = ((String) in.readValue((String.class.getClassLoader())));
        this.orderBomID = ((String) in.readValue((String.class.getClassLoader())));
        this.itemcode = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.expectedQty = ((String) in.readValue((String.class.getClassLoader())));
        this.expectedUOM = ((String) in.readValue((String.class.getClassLoader())));
        this.receivedQty = ((String) in.readValue((String.class.getClassLoader())));
    }

    public RecordProdOutputItem() {
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getOrderBomID() {
        return orderBomID;
    }

    public void setOrderBomID(String orderBomID) {
        this.orderBomID = orderBomID;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpectedQty() {
        return expectedQty;
    }

    public void setExpectedQty(String expectedQty) {
        this.expectedQty = expectedQty;
    }

    public String getExpectedUOM() {
        return expectedUOM;
    }

    public void setExpectedUOM(String expectedUOM) {
        this.expectedUOM = expectedUOM;
    }

    public String getReceivedQty() {
        return receivedQty;
    }

    public void setReceivedQty(String receivedQty) {
        this.receivedQty = receivedQty;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(itemId);
        dest.writeValue(orderBomID);
        dest.writeValue(itemcode);
        dest.writeValue(description);
        dest.writeValue(expectedQty);
        dest.writeValue(expectedUOM);
        dest.writeValue(receivedQty);
    }

    public int describeContents() {
        return 0;
    }

}
