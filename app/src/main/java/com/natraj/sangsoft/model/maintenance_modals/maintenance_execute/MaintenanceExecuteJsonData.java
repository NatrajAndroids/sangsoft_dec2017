
package com.natraj.sangsoft.model.maintenance_modals.maintenance_execute;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MaintenanceExecuteJsonData implements Parcelable
{

    @SerializedName("rows")
    @Expose
    private List<MaintenanceExecuteRow> rows = new ArrayList<MaintenanceExecuteRow>();
    public final static Creator<MaintenanceExecuteJsonData> CREATOR = new Creator<MaintenanceExecuteJsonData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MaintenanceExecuteJsonData createFromParcel(Parcel in) {
            return new MaintenanceExecuteJsonData(in);
        }

        public MaintenanceExecuteJsonData[] newArray(int size) {
            return (new MaintenanceExecuteJsonData[size]);
        }

    }
            ;

    protected MaintenanceExecuteJsonData(Parcel in) {
        in.readList(this.rows, (MaintenanceExecuteRow.class.getClassLoader()));
    }

    public MaintenanceExecuteJsonData() {
    }

    public List<MaintenanceExecuteRow> getRows() {
        return rows;
    }

    public void setRows(List<MaintenanceExecuteRow> rows) {
        this.rows = rows;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(rows);
    }

    public int describeContents() {
        return 0;
    }

}