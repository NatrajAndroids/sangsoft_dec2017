
package com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_overview;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OverviewJsonData implements Parcelable
{

    @SerializedName("items")
    @Expose
    private List<OverviewItem> items = new ArrayList<OverviewItem>();
    @SerializedName("machines")
    @Expose
    private List<OverviewMachine> machines = new ArrayList<OverviewMachine>();
    public final static Creator<OverviewJsonData> CREATOR = new Creator<OverviewJsonData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OverviewJsonData createFromParcel(Parcel in) {
            return new OverviewJsonData(in);
        }

        public OverviewJsonData[] newArray(int size) {
            return (new OverviewJsonData[size]);
        }

    }
            ;

    protected OverviewJsonData(Parcel in) {
        in.readList(this.items, (OverviewItem.class.getClassLoader()));
        in.readList(this.machines, (OverviewMachine.class.getClassLoader()));
    }

    public OverviewJsonData() {
    }

    public List<OverviewItem> getItems() {
        return items;
    }

    public void setItems(List<OverviewItem> items) {
        this.items = items;
    }

    public List<OverviewMachine> getMachines() {
        return machines;
    }

    public void setMachines(List<OverviewMachine> machines) {
        this.machines = machines;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(items);
        dest.writeList(machines);
    }

    public int describeContents() {
        return 0;
    }

}