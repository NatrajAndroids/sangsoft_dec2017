
package com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_bar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OverviewItemBarStrings implements Parcelable
{

    @SerializedName("btnImg_video")
    @Expose
    private String btnImgVideo="";
    @SerializedName("btnInstruction")
    @Expose
    private String btnInstruction="";
    @SerializedName("btnOthers")
    @Expose
    private String btnOthers="";
    @SerializedName("btnOverview")
    @Expose
    private String btnOverview="";
    public final static Creator<OverviewItemBarStrings> CREATOR = new Creator<OverviewItemBarStrings>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OverviewItemBarStrings createFromParcel(Parcel in) {
            return new OverviewItemBarStrings(in);
        }

        public OverviewItemBarStrings[] newArray(int size) {
            return (new OverviewItemBarStrings[size]);
        }

    }
    ;

    protected OverviewItemBarStrings(Parcel in) {
        this.btnImgVideo = ((String) in.readValue((String.class.getClassLoader())));
        this.btnInstruction = ((String) in.readValue((String.class.getClassLoader())));
        this.btnOthers = ((String) in.readValue((String.class.getClassLoader())));
        this.btnOverview = ((String) in.readValue((String.class.getClassLoader())));
    }

    public OverviewItemBarStrings() {
    }

    public String getBtnImgVideo() {
        return btnImgVideo;
    }

    public void setBtnImgVideo(String btnImgVideo) {
        this.btnImgVideo = btnImgVideo;
    }

    public String getBtnInstruction() {
        return btnInstruction;
    }

    public void setBtnInstruction(String btnInstruction) {
        this.btnInstruction = btnInstruction;
    }

    public String getBtnOthers() {
        return btnOthers;
    }

    public void setBtnOthers(String btnOthers) {
        this.btnOthers = btnOthers;
    }

    public String getBtnOverview() {
        return btnOverview;
    }

    public void setBtnOverview(String btnOverview) {
        this.btnOverview = btnOverview;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(btnImgVideo);
        dest.writeValue(btnInstruction);
        dest.writeValue(btnOthers);
        dest.writeValue(btnOverview);
    }

    public int describeContents() {
        return  0;
    }

}
