
package com.natraj.sangsoft.model.production_modal.indent_progress_modal;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IndentProgressItemsModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;
    @SerializedName("strings")
    @Expose
    private IndentProgressStrings strings;
    @SerializedName("IndentActivity")
    @Expose
    private List<IndentActivity> indentActivity = new ArrayList<IndentActivity>();
    public final static Parcelable.Creator<IndentProgressItemsModal> CREATOR = new Creator<IndentProgressItemsModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public IndentProgressItemsModal createFromParcel(Parcel in) {
            return new IndentProgressItemsModal(in);
        }

        public IndentProgressItemsModal[] newArray(int size) {
            return (new IndentProgressItemsModal[size]);
        }

    }
            ;

    protected IndentProgressItemsModal(Parcel in) {
        this.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((IndentProgressStrings) in.readValue((IndentProgressStrings.class.getClassLoader())));
        in.readList(this.indentActivity, (com.natraj.sangsoft.model.production_modal.indent_progress_modal.IndentActivity.class.getClassLoader()));
    }

    public IndentProgressItemsModal() {
    }

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public IndentProgressStrings getStrings() {
        return strings;
    }

    public void setStrings(IndentProgressStrings strings) {
        this.strings = strings;
    }

    public List<IndentActivity> getIndentActivity() {
        return indentActivity;
    }

    public void setIndentActivity(List<IndentActivity> indentActivity) {
        this.indentActivity = indentActivity;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeList(indentActivity);
    }

    public int describeContents() {
        return 0;
    }

}
