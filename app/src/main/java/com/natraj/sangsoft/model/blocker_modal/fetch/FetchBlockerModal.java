package com.natraj.sangsoft.model.blocker_modal.fetch;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FetchBlockerModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;
    @SerializedName("strings")
    @Expose
    private FetchBlockerStrings strings;
    @SerializedName("jsonData")
    @Expose
    private FetchBlockerJsonData jsonData;
    public final static Parcelable.Creator<FetchBlockerModal> CREATOR = new Creator<FetchBlockerModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public FetchBlockerModal createFromParcel(Parcel in) {
            return new FetchBlockerModal(in);
        }

        public FetchBlockerModal[] newArray(int size) {
            return (new FetchBlockerModal[size]);
        }

    }
            ;

    protected FetchBlockerModal(Parcel in) {
        this.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((FetchBlockerStrings) in.readValue((FetchBlockerStrings.class.getClassLoader())));
        this.jsonData = ((FetchBlockerJsonData) in.readValue((FetchBlockerJsonData.class.getClassLoader())));
    }

    public FetchBlockerModal() {
    }

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public FetchBlockerStrings getStrings() {
        return strings;
    }

    public void setStrings(FetchBlockerStrings strings) {
        this.strings = strings;
    }

    public FetchBlockerJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(FetchBlockerJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return 0;
    }

}
