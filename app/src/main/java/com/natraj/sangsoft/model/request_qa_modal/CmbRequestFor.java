
package com.natraj.sangsoft.model.request_qa_modal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CmbRequestFor implements Parcelable
{

    @SerializedName("reqForID")
    @Expose
    private String reqForID;
    @SerializedName("reqFor")
    @Expose
    private String reqFor;
    public final static Parcelable.Creator<CmbRequestFor> CREATOR = new Creator<CmbRequestFor>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CmbRequestFor createFromParcel(Parcel in) {
            return new CmbRequestFor(in);
        }

        public CmbRequestFor[] newArray(int size) {
            return (new CmbRequestFor[size]);
        }

    }
            ;

    protected CmbRequestFor(Parcel in) {
        this.reqForID = ((String) in.readValue((String.class.getClassLoader())));
        this.reqFor = ((String) in.readValue((String.class.getClassLoader())));
    }

    public CmbRequestFor() {
    }

    public String getReqForID() {
        return reqForID;
    }

    public void setReqForID(String reqForID) {
        this.reqForID = reqForID;
    }

    public String getReqFor() {
        return reqFor;
    }

    public void setReqFor(String reqFor) {
        this.reqFor = reqFor;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(reqForID);
        dest.writeValue(reqFor);
    }

    public int describeContents() {
        return 0;
    }

}
