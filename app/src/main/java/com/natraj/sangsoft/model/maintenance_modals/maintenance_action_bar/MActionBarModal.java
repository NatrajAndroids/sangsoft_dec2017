package com.natraj.sangsoft.model.maintenance_modals.maintenance_action_bar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MActionBarModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError=0;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg="";
    @SerializedName("strings")
    @Expose
    private MActionBarStrings strings;
    @SerializedName("jsonData")
    @Expose
    private List<Object> jsonData = new ArrayList<Object>();
    public final static Creator<MActionBarModal> CREATOR = new Creator<MActionBarModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MActionBarModal createFromParcel(Parcel in) {
            return new MActionBarModal(in);
        }

        public MActionBarModal[] newArray(int size) {
            return (new MActionBarModal[size]);
        }

    }
            ;

    protected MActionBarModal(Parcel in) {
        this.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((MActionBarStrings) in.readValue((MActionBarStrings.class.getClassLoader())));
        in.readList(this.jsonData, (Object.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public MActionBarModal() {
    }

    /**
     *
     * @param strings
     * @param errorMsg
     * @param isError
     * @param jsonData
     */
    public MActionBarModal(Integer isError, String errorMsg, MActionBarStrings strings, List<Object> jsonData) {
        super();
        this.isError = isError;
        this.errorMsg = errorMsg;
        this.strings = strings;
        this.jsonData = jsonData;
    }

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public MActionBarModal withIsError(Integer isError) {
        this.isError = isError;
        return this;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public MActionBarModal withErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
        return this;
    }

    public MActionBarStrings getStrings() {
        return strings;
    }

    public void setStrings(MActionBarStrings strings) {
        this.strings = strings;
    }

    public MActionBarModal withStrings(MActionBarStrings strings) {
        this.strings = strings;
        return this;
    }

    public List<Object> getJsonData() {
        return jsonData;
    }

    public void setJsonData(List<Object> jsonData) {
        this.jsonData = jsonData;
    }

    public MActionBarModal withJsonData(List<Object> jsonData) {
        this.jsonData = jsonData;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeList(jsonData);
    }

    public int describeContents() {
        return 0;
    }

}