
package com.natraj.sangsoft.model.maintenance_modals.maintenance_execute;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaintenanceExecuteEventStatusRow implements Parcelable
{

    @SerializedName("srNo")
    @Expose
    private Integer srNo= 0;
    @SerializedName("statusName")
    @Expose
    private String statusName="";
    @SerializedName("statusName_ml")
    @Expose
    private String statusNameMl="";
    @SerializedName("statusValue")
    @Expose
    private String statusValue="";
    public final static Creator<MaintenanceExecuteEventStatusRow> CREATOR = new Creator<MaintenanceExecuteEventStatusRow>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MaintenanceExecuteEventStatusRow createFromParcel(Parcel in) {
            return new MaintenanceExecuteEventStatusRow(in);
        }

        public MaintenanceExecuteEventStatusRow[] newArray(int size) {
            return (new MaintenanceExecuteEventStatusRow[size]);
        }

    }
            ;

    protected MaintenanceExecuteEventStatusRow(Parcel in) {
        this.srNo = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.statusName = ((String) in.readValue((String.class.getClassLoader())));
        this.statusNameMl = ((String) in.readValue((String.class.getClassLoader())));
        this.statusValue = ((String) in.readValue((String.class.getClassLoader())));
    }

    public MaintenanceExecuteEventStatusRow() {
    }

    public Integer getSrNo() {
        return srNo;
    }

    public void setSrNo(Integer srNo) {
        this.srNo = srNo;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStatusNameMl() {
        return statusNameMl;
    }

    public void setStatusNameMl(String statusNameMl) {
        this.statusNameMl = statusNameMl;
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(srNo);
        dest.writeValue(statusName);
        dest.writeValue(statusNameMl);
        dest.writeValue(statusValue);
    }

    public int describeContents() {
        return 0;
    }

}