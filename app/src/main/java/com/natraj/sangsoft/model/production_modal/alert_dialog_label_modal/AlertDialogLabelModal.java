
package com.natraj.sangsoft.model.production_modal.alert_dialog_label_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AlertDialogLabelModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError=0;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg="";
    @SerializedName("strings")
    @Expose
    private AlertDialogStrings strings;
    @SerializedName("jsonData")
    @Expose
    private List<Object> jsonData = new ArrayList<Object>();
    public final static Creator<AlertDialogLabelModal> CREATOR = new Creator<AlertDialogLabelModal>() {


        @SuppressWarnings({
            "unchecked"
        })
        public AlertDialogLabelModal createFromParcel(Parcel in) {
            return new AlertDialogLabelModal(in);
        }

        public AlertDialogLabelModal[] newArray(int size) {
            return (new AlertDialogLabelModal[size]);
        }

    }
    ;

    protected AlertDialogLabelModal(Parcel in) {
        this.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((AlertDialogStrings) in.readValue((AlertDialogStrings.class.getClassLoader())));
        in.readList(this.jsonData, (Object.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public AlertDialogLabelModal() {
    }

    /**
     * 
     * @param strings
     * @param errorMsg
     * @param isError
     * @param jsonData
     */
    public AlertDialogLabelModal(Integer isError, String errorMsg, AlertDialogStrings strings, List<Object> jsonData) {
        super();
        this.isError = isError;
        this.errorMsg = errorMsg;
        this.strings = strings;
        this.jsonData = jsonData;
    }

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public AlertDialogStrings getStrings() {
        return strings;
    }

    public void setStrings(AlertDialogStrings strings) {
        this.strings = strings;
    }

    public List<Object> getJsonData() {
        return jsonData;
    }

    public void setJsonData(List<Object> jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeList(jsonData);
    }

    public int describeContents() {
        return  0;
    }

}
