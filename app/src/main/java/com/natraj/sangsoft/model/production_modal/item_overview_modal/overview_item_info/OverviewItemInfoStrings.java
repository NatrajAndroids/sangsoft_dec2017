
package com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_info;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OverviewItemInfoStrings implements Parcelable
{

    @SerializedName("lblItemDesc")
    @Expose
    private String lblItemDesc="";
    @SerializedName("lblERPcode")
    @Expose
    private String lblERPcode="";
    @SerializedName("lblProcess")
    @Expose
    private String lblProcess="";
    public final static Creator<OverviewItemInfoStrings> CREATOR = new Creator<OverviewItemInfoStrings>() {


        @SuppressWarnings({
            "unchecked"
        })
        public OverviewItemInfoStrings createFromParcel(Parcel in) {
            return new OverviewItemInfoStrings(in);
        }

        public OverviewItemInfoStrings[] newArray(int size) {
            return (new OverviewItemInfoStrings[size]);
        }

    }
    ;

    protected OverviewItemInfoStrings(Parcel in) {
        this.lblItemDesc = ((String) in.readValue((String.class.getClassLoader())));
        this.lblERPcode = ((String) in.readValue((String.class.getClassLoader())));
        this.lblProcess = ((String) in.readValue((String.class.getClassLoader())));
    }

    public OverviewItemInfoStrings() {
    }

    public String getLblItemDesc() {
        return lblItemDesc;
    }

    public void setLblItemDesc(String lblItemDesc) {
        this.lblItemDesc = lblItemDesc;
    }

    public String getLblERPcode() {
        return lblERPcode;
    }

    public void setLblERPcode(String lblERPcode) {
        this.lblERPcode = lblERPcode;
    }

    public String getLblProcess() {
        return lblProcess;
    }

    public void setLblProcess(String lblProcess) {
        this.lblProcess = lblProcess;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(lblItemDesc);
        dest.writeValue(lblERPcode);
        dest.writeValue(lblProcess);
    }

    public int describeContents() {
        return  0;
    }

}
