
package com.natraj.sangsoft.model.production_modal.item_instructions_modal.instructions_item_details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InstructionsStrings implements Parcelable
{

    @SerializedName("areaTitle")
    @Expose
    private String areaTitle="";
    @SerializedName("tabSafety")
    @Expose
    private String tabSafety="";
    @SerializedName("tabWork")
    @Expose
    private String tabWork="";
    public final static Creator<InstructionsStrings> CREATOR = new Creator<InstructionsStrings>() {


        @SuppressWarnings({
            "unchecked"
        })
        public InstructionsStrings createFromParcel(Parcel in) {
            return new InstructionsStrings(in);
        }

        public InstructionsStrings[] newArray(int size) {
            return (new InstructionsStrings[size]);
        }

    }
    ;

    protected InstructionsStrings(Parcel in) {
        this.areaTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.tabSafety = ((String) in.readValue((String.class.getClassLoader())));
        this.tabWork = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public InstructionsStrings() {
    }

    /**
     * 
     * @param areaTitle
     * @param tabWork
     * @param tabSafety
     */
    public InstructionsStrings(String areaTitle, String tabSafety, String tabWork) {
        super();
        this.areaTitle = areaTitle;
        this.tabSafety = tabSafety;
        this.tabWork = tabWork;
    }

    public String getAreaTitle() {
        return areaTitle;
    }

    public void setAreaTitle(String areaTitle) {
        this.areaTitle = areaTitle;
    }

    public String getTabSafety() {
        return tabSafety;
    }

    public void setTabSafety(String tabSafety) {
        this.tabSafety = tabSafety;
    }

    public String getTabWork() {
        return tabWork;
    }

    public void setTabWork(String tabWork) {
        this.tabWork = tabWork;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(areaTitle);
        dest.writeValue(tabSafety);
        dest.writeValue(tabWork);
    }

    public int describeContents() {
        return  0;
    }

}
