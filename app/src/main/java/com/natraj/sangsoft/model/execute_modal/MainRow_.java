
package com.natraj.sangsoft.model.execute_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainRow_ implements Parcelable
{

    @SerializedName("end_time")
    @Expose
    private String endTime="";
    @SerializedName("filter")
    @Expose
    private MainFilter_ filter;
    @SerializedName("sch_status")
    @Expose
    private String schStatus="";
    @SerializedName("start_date")
    @Expose
    private String startDate="";
    @SerializedName("start_time")
    @Expose
    private String startTime="";
    @SerializedName("stop_date")
    @Expose
    private String stopDate="";
    @SerializedName("updated_by")
    @Expose
    private String updatedBy="";
    @SerializedName("updation_date")
    @Expose
    private String updationDate="";
    public final static Creator<MainRow_> CREATOR = new Creator<MainRow_>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MainRow_ createFromParcel(Parcel in) {
            return new MainRow_(in);
        }

        public MainRow_[] newArray(int size) {
            return (new MainRow_[size]);
        }

    }
            ;

    protected MainRow_(Parcel in) {
        this.endTime = ((String) in.readValue((String.class.getClassLoader())));
        this.filter = ((MainFilter_) in.readValue((MainFilter_.class.getClassLoader())));
        this.schStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.startDate = ((String) in.readValue((String.class.getClassLoader())));
        this.startTime = ((String) in.readValue((String.class.getClassLoader())));
        this.stopDate = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedBy = ((String) in.readValue((String.class.getClassLoader())));
        this.updationDate = ((String) in.readValue((String.class.getClassLoader())));
    }

    public MainRow_() {
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public MainFilter_ getFilter() {
        return filter;
    }

    public void setFilter(MainFilter_ filter) {
        this.filter = filter;
    }

    public String getSchStatus() {
        return schStatus;
    }

    public void setSchStatus(String schStatus) {
        this.schStatus = schStatus;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStopDate() {
        return stopDate;
    }

    public void setStopDate(String stopDate) {
        this.stopDate = stopDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdationDate() {
        return updationDate;
    }

    public void setUpdationDate(String updationDate) {
        this.updationDate = updationDate;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(endTime);
        dest.writeValue(filter);
        dest.writeValue(schStatus);
        dest.writeValue(startDate);
        dest.writeValue(startTime);
        dest.writeValue(stopDate);
        dest.writeValue(updatedBy);
        dest.writeValue(updationDate);
    }

    public int describeContents() {
        return 0;
    }

}