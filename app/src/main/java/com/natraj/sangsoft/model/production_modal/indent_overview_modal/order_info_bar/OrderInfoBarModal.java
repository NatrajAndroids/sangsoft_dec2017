package com.natraj.sangsoft.model.production_modal.indent_overview_modal.order_info_bar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderInfoBarModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private String isError;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;
    @SerializedName("jsonData")
    @Expose
    private OrderInfoBarJsonData jsonData;
    public final static Parcelable.Creator<OrderInfoBarModal> CREATOR = new Creator<OrderInfoBarModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OrderInfoBarModal createFromParcel(Parcel in) {
            return new OrderInfoBarModal(in);
        }

        public OrderInfoBarModal[] newArray(int size) {
            return (new OrderInfoBarModal[size]);
        }

    }
            ;

    protected OrderInfoBarModal(Parcel in) {
        this.isError = ((String) in.readValue((String.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.jsonData = ((OrderInfoBarJsonData) in.readValue((OrderInfoBarJsonData.class.getClassLoader())));
    }

    public OrderInfoBarModal() {
    }

    public String getIsError() {
        return isError;
    }

    public void setIsError(String isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public OrderInfoBarJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(OrderInfoBarJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return 0;
    }

}
