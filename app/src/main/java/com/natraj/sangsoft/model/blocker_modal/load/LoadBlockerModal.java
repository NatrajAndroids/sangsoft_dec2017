package com.natraj.sangsoft.model.blocker_modal.load;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoadBlockerModal implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;
    @SerializedName("strings")
    @Expose
    private LoadBlockerStrings strings;
    @SerializedName("jsonData")
    @Expose
    private LoadBlockerJsonData jsonData;
    public final static Parcelable.Creator<LoadBlockerModal> CREATOR = new Creator<LoadBlockerModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LoadBlockerModal createFromParcel(Parcel in) {
            return new LoadBlockerModal(in);
        }

        public LoadBlockerModal[] newArray(int size) {
            return (new LoadBlockerModal[size]);
        }

    }
            ;

    protected LoadBlockerModal(Parcel in) {
        this.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((LoadBlockerStrings) in.readValue((LoadBlockerStrings.class.getClassLoader())));
        this.jsonData = ((LoadBlockerJsonData) in.readValue((LoadBlockerJsonData.class.getClassLoader())));
    }

    public LoadBlockerModal() {
    }

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public LoadBlockerStrings getStrings() {
        return strings;
    }

    public void setStrings(LoadBlockerStrings strings) {
        this.strings = strings;
    }

    public LoadBlockerJsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(LoadBlockerJsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return 0;
    }

}