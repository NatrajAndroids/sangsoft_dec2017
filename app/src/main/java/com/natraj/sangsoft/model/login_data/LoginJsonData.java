
package com.natraj.sangsoft.model.login_data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginJsonData implements Parcelable
{

    @SerializedName("userID")
    @Expose
    private String userID="";
    @SerializedName("userName")
    @Expose
    private String userName="";
    @SerializedName("machine_id")
    @Expose
    private String machineId="";
    @SerializedName("machine_name")
    @Expose
    private String machineName="";
    @SerializedName("machine_code")
    @Expose
    private String machineCode="";
    @SerializedName("success")
    @Expose
    private String success="";
    @SerializedName("loginMsg")
    @Expose
    private String loginMsg="";
    public final static Creator<LoginJsonData> CREATOR = new Creator<LoginJsonData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LoginJsonData createFromParcel(Parcel in) {
            return new LoginJsonData(in);
        }

        public LoginJsonData[] newArray(int size) {
            return (new LoginJsonData[size]);
        }

    }
            ;

    protected LoginJsonData(Parcel in) {
        this.userID = ((String) in.readValue((String.class.getClassLoader())));
        this.userName = ((String) in.readValue((String.class.getClassLoader())));
        this.machineId = ((String) in.readValue((String.class.getClassLoader())));
        this.machineName = ((String) in.readValue((String.class.getClassLoader())));
        this.machineCode = ((String) in.readValue((String.class.getClassLoader())));
        this.success = ((String) in.readValue((String.class.getClassLoader())));
        this.loginMsg = ((String) in.readValue((String.class.getClassLoader())));
    }

    public LoginJsonData() {
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMachineId() {
        return machineId;
    }

    public void setMachineId(String machineId) {
        this.machineId = machineId;
    }

    public String getMachineName() {
        return machineName;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    public String getMachineCode() {
        return machineCode;
    }

    public void setMachineCode(String machineCode) {
        this.machineCode = machineCode;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getLoginMsg() {
        return loginMsg;
    }

    public void setLoginMsg(String loginMsg) {
        this.loginMsg = loginMsg;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(userID);
        dest.writeValue(userName);
        dest.writeValue(machineId);
        dest.writeValue(machineName);
        dest.writeValue(machineCode);
        dest.writeValue(success);
        dest.writeValue(loginMsg);
    }

    public int describeContents() {
        return 0;
    }

}