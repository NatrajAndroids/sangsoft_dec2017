package com.natraj.sangsoft.model.production_modal.indent_record_prod;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecordProdJsonData implements Parcelable
{

    @SerializedName("indentUOM")
    @Expose
    private String indentUOM;
    @SerializedName("indentQty")
    @Expose
    private String indentQty;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("statusVal")
    @Expose
    private String statusVal;
    @SerializedName("outputItems")
    @Expose
    private List<RecordProdOutputItem> outputItems = new ArrayList<RecordProdOutputItem>();
    public final static Parcelable.Creator<RecordProdJsonData> CREATOR = new Creator<RecordProdJsonData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RecordProdJsonData createFromParcel(Parcel in) {
            return new RecordProdJsonData(in);
        }

        public RecordProdJsonData[] newArray(int size) {
            return (new RecordProdJsonData[size]);
        }

    }
            ;

    protected RecordProdJsonData(Parcel in) {
        this.indentUOM = ((String) in.readValue((String.class.getClassLoader())));
        this.indentQty = ((String) in.readValue((String.class.getClassLoader())));
        this.comment = ((String) in.readValue((String.class.getClassLoader())));
        this.statusVal = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.outputItems, (RecordProdOutputItem.class.getClassLoader()));
    }

    public RecordProdJsonData() {
    }

    public String getIndentUOM() {
        return indentUOM;
    }

    public void setIndentUOM(String indentUOM) {
        this.indentUOM = indentUOM;
    }

    public String getIndentQty() {
        return indentQty;
    }

    public void setIndentQty(String indentQty) {
        this.indentQty = indentQty;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatusVal() {
        return statusVal;
    }

    public void setStatusVal(String statusVal) {
        this.statusVal = statusVal;
    }

    public List<RecordProdOutputItem> getOutputItems() {
        return outputItems;
    }

    public void setOutputItems(List<RecordProdOutputItem> outputItems) {
        this.outputItems = outputItems;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(indentUOM);
        dest.writeValue(indentQty);
        dest.writeValue(comment);
        dest.writeValue(statusVal);
        dest.writeList(outputItems);
    }

    public int describeContents() {
        return 0;
    }

}
