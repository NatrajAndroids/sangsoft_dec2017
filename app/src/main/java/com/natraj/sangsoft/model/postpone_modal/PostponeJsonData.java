package com.natraj.sangsoft.model.postpone_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostponeJsonData implements Parcelable
{

    @SerializedName("sch_id")
    @Expose
    private String schId;
    @SerializedName("post_time")
    @Expose
    private String postTime;
    public final static Parcelable.Creator<PostponeJsonData> CREATOR = new Creator<PostponeJsonData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PostponeJsonData createFromParcel(Parcel in) {
            return new PostponeJsonData(in);
        }

        public PostponeJsonData[] newArray(int size) {
            return (new PostponeJsonData[size]);
        }

    }
            ;

    protected PostponeJsonData(Parcel in) {
        this.schId = ((String) in.readValue((String.class.getClassLoader())));
        this.postTime = ((String) in.readValue((String.class.getClassLoader())));
    }

    public PostponeJsonData() {
    }

    public String getSchId() {
        return schId;
    }

    public void setSchId(String schId) {
        this.schId = schId;
    }

    public String getPostTime() {
        return postTime;
    }

    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(schId);
        dest.writeValue(postTime);
    }

    public int describeContents() {
        return 0;
    }

}
