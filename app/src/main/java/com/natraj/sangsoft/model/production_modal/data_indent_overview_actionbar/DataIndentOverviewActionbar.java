
package com.natraj.sangsoft.model.production_modal.data_indent_overview_actionbar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataIndentOverviewActionbar implements Parcelable
{

    @SerializedName("is_error")
    @Expose
    private Integer isError=0;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg="";
    @SerializedName("strings")
    @Expose
    private Strings strings;
    @SerializedName("jsonData")
    @Expose
    private JsonData jsonData;
    public final static Creator<DataIndentOverviewActionbar> CREATOR = new Creator<DataIndentOverviewActionbar>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DataIndentOverviewActionbar createFromParcel(Parcel in) {
            return new DataIndentOverviewActionbar(in);
        }

        public DataIndentOverviewActionbar[] newArray(int size) {
            return (new DataIndentOverviewActionbar[size]);
        }

    }
            ;

    protected DataIndentOverviewActionbar(Parcel in) {
        this.isError = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.errorMsg = ((String) in.readValue((String.class.getClassLoader())));
        this.strings = ((Strings) in.readValue((Strings.class.getClassLoader())));
        this.jsonData = ((JsonData) in.readValue((JsonData.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public DataIndentOverviewActionbar() {
    }

    /**
     *
     * @param strings
     * @param errorMsg
     * @param isError
     * @param jsonData
     */
    public DataIndentOverviewActionbar(Integer isError, String errorMsg, Strings strings, JsonData jsonData) {
        super();
        this.isError = isError;
        this.errorMsg = errorMsg;
        this.strings = strings;
        this.jsonData = jsonData;
    }

    public Integer getIsError() {
        return isError;
    }

    public void setIsError(Integer isError) {
        this.isError = isError;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Strings getStrings() {
        return strings;
    }

    public void setStrings(Strings strings) {
        this.strings = strings;
    }

    public JsonData getJsonData() {
        return jsonData;
    }

    public void setJsonData(JsonData jsonData) {
        this.jsonData = jsonData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(isError);
        dest.writeValue(errorMsg);
        dest.writeValue(strings);
        dest.writeValue(jsonData);
    }

    public int describeContents() {
        return 0;
    }

}