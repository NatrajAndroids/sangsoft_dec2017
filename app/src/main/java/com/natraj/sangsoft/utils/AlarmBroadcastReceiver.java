package com.natraj.sangsoft.utils;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.natraj.sangsoft.R;
import com.natraj.sangsoft.ui.maintenance_activity.MaintListActivity;

public class AlarmBroadcastReceiver extends BroadcastReceiver implements View.OnClickListener {

    MaintListActivity maintListActivity;
    private Dialog dialog_record_activity;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null)
            return;
        String actName = intent.getStringExtra("name");
        String strDate = intent.getStringExtra("date");
        String sTime = intent.getStringExtra("start_time");
        String eTime = intent.getStringExtra("end_time");
        String sDate = intent.getStringExtra("start_date");
        String eDate = intent.getStringExtra("end_date");
       // recordActivityDialog(actName, sTime, eTime, sDate, eDate);
    }

    /*
     *  Dialogue for scheduler and blocker
     * */
/*
    public void recordActivityDialog(String activityName, String startTime, String endTime,
                                     String startDate, String endDate) {

        dialog_record_activity = new Dialog(MaintListActivity.mContext);
        dialog_record_activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_record_activity.setContentView(R.layout.dialog_record_activity);
        dialog_record_activity.setCanceledOnTouchOutside(false);
        dialog_record_activity.setCancelable(false);
        if (dialog_record_activity.getWindow() != null)
            dialog_record_activity.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ((TextView) dialog_record_activity.findViewById(R.id.txtRecordActivity)).setOnClickListener(this);
        ((TextView) dialog_record_activity.findViewById(R.id.txtActivityName)).setText(activityName);
        ((TextView) dialog_record_activity.findViewById(R.id.txtDate)).setText(startDate);
        ((TextView) dialog_record_activity.findViewById(R.id.txtTime)).setText(endTime);
        ((TextView) dialog_record_activity.findViewById(R.id.txtDueTime))
                .setText("Due between: " + startTime + " till " + endTime);
        ((TextView) dialog_record_activity.findViewById(R.id.txtPostpone)).setOnClickListener(this);
        dialog_record_activity.show();
        Window window = dialog_record_activity.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
    }
*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtRecordActivity:
                break;
            case R.id.txtPostpone:
                dialog_record_activity.dismiss();
                break;
        }
    }
}

