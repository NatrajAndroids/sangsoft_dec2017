package com.natraj.sangsoft.utils;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.natraj.sangsoft.retrofit_provider.RetrofitApiClient;
import com.natraj.sangsoft.retrofit_provider.RetrofitService;


public class BaseFragment extends Fragment {

    public RetrofitApiClient retrofitApiClient;
    public RetrofitApiClient retrofitRxClient;
    public Context mContext;

    public BaseFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mContext = getActivity();
        try {
            retrofitApiClient = RetrofitService.getRetrofit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
