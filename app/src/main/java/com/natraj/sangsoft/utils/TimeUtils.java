package com.natraj.sangsoft.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeUtils {

    public static String ALL = "ALL";
    public static String DATE_TIME = "DATE-TIME";

    public static String[] getCalenderTime(String type) {
        SimpleDateFormat dateFormate = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        SimpleDateFormat timeFormate = new SimpleDateFormat("hh:mm aaa", Locale.getDefault());
        SimpleDateFormat weekDayFormat = new SimpleDateFormat("EEEE", Locale.getDefault());

        String[] cal = new String[3];
        Calendar c = Calendar.getInstance();
        String date = dateFormate.format(c.getTime());
        String time = timeFormate.format(c.getTime());
        String weekDay = weekDayFormat.format(c.getTime());
        if (type.equals(ALL)) {
            cal[0] = date;
            cal[1] = time;
            cal[2] = weekDay;
        } else if (type.equals(DATE_TIME)) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            cal[0] = simpleDateFormat.format(c.getTime());
            cal[1] = time;
        }
        return cal;
    }

    public static Date getDate(String strDate) {
        SimpleDateFormat dateFormate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        Date date = null;
        try {
            date = dateFormate.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


}
