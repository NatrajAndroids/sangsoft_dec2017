package com.natraj.sangsoft.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ikovac.timepickerwithseconds.MyTimePickerDialog;
import com.natraj.sangsoft.R;
import com.natraj.sangsoft.constant.Constant;
import com.natraj.sangsoft.model.blocker_modal.fetch.FetchBlockerModal;
import com.natraj.sangsoft.model.blocker_modal.load.LoadBlockerModal;
import com.natraj.sangsoft.model.postpone_modal.PostponeModal;
import com.natraj.sangsoft.retrofit_provider.RetrofitApiClient;
import com.natraj.sangsoft.retrofit_provider.RetrofitService;
import com.natraj.sangsoft.retrofit_provider.WebResponse;
import com.natraj.sangsoft.ui.maintenance_activity.MaintExecuteActivity;
import com.quenDel.multiUtils.neworks.NetworkHandler;
import com.quenDel.multiUtils.utils.AppAlerts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Response;

public class BaseActivity extends AppCompatActivity {

    int mYear = 0, mMonth = 0, mDay = 0;
    private String strfrom = "";
    private String strTill = "";
    private Dialog dialog_record_activity;
    private String userId = "";
    private String mchId = "";
    private String lang = "";
    private String sch_id = "";
    private String sch_name = "";

    public NetworkHandler networkHandler;
    public RetrofitApiClient retrofitApiClient;
    public RetrofitApiClient retrofitRxClient;
    public Context mContext;

    private Handler myHandler;
    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
            fetchBlocker();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        Constant.BASE_URL = AppPreference.getStringPreference(mContext, "base_url");

        networkHandler = new NetworkHandler(BaseActivity.this);
        retrofitRxClient = RetrofitService.getRxClient();
        try {
            retrofitApiClient = RetrofitService.getRetrofit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        init();
    }

    private void init() {
        userId = AppPreference.getStringPreference(mContext, Constant.USERNAME);
        mchId = AppPreference.getConstStringPreference(mContext, Constant.MACHINE_ID);
        lang = AppPreference.getStringPreference(mContext, Constant.LANGUAGE);

        myHandler = new Handler();
        myHandler.postDelayed(myRunnable, 1000 * 30);
    }

    private void fetchBlocker() {
        String op = "fetchBlocker";

        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.fetchBlockerData(new Dialog(mContext), retrofitApiClient.fetchBlocker
                    (mchId, userId, op, lang), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<FetchBlockerModal> response = (Response<FetchBlockerModal>) result;
                    FetchBlockerModal mainModal = response.body();
                    Log.e("Fetch blocker", "" + mainModal);
                    if (mainModal.getIsError().equals(0)) {
                        int count = Integer.parseInt(mainModal.getJsonData().getSchCount());
                        if (count > 0) {
                            sch_id = mainModal.getJsonData().getSchId();
                            sch_name = "";
                            checkPostpone(mainModal.getJsonData().getPostTime());
                        } else {
                            AppAlerts.show(mContext, "Schedule count is zero");
                        }

                    } else {
                        AppAlerts.show(mContext, mainModal.getErrorMsg());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    /*
     *  Check postpone time to current time
     * */
    private void checkPostpone(String postTime) {
        //postTime = "2017-04-14 15:18:00";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        try {
            Date postponeDate = dateFormat.parse(postTime);
            long currentTime = System.currentTimeMillis();
            long postponeTime = postponeDate.getTime();
            Log.e("Postpone_time", String.valueOf(postponeTime));
            //loadBlocker(sch_id);
            if (currentTime >= postponeTime) {
                loadBlocker(sch_id);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void loadBlocker(String schId) {
        String op = "loadBlocker";
        if (networkHandler.isNetworkAvailable()) {
            RetrofitService.loadBlockerData(new Dialog(mContext), retrofitApiClient.loadBlocker
                    (mchId, userId, op, schId, lang), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    Response<LoadBlockerModal> response = (Response<LoadBlockerModal>) result;
                    LoadBlockerModal mainModal = response.body();
                    if (mainModal.getIsError().equals(0)) {
                        if (mainModal.getJsonData() == null || mainModal.getJsonData().getSchId() == null) {
                            AppAlerts.show(mContext, "Json data null");
                        } else {

                            strfrom = mainModal.getJsonData().getFromTime();
                            strTill = mainModal.getJsonData().getTillTime();
                            myHandler.removeCallbacks(myRunnable);
                            if (strfrom.isEmpty())
                                return;
                            postponeDialog(mainModal.getStrings().getBtnRecordAct(), mainModal.getStrings().getLblPostpone(),
                                    mainModal.getStrings().getLblDue(), mainModal.getStrings().getLblOr());
                        }
                    } else {
                        AppAlerts.show(mContext, mainModal.getErrorMsg());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    AppAlerts.show(mContext, error);
                }
            });
        } else {
            networkHandler.showAlert();
        }
    }

    /*
     *  Postpone dialogue
     * */
    private void postponeDialog(String strRecordLbl, String strPostLbl, String strDueLbl, String strORLbl) {
        if (dialog_record_activity != null && dialog_record_activity.isShowing())
            dialog_record_activity.dismiss();
        dialog_record_activity = new Dialog(mContext);
        dialog_record_activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_record_activity.setContentView(R.layout.dialog_record_activity);
        dialog_record_activity.setCanceledOnTouchOutside(false);

        dialog_record_activity.setCancelable(false);
        if (dialog_record_activity.getWindow() != null)
            dialog_record_activity.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        /*Due between: 5:00 am till 9:30 am*/

        String[] separated = strfrom.split(" ");
        ((TextView) dialog_record_activity.findViewById(R.id.txtDate)).setText(separated[0]);
        ((TextView) dialog_record_activity.findViewById(R.id.txtTime)).setText(separated[1]);
        ((TextView) dialog_record_activity.findViewById(R.id.txtRecordActivity)).setText(strRecordLbl);
        ((TextView) dialog_record_activity.findViewById(R.id.txtPostTill)).setText(strPostLbl);
        ((TextView) dialog_record_activity.findViewById(R.id.txtOrlbl)).setText(strORLbl);
        ((TextView) dialog_record_activity.findViewById(R.id.txtDueTime))
                .setText(String.format("%s %s %s %s", strDueLbl, strfrom, strORLbl, strTill));

        // Time picker
        ((TextView) dialog_record_activity.findViewById(R.id.txtTime)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeDialogue();
            }
        });

        ((TextView) dialog_record_activity.findViewById(R.id.txtRecordActivity)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_record_activity.dismiss();
                Intent intent = new Intent(mContext, MaintExecuteActivity.class);
                intent.putExtra("sch_id", sch_id);
                intent.putExtra("sch_name", sch_name);
                startActivity(intent);
            }
        });

        // Time picker
        ((TextView) dialog_record_activity.findViewById(R.id.txtDate)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateDialogue();
            }
        });

        // Postpone Api
        ((TextView) dialog_record_activity.findViewById(R.id.txtPostpone)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postponeApi();
            }
        });
        dialog_record_activity.show();
        Window window = dialog_record_activity.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
    }

    /*
     *  Postpone Time picker
     * */
    private void timeDialogue() {
        Calendar calendar = Calendar.getInstance();
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);

        MyTimePickerDialog timePickerDialog = new MyTimePickerDialog(mContext, new MyTimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(com.ikovac.timepickerwithseconds.TimePicker view, int hourOfDay, int minute, int seconds) {
                if (hourOfDay < 10 && minute >= 10 && seconds >= 10) {
                    ((TextView) dialog_record_activity.findViewById(R.id.txtTime)).setText("0" + hourOfDay + ":" + minute + ":" + seconds);
                } else if (minute < 10 && hourOfDay >= 10 && seconds >= 10) {
                    ((TextView) dialog_record_activity.findViewById(R.id.txtTime)).setText(hourOfDay + ":" + "0" + minute + ":" + seconds);
                } else if (seconds < 10 && hourOfDay >= 10 && minute >= 10) {
                    ((TextView) dialog_record_activity.findViewById(R.id.txtTime)).setText(hourOfDay + ":" + minute + ":" + "0" + seconds);
                } else if (hourOfDay < 10 && minute < 10 && seconds < 10) {
                    ((TextView) dialog_record_activity.findViewById(R.id.txtTime)).setText("0" + hourOfDay + ":" + "0" + minute + ":" + "0" + seconds);
                } else {
                    ((TextView) dialog_record_activity.findViewById(R.id.txtTime)).setText(hourOfDay + ":" + minute + ":" + seconds);
                }
            }
        }, hour, minute, second, true);
        timePickerDialog.setTitle("Select time");
        timePickerDialog.show();
    }

    /*
     *  Postpone Date picker
     * */
    private void dateDialogue() {
        Calendar calendar = Calendar.getInstance();
        if (mYear == 0 || mMonth == 0 || mDay == 0) {
            mYear = calendar.get(Calendar.YEAR);
            mMonth = calendar.get(Calendar.MONTH);
            mDay = calendar.get(Calendar.DAY_OF_MONTH);
        }
        DatePickerDialog dialog = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;
                ((TextView) dialog_record_activity.findViewById(R.id.txtDate))
                        .setText(dayOfMonth + "-" + (month + 1) + "-" + year);
            }
        }, mYear, mMonth, mDay);
        //dialog.getDatePicker().setMinDate(System.currentTimeMillis());
        dialog.show();
    }

    /*
     *  Call postpone api
     * */
    private void postponeApi() {
        String op = "postponeBlocker";
        //String tillTime = "2017-02-27 10:15:00";
        //String strPostTime = "2017-02-27 10:15:00";
        String strPostTime = ((TextView) dialog_record_activity.findViewById(R.id.txtDate)).getText().toString() + " " +
                ((TextView) dialog_record_activity.findViewById(R.id.txtTime)).getText().toString();
        Log.e("Textview_Date", strPostTime);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        try {
            Date postponeDate = dateFormat.parse(strPostTime);
            Date postFromDate = dateFormat.parse(strfrom);
            Date postTillDate = dateFormat.parse(strTill);
            //Date postTillDate = dateFormat.parse(tillTime);
            long postponeTime = postponeDate.getTime();
            long longFromTime = postFromDate.getTime();
            long longTillTime = postTillDate.getTime();

            if (postponeTime >= longFromTime && postponeTime <= longTillTime) {
                RetrofitService.postponeActivity(new Dialog(mContext), retrofitApiClient.postponeActivity
                        (mchId, userId, op, sch_id, strPostTime, lang), new WebResponse() {
                    @Override
                    public void onResponseSuccess(Response<?> result) {
                        Response<PostponeModal> response = (Response<PostponeModal>) result;
                        PostponeModal postponeModal = response.body();
                        if (postponeModal.getIsError().equals("0")) {
                            AppAlerts.show(mContext, "Activity postpone");
                            dialog_record_activity.dismiss();
                            //myHandler = new Handler();
                            myHandler.postDelayed(myRunnable, 1000 * 120);
                        } else {
                            AppAlerts.show(mContext, "Please check postpone date and time");
                        }
                    }

                    @Override
                    public void onResponseFailed(String error) {
                        AppAlerts.show(mContext, error);
                    }
                });
            } else {
                AppAlerts.show(mContext, "Your date and time must be between " + " " + strfrom + " " + "to" + " " + strTill);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        myHandler.removeCallbacks(myRunnable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        myHandler.postDelayed(myRunnable, 1000 * 30);
    }
}
