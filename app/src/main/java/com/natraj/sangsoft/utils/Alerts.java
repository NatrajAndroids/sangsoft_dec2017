package com.natraj.sangsoft.utils;

import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by pc6 on 4/11/2017.
 */

public class Alerts {

    public static void show(View container, String msg) {
        Snackbar.make(container, msg, Snackbar.LENGTH_LONG).show();
    }

    public static void withFeedback(View container, String msg, String btnTitle, View.OnClickListener onClickListener) {
        Snackbar snackbar = Snackbar.make(container, msg, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(btnTitle, onClickListener);
        snackbar.show();
    }

}
