package com.natraj.sangsoft.retrofit_provider;

import com.natraj.sangsoft.constant.Constant;
import com.natraj.sangsoft.model.blocker_modal.fetch.FetchBlockerModal;
import com.natraj.sangsoft.model.blocker_modal.load.LoadBlockerModal;
import com.natraj.sangsoft.model.login_data.LoginUserModal;
import com.natraj.sangsoft.model.login_page_label.LoginFormPageData;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_action_bar.MActionBarModal;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_activity_list_modal.MaintListModal;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_execute.MaintenanceExecuteModal;
import com.natraj.sangsoft.model.postpone_modal.PostponeModal;
import com.natraj.sangsoft.model.production_modal.alert_dialog_label_modal.AlertDialogLabelModal;
import com.natraj.sangsoft.model.production_modal.data_indent_overview.DataIndentOverview;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_info_bar_modal.IndentInfoBarModal;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_actionbar.IndentOverviewActionBar;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_header_info.CommonDataModal;
import com.natraj.sangsoft.model.production_modal.indent_progress_modal.IndentProgressItemsModal;
import com.natraj.sangsoft.model.production_modal.indent_record_prod.RecordProdModal;
import com.natraj.sangsoft.model.production_modal.item_instructions_modal.instructions_item_details.InstructionsItemDetailsModal;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_indent_item_list.OverviewIndentItemListModal;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_bar.OverviewItemBarModal;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_info.OverviewItemInfoModal;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_overview.OverviewItemModal;
import com.natraj.sangsoft.model.receive_rm_data_modal.RmDataModal;
import com.natraj.sangsoft.model.record_qa_modal.load_form.RecordQaModal;
import com.natraj.sangsoft.model.record_qa_modal.question_set.QuestionModal;
import com.natraj.sangsoft.model.request_qa_modal.RequestQaModal;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.QueryMap;

public interface RetrofitApiClient {

    @FormUrlEncoded
    @POST(Constant.LOGIN_URL)
    Call<LoginFormPageData> logiPageLabel(@Field("mch_id") String mch_id,
                                          @Field("op") String op,
                                          @Field("lang") String lang);

    @FormUrlEncoded
    @POST(Constant.LOGIN_URL)
    Call<LoginUserModal> userLogin(@Field("mch_id") String mch_id,
                                   @Field("op") String op,
                                   @Field("lang") String lang,
                                   @Field("empID") String empID,
                                   @Field("pwd") String pwd);

    @FormUrlEncoded
    @POST(Constant.INDENT_OVERVIEW_DATA)
    Call<DataIndentOverview> indentOverviewWA(@Field("mch_id") String mch_id,
                                              @Field("op") String op,
                                              @Field("user_id") String user_id,
                                              @Field("lang") String lang,
                                              @Field("order_id") String order_id);

    @FormUrlEncoded
    @POST(Constant.INDENT_PROGRESS_DATA)
    Call<IndentProgressItemsModal> indentProgressDataList(@Field("op") String op);

    @FormUrlEncoded
    @POST(Constant.REQUEST_QA)
    Call<RequestQaModal> requestQaData(@Field("op") String op, @Field("lang") String lang);

    @FormUrlEncoded
    @POST(Constant.RECORD_QA)
    Call<RecordQaModal> recordQaData(@Field("mch_id") String mch_id,
                                     @Field("user_id") String user_id,
                                     @Field("op") String op,
                                     @Field("order_id") String order_id,
                                     @Field("QAID") String QAID,
                                     @Field("lang") String lang);

    @FormUrlEncoded
    @POST(Constant.RECORD_QA)
    Call<QuestionModal> recordQuesData(@Field("mch_id") String mch_id,
                                       @Field("user_id") String user_id,
                                       @Field("op") String op,
                                       @Field("order_id") String order_id,
                                       @Field("QAID") String QAID,
                                       @Field("lang") String lang);

    @FormUrlEncoded
    @POST(Constant.INDENT_OVERVIEW_URL)
    Call<IndentOverviewActionBar> indentActionbar(@Field("mch_id") String mch_id,
                                                  @Field("op") String op,
                                                  @Field("user_id") String user_id,
                                                  @Field("lang") String lang);

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //Maintenance action bar
    @FormUrlEncoded
    @POST(Constant.Maintenance_Action_Bar)
    Call<MActionBarModal> maintenanceActionbar(@Field("machine_id") String mch_id,
                                               @Field("op") String op,
                                               @Field("user_id") String user_id,
                                               @Field("lang") String lang);

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //Maintenance execution
    @FormUrlEncoded
    @POST(Constant.Maintenance_Action_Bar)
    Call<MaintenanceExecuteModal> maintenanceExecute(@Field("op") String op,
                                                     @Field("user_id") String user_id,
                                                     @Field("machine_id") String mch_id,
                                                     @Field("lang") String lang,
                                                     @Field("sch_id") String sch_id);

    //Maintenance activity list
    @FormUrlEncoded
    @POST(Constant.Maintenance_Action_Bar)
    Call<MaintListModal> maintActivityList(@Field("machine_id") String mch_id,
                                           @Field("op") String op,
                                           @Field("user_id") String user_id,
                                           @Field("lang") String lang,
                                           @Field("sch_status") String sch_status);

    @FormUrlEncoded
    @POST(Constant.INDENT_OVERVIEW_URL)
    Call<IndentInfoBarModal> indentInfobar(@Field("mch_id") String mch_id,
                                           @Field("op") String op,
                                           @Field("user_id") String user_id,
                                           @Field("lang") String lang,
                                           @Field("order_id") String indent_id);

    @FormUrlEncoded
    @POST(Constant.INDENT_OVERVIEW_URL)
    Call<IndentInfoBarModal> orderInfobar(@Field("mch_id") String mch_id,
                                          @Field("op") String op,
                                          @Field("user_id") String user_id,
                                          @Field("lang") String lang,
                                          @Field("order_id") String indent_id);

    @FormUrlEncoded
    @POST(Constant.RECEIVE_RM_DATA)
    Call<RmDataModal> setReceiveRmWorkArea(@Field("op") String op,
                                           @Field("mch_id") String mch_id,
                                           @Field("user_id") String user_id,
                                           @Field("lang") String lang,
                                           @Field("order_id") String indent_id);

    @FormUrlEncoded
    @POST(Constant.RECORD_PROD_DATA)
    Call<RecordProdModal> recordProdData(@Field("op") String op,
                                         @Field("mch_id") String mch_id,
                                         @Field("user_id") String user_id,
                                         @Field("lang") String lang,
                                         @Field("order_id") String indent_id);

    //////////////////////////////////////////////////////////////////////////////////////
    // ITEM OVERVIEW
    @FormUrlEncoded
    @POST(Constant.ITEM_OVERVIEW)
    Call<OverviewItemBarModal> itemOverviewActionBarData(@Field("op") String op,
                                                         @Field("mch_id") String empID,
                                                         @Field("lang") String lang,
                                                         @Field("item_id") String item_id);

    @FormUrlEncoded
    @POST(Constant.ITEM_OVERVIEW)
    Call<OverviewItemInfoModal> itemOverviewItemInfo(@Field("op") String op,
                                                     @Field("mch_id") String empID,
                                                     @Field("lang") String lang,
                                                     @Field("item_id") String item_id,
                                                     @Field("process_id") String process_id);

    @FormUrlEncoded
    @POST(Constant.ITEM_OVERVIEW)
    Call<OverviewItemModal> itemOverviewAllData(@Field("op") String op,
                                                @Field("mch_id") String empID,
                                                @Field("lang") String lang,
                                                @Field("item_id") String item_id,
                                                @Field("process_id") String process_id);

    //////////////////////////////////////////////////////////////////////////////////////
    // ITEM INSTRUCTIONS
    @FormUrlEncoded
    @POST(Constant.ITEM_INSTRUCTIONS_TOOLBAR)
    Call<CommonDataModal> commonDataItems(@Field("op") String op, @Field("user_id") String user_id,
                                          @Field("lang") String lang);

    @Multipart
    @POST(Constant.ITEM_INSTRUCTIONS)
    Call<InstructionsItemDetailsModal> itemInstructionsFinalData(@Part("mch_id") RequestBody mch_id,
                                                                 @Part("op") RequestBody op,
                                                                 @Part("user_id") RequestBody user_id,
                                                                 @Part("lang") RequestBody lang,
                                                                 @QueryMap Map<String, Object> map);

    ////////////////////////////////////////////////////////////////////////////////////////
    //Set Order OverviewIndentItem List in Dialog
    @Multipart
    @POST(Constant.INDENT_ITEM_LIST)
    Call<OverviewIndentItemListModal> indentItemListDialog(@Part("mch_id") RequestBody mch_id,
                                                           @Part("op") RequestBody op,
                                                           @Part("user_id") RequestBody user_id,
                                                           @Part("lang") RequestBody lang,
                                                           @QueryMap Map<String, Object> map);

    ////////////////////////////////////////////////////////////////////////////////////
    //Alert label
    @Multipart
    @POST(Constant.INDENT_ITEM_LIST)
    Call<AlertDialogLabelModal> alertDialogLabel(@Part("mch_id") RequestBody mch_id,
                                                 @Part("op") RequestBody op,
                                                 @Part("user_id") RequestBody user_id,
                                                 @Part("lang") RequestBody lang,
                                                 @QueryMap Map<String, Object> map);

    ////////////////////////////////////////////////////////////////////////////////////
    //Save json data
    @Headers("Content-Type: application/json")
    @POST(Constant.SAVE_JSON_DATA)
    Call<ResponseBody> saveJsonData(@Body RequestBody body);

    ////////////////////////////////////////////////////////////////////////////////////
    //Save Raw material data
    @Headers("Content-Type: application/json")
    @POST(Constant.SAVE_RM_DATA)
    Call<ResponseBody> saveRMData(@Body RequestBody body);

    ////////////////////////////////////////////////////////////////////////////////////
    //Save Raw material data
    @Headers("Content-Type: application/json")
    @POST(Constant.SAVE_RECORD_DATA)
    Call<ResponseBody> saveRecordData(@Body RequestBody body);

    ////////////////////////////////////////////////////////////////////////////////////
    // Fetch blocker data
    @FormUrlEncoded
    @POST(Constant.BLOCKER_DATA)
    Call<FetchBlockerModal> fetchBlocker(@Field("machine_id") String mch_id,
                                         @Field("user_id") String user_id,
                                         @Field("op") String op,
                                         @Field("lang") String lang);

    ////////////////////////////////////////////////////////////////////////////////////
    // Load blocker data
    @FormUrlEncoded
    @POST(Constant.BLOCKER_DATA)
    Call<LoadBlockerModal> loadBlocker(@Field("machine_id") String mch_id,
                                       @Field("user_id") String user_id,
                                       @Field("op") String op,
                                       @Field("sch_id") String sch_id,
                                       @Field("lang") String lang);

    ////////////////////////////////////////////////////////////////////////////////////
    // Postpone Activity
    @FormUrlEncoded
    @POST(Constant.BLOCKER_DATA)
    Call<PostponeModal> postponeActivity(@Field("machine_id") String mch_id,
                                         @Field("user_id") String user_id,
                                         @Field("op") String op,
                                         @Field("sch_id") String sch_id,
                                         @Field("post_time") String post_time,
                                         @Field("lang") String lang);

    @Headers("Content-Type: application/json")
    @POST(Constant.SAVE_REQUEST_QA_DATA)
    Call<ResponseBody> saveRequestQAData(@Body RequestBody body);

    @Headers("Content-Type: application/json")
    @POST(Constant.SAVE_RECORD_QA_DATA)
    Call<ResponseBody> saveRecordQAData(@Body RequestBody body);
}

/* @Multipart
    @POST(Constant.SAVE_JSON_DATA)
    Call<ResponseBody> saveJsonData(@Part("mch_id") RequestBody mch_id,
                                    @Part("op") RequestBody op,
                                    @Part("user_id") RequestBody user_id,
                                    @Part("lang") RequestBody lang,
                                    @QueryMap Map<String, Object> map);*/