package com.natraj.sangsoft.retrofit_provider;

import android.app.Dialog;

import com.natraj.sangsoft.constant.Constant;
import com.natraj.sangsoft.model.blocker_modal.fetch.FetchBlockerModal;
import com.natraj.sangsoft.model.blocker_modal.load.LoadBlockerModal;
import com.natraj.sangsoft.model.login_data.LoginUserModal;
import com.natraj.sangsoft.model.login_page_label.LoginFormPageData;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_action_bar.MActionBarModal;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_activity_list_modal.MaintListModal;
import com.natraj.sangsoft.model.maintenance_modals.maintenance_execute.MaintenanceExecuteModal;
import com.natraj.sangsoft.model.postpone_modal.PostponeModal;
import com.natraj.sangsoft.model.production_modal.alert_dialog_label_modal.AlertDialogLabelModal;
import com.natraj.sangsoft.model.production_modal.data_indent_overview.DataIndentOverview;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_info_bar_modal.IndentInfoBarModal;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_actionbar.IndentOverviewActionBar;
import com.natraj.sangsoft.model.production_modal.indent_overview_modal.indent_overview_header_info.CommonDataModal;
import com.natraj.sangsoft.model.production_modal.indent_progress_modal.IndentProgressItemsModal;
import com.natraj.sangsoft.model.production_modal.indent_record_prod.RecordProdModal;
import com.natraj.sangsoft.model.production_modal.item_instructions_modal.instructions_item_details.InstructionsItemDetailsModal;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_indent_item_list.OverviewIndentItemListModal;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_bar.OverviewItemBarModal;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_info.OverviewItemInfoModal;
import com.natraj.sangsoft.model.production_modal.item_overview_modal.overview_item_overview.OverviewItemModal;
import com.natraj.sangsoft.model.receive_rm_data_modal.RmDataModal;
import com.natraj.sangsoft.model.record_qa_modal.load_form.RecordQaModal;
import com.natraj.sangsoft.model.record_qa_modal.question_set.QuestionModal;
import com.natraj.sangsoft.model.request_qa_modal.RequestQaModal;
import com.quenDel.multiUtils.utils.AppProgressDialog;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {

    private static RetrofitApiClient client;

    public RetrofitService() throws Exception {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        client = retrofit.create(RetrofitApiClient.class);
    }

    public static RetrofitApiClient getRxClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constant.BASE_URL)
                .build();
        return retrofit.create(RetrofitApiClient.class);
    }

    public static RetrofitApiClient getRetrofit() throws Exception {
        //  if (client == null)
        new RetrofitService();
        return client;
    }


    /***
     *    SERVER CALL WITH REQUIRED RESPONSE
     * */

    public static void getLoginPageLabelResponse(final Dialog dialog, final Call<LoginFormPageData> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<LoginFormPageData>() {
            @Override
            public void onResponse(Call<LoginFormPageData> call, Response<LoginFormPageData> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<LoginFormPageData> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getLoginPageUserResponse(final Dialog dialog, final Call<LoginUserModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<LoginUserModal>() {
            @Override
            public void onResponse(Call<LoginUserModal> call, Response<LoginUserModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<LoginUserModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getReceiveRmWorkAreaData(final Dialog dialog, final Call<RmDataModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<RmDataModal>() {
            @Override
            public void onResponse(Call<RmDataModal> call, Response<RmDataModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<RmDataModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getRecordProdData(final Dialog dialog, final Call<RecordProdModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<RecordProdModal>() {
            @Override
            public void onResponse(Call<RecordProdModal> call, Response<RecordProdModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<RecordProdModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getIndentDataList(final Dialog dialog, final Call<IndentProgressItemsModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<IndentProgressItemsModal>() {
            @Override
            public void onResponse(Call<IndentProgressItemsModal> call, Response<IndentProgressItemsModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<IndentProgressItemsModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getRequestQaItems(final Dialog dialog, final Call<RequestQaModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<RequestQaModal>() {
            @Override
            public void onResponse(Call<RequestQaModal> call, Response<RequestQaModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<RequestQaModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getIndentWAResponce(final Dialog dialog, final Call<DataIndentOverview> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<DataIndentOverview>() {
            @Override
            public void onResponse(Call<DataIndentOverview> call, Response<DataIndentOverview> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<DataIndentOverview> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getIndentActionBarResponce(final Dialog dialog, final Call<IndentOverviewActionBar> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<IndentOverviewActionBar>() {
            @Override
            public void onResponse(Call<IndentOverviewActionBar> call, Response<IndentOverviewActionBar> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<IndentOverviewActionBar> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getIndentInfoBarResponce(final Dialog dialog, final Call<IndentInfoBarModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<IndentInfoBarModal>() {
            @Override
            public void onResponse(Call<IndentInfoBarModal> call, Response<IndentInfoBarModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<IndentInfoBarModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getOrderInfoBarResponce(final Dialog dialog, final Call<IndentInfoBarModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<IndentInfoBarModal>() {
            @Override
            public void onResponse(Call<IndentInfoBarModal> call, Response<IndentInfoBarModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<IndentInfoBarModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getItemOverviewActionBarData(final Dialog dialog, final Call<OverviewItemBarModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<OverviewItemBarModal>() {
            @Override
            public void onResponse(Call<OverviewItemBarModal> call, Response<OverviewItemBarModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<OverviewItemBarModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getItemOverviewItemInfo(final Dialog dialog, final Call<OverviewItemInfoModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<OverviewItemInfoModal>() {
            @Override
            public void onResponse(Call<OverviewItemInfoModal> call, Response<OverviewItemInfoModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<OverviewItemInfoModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getItemOverviewAllData(final Dialog dialog, final Call<OverviewItemModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<OverviewItemModal>() {
            @Override
            public void onResponse(Call<OverviewItemModal> call, Response<OverviewItemModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<OverviewItemModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getItemInstructionsFinalData(final Dialog dialog, final Call<InstructionsItemDetailsModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<InstructionsItemDetailsModal>() {
            @Override
            public void onResponse(Call<InstructionsItemDetailsModal> call, Response<InstructionsItemDetailsModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<InstructionsItemDetailsModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getIndentItemListDialog(final Dialog dialog, final Call<OverviewIndentItemListModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<OverviewIndentItemListModal>() {
            @Override
            public void onResponse(Call<OverviewIndentItemListModal> call, Response<OverviewIndentItemListModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<OverviewIndentItemListModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getAlertDialogLabel(final Dialog dialog, final Call<AlertDialogLabelModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<AlertDialogLabelModal>() {
            @Override
            public void onResponse(Call<AlertDialogLabelModal> call, Response<AlertDialogLabelModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<AlertDialogLabelModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getCommonDataLbl(final Dialog dialog, final Call<CommonDataModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<CommonDataModal>() {
            @Override
            public void onResponse(Call<CommonDataModal> call, Response<CommonDataModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<CommonDataModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void maintenanceActionBarData(final Dialog dialog, final Call<MActionBarModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<MActionBarModal>() {
            @Override
            public void onResponse(Call<MActionBarModal> call, Response<MActionBarModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<MActionBarModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void maintenanceExecuteData(final Dialog dialog, final Call<MaintenanceExecuteModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<MaintenanceExecuteModal>() {
            @Override
            public void onResponse(Call<MaintenanceExecuteModal> call, Response<MaintenanceExecuteModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<MaintenanceExecuteModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getSaveDataResponse(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void maintActivityListData(final Dialog dialog, final Call<MaintListModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<MaintListModal>() {
            @Override
            public void onResponse(Call<MaintListModal> call, Response<MaintListModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<MaintListModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    /*
     *   Fetch blocker data
     * */
    public static void fetchBlockerData(final Dialog dialog, final Call<FetchBlockerModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<FetchBlockerModal>() {
            @Override
            public void onResponse(Call<FetchBlockerModal> call, Response<FetchBlockerModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<FetchBlockerModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    /*
     *   Load blocker data
     * */
    public static void loadBlockerData(final Dialog dialog, final Call<LoadBlockerModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<LoadBlockerModal>() {
            @Override
            public void onResponse(Call<LoadBlockerModal> call, Response<LoadBlockerModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<LoadBlockerModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    /*
     *   Postpone activity data
     * */
    public static void postponeActivity(final Dialog dialog, final Call<PostponeModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<PostponeModal>() {
            @Override
            public void onResponse(Call<PostponeModal> call, Response<PostponeModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<PostponeModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    /*
     *  Save RM JSON data
     * */
    public static void getSaveRMData(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    /*
     *  Save RM JSON data
     * */
    public static void getSaveRecordData(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    /*
     *  Save RM JSON data
     * */
    public static void getSaveRequestQAData(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    /*
     *  Save Record QA JSON data
     * */
    public static void getSaveRecordQAData(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getRecordQAData(final Dialog dialog, final Call<RecordQaModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<RecordQaModal>() {
            @Override
            public void onResponse(Call<RecordQaModal> call, Response<RecordQaModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<RecordQaModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getRequestQuesData(final Dialog dialog, final Call<QuestionModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<QuestionModal>() {
            @Override
            public void onResponse(Call<QuestionModal> call, Response<QuestionModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<QuestionModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

}

