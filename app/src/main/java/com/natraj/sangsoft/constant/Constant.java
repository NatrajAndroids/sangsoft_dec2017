package com.natraj.sangsoft.constant;

public class Constant {

    public static final String LOGIN_URL = "login_data.php";
    public static final String INDENT_OVERVIEW_URL = "common_data.php";
    public static final String Maintenance_Action_Bar = "maint_data.php";
    public static final String INDENT_OVERVIEW_DATA = "indent_data.php";
    public static final String INDENT_PROGRESS_DATA = "indentProgress_data.php";
    public static final String RECEIVE_RM_DATA = "receiveRM_data.php";
    public static final String RECORD_PROD_DATA = "recordProd_data.php";
    public static final String REQUEST_QA = "requestQA_data.php";
    public static final String RECORD_QA = "recordQA_Data.php";

    public static final String ITEM_OVERVIEW = "item_data.php";
    public static final String ITEM_INSTRUCTIONS_TOOLBAR = "common_data.php";
    public static final String ITEM_INSTRUCTIONS = "item_details.php";
    public static final String INDENT_ITEM_LIST = "item_search.php";
    public static final String SAVE_JSON_DATA = "maint_save.php";
    public static final String SAVE_RM_DATA = "receiveRM_save.php";
    public static final String SAVE_RECORD_DATA = "recordProd_save.php";
    public static final String SAVE_REQUEST_QA_DATA = "requestQA_save.php";
    public static final String SAVE_RECORD_QA_DATA = "recordQA_save.php";

    public static final String BLOCKER_DATA = "blocker_data.php";

    ///Preference constant
    /*BASE_URL = http://www.sangsoft.in/demo/lt_api/api/*/
    public static String BASE_URL = "";
    public static String IS_URL = "url";
    public static String IS_LOGIN = "isLogin";
    public static String MERCHANT_ACCOUNT = "pref_Account";
    public static String MERCHANT_QUESTION = "pref_Question";
    public static String USER_FEEDBACK = "pref_Feedback";

    public static String USERNAME = "pref_username";
    public static String PASSWORD = "pref_password";
    public static String MACHINE_ID = "machine_id";
    public static String MACHINE_ID_TOOLBAR = "machine_id_toolbar";
    public static String LANGUAGE = "language";
    public static String INDENT_ID = "indent_id";
    public static String ITEM_ID = "item_id";
    public static String URL_SPINNER = "url_spinner";

    public static String SELECTED_URL = "selected_url";
    public static String SELECTED_URL_POSITION = "selected_url_position";
    public static String SELECTED_LANGUAGE = "selected_language";
    public static String SELECTED_LANGUAGE_POSITION = "selected_language_position";
    public static String SELECTED_PROCESS_POSITION = "selected_process_position";
}
