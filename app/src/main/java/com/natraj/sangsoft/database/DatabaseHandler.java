package com.natraj.sangsoft.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.natraj.sangsoft.model.UrlModal;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "url.db";
    // Url table name
    private static final String TABLE_URL = "url_table";
    // Url Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_URL = "url";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_URL_TABLE = "CREATE TABLE " + TABLE_URL + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_URL + " TEXT" + ")";
        db.execSQL(CREATE_URL_TABLE);
        Log.e("Table", CREATE_URL_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_URL);
        onCreate(db);
    }

    //Adding new Url
    public void addUrl(UrlModal urlModal) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, urlModal.get_name());
        values.put(KEY_URL, urlModal.get_url());

        // Inserting InstructionsRow
        db.insert(TABLE_URL, null, values);
        db.close(); // Closing database connection
    }

    // Getting single url
    public UrlModal getSingleUrl(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_URL, new String[]{KEY_ID, KEY_NAME, KEY_URL}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        return new UrlModal(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2));
    }

    //Getting all Url list
    public List<UrlModal> getAllUrlList() {
        List<UrlModal> urlModalList = new ArrayList<UrlModal>();
        String selectQuery = "SELECT * FROM " + TABLE_URL;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                UrlModal urlModal = new UrlModal();
                urlModal.set_id(Integer.parseInt(cursor.getString(0)));
                urlModal.set_name(cursor.getString(1));
                urlModal.set_url(cursor.getString(2));
                urlModalList.add(urlModal);
            } while (cursor.moveToNext());
        }

        return urlModalList;
    }

    // Updating single urlModal
    public int updateUrl(UrlModal urlModal) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, urlModal.get_id());
        values.put(KEY_NAME, urlModal.get_name());
        values.put(KEY_URL, urlModal.get_url());

        int updateValue = db.update(TABLE_URL, values, KEY_ID + " = ?", new String[]{String.valueOf(urlModal.get_id())});
        db.close();
        return updateValue;

        /*int updateValue = db.update(TABLE_URL, values, KEY_ID + " = ?", new String[]{String.valueOf(urlModal.get_id())});*/
    }

    // Deleting single url
    public void deleteContact(UrlModal urlModal) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_URL, KEY_ID + " = ?",
                new String[]{String.valueOf(urlModal.get_id())});
        db.close();
    }

    // Getting url Count
    public boolean getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_URL;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int i = cursor.getCount();
        cursor.close();
        if (i > 0) {
            return true;
        } else {
            return false;
        }
    }

    //check exist data
    public boolean verification(String _username) {
        SQLiteDatabase db = this.getReadableDatabase();
        int count = -1;
        Cursor c = null;
        try {
            String query = "SELECT COUNT(*) FROM " + TABLE_URL + " WHERE " + KEY_NAME + " = ?";
            c = db.rawQuery(query, new String[]{_username});
            if (c.moveToFirst()) {
                count = c.getInt(0);
            }
            return count > 0;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }
}